(function(root) {

	/////////////////////////////
	///浏览器对象模拟:支持Vue.compile
	root.__initElement = function(element) {
		Object.defineProperty(element, "textContent", {
			get() {
				return element.$textContent();
			},
			set(value) {
                return element.$setTextContent(value);
            }
		});

		Object.defineProperty(element, "innerHTML", {
			get() {
				return element.$getInnerHTML();
			},
			set(value) {
				return element.$setInnerHTML(value);
			}
		});
	};
	root.location = {
		port: 0
	};
	root.window.document = root.document;
	root.window.location = root.location;

})(typeof self !== 'undefined' ? self : this);