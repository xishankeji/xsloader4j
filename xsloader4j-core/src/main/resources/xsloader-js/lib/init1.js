(function(root) {
	function buildString(args) {
		let strs = [];
		for (let i = 0; i < args.length; i++) {
			strs.push(args[i]);
		}
		return strs.join("");
	}

	root.console = {
		assert(condition, str) {
			if (!condition) {
				this.warn(str);
			}
		},
		debug() {
			$jsBridge$.logText(buildString(arguments), "debug");
		},
		log() {
			$jsBridge$.logText(buildString(arguments), "log");
		},
		info() {
			$jsBridge$.logText(buildString(arguments), "info");
		},
		warn() {
			$jsBridge$.logText(buildString(arguments), "warn");
		},
		error() {
			$jsBridge$.logText(buildString(arguments), "error");
		},
		trace() {
			$jsBridge$.logText(buildString(arguments), "warn");
		},
	};

	root.globalModules={};

	root.countSubstr = function(string, substr) {
		let n = 0;
		if (substr) {
			let len = string.length - substr.length;
			for (let i = 0; i <= len; i++) {
				if (string.substring(i, i + substr.length) == substr) {
					n++;
				}
			}
		}
		return n;
	}

	root.define=function(name,callback){
		root.globalModules[name]=callback();
	};

	if(!root.self){
		root.self=root;
	}

	root.loadLib("/babel-7.15.3/babel.js");
	root.loadLib("/vue-2.6.14-server-compiler.js");
	root.loadLib("/webpack-sources.js");

	root.Buffer=root.globalModules['buffer'].Buffer;

})(typeof self !== 'undefined' ? self : this);
