if(typeof xsloader!='undefined'){xsloader.__ignoreCurrentRequireDep=true;}(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["webpack-sources"] = factory();
	else
		root["webpack-sources"] = factory();
})(self, function() {
return /******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./main.js":
/*!*****************!*\
  !*** ./main.js ***!
  \*****************/
/***/ (function(__unused_webpack_module, __unused_webpack_exports, __webpack_require__) {

var global = typeof window != 'undefined' ? window : typeof self !== 'undefined' ? self : typeof root !== 'undefined' ? root : this;
var defineEnv = 'xsloader' in global ? xsloader : global;

var var0 = __webpack_require__(/*! webpack-sources */ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/index.js");

var var1 = __webpack_require__(/*! buffer */ "./node_modules/_buffer@6.0.3@buffer/index.js");

var var2 = __webpack_require__(/*! source-map */ "./node_modules/_source-map@0.7.3@source-map/source-map.js");

defineEnv.define('webpack-sources', function () {
  return var0;
});
defineEnv.define('buffer', function () {
  return var1;
});
defineEnv.define('source-map', function () {
  return var2;
});

/***/ }),

/***/ "./node_modules/_base64-js@1.5.1@base64-js/index.js":
/*!**********************************************************!*\
  !*** ./node_modules/_base64-js@1.5.1@base64-js/index.js ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, exports) => {

"use strict";


exports.byteLength = byteLength;
exports.toByteArray = toByteArray;
exports.fromByteArray = fromByteArray;
var lookup = [];
var revLookup = [];
var Arr = typeof Uint8Array !== 'undefined' ? Uint8Array : Array;
var code = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

for (var i = 0, len = code.length; i < len; ++i) {
  lookup[i] = code[i];
  revLookup[code.charCodeAt(i)] = i;
} // Support decoding URL-safe base64 strings, as Node.js does.
// See: https://en.wikipedia.org/wiki/Base64#URL_applications


revLookup['-'.charCodeAt(0)] = 62;
revLookup['_'.charCodeAt(0)] = 63;

function getLens(b64) {
  var len = b64.length;

  if (len % 4 > 0) {
    throw new Error('Invalid string. Length must be a multiple of 4');
  } // Trim off extra bytes after placeholder bytes are found
  // See: https://github.com/beatgammit/base64-js/issues/42


  var validLen = b64.indexOf('=');
  if (validLen === -1) validLen = len;
  var placeHoldersLen = validLen === len ? 0 : 4 - validLen % 4;
  return [validLen, placeHoldersLen];
} // base64 is 4/3 + up to two characters of the original data


function byteLength(b64) {
  var lens = getLens(b64);
  var validLen = lens[0];
  var placeHoldersLen = lens[1];
  return (validLen + placeHoldersLen) * 3 / 4 - placeHoldersLen;
}

function _byteLength(b64, validLen, placeHoldersLen) {
  return (validLen + placeHoldersLen) * 3 / 4 - placeHoldersLen;
}

function toByteArray(b64) {
  var tmp;
  var lens = getLens(b64);
  var validLen = lens[0];
  var placeHoldersLen = lens[1];
  var arr = new Arr(_byteLength(b64, validLen, placeHoldersLen));
  var curByte = 0; // if there are placeholders, only get up to the last complete 4 chars

  var len = placeHoldersLen > 0 ? validLen - 4 : validLen;
  var i;

  for (i = 0; i < len; i += 4) {
    tmp = revLookup[b64.charCodeAt(i)] << 18 | revLookup[b64.charCodeAt(i + 1)] << 12 | revLookup[b64.charCodeAt(i + 2)] << 6 | revLookup[b64.charCodeAt(i + 3)];
    arr[curByte++] = tmp >> 16 & 0xFF;
    arr[curByte++] = tmp >> 8 & 0xFF;
    arr[curByte++] = tmp & 0xFF;
  }

  if (placeHoldersLen === 2) {
    tmp = revLookup[b64.charCodeAt(i)] << 2 | revLookup[b64.charCodeAt(i + 1)] >> 4;
    arr[curByte++] = tmp & 0xFF;
  }

  if (placeHoldersLen === 1) {
    tmp = revLookup[b64.charCodeAt(i)] << 10 | revLookup[b64.charCodeAt(i + 1)] << 4 | revLookup[b64.charCodeAt(i + 2)] >> 2;
    arr[curByte++] = tmp >> 8 & 0xFF;
    arr[curByte++] = tmp & 0xFF;
  }

  return arr;
}

function tripletToBase64(num) {
  return lookup[num >> 18 & 0x3F] + lookup[num >> 12 & 0x3F] + lookup[num >> 6 & 0x3F] + lookup[num & 0x3F];
}

function encodeChunk(uint8, start, end) {
  var tmp;
  var output = [];

  for (var i = start; i < end; i += 3) {
    tmp = (uint8[i] << 16 & 0xFF0000) + (uint8[i + 1] << 8 & 0xFF00) + (uint8[i + 2] & 0xFF);
    output.push(tripletToBase64(tmp));
  }

  return output.join('');
}

function fromByteArray(uint8) {
  var tmp;
  var len = uint8.length;
  var extraBytes = len % 3; // if we have 1 byte left, pad 2 bytes

  var parts = [];
  var maxChunkLength = 16383; // must be multiple of 3
  // go through the array every three bytes, we'll deal with trailing stuff later

  for (var i = 0, len2 = len - extraBytes; i < len2; i += maxChunkLength) {
    parts.push(encodeChunk(uint8, i, i + maxChunkLength > len2 ? len2 : i + maxChunkLength));
  } // pad the end with zeros, but make sure to not forget the extra bytes


  if (extraBytes === 1) {
    tmp = uint8[len - 1];
    parts.push(lookup[tmp >> 2] + lookup[tmp << 4 & 0x3F] + '==');
  } else if (extraBytes === 2) {
    tmp = (uint8[len - 2] << 8) + uint8[len - 1];
    parts.push(lookup[tmp >> 10] + lookup[tmp >> 4 & 0x3F] + lookup[tmp << 2 & 0x3F] + '=');
  }

  return parts.join('');
}

/***/ }),

/***/ "./node_modules/_buffer@6.0.3@buffer/index.js":
/*!****************************************************!*\
  !*** ./node_modules/_buffer@6.0.3@buffer/index.js ***!
  \****************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";
/*!
 * The buffer module from node.js, for the browser.
 *
 * @author   Feross Aboukhadijeh <https://feross.org>
 * @license  MIT
 */

/* eslint-disable no-proto */


function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var base64 = __webpack_require__(/*! base64-js */ "./node_modules/_base64-js@1.5.1@base64-js/index.js");

var ieee754 = __webpack_require__(/*! ieee754 */ "./node_modules/_ieee754@1.2.1@ieee754/index.js");

var customInspectSymbol = typeof Symbol === 'function' && typeof Symbol['for'] === 'function' ? // eslint-disable-line dot-notation
Symbol['for']('nodejs.util.inspect.custom') // eslint-disable-line dot-notation
: null;
exports.Buffer = Buffer;
exports.SlowBuffer = SlowBuffer;
exports.INSPECT_MAX_BYTES = 50;
var K_MAX_LENGTH = 0x7fffffff;
exports.kMaxLength = K_MAX_LENGTH;
/**
 * If `Buffer.TYPED_ARRAY_SUPPORT`:
 *   === true    Use Uint8Array implementation (fastest)
 *   === false   Print warning and recommend using `buffer` v4.x which has an Object
 *               implementation (most compatible, even IE6)
 *
 * Browsers that support typed arrays are IE 10+, Firefox 4+, Chrome 7+, Safari 5.1+,
 * Opera 11.6+, iOS 4.2+.
 *
 * We report that the browser does not support typed arrays if the are not subclassable
 * using __proto__. Firefox 4-29 lacks support for adding new properties to `Uint8Array`
 * (See: https://bugzilla.mozilla.org/show_bug.cgi?id=695438). IE 10 lacks support
 * for __proto__ and has a buggy typed array implementation.
 */

Buffer.TYPED_ARRAY_SUPPORT = typedArraySupport();

if (!Buffer.TYPED_ARRAY_SUPPORT && typeof console !== 'undefined' && typeof console.error === 'function') {
  console.error('This browser lacks typed array (Uint8Array) support which is required by ' + '`buffer` v5.x. Use `buffer` v4.x if you require old browser support.');
}

function typedArraySupport() {
  // Can typed array instances can be augmented?
  try {
    var arr = new Uint8Array(1);
    var proto = {
      foo: function foo() {
        return 42;
      }
    };
    Object.setPrototypeOf(proto, Uint8Array.prototype);
    Object.setPrototypeOf(arr, proto);
    return arr.foo() === 42;
  } catch (e) {
    return false;
  }
}

Object.defineProperty(Buffer.prototype, 'parent', {
  enumerable: true,
  get: function get() {
    if (!Buffer.isBuffer(this)) return undefined;
    return this.buffer;
  }
});
Object.defineProperty(Buffer.prototype, 'offset', {
  enumerable: true,
  get: function get() {
    if (!Buffer.isBuffer(this)) return undefined;
    return this.byteOffset;
  }
});

function createBuffer(length) {
  if (length > K_MAX_LENGTH) {
    throw new RangeError('The value "' + length + '" is invalid for option "size"');
  } // Return an augmented `Uint8Array` instance


  var buf = new Uint8Array(length);
  Object.setPrototypeOf(buf, Buffer.prototype);
  return buf;
}
/**
 * The Buffer constructor returns instances of `Uint8Array` that have their
 * prototype changed to `Buffer.prototype`. Furthermore, `Buffer` is a subclass of
 * `Uint8Array`, so the returned instances will have all the node `Buffer` methods
 * and the `Uint8Array` methods. Square bracket notation works as expected -- it
 * returns a single octet.
 *
 * The `Uint8Array` prototype remains unmodified.
 */


function Buffer(arg, encodingOrOffset, length) {
  // Common case.
  if (typeof arg === 'number') {
    if (typeof encodingOrOffset === 'string') {
      throw new TypeError('The "string" argument must be of type string. Received type number');
    }

    return allocUnsafe(arg);
  }

  return from(arg, encodingOrOffset, length);
}

Buffer.poolSize = 8192; // not used by this implementation

function from(value, encodingOrOffset, length) {
  if (typeof value === 'string') {
    return fromString(value, encodingOrOffset);
  }

  if (ArrayBuffer.isView(value)) {
    return fromArrayView(value);
  }

  if (value == null) {
    throw new TypeError('The first argument must be one of type string, Buffer, ArrayBuffer, Array, ' + 'or Array-like Object. Received type ' + _typeof(value));
  }

  if (isInstance(value, ArrayBuffer) || value && isInstance(value.buffer, ArrayBuffer)) {
    return fromArrayBuffer(value, encodingOrOffset, length);
  }

  if (typeof SharedArrayBuffer !== 'undefined' && (isInstance(value, SharedArrayBuffer) || value && isInstance(value.buffer, SharedArrayBuffer))) {
    return fromArrayBuffer(value, encodingOrOffset, length);
  }

  if (typeof value === 'number') {
    throw new TypeError('The "value" argument must not be of type number. Received type number');
  }

  var valueOf = value.valueOf && value.valueOf();

  if (valueOf != null && valueOf !== value) {
    return Buffer.from(valueOf, encodingOrOffset, length);
  }

  var b = fromObject(value);
  if (b) return b;

  if (typeof Symbol !== 'undefined' && Symbol.toPrimitive != null && typeof value[Symbol.toPrimitive] === 'function') {
    return Buffer.from(value[Symbol.toPrimitive]('string'), encodingOrOffset, length);
  }

  throw new TypeError('The first argument must be one of type string, Buffer, ArrayBuffer, Array, ' + 'or Array-like Object. Received type ' + _typeof(value));
}
/**
 * Functionally equivalent to Buffer(arg, encoding) but throws a TypeError
 * if value is a number.
 * Buffer.from(str[, encoding])
 * Buffer.from(array)
 * Buffer.from(buffer)
 * Buffer.from(arrayBuffer[, byteOffset[, length]])
 **/


Buffer.from = function (value, encodingOrOffset, length) {
  return from(value, encodingOrOffset, length);
}; // Note: Change prototype *after* Buffer.from is defined to workaround Chrome bug:
// https://github.com/feross/buffer/pull/148


Object.setPrototypeOf(Buffer.prototype, Uint8Array.prototype);
Object.setPrototypeOf(Buffer, Uint8Array);

function assertSize(size) {
  if (typeof size !== 'number') {
    throw new TypeError('"size" argument must be of type number');
  } else if (size < 0) {
    throw new RangeError('The value "' + size + '" is invalid for option "size"');
  }
}

function alloc(size, fill, encoding) {
  assertSize(size);

  if (size <= 0) {
    return createBuffer(size);
  }

  if (fill !== undefined) {
    // Only pay attention to encoding if it's a string. This
    // prevents accidentally sending in a number that would
    // be interpreted as a start offset.
    return typeof encoding === 'string' ? createBuffer(size).fill(fill, encoding) : createBuffer(size).fill(fill);
  }

  return createBuffer(size);
}
/**
 * Creates a new filled Buffer instance.
 * alloc(size[, fill[, encoding]])
 **/


Buffer.alloc = function (size, fill, encoding) {
  return alloc(size, fill, encoding);
};

function allocUnsafe(size) {
  assertSize(size);
  return createBuffer(size < 0 ? 0 : checked(size) | 0);
}
/**
 * Equivalent to Buffer(num), by default creates a non-zero-filled Buffer instance.
 * */


Buffer.allocUnsafe = function (size) {
  return allocUnsafe(size);
};
/**
 * Equivalent to SlowBuffer(num), by default creates a non-zero-filled Buffer instance.
 */


Buffer.allocUnsafeSlow = function (size) {
  return allocUnsafe(size);
};

function fromString(string, encoding) {
  if (typeof encoding !== 'string' || encoding === '') {
    encoding = 'utf8';
  }

  if (!Buffer.isEncoding(encoding)) {
    throw new TypeError('Unknown encoding: ' + encoding);
  }

  var length = byteLength(string, encoding) | 0;
  var buf = createBuffer(length);
  var actual = buf.write(string, encoding);

  if (actual !== length) {
    // Writing a hex string, for example, that contains invalid characters will
    // cause everything after the first invalid character to be ignored. (e.g.
    // 'abxxcd' will be treated as 'ab')
    buf = buf.slice(0, actual);
  }

  return buf;
}

function fromArrayLike(array) {
  var length = array.length < 0 ? 0 : checked(array.length) | 0;
  var buf = createBuffer(length);

  for (var i = 0; i < length; i += 1) {
    buf[i] = array[i] & 255;
  }

  return buf;
}

function fromArrayView(arrayView) {
  if (isInstance(arrayView, Uint8Array)) {
    var copy = new Uint8Array(arrayView);
    return fromArrayBuffer(copy.buffer, copy.byteOffset, copy.byteLength);
  }

  return fromArrayLike(arrayView);
}

function fromArrayBuffer(array, byteOffset, length) {
  if (byteOffset < 0 || array.byteLength < byteOffset) {
    throw new RangeError('"offset" is outside of buffer bounds');
  }

  if (array.byteLength < byteOffset + (length || 0)) {
    throw new RangeError('"length" is outside of buffer bounds');
  }

  var buf;

  if (byteOffset === undefined && length === undefined) {
    buf = new Uint8Array(array);
  } else if (length === undefined) {
    buf = new Uint8Array(array, byteOffset);
  } else {
    buf = new Uint8Array(array, byteOffset, length);
  } // Return an augmented `Uint8Array` instance


  Object.setPrototypeOf(buf, Buffer.prototype);
  return buf;
}

function fromObject(obj) {
  if (Buffer.isBuffer(obj)) {
    var len = checked(obj.length) | 0;
    var buf = createBuffer(len);

    if (buf.length === 0) {
      return buf;
    }

    obj.copy(buf, 0, 0, len);
    return buf;
  }

  if (obj.length !== undefined) {
    if (typeof obj.length !== 'number' || numberIsNaN(obj.length)) {
      return createBuffer(0);
    }

    return fromArrayLike(obj);
  }

  if (obj.type === 'Buffer' && Array.isArray(obj.data)) {
    return fromArrayLike(obj.data);
  }
}

function checked(length) {
  // Note: cannot use `length < K_MAX_LENGTH` here because that fails when
  // length is NaN (which is otherwise coerced to zero.)
  if (length >= K_MAX_LENGTH) {
    throw new RangeError('Attempt to allocate Buffer larger than maximum ' + 'size: 0x' + K_MAX_LENGTH.toString(16) + ' bytes');
  }

  return length | 0;
}

function SlowBuffer(length) {
  if (+length != length) {
    // eslint-disable-line eqeqeq
    length = 0;
  }

  return Buffer.alloc(+length);
}

Buffer.isBuffer = function isBuffer(b) {
  return b != null && b._isBuffer === true && b !== Buffer.prototype; // so Buffer.isBuffer(Buffer.prototype) will be false
};

Buffer.compare = function compare(a, b) {
  if (isInstance(a, Uint8Array)) a = Buffer.from(a, a.offset, a.byteLength);
  if (isInstance(b, Uint8Array)) b = Buffer.from(b, b.offset, b.byteLength);

  if (!Buffer.isBuffer(a) || !Buffer.isBuffer(b)) {
    throw new TypeError('The "buf1", "buf2" arguments must be one of type Buffer or Uint8Array');
  }

  if (a === b) return 0;
  var x = a.length;
  var y = b.length;

  for (var i = 0, len = Math.min(x, y); i < len; ++i) {
    if (a[i] !== b[i]) {
      x = a[i];
      y = b[i];
      break;
    }
  }

  if (x < y) return -1;
  if (y < x) return 1;
  return 0;
};

Buffer.isEncoding = function isEncoding(encoding) {
  switch (String(encoding).toLowerCase()) {
    case 'hex':
    case 'utf8':
    case 'utf-8':
    case 'ascii':
    case 'latin1':
    case 'binary':
    case 'base64':
    case 'ucs2':
    case 'ucs-2':
    case 'utf16le':
    case 'utf-16le':
      return true;

    default:
      return false;
  }
};

Buffer.concat = function concat(list, length) {
  if (!Array.isArray(list)) {
    throw new TypeError('"list" argument must be an Array of Buffers');
  }

  if (list.length === 0) {
    return Buffer.alloc(0);
  }

  var i;

  if (length === undefined) {
    length = 0;

    for (i = 0; i < list.length; ++i) {
      length += list[i].length;
    }
  }

  var buffer = Buffer.allocUnsafe(length);
  var pos = 0;

  for (i = 0; i < list.length; ++i) {
    var buf = list[i];

    if (isInstance(buf, Uint8Array)) {
      if (pos + buf.length > buffer.length) {
        if (!Buffer.isBuffer(buf)) buf = Buffer.from(buf);
        buf.copy(buffer, pos);
      } else {
        Uint8Array.prototype.set.call(buffer, buf, pos);
      }
    } else if (!Buffer.isBuffer(buf)) {
      throw new TypeError('"list" argument must be an Array of Buffers');
    } else {
      buf.copy(buffer, pos);
    }

    pos += buf.length;
  }

  return buffer;
};

function byteLength(string, encoding) {
  if (Buffer.isBuffer(string)) {
    return string.length;
  }

  if (ArrayBuffer.isView(string) || isInstance(string, ArrayBuffer)) {
    return string.byteLength;
  }

  if (typeof string !== 'string') {
    throw new TypeError('The "string" argument must be one of type string, Buffer, or ArrayBuffer. ' + 'Received type ' + _typeof(string));
  }

  var len = string.length;
  var mustMatch = arguments.length > 2 && arguments[2] === true;
  if (!mustMatch && len === 0) return 0; // Use a for loop to avoid recursion

  var loweredCase = false;

  for (;;) {
    switch (encoding) {
      case 'ascii':
      case 'latin1':
      case 'binary':
        return len;

      case 'utf8':
      case 'utf-8':
        return utf8ToBytes(string).length;

      case 'ucs2':
      case 'ucs-2':
      case 'utf16le':
      case 'utf-16le':
        return len * 2;

      case 'hex':
        return len >>> 1;

      case 'base64':
        return base64ToBytes(string).length;

      default:
        if (loweredCase) {
          return mustMatch ? -1 : utf8ToBytes(string).length; // assume utf8
        }

        encoding = ('' + encoding).toLowerCase();
        loweredCase = true;
    }
  }
}

Buffer.byteLength = byteLength;

function slowToString(encoding, start, end) {
  var loweredCase = false; // No need to verify that "this.length <= MAX_UINT32" since it's a read-only
  // property of a typed array.
  // This behaves neither like String nor Uint8Array in that we set start/end
  // to their upper/lower bounds if the value passed is out of range.
  // undefined is handled specially as per ECMA-262 6th Edition,
  // Section 13.3.3.7 Runtime Semantics: KeyedBindingInitialization.

  if (start === undefined || start < 0) {
    start = 0;
  } // Return early if start > this.length. Done here to prevent potential uint32
  // coercion fail below.


  if (start > this.length) {
    return '';
  }

  if (end === undefined || end > this.length) {
    end = this.length;
  }

  if (end <= 0) {
    return '';
  } // Force coercion to uint32. This will also coerce falsey/NaN values to 0.


  end >>>= 0;
  start >>>= 0;

  if (end <= start) {
    return '';
  }

  if (!encoding) encoding = 'utf8';

  while (true) {
    switch (encoding) {
      case 'hex':
        return hexSlice(this, start, end);

      case 'utf8':
      case 'utf-8':
        return utf8Slice(this, start, end);

      case 'ascii':
        return asciiSlice(this, start, end);

      case 'latin1':
      case 'binary':
        return latin1Slice(this, start, end);

      case 'base64':
        return base64Slice(this, start, end);

      case 'ucs2':
      case 'ucs-2':
      case 'utf16le':
      case 'utf-16le':
        return utf16leSlice(this, start, end);

      default:
        if (loweredCase) throw new TypeError('Unknown encoding: ' + encoding);
        encoding = (encoding + '').toLowerCase();
        loweredCase = true;
    }
  }
} // This property is used by `Buffer.isBuffer` (and the `is-buffer` npm package)
// to detect a Buffer instance. It's not possible to use `instanceof Buffer`
// reliably in a browserify context because there could be multiple different
// copies of the 'buffer' package in use. This method works even for Buffer
// instances that were created from another copy of the `buffer` package.
// See: https://github.com/feross/buffer/issues/154


Buffer.prototype._isBuffer = true;

function swap(b, n, m) {
  var i = b[n];
  b[n] = b[m];
  b[m] = i;
}

Buffer.prototype.swap16 = function swap16() {
  var len = this.length;

  if (len % 2 !== 0) {
    throw new RangeError('Buffer size must be a multiple of 16-bits');
  }

  for (var i = 0; i < len; i += 2) {
    swap(this, i, i + 1);
  }

  return this;
};

Buffer.prototype.swap32 = function swap32() {
  var len = this.length;

  if (len % 4 !== 0) {
    throw new RangeError('Buffer size must be a multiple of 32-bits');
  }

  for (var i = 0; i < len; i += 4) {
    swap(this, i, i + 3);
    swap(this, i + 1, i + 2);
  }

  return this;
};

Buffer.prototype.swap64 = function swap64() {
  var len = this.length;

  if (len % 8 !== 0) {
    throw new RangeError('Buffer size must be a multiple of 64-bits');
  }

  for (var i = 0; i < len; i += 8) {
    swap(this, i, i + 7);
    swap(this, i + 1, i + 6);
    swap(this, i + 2, i + 5);
    swap(this, i + 3, i + 4);
  }

  return this;
};

Buffer.prototype.toString = function toString() {
  var length = this.length;
  if (length === 0) return '';
  if (arguments.length === 0) return utf8Slice(this, 0, length);
  return slowToString.apply(this, arguments);
};

Buffer.prototype.toLocaleString = Buffer.prototype.toString;

Buffer.prototype.equals = function equals(b) {
  if (!Buffer.isBuffer(b)) throw new TypeError('Argument must be a Buffer');
  if (this === b) return true;
  return Buffer.compare(this, b) === 0;
};

Buffer.prototype.inspect = function inspect() {
  var str = '';
  var max = exports.INSPECT_MAX_BYTES;
  str = this.toString('hex', 0, max).replace(/(.{2})/g, '$1 ').trim();
  if (this.length > max) str += ' ... ';
  return '<Buffer ' + str + '>';
};

if (customInspectSymbol) {
  Buffer.prototype[customInspectSymbol] = Buffer.prototype.inspect;
}

Buffer.prototype.compare = function compare(target, start, end, thisStart, thisEnd) {
  if (isInstance(target, Uint8Array)) {
    target = Buffer.from(target, target.offset, target.byteLength);
  }

  if (!Buffer.isBuffer(target)) {
    throw new TypeError('The "target" argument must be one of type Buffer or Uint8Array. ' + 'Received type ' + _typeof(target));
  }

  if (start === undefined) {
    start = 0;
  }

  if (end === undefined) {
    end = target ? target.length : 0;
  }

  if (thisStart === undefined) {
    thisStart = 0;
  }

  if (thisEnd === undefined) {
    thisEnd = this.length;
  }

  if (start < 0 || end > target.length || thisStart < 0 || thisEnd > this.length) {
    throw new RangeError('out of range index');
  }

  if (thisStart >= thisEnd && start >= end) {
    return 0;
  }

  if (thisStart >= thisEnd) {
    return -1;
  }

  if (start >= end) {
    return 1;
  }

  start >>>= 0;
  end >>>= 0;
  thisStart >>>= 0;
  thisEnd >>>= 0;
  if (this === target) return 0;
  var x = thisEnd - thisStart;
  var y = end - start;
  var len = Math.min(x, y);
  var thisCopy = this.slice(thisStart, thisEnd);
  var targetCopy = target.slice(start, end);

  for (var i = 0; i < len; ++i) {
    if (thisCopy[i] !== targetCopy[i]) {
      x = thisCopy[i];
      y = targetCopy[i];
      break;
    }
  }

  if (x < y) return -1;
  if (y < x) return 1;
  return 0;
}; // Finds either the first index of `val` in `buffer` at offset >= `byteOffset`,
// OR the last index of `val` in `buffer` at offset <= `byteOffset`.
//
// Arguments:
// - buffer - a Buffer to search
// - val - a string, Buffer, or number
// - byteOffset - an index into `buffer`; will be clamped to an int32
// - encoding - an optional encoding, relevant is val is a string
// - dir - true for indexOf, false for lastIndexOf


function bidirectionalIndexOf(buffer, val, byteOffset, encoding, dir) {
  // Empty buffer means no match
  if (buffer.length === 0) return -1; // Normalize byteOffset

  if (typeof byteOffset === 'string') {
    encoding = byteOffset;
    byteOffset = 0;
  } else if (byteOffset > 0x7fffffff) {
    byteOffset = 0x7fffffff;
  } else if (byteOffset < -0x80000000) {
    byteOffset = -0x80000000;
  }

  byteOffset = +byteOffset; // Coerce to Number.

  if (numberIsNaN(byteOffset)) {
    // byteOffset: it it's undefined, null, NaN, "foo", etc, search whole buffer
    byteOffset = dir ? 0 : buffer.length - 1;
  } // Normalize byteOffset: negative offsets start from the end of the buffer


  if (byteOffset < 0) byteOffset = buffer.length + byteOffset;

  if (byteOffset >= buffer.length) {
    if (dir) return -1;else byteOffset = buffer.length - 1;
  } else if (byteOffset < 0) {
    if (dir) byteOffset = 0;else return -1;
  } // Normalize val


  if (typeof val === 'string') {
    val = Buffer.from(val, encoding);
  } // Finally, search either indexOf (if dir is true) or lastIndexOf


  if (Buffer.isBuffer(val)) {
    // Special case: looking for empty string/buffer always fails
    if (val.length === 0) {
      return -1;
    }

    return arrayIndexOf(buffer, val, byteOffset, encoding, dir);
  } else if (typeof val === 'number') {
    val = val & 0xFF; // Search for a byte value [0-255]

    if (typeof Uint8Array.prototype.indexOf === 'function') {
      if (dir) {
        return Uint8Array.prototype.indexOf.call(buffer, val, byteOffset);
      } else {
        return Uint8Array.prototype.lastIndexOf.call(buffer, val, byteOffset);
      }
    }

    return arrayIndexOf(buffer, [val], byteOffset, encoding, dir);
  }

  throw new TypeError('val must be string, number or Buffer');
}

function arrayIndexOf(arr, val, byteOffset, encoding, dir) {
  var indexSize = 1;
  var arrLength = arr.length;
  var valLength = val.length;

  if (encoding !== undefined) {
    encoding = String(encoding).toLowerCase();

    if (encoding === 'ucs2' || encoding === 'ucs-2' || encoding === 'utf16le' || encoding === 'utf-16le') {
      if (arr.length < 2 || val.length < 2) {
        return -1;
      }

      indexSize = 2;
      arrLength /= 2;
      valLength /= 2;
      byteOffset /= 2;
    }
  }

  function read(buf, i) {
    if (indexSize === 1) {
      return buf[i];
    } else {
      return buf.readUInt16BE(i * indexSize);
    }
  }

  var i;

  if (dir) {
    var foundIndex = -1;

    for (i = byteOffset; i < arrLength; i++) {
      if (read(arr, i) === read(val, foundIndex === -1 ? 0 : i - foundIndex)) {
        if (foundIndex === -1) foundIndex = i;
        if (i - foundIndex + 1 === valLength) return foundIndex * indexSize;
      } else {
        if (foundIndex !== -1) i -= i - foundIndex;
        foundIndex = -1;
      }
    }
  } else {
    if (byteOffset + valLength > arrLength) byteOffset = arrLength - valLength;

    for (i = byteOffset; i >= 0; i--) {
      var found = true;

      for (var j = 0; j < valLength; j++) {
        if (read(arr, i + j) !== read(val, j)) {
          found = false;
          break;
        }
      }

      if (found) return i;
    }
  }

  return -1;
}

Buffer.prototype.includes = function includes(val, byteOffset, encoding) {
  return this.indexOf(val, byteOffset, encoding) !== -1;
};

Buffer.prototype.indexOf = function indexOf(val, byteOffset, encoding) {
  return bidirectionalIndexOf(this, val, byteOffset, encoding, true);
};

Buffer.prototype.lastIndexOf = function lastIndexOf(val, byteOffset, encoding) {
  return bidirectionalIndexOf(this, val, byteOffset, encoding, false);
};

function hexWrite(buf, string, offset, length) {
  offset = Number(offset) || 0;
  var remaining = buf.length - offset;

  if (!length) {
    length = remaining;
  } else {
    length = Number(length);

    if (length > remaining) {
      length = remaining;
    }
  }

  var strLen = string.length;

  if (length > strLen / 2) {
    length = strLen / 2;
  }

  var i;

  for (i = 0; i < length; ++i) {
    var parsed = parseInt(string.substr(i * 2, 2), 16);
    if (numberIsNaN(parsed)) return i;
    buf[offset + i] = parsed;
  }

  return i;
}

function utf8Write(buf, string, offset, length) {
  return blitBuffer(utf8ToBytes(string, buf.length - offset), buf, offset, length);
}

function asciiWrite(buf, string, offset, length) {
  return blitBuffer(asciiToBytes(string), buf, offset, length);
}

function base64Write(buf, string, offset, length) {
  return blitBuffer(base64ToBytes(string), buf, offset, length);
}

function ucs2Write(buf, string, offset, length) {
  return blitBuffer(utf16leToBytes(string, buf.length - offset), buf, offset, length);
}

Buffer.prototype.write = function write(string, offset, length, encoding) {
  // Buffer#write(string)
  if (offset === undefined) {
    encoding = 'utf8';
    length = this.length;
    offset = 0; // Buffer#write(string, encoding)
  } else if (length === undefined && typeof offset === 'string') {
    encoding = offset;
    length = this.length;
    offset = 0; // Buffer#write(string, offset[, length][, encoding])
  } else if (isFinite(offset)) {
    offset = offset >>> 0;

    if (isFinite(length)) {
      length = length >>> 0;
      if (encoding === undefined) encoding = 'utf8';
    } else {
      encoding = length;
      length = undefined;
    }
  } else {
    throw new Error('Buffer.write(string, encoding, offset[, length]) is no longer supported');
  }

  var remaining = this.length - offset;
  if (length === undefined || length > remaining) length = remaining;

  if (string.length > 0 && (length < 0 || offset < 0) || offset > this.length) {
    throw new RangeError('Attempt to write outside buffer bounds');
  }

  if (!encoding) encoding = 'utf8';
  var loweredCase = false;

  for (;;) {
    switch (encoding) {
      case 'hex':
        return hexWrite(this, string, offset, length);

      case 'utf8':
      case 'utf-8':
        return utf8Write(this, string, offset, length);

      case 'ascii':
      case 'latin1':
      case 'binary':
        return asciiWrite(this, string, offset, length);

      case 'base64':
        // Warning: maxLength not taken into account in base64Write
        return base64Write(this, string, offset, length);

      case 'ucs2':
      case 'ucs-2':
      case 'utf16le':
      case 'utf-16le':
        return ucs2Write(this, string, offset, length);

      default:
        if (loweredCase) throw new TypeError('Unknown encoding: ' + encoding);
        encoding = ('' + encoding).toLowerCase();
        loweredCase = true;
    }
  }
};

Buffer.prototype.toJSON = function toJSON() {
  return {
    type: 'Buffer',
    data: Array.prototype.slice.call(this._arr || this, 0)
  };
};

function base64Slice(buf, start, end) {
  if (start === 0 && end === buf.length) {
    return base64.fromByteArray(buf);
  } else {
    return base64.fromByteArray(buf.slice(start, end));
  }
}

function utf8Slice(buf, start, end) {
  end = Math.min(buf.length, end);
  var res = [];
  var i = start;

  while (i < end) {
    var firstByte = buf[i];
    var codePoint = null;
    var bytesPerSequence = firstByte > 0xEF ? 4 : firstByte > 0xDF ? 3 : firstByte > 0xBF ? 2 : 1;

    if (i + bytesPerSequence <= end) {
      var secondByte = void 0,
          thirdByte = void 0,
          fourthByte = void 0,
          tempCodePoint = void 0;

      switch (bytesPerSequence) {
        case 1:
          if (firstByte < 0x80) {
            codePoint = firstByte;
          }

          break;

        case 2:
          secondByte = buf[i + 1];

          if ((secondByte & 0xC0) === 0x80) {
            tempCodePoint = (firstByte & 0x1F) << 0x6 | secondByte & 0x3F;

            if (tempCodePoint > 0x7F) {
              codePoint = tempCodePoint;
            }
          }

          break;

        case 3:
          secondByte = buf[i + 1];
          thirdByte = buf[i + 2];

          if ((secondByte & 0xC0) === 0x80 && (thirdByte & 0xC0) === 0x80) {
            tempCodePoint = (firstByte & 0xF) << 0xC | (secondByte & 0x3F) << 0x6 | thirdByte & 0x3F;

            if (tempCodePoint > 0x7FF && (tempCodePoint < 0xD800 || tempCodePoint > 0xDFFF)) {
              codePoint = tempCodePoint;
            }
          }

          break;

        case 4:
          secondByte = buf[i + 1];
          thirdByte = buf[i + 2];
          fourthByte = buf[i + 3];

          if ((secondByte & 0xC0) === 0x80 && (thirdByte & 0xC0) === 0x80 && (fourthByte & 0xC0) === 0x80) {
            tempCodePoint = (firstByte & 0xF) << 0x12 | (secondByte & 0x3F) << 0xC | (thirdByte & 0x3F) << 0x6 | fourthByte & 0x3F;

            if (tempCodePoint > 0xFFFF && tempCodePoint < 0x110000) {
              codePoint = tempCodePoint;
            }
          }

      }
    }

    if (codePoint === null) {
      // we did not generate a valid codePoint so insert a
      // replacement char (U+FFFD) and advance only 1 byte
      codePoint = 0xFFFD;
      bytesPerSequence = 1;
    } else if (codePoint > 0xFFFF) {
      // encode to utf16 (surrogate pair dance)
      codePoint -= 0x10000;
      res.push(codePoint >>> 10 & 0x3FF | 0xD800);
      codePoint = 0xDC00 | codePoint & 0x3FF;
    }

    res.push(codePoint);
    i += bytesPerSequence;
  }

  return decodeCodePointsArray(res);
} // Based on http://stackoverflow.com/a/22747272/680742, the browser with
// the lowest limit is Chrome, with 0x10000 args.
// We go 1 magnitude less, for safety


var MAX_ARGUMENTS_LENGTH = 0x1000;

function decodeCodePointsArray(codePoints) {
  var len = codePoints.length;

  if (len <= MAX_ARGUMENTS_LENGTH) {
    return String.fromCharCode.apply(String, codePoints); // avoid extra slice()
  } // Decode in chunks to avoid "call stack size exceeded".


  var res = '';
  var i = 0;

  while (i < len) {
    res += String.fromCharCode.apply(String, codePoints.slice(i, i += MAX_ARGUMENTS_LENGTH));
  }

  return res;
}

function asciiSlice(buf, start, end) {
  var ret = '';
  end = Math.min(buf.length, end);

  for (var i = start; i < end; ++i) {
    ret += String.fromCharCode(buf[i] & 0x7F);
  }

  return ret;
}

function latin1Slice(buf, start, end) {
  var ret = '';
  end = Math.min(buf.length, end);

  for (var i = start; i < end; ++i) {
    ret += String.fromCharCode(buf[i]);
  }

  return ret;
}

function hexSlice(buf, start, end) {
  var len = buf.length;
  if (!start || start < 0) start = 0;
  if (!end || end < 0 || end > len) end = len;
  var out = '';

  for (var i = start; i < end; ++i) {
    out += hexSliceLookupTable[buf[i]];
  }

  return out;
}

function utf16leSlice(buf, start, end) {
  var bytes = buf.slice(start, end);
  var res = ''; // If bytes.length is odd, the last 8 bits must be ignored (same as node.js)

  for (var i = 0; i < bytes.length - 1; i += 2) {
    res += String.fromCharCode(bytes[i] + bytes[i + 1] * 256);
  }

  return res;
}

Buffer.prototype.slice = function slice(start, end) {
  var len = this.length;
  start = ~~start;
  end = end === undefined ? len : ~~end;

  if (start < 0) {
    start += len;
    if (start < 0) start = 0;
  } else if (start > len) {
    start = len;
  }

  if (end < 0) {
    end += len;
    if (end < 0) end = 0;
  } else if (end > len) {
    end = len;
  }

  if (end < start) end = start;
  var newBuf = this.subarray(start, end); // Return an augmented `Uint8Array` instance

  Object.setPrototypeOf(newBuf, Buffer.prototype);
  return newBuf;
};
/*
 * Need to make sure that buffer isn't trying to write out of bounds.
 */


function checkOffset(offset, ext, length) {
  if (offset % 1 !== 0 || offset < 0) throw new RangeError('offset is not uint');
  if (offset + ext > length) throw new RangeError('Trying to access beyond buffer length');
}

Buffer.prototype.readUintLE = Buffer.prototype.readUIntLE = function readUIntLE(offset, byteLength, noAssert) {
  offset = offset >>> 0;
  byteLength = byteLength >>> 0;
  if (!noAssert) checkOffset(offset, byteLength, this.length);
  var val = this[offset];
  var mul = 1;
  var i = 0;

  while (++i < byteLength && (mul *= 0x100)) {
    val += this[offset + i] * mul;
  }

  return val;
};

Buffer.prototype.readUintBE = Buffer.prototype.readUIntBE = function readUIntBE(offset, byteLength, noAssert) {
  offset = offset >>> 0;
  byteLength = byteLength >>> 0;

  if (!noAssert) {
    checkOffset(offset, byteLength, this.length);
  }

  var val = this[offset + --byteLength];
  var mul = 1;

  while (byteLength > 0 && (mul *= 0x100)) {
    val += this[offset + --byteLength] * mul;
  }

  return val;
};

Buffer.prototype.readUint8 = Buffer.prototype.readUInt8 = function readUInt8(offset, noAssert) {
  offset = offset >>> 0;
  if (!noAssert) checkOffset(offset, 1, this.length);
  return this[offset];
};

Buffer.prototype.readUint16LE = Buffer.prototype.readUInt16LE = function readUInt16LE(offset, noAssert) {
  offset = offset >>> 0;
  if (!noAssert) checkOffset(offset, 2, this.length);
  return this[offset] | this[offset + 1] << 8;
};

Buffer.prototype.readUint16BE = Buffer.prototype.readUInt16BE = function readUInt16BE(offset, noAssert) {
  offset = offset >>> 0;
  if (!noAssert) checkOffset(offset, 2, this.length);
  return this[offset] << 8 | this[offset + 1];
};

Buffer.prototype.readUint32LE = Buffer.prototype.readUInt32LE = function readUInt32LE(offset, noAssert) {
  offset = offset >>> 0;
  if (!noAssert) checkOffset(offset, 4, this.length);
  return (this[offset] | this[offset + 1] << 8 | this[offset + 2] << 16) + this[offset + 3] * 0x1000000;
};

Buffer.prototype.readUint32BE = Buffer.prototype.readUInt32BE = function readUInt32BE(offset, noAssert) {
  offset = offset >>> 0;
  if (!noAssert) checkOffset(offset, 4, this.length);
  return this[offset] * 0x1000000 + (this[offset + 1] << 16 | this[offset + 2] << 8 | this[offset + 3]);
};

Buffer.prototype.readBigUInt64LE = defineBigIntMethod(function readBigUInt64LE(offset) {
  offset = offset >>> 0;
  validateNumber(offset, 'offset');
  var first = this[offset];
  var last = this[offset + 7];

  if (first === undefined || last === undefined) {
    boundsError(offset, this.length - 8);
  }

  var lo = first + this[++offset] * Math.pow(2, 8) + this[++offset] * Math.pow(2, 16) + this[++offset] * Math.pow(2, 24);
  var hi = this[++offset] + this[++offset] * Math.pow(2, 8) + this[++offset] * Math.pow(2, 16) + last * Math.pow(2, 24);
  return BigInt(lo) + (BigInt(hi) << BigInt(32));
});
Buffer.prototype.readBigUInt64BE = defineBigIntMethod(function readBigUInt64BE(offset) {
  offset = offset >>> 0;
  validateNumber(offset, 'offset');
  var first = this[offset];
  var last = this[offset + 7];

  if (first === undefined || last === undefined) {
    boundsError(offset, this.length - 8);
  }

  var hi = first * Math.pow(2, 24) + this[++offset] * Math.pow(2, 16) + this[++offset] * Math.pow(2, 8) + this[++offset];
  var lo = this[++offset] * Math.pow(2, 24) + this[++offset] * Math.pow(2, 16) + this[++offset] * Math.pow(2, 8) + last;
  return (BigInt(hi) << BigInt(32)) + BigInt(lo);
});

Buffer.prototype.readIntLE = function readIntLE(offset, byteLength, noAssert) {
  offset = offset >>> 0;
  byteLength = byteLength >>> 0;
  if (!noAssert) checkOffset(offset, byteLength, this.length);
  var val = this[offset];
  var mul = 1;
  var i = 0;

  while (++i < byteLength && (mul *= 0x100)) {
    val += this[offset + i] * mul;
  }

  mul *= 0x80;
  if (val >= mul) val -= Math.pow(2, 8 * byteLength);
  return val;
};

Buffer.prototype.readIntBE = function readIntBE(offset, byteLength, noAssert) {
  offset = offset >>> 0;
  byteLength = byteLength >>> 0;
  if (!noAssert) checkOffset(offset, byteLength, this.length);
  var i = byteLength;
  var mul = 1;
  var val = this[offset + --i];

  while (i > 0 && (mul *= 0x100)) {
    val += this[offset + --i] * mul;
  }

  mul *= 0x80;
  if (val >= mul) val -= Math.pow(2, 8 * byteLength);
  return val;
};

Buffer.prototype.readInt8 = function readInt8(offset, noAssert) {
  offset = offset >>> 0;
  if (!noAssert) checkOffset(offset, 1, this.length);
  if (!(this[offset] & 0x80)) return this[offset];
  return (0xff - this[offset] + 1) * -1;
};

Buffer.prototype.readInt16LE = function readInt16LE(offset, noAssert) {
  offset = offset >>> 0;
  if (!noAssert) checkOffset(offset, 2, this.length);
  var val = this[offset] | this[offset + 1] << 8;
  return val & 0x8000 ? val | 0xFFFF0000 : val;
};

Buffer.prototype.readInt16BE = function readInt16BE(offset, noAssert) {
  offset = offset >>> 0;
  if (!noAssert) checkOffset(offset, 2, this.length);
  var val = this[offset + 1] | this[offset] << 8;
  return val & 0x8000 ? val | 0xFFFF0000 : val;
};

Buffer.prototype.readInt32LE = function readInt32LE(offset, noAssert) {
  offset = offset >>> 0;
  if (!noAssert) checkOffset(offset, 4, this.length);
  return this[offset] | this[offset + 1] << 8 | this[offset + 2] << 16 | this[offset + 3] << 24;
};

Buffer.prototype.readInt32BE = function readInt32BE(offset, noAssert) {
  offset = offset >>> 0;
  if (!noAssert) checkOffset(offset, 4, this.length);
  return this[offset] << 24 | this[offset + 1] << 16 | this[offset + 2] << 8 | this[offset + 3];
};

Buffer.prototype.readBigInt64LE = defineBigIntMethod(function readBigInt64LE(offset) {
  offset = offset >>> 0;
  validateNumber(offset, 'offset');
  var first = this[offset];
  var last = this[offset + 7];

  if (first === undefined || last === undefined) {
    boundsError(offset, this.length - 8);
  }

  var val = this[offset + 4] + this[offset + 5] * Math.pow(2, 8) + this[offset + 6] * Math.pow(2, 16) + (last << 24); // Overflow

  return (BigInt(val) << BigInt(32)) + BigInt(first + this[++offset] * Math.pow(2, 8) + this[++offset] * Math.pow(2, 16) + this[++offset] * Math.pow(2, 24));
});
Buffer.prototype.readBigInt64BE = defineBigIntMethod(function readBigInt64BE(offset) {
  offset = offset >>> 0;
  validateNumber(offset, 'offset');
  var first = this[offset];
  var last = this[offset + 7];

  if (first === undefined || last === undefined) {
    boundsError(offset, this.length - 8);
  }

  var val = (first << 24) + // Overflow
  this[++offset] * Math.pow(2, 16) + this[++offset] * Math.pow(2, 8) + this[++offset];
  return (BigInt(val) << BigInt(32)) + BigInt(this[++offset] * Math.pow(2, 24) + this[++offset] * Math.pow(2, 16) + this[++offset] * Math.pow(2, 8) + last);
});

Buffer.prototype.readFloatLE = function readFloatLE(offset, noAssert) {
  offset = offset >>> 0;
  if (!noAssert) checkOffset(offset, 4, this.length);
  return ieee754.read(this, offset, true, 23, 4);
};

Buffer.prototype.readFloatBE = function readFloatBE(offset, noAssert) {
  offset = offset >>> 0;
  if (!noAssert) checkOffset(offset, 4, this.length);
  return ieee754.read(this, offset, false, 23, 4);
};

Buffer.prototype.readDoubleLE = function readDoubleLE(offset, noAssert) {
  offset = offset >>> 0;
  if (!noAssert) checkOffset(offset, 8, this.length);
  return ieee754.read(this, offset, true, 52, 8);
};

Buffer.prototype.readDoubleBE = function readDoubleBE(offset, noAssert) {
  offset = offset >>> 0;
  if (!noAssert) checkOffset(offset, 8, this.length);
  return ieee754.read(this, offset, false, 52, 8);
};

function checkInt(buf, value, offset, ext, max, min) {
  if (!Buffer.isBuffer(buf)) throw new TypeError('"buffer" argument must be a Buffer instance');
  if (value > max || value < min) throw new RangeError('"value" argument is out of bounds');
  if (offset + ext > buf.length) throw new RangeError('Index out of range');
}

Buffer.prototype.writeUintLE = Buffer.prototype.writeUIntLE = function writeUIntLE(value, offset, byteLength, noAssert) {
  value = +value;
  offset = offset >>> 0;
  byteLength = byteLength >>> 0;

  if (!noAssert) {
    var maxBytes = Math.pow(2, 8 * byteLength) - 1;
    checkInt(this, value, offset, byteLength, maxBytes, 0);
  }

  var mul = 1;
  var i = 0;
  this[offset] = value & 0xFF;

  while (++i < byteLength && (mul *= 0x100)) {
    this[offset + i] = value / mul & 0xFF;
  }

  return offset + byteLength;
};

Buffer.prototype.writeUintBE = Buffer.prototype.writeUIntBE = function writeUIntBE(value, offset, byteLength, noAssert) {
  value = +value;
  offset = offset >>> 0;
  byteLength = byteLength >>> 0;

  if (!noAssert) {
    var maxBytes = Math.pow(2, 8 * byteLength) - 1;
    checkInt(this, value, offset, byteLength, maxBytes, 0);
  }

  var i = byteLength - 1;
  var mul = 1;
  this[offset + i] = value & 0xFF;

  while (--i >= 0 && (mul *= 0x100)) {
    this[offset + i] = value / mul & 0xFF;
  }

  return offset + byteLength;
};

Buffer.prototype.writeUint8 = Buffer.prototype.writeUInt8 = function writeUInt8(value, offset, noAssert) {
  value = +value;
  offset = offset >>> 0;
  if (!noAssert) checkInt(this, value, offset, 1, 0xff, 0);
  this[offset] = value & 0xff;
  return offset + 1;
};

Buffer.prototype.writeUint16LE = Buffer.prototype.writeUInt16LE = function writeUInt16LE(value, offset, noAssert) {
  value = +value;
  offset = offset >>> 0;
  if (!noAssert) checkInt(this, value, offset, 2, 0xffff, 0);
  this[offset] = value & 0xff;
  this[offset + 1] = value >>> 8;
  return offset + 2;
};

Buffer.prototype.writeUint16BE = Buffer.prototype.writeUInt16BE = function writeUInt16BE(value, offset, noAssert) {
  value = +value;
  offset = offset >>> 0;
  if (!noAssert) checkInt(this, value, offset, 2, 0xffff, 0);
  this[offset] = value >>> 8;
  this[offset + 1] = value & 0xff;
  return offset + 2;
};

Buffer.prototype.writeUint32LE = Buffer.prototype.writeUInt32LE = function writeUInt32LE(value, offset, noAssert) {
  value = +value;
  offset = offset >>> 0;
  if (!noAssert) checkInt(this, value, offset, 4, 0xffffffff, 0);
  this[offset + 3] = value >>> 24;
  this[offset + 2] = value >>> 16;
  this[offset + 1] = value >>> 8;
  this[offset] = value & 0xff;
  return offset + 4;
};

Buffer.prototype.writeUint32BE = Buffer.prototype.writeUInt32BE = function writeUInt32BE(value, offset, noAssert) {
  value = +value;
  offset = offset >>> 0;
  if (!noAssert) checkInt(this, value, offset, 4, 0xffffffff, 0);
  this[offset] = value >>> 24;
  this[offset + 1] = value >>> 16;
  this[offset + 2] = value >>> 8;
  this[offset + 3] = value & 0xff;
  return offset + 4;
};

function wrtBigUInt64LE(buf, value, offset, min, max) {
  checkIntBI(value, min, max, buf, offset, 7);
  var lo = Number(value & BigInt(0xffffffff));
  buf[offset++] = lo;
  lo = lo >> 8;
  buf[offset++] = lo;
  lo = lo >> 8;
  buf[offset++] = lo;
  lo = lo >> 8;
  buf[offset++] = lo;
  var hi = Number(value >> BigInt(32) & BigInt(0xffffffff));
  buf[offset++] = hi;
  hi = hi >> 8;
  buf[offset++] = hi;
  hi = hi >> 8;
  buf[offset++] = hi;
  hi = hi >> 8;
  buf[offset++] = hi;
  return offset;
}

function wrtBigUInt64BE(buf, value, offset, min, max) {
  checkIntBI(value, min, max, buf, offset, 7);
  var lo = Number(value & BigInt(0xffffffff));
  buf[offset + 7] = lo;
  lo = lo >> 8;
  buf[offset + 6] = lo;
  lo = lo >> 8;
  buf[offset + 5] = lo;
  lo = lo >> 8;
  buf[offset + 4] = lo;
  var hi = Number(value >> BigInt(32) & BigInt(0xffffffff));
  buf[offset + 3] = hi;
  hi = hi >> 8;
  buf[offset + 2] = hi;
  hi = hi >> 8;
  buf[offset + 1] = hi;
  hi = hi >> 8;
  buf[offset] = hi;
  return offset + 8;
}

Buffer.prototype.writeBigUInt64LE = defineBigIntMethod(function writeBigUInt64LE(value) {
  var offset = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
  return wrtBigUInt64LE(this, value, offset, BigInt(0), BigInt('0xffffffffffffffff'));
});
Buffer.prototype.writeBigUInt64BE = defineBigIntMethod(function writeBigUInt64BE(value) {
  var offset = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
  return wrtBigUInt64BE(this, value, offset, BigInt(0), BigInt('0xffffffffffffffff'));
});

Buffer.prototype.writeIntLE = function writeIntLE(value, offset, byteLength, noAssert) {
  value = +value;
  offset = offset >>> 0;

  if (!noAssert) {
    var limit = Math.pow(2, 8 * byteLength - 1);
    checkInt(this, value, offset, byteLength, limit - 1, -limit);
  }

  var i = 0;
  var mul = 1;
  var sub = 0;
  this[offset] = value & 0xFF;

  while (++i < byteLength && (mul *= 0x100)) {
    if (value < 0 && sub === 0 && this[offset + i - 1] !== 0) {
      sub = 1;
    }

    this[offset + i] = (value / mul >> 0) - sub & 0xFF;
  }

  return offset + byteLength;
};

Buffer.prototype.writeIntBE = function writeIntBE(value, offset, byteLength, noAssert) {
  value = +value;
  offset = offset >>> 0;

  if (!noAssert) {
    var limit = Math.pow(2, 8 * byteLength - 1);
    checkInt(this, value, offset, byteLength, limit - 1, -limit);
  }

  var i = byteLength - 1;
  var mul = 1;
  var sub = 0;
  this[offset + i] = value & 0xFF;

  while (--i >= 0 && (mul *= 0x100)) {
    if (value < 0 && sub === 0 && this[offset + i + 1] !== 0) {
      sub = 1;
    }

    this[offset + i] = (value / mul >> 0) - sub & 0xFF;
  }

  return offset + byteLength;
};

Buffer.prototype.writeInt8 = function writeInt8(value, offset, noAssert) {
  value = +value;
  offset = offset >>> 0;
  if (!noAssert) checkInt(this, value, offset, 1, 0x7f, -0x80);
  if (value < 0) value = 0xff + value + 1;
  this[offset] = value & 0xff;
  return offset + 1;
};

Buffer.prototype.writeInt16LE = function writeInt16LE(value, offset, noAssert) {
  value = +value;
  offset = offset >>> 0;
  if (!noAssert) checkInt(this, value, offset, 2, 0x7fff, -0x8000);
  this[offset] = value & 0xff;
  this[offset + 1] = value >>> 8;
  return offset + 2;
};

Buffer.prototype.writeInt16BE = function writeInt16BE(value, offset, noAssert) {
  value = +value;
  offset = offset >>> 0;
  if (!noAssert) checkInt(this, value, offset, 2, 0x7fff, -0x8000);
  this[offset] = value >>> 8;
  this[offset + 1] = value & 0xff;
  return offset + 2;
};

Buffer.prototype.writeInt32LE = function writeInt32LE(value, offset, noAssert) {
  value = +value;
  offset = offset >>> 0;
  if (!noAssert) checkInt(this, value, offset, 4, 0x7fffffff, -0x80000000);
  this[offset] = value & 0xff;
  this[offset + 1] = value >>> 8;
  this[offset + 2] = value >>> 16;
  this[offset + 3] = value >>> 24;
  return offset + 4;
};

Buffer.prototype.writeInt32BE = function writeInt32BE(value, offset, noAssert) {
  value = +value;
  offset = offset >>> 0;
  if (!noAssert) checkInt(this, value, offset, 4, 0x7fffffff, -0x80000000);
  if (value < 0) value = 0xffffffff + value + 1;
  this[offset] = value >>> 24;
  this[offset + 1] = value >>> 16;
  this[offset + 2] = value >>> 8;
  this[offset + 3] = value & 0xff;
  return offset + 4;
};

Buffer.prototype.writeBigInt64LE = defineBigIntMethod(function writeBigInt64LE(value) {
  var offset = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
  return wrtBigUInt64LE(this, value, offset, -BigInt('0x8000000000000000'), BigInt('0x7fffffffffffffff'));
});
Buffer.prototype.writeBigInt64BE = defineBigIntMethod(function writeBigInt64BE(value) {
  var offset = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
  return wrtBigUInt64BE(this, value, offset, -BigInt('0x8000000000000000'), BigInt('0x7fffffffffffffff'));
});

function checkIEEE754(buf, value, offset, ext, max, min) {
  if (offset + ext > buf.length) throw new RangeError('Index out of range');
  if (offset < 0) throw new RangeError('Index out of range');
}

function writeFloat(buf, value, offset, littleEndian, noAssert) {
  value = +value;
  offset = offset >>> 0;

  if (!noAssert) {
    checkIEEE754(buf, value, offset, 4, 3.4028234663852886e+38, -3.4028234663852886e+38);
  }

  ieee754.write(buf, value, offset, littleEndian, 23, 4);
  return offset + 4;
}

Buffer.prototype.writeFloatLE = function writeFloatLE(value, offset, noAssert) {
  return writeFloat(this, value, offset, true, noAssert);
};

Buffer.prototype.writeFloatBE = function writeFloatBE(value, offset, noAssert) {
  return writeFloat(this, value, offset, false, noAssert);
};

function writeDouble(buf, value, offset, littleEndian, noAssert) {
  value = +value;
  offset = offset >>> 0;

  if (!noAssert) {
    checkIEEE754(buf, value, offset, 8, 1.7976931348623157E+308, -1.7976931348623157E+308);
  }

  ieee754.write(buf, value, offset, littleEndian, 52, 8);
  return offset + 8;
}

Buffer.prototype.writeDoubleLE = function writeDoubleLE(value, offset, noAssert) {
  return writeDouble(this, value, offset, true, noAssert);
};

Buffer.prototype.writeDoubleBE = function writeDoubleBE(value, offset, noAssert) {
  return writeDouble(this, value, offset, false, noAssert);
}; // copy(targetBuffer, targetStart=0, sourceStart=0, sourceEnd=buffer.length)


Buffer.prototype.copy = function copy(target, targetStart, start, end) {
  if (!Buffer.isBuffer(target)) throw new TypeError('argument should be a Buffer');
  if (!start) start = 0;
  if (!end && end !== 0) end = this.length;
  if (targetStart >= target.length) targetStart = target.length;
  if (!targetStart) targetStart = 0;
  if (end > 0 && end < start) end = start; // Copy 0 bytes; we're done

  if (end === start) return 0;
  if (target.length === 0 || this.length === 0) return 0; // Fatal error conditions

  if (targetStart < 0) {
    throw new RangeError('targetStart out of bounds');
  }

  if (start < 0 || start >= this.length) throw new RangeError('Index out of range');
  if (end < 0) throw new RangeError('sourceEnd out of bounds'); // Are we oob?

  if (end > this.length) end = this.length;

  if (target.length - targetStart < end - start) {
    end = target.length - targetStart + start;
  }

  var len = end - start;

  if (this === target && typeof Uint8Array.prototype.copyWithin === 'function') {
    // Use built-in when available, missing from IE11
    this.copyWithin(targetStart, start, end);
  } else {
    Uint8Array.prototype.set.call(target, this.subarray(start, end), targetStart);
  }

  return len;
}; // Usage:
//    buffer.fill(number[, offset[, end]])
//    buffer.fill(buffer[, offset[, end]])
//    buffer.fill(string[, offset[, end]][, encoding])


Buffer.prototype.fill = function fill(val, start, end, encoding) {
  // Handle string cases:
  if (typeof val === 'string') {
    if (typeof start === 'string') {
      encoding = start;
      start = 0;
      end = this.length;
    } else if (typeof end === 'string') {
      encoding = end;
      end = this.length;
    }

    if (encoding !== undefined && typeof encoding !== 'string') {
      throw new TypeError('encoding must be a string');
    }

    if (typeof encoding === 'string' && !Buffer.isEncoding(encoding)) {
      throw new TypeError('Unknown encoding: ' + encoding);
    }

    if (val.length === 1) {
      var code = val.charCodeAt(0);

      if (encoding === 'utf8' && code < 128 || encoding === 'latin1') {
        // Fast path: If `val` fits into a single byte, use that numeric value.
        val = code;
      }
    }
  } else if (typeof val === 'number') {
    val = val & 255;
  } else if (typeof val === 'boolean') {
    val = Number(val);
  } // Invalid ranges are not set to a default, so can range check early.


  if (start < 0 || this.length < start || this.length < end) {
    throw new RangeError('Out of range index');
  }

  if (end <= start) {
    return this;
  }

  start = start >>> 0;
  end = end === undefined ? this.length : end >>> 0;
  if (!val) val = 0;
  var i;

  if (typeof val === 'number') {
    for (i = start; i < end; ++i) {
      this[i] = val;
    }
  } else {
    var bytes = Buffer.isBuffer(val) ? val : Buffer.from(val, encoding);
    var len = bytes.length;

    if (len === 0) {
      throw new TypeError('The value "' + val + '" is invalid for argument "value"');
    }

    for (i = 0; i < end - start; ++i) {
      this[i + start] = bytes[i % len];
    }
  }

  return this;
}; // CUSTOM ERRORS
// =============
// Simplified versions from Node, changed for Buffer-only usage


var errors = {};

function E(sym, getMessage, Base) {
  errors[sym] = /*#__PURE__*/function (_Base) {
    _inherits(NodeError, _Base);

    var _super = _createSuper(NodeError);

    function NodeError() {
      var _this;

      _classCallCheck(this, NodeError);

      _this = _super.call(this);
      Object.defineProperty(_assertThisInitialized(_this), 'message', {
        value: getMessage.apply(_assertThisInitialized(_this), arguments),
        writable: true,
        configurable: true
      }); // Add the error code to the name to include it in the stack trace.

      _this.name = "".concat(_this.name, " [").concat(sym, "]"); // Access the stack to generate the error message including the error code
      // from the name.

      _this.stack; // eslint-disable-line no-unused-expressions
      // Reset the name to the actual name.

      delete _this.name;
      return _this;
    }

    _createClass(NodeError, [{
      key: "code",
      get: function get() {
        return sym;
      },
      set: function set(value) {
        Object.defineProperty(this, 'code', {
          configurable: true,
          enumerable: true,
          value: value,
          writable: true
        });
      }
    }, {
      key: "toString",
      value: function toString() {
        return "".concat(this.name, " [").concat(sym, "]: ").concat(this.message);
      }
    }]);

    return NodeError;
  }(Base);
}

E('ERR_BUFFER_OUT_OF_BOUNDS', function (name) {
  if (name) {
    return "".concat(name, " is outside of buffer bounds");
  }

  return 'Attempt to access memory outside buffer bounds';
}, RangeError);
E('ERR_INVALID_ARG_TYPE', function (name, actual) {
  return "The \"".concat(name, "\" argument must be of type number. Received type ").concat(_typeof(actual));
}, TypeError);
E('ERR_OUT_OF_RANGE', function (str, range, input) {
  var msg = "The value of \"".concat(str, "\" is out of range.");
  var received = input;

  if (Number.isInteger(input) && Math.abs(input) > Math.pow(2, 32)) {
    received = addNumericalSeparator(String(input));
  } else if (typeof input === 'bigint') {
    received = String(input);

    if (input > Math.pow(BigInt(2), BigInt(32)) || input < -Math.pow(BigInt(2), BigInt(32))) {
      received = addNumericalSeparator(received);
    }

    received += 'n';
  }

  msg += " It must be ".concat(range, ". Received ").concat(received);
  return msg;
}, RangeError);

function addNumericalSeparator(val) {
  var res = '';
  var i = val.length;
  var start = val[0] === '-' ? 1 : 0;

  for (; i >= start + 4; i -= 3) {
    res = "_".concat(val.slice(i - 3, i)).concat(res);
  }

  return "".concat(val.slice(0, i)).concat(res);
} // CHECK FUNCTIONS
// ===============


function checkBounds(buf, offset, byteLength) {
  validateNumber(offset, 'offset');

  if (buf[offset] === undefined || buf[offset + byteLength] === undefined) {
    boundsError(offset, buf.length - (byteLength + 1));
  }
}

function checkIntBI(value, min, max, buf, offset, byteLength) {
  if (value > max || value < min) {
    var n = typeof min === 'bigint' ? 'n' : '';
    var range;

    if (byteLength > 3) {
      if (min === 0 || min === BigInt(0)) {
        range = ">= 0".concat(n, " and < 2").concat(n, " ** ").concat((byteLength + 1) * 8).concat(n);
      } else {
        range = ">= -(2".concat(n, " ** ").concat((byteLength + 1) * 8 - 1).concat(n, ") and < 2 ** ") + "".concat((byteLength + 1) * 8 - 1).concat(n);
      }
    } else {
      range = ">= ".concat(min).concat(n, " and <= ").concat(max).concat(n);
    }

    throw new errors.ERR_OUT_OF_RANGE('value', range, value);
  }

  checkBounds(buf, offset, byteLength);
}

function validateNumber(value, name) {
  if (typeof value !== 'number') {
    throw new errors.ERR_INVALID_ARG_TYPE(name, 'number', value);
  }
}

function boundsError(value, length, type) {
  if (Math.floor(value) !== value) {
    validateNumber(value, type);
    throw new errors.ERR_OUT_OF_RANGE(type || 'offset', 'an integer', value);
  }

  if (length < 0) {
    throw new errors.ERR_BUFFER_OUT_OF_BOUNDS();
  }

  throw new errors.ERR_OUT_OF_RANGE(type || 'offset', ">= ".concat(type ? 1 : 0, " and <= ").concat(length), value);
} // HELPER FUNCTIONS
// ================


var INVALID_BASE64_RE = /[^+/0-9A-Za-z-_]/g;

function base64clean(str) {
  // Node takes equal signs as end of the Base64 encoding
  str = str.split('=')[0]; // Node strips out invalid characters like \n and \t from the string, base64-js does not

  str = str.trim().replace(INVALID_BASE64_RE, ''); // Node converts strings with length < 2 to ''

  if (str.length < 2) return ''; // Node allows for non-padded base64 strings (missing trailing ===), base64-js does not

  while (str.length % 4 !== 0) {
    str = str + '=';
  }

  return str;
}

function utf8ToBytes(string, units) {
  units = units || Infinity;
  var codePoint;
  var length = string.length;
  var leadSurrogate = null;
  var bytes = [];

  for (var i = 0; i < length; ++i) {
    codePoint = string.charCodeAt(i); // is surrogate component

    if (codePoint > 0xD7FF && codePoint < 0xE000) {
      // last char was a lead
      if (!leadSurrogate) {
        // no lead yet
        if (codePoint > 0xDBFF) {
          // unexpected trail
          if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD);
          continue;
        } else if (i + 1 === length) {
          // unpaired lead
          if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD);
          continue;
        } // valid lead


        leadSurrogate = codePoint;
        continue;
      } // 2 leads in a row


      if (codePoint < 0xDC00) {
        if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD);
        leadSurrogate = codePoint;
        continue;
      } // valid surrogate pair


      codePoint = (leadSurrogate - 0xD800 << 10 | codePoint - 0xDC00) + 0x10000;
    } else if (leadSurrogate) {
      // valid bmp char, but last char was a lead
      if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD);
    }

    leadSurrogate = null; // encode utf8

    if (codePoint < 0x80) {
      if ((units -= 1) < 0) break;
      bytes.push(codePoint);
    } else if (codePoint < 0x800) {
      if ((units -= 2) < 0) break;
      bytes.push(codePoint >> 0x6 | 0xC0, codePoint & 0x3F | 0x80);
    } else if (codePoint < 0x10000) {
      if ((units -= 3) < 0) break;
      bytes.push(codePoint >> 0xC | 0xE0, codePoint >> 0x6 & 0x3F | 0x80, codePoint & 0x3F | 0x80);
    } else if (codePoint < 0x110000) {
      if ((units -= 4) < 0) break;
      bytes.push(codePoint >> 0x12 | 0xF0, codePoint >> 0xC & 0x3F | 0x80, codePoint >> 0x6 & 0x3F | 0x80, codePoint & 0x3F | 0x80);
    } else {
      throw new Error('Invalid code point');
    }
  }

  return bytes;
}

function asciiToBytes(str) {
  var byteArray = [];

  for (var i = 0; i < str.length; ++i) {
    // Node's code seems to be doing this and not & 0x7F..
    byteArray.push(str.charCodeAt(i) & 0xFF);
  }

  return byteArray;
}

function utf16leToBytes(str, units) {
  var c, hi, lo;
  var byteArray = [];

  for (var i = 0; i < str.length; ++i) {
    if ((units -= 2) < 0) break;
    c = str.charCodeAt(i);
    hi = c >> 8;
    lo = c % 256;
    byteArray.push(lo);
    byteArray.push(hi);
  }

  return byteArray;
}

function base64ToBytes(str) {
  return base64.toByteArray(base64clean(str));
}

function blitBuffer(src, dst, offset, length) {
  var i;

  for (i = 0; i < length; ++i) {
    if (i + offset >= dst.length || i >= src.length) break;
    dst[i + offset] = src[i];
  }

  return i;
} // ArrayBuffer or Uint8Array objects from other contexts (i.e. iframes) do not pass
// the `instanceof` check but they should be treated as of that type.
// See: https://github.com/feross/buffer/issues/166


function isInstance(obj, type) {
  return obj instanceof type || obj != null && obj.constructor != null && obj.constructor.name != null && obj.constructor.name === type.name;
}

function numberIsNaN(obj) {
  // For IE11 support
  return obj !== obj; // eslint-disable-line no-self-compare
} // Create lookup table for `toString('hex')`
// See: https://github.com/feross/buffer/issues/219


var hexSliceLookupTable = function () {
  var alphabet = '0123456789abcdef';
  var table = new Array(256);

  for (var i = 0; i < 16; ++i) {
    var i16 = i * 16;

    for (var j = 0; j < 16; ++j) {
      table[i16 + j] = alphabet[i] + alphabet[j];
    }
  }

  return table;
}(); // Return not function with Error if BigInt not supported


function defineBigIntMethod(fn) {
  return typeof BigInt === 'undefined' ? BufferBigIntNotDefined : fn;
}

function BufferBigIntNotDefined() {
  throw new Error('BigInt not supported');
}

/***/ }),

/***/ "./node_modules/_ieee754@1.2.1@ieee754/index.js":
/*!******************************************************!*\
  !*** ./node_modules/_ieee754@1.2.1@ieee754/index.js ***!
  \******************************************************/
/***/ ((__unused_webpack_module, exports) => {

/*! ieee754. BSD-3-Clause License. Feross Aboukhadijeh <https://feross.org/opensource> */
exports.read = function (buffer, offset, isLE, mLen, nBytes) {
  var e, m;
  var eLen = nBytes * 8 - mLen - 1;
  var eMax = (1 << eLen) - 1;
  var eBias = eMax >> 1;
  var nBits = -7;
  var i = isLE ? nBytes - 1 : 0;
  var d = isLE ? -1 : 1;
  var s = buffer[offset + i];
  i += d;
  e = s & (1 << -nBits) - 1;
  s >>= -nBits;
  nBits += eLen;

  for (; nBits > 0; e = e * 256 + buffer[offset + i], i += d, nBits -= 8) {}

  m = e & (1 << -nBits) - 1;
  e >>= -nBits;
  nBits += mLen;

  for (; nBits > 0; m = m * 256 + buffer[offset + i], i += d, nBits -= 8) {}

  if (e === 0) {
    e = 1 - eBias;
  } else if (e === eMax) {
    return m ? NaN : (s ? -1 : 1) * Infinity;
  } else {
    m = m + Math.pow(2, mLen);
    e = e - eBias;
  }

  return (s ? -1 : 1) * m * Math.pow(2, e - mLen);
};

exports.write = function (buffer, value, offset, isLE, mLen, nBytes) {
  var e, m, c;
  var eLen = nBytes * 8 - mLen - 1;
  var eMax = (1 << eLen) - 1;
  var eBias = eMax >> 1;
  var rt = mLen === 23 ? Math.pow(2, -24) - Math.pow(2, -77) : 0;
  var i = isLE ? 0 : nBytes - 1;
  var d = isLE ? 1 : -1;
  var s = value < 0 || value === 0 && 1 / value < 0 ? 1 : 0;
  value = Math.abs(value);

  if (isNaN(value) || value === Infinity) {
    m = isNaN(value) ? 1 : 0;
    e = eMax;
  } else {
    e = Math.floor(Math.log(value) / Math.LN2);

    if (value * (c = Math.pow(2, -e)) < 1) {
      e--;
      c *= 2;
    }

    if (e + eBias >= 1) {
      value += rt / c;
    } else {
      value += rt * Math.pow(2, 1 - eBias);
    }

    if (value * c >= 2) {
      e++;
      c /= 2;
    }

    if (e + eBias >= eMax) {
      m = 0;
      e = eMax;
    } else if (e + eBias >= 1) {
      m = (value * c - 1) * Math.pow(2, mLen);
      e = e + eBias;
    } else {
      m = value * Math.pow(2, eBias - 1) * Math.pow(2, mLen);
      e = 0;
    }
  }

  for (; mLen >= 8; buffer[offset + i] = m & 0xff, i += d, m /= 256, mLen -= 8) {}

  e = e << mLen | m;
  eLen += mLen;

  for (; eLen > 0; buffer[offset + i] = e & 0xff, i += d, e /= 256, eLen -= 8) {}

  buffer[offset + i - d] |= s * 128;
};

/***/ }),

/***/ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/CodeNode.js":
/*!*****************************************************************************!*\
  !*** ./node_modules/_source-list-map@2.0.1@source-list-map/lib/CodeNode.js ***!
  \*****************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/


function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var getNumberOfLines = __webpack_require__(/*! ./helpers */ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/helpers.js").getNumberOfLines;

var getUnfinishedLine = __webpack_require__(/*! ./helpers */ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/helpers.js").getUnfinishedLine;

var CodeNode = /*#__PURE__*/function () {
  function CodeNode(generatedCode) {
    _classCallCheck(this, CodeNode);

    this.generatedCode = generatedCode;
  }

  _createClass(CodeNode, [{
    key: "clone",
    value: function clone() {
      return new CodeNode(this.generatedCode);
    }
  }, {
    key: "getGeneratedCode",
    value: function getGeneratedCode() {
      return this.generatedCode;
    }
  }, {
    key: "getMappings",
    value: function getMappings(mappingsContext) {
      var lines = getNumberOfLines(this.generatedCode);
      var mapping = Array(lines + 1).join(";");

      if (lines > 0) {
        mappingsContext.unfinishedGeneratedLine = getUnfinishedLine(this.generatedCode);

        if (mappingsContext.unfinishedGeneratedLine > 0) {
          return mapping + "A";
        } else {
          return mapping;
        }
      } else {
        var prevUnfinished = mappingsContext.unfinishedGeneratedLine;
        mappingsContext.unfinishedGeneratedLine += getUnfinishedLine(this.generatedCode);

        if (prevUnfinished === 0 && mappingsContext.unfinishedGeneratedLine > 0) {
          return "A";
        } else {
          return "";
        }
      }
    }
  }, {
    key: "addGeneratedCode",
    value: function addGeneratedCode(generatedCode) {
      this.generatedCode += generatedCode;
    }
  }, {
    key: "mapGeneratedCode",
    value: function mapGeneratedCode(fn) {
      var generatedCode = fn(this.generatedCode);
      return new CodeNode(generatedCode);
    }
  }, {
    key: "getNormalizedNodes",
    value: function getNormalizedNodes() {
      return [this];
    }
  }, {
    key: "merge",
    value: function merge(otherNode) {
      if (otherNode instanceof CodeNode) {
        this.generatedCode += otherNode.generatedCode;
        return this;
      }

      return false;
    }
  }]);

  return CodeNode;
}();

module.exports = CodeNode;

/***/ }),

/***/ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/MappingsContext.js":
/*!************************************************************************************!*\
  !*** ./node_modules/_source-list-map@2.0.1@source-list-map/lib/MappingsContext.js ***!
  \************************************************************************************/
/***/ ((module) => {

"use strict";
/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/


function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it.return != null) it.return(); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var MappingsContext = /*#__PURE__*/function () {
  function MappingsContext() {
    _classCallCheck(this, MappingsContext);

    this.sourcesIndices = new Map();
    this.sourcesContent = new Map();
    this.hasSourceContent = false;
    this.currentOriginalLine = 1;
    this.currentSource = 0;
    this.unfinishedGeneratedLine = false;
  }

  _createClass(MappingsContext, [{
    key: "ensureSource",
    value: function ensureSource(source, originalSource) {
      var idx = this.sourcesIndices.get(source);

      if (typeof idx === "number") {
        return idx;
      }

      idx = this.sourcesIndices.size;
      this.sourcesIndices.set(source, idx);
      this.sourcesContent.set(source, originalSource);
      if (typeof originalSource === "string") this.hasSourceContent = true;
      return idx;
    }
  }, {
    key: "getArrays",
    value: function getArrays() {
      var sources = [];
      var sourcesContent = [];

      var _iterator = _createForOfIteratorHelper(this.sourcesContent),
          _step;

      try {
        for (_iterator.s(); !(_step = _iterator.n()).done;) {
          var pair = _step.value;
          sources.push(pair[0]);
          sourcesContent.push(pair[1]);
        }
      } catch (err) {
        _iterator.e(err);
      } finally {
        _iterator.f();
      }

      return {
        sources: sources,
        sourcesContent: sourcesContent
      };
    }
  }]);

  return MappingsContext;
}();

module.exports = MappingsContext;

/***/ }),

/***/ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/SingleLineNode.js":
/*!***********************************************************************************!*\
  !*** ./node_modules/_source-list-map@2.0.1@source-list-map/lib/SingleLineNode.js ***!
  \***********************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/


function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var base64VLQ = __webpack_require__(/*! ./base64-vlq */ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/base64-vlq.js");

var getNumberOfLines = __webpack_require__(/*! ./helpers */ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/helpers.js").getNumberOfLines;

var getUnfinishedLine = __webpack_require__(/*! ./helpers */ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/helpers.js").getUnfinishedLine;

var LINE_MAPPING = ";AAAA";

var SingleLineNode = /*#__PURE__*/function () {
  function SingleLineNode(generatedCode, source, originalSource, line) {
    _classCallCheck(this, SingleLineNode);

    this.generatedCode = generatedCode;
    this.originalSource = originalSource;
    this.source = source;
    this.line = line || 1;
    this._numberOfLines = getNumberOfLines(this.generatedCode);
    this._endsWithNewLine = generatedCode[generatedCode.length - 1] === "\n";
  }

  _createClass(SingleLineNode, [{
    key: "clone",
    value: function clone() {
      return new SingleLineNode(this.generatedCode, this.source, this.originalSource, this.line);
    }
  }, {
    key: "getGeneratedCode",
    value: function getGeneratedCode() {
      return this.generatedCode;
    }
  }, {
    key: "getMappings",
    value: function getMappings(mappingsContext) {
      if (!this.generatedCode) return "";
      var lines = this._numberOfLines;
      var sourceIdx = mappingsContext.ensureSource(this.source, this.originalSource);
      var mappings = "A"; // generated column 0

      if (mappingsContext.unfinishedGeneratedLine) mappings = "," + base64VLQ.encode(mappingsContext.unfinishedGeneratedLine);
      mappings += base64VLQ.encode(sourceIdx - mappingsContext.currentSource); // source index

      mappings += base64VLQ.encode(this.line - mappingsContext.currentOriginalLine); // original line index

      mappings += "A"; // original column 0

      mappingsContext.currentSource = sourceIdx;
      mappingsContext.currentOriginalLine = this.line;
      var unfinishedGeneratedLine = mappingsContext.unfinishedGeneratedLine = getUnfinishedLine(this.generatedCode);
      mappings += Array(lines).join(LINE_MAPPING);

      if (unfinishedGeneratedLine === 0) {
        mappings += ";";
      } else {
        if (lines !== 0) mappings += LINE_MAPPING;
      }

      return mappings;
    }
  }, {
    key: "getNormalizedNodes",
    value: function getNormalizedNodes() {
      return [this];
    }
  }, {
    key: "mapGeneratedCode",
    value: function mapGeneratedCode(fn) {
      var generatedCode = fn(this.generatedCode);
      return new SingleLineNode(generatedCode, this.source, this.originalSource, this.line);
    }
  }, {
    key: "merge",
    value: function merge(otherNode) {
      if (otherNode instanceof SingleLineNode) {
        return this.mergeSingleLineNode(otherNode);
      }

      return false;
    }
  }, {
    key: "mergeSingleLineNode",
    value: function mergeSingleLineNode(otherNode) {
      if (this.source === otherNode.source && this.originalSource === otherNode.originalSource) {
        if (this.line === otherNode.line) {
          this.generatedCode += otherNode.generatedCode;
          this._numberOfLines += otherNode._numberOfLines;
          this._endsWithNewLine = otherNode._endsWithNewLine;
          return this;
        } else if (this.line + 1 === otherNode.line && this._endsWithNewLine && this._numberOfLines === 1 && otherNode._numberOfLines <= 1) {
          return new SourceNode(this.generatedCode + otherNode.generatedCode, this.source, this.originalSource, this.line);
        }
      }

      return false;
    }
  }]);

  return SingleLineNode;
}();

module.exports = SingleLineNode;

var SourceNode = __webpack_require__(/*! ./SourceNode */ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/SourceNode.js"); // circular dependency

/***/ }),

/***/ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/SourceListMap.js":
/*!**********************************************************************************!*\
  !*** ./node_modules/_source-list-map@2.0.1@source-list-map/lib/SourceListMap.js ***!
  \**********************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/


function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var CodeNode = __webpack_require__(/*! ./CodeNode */ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/CodeNode.js");

var SourceNode = __webpack_require__(/*! ./SourceNode */ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/SourceNode.js");

var MappingsContext = __webpack_require__(/*! ./MappingsContext */ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/MappingsContext.js");

var getNumberOfLines = __webpack_require__(/*! ./helpers */ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/helpers.js").getNumberOfLines;

var SourceListMap = /*#__PURE__*/function () {
  function SourceListMap(generatedCode, source, originalSource) {
    _classCallCheck(this, SourceListMap);

    if (Array.isArray(generatedCode)) {
      this.children = generatedCode;
    } else {
      this.children = [];
      if (generatedCode || source) this.add(generatedCode, source, originalSource);
    }
  }

  _createClass(SourceListMap, [{
    key: "add",
    value: function add(generatedCode, source, originalSource) {
      if (typeof generatedCode === "string") {
        if (source) {
          this.children.push(new SourceNode(generatedCode, source, originalSource));
        } else if (this.children.length > 0 && this.children[this.children.length - 1] instanceof CodeNode) {
          this.children[this.children.length - 1].addGeneratedCode(generatedCode);
        } else {
          this.children.push(new CodeNode(generatedCode));
        }
      } else if (generatedCode.getMappings && generatedCode.getGeneratedCode) {
        this.children.push(generatedCode);
      } else if (generatedCode.children) {
        generatedCode.children.forEach(function (sln) {
          this.children.push(sln);
        }, this);
      } else {
        throw new Error("Invalid arguments to SourceListMap.protfotype.add: Expected string, Node or SourceListMap");
      }
    }
  }, {
    key: "preprend",
    value: function preprend(generatedCode, source, originalSource) {
      if (typeof generatedCode === "string") {
        if (source) {
          this.children.unshift(new SourceNode(generatedCode, source, originalSource));
        } else if (this.children.length > 0 && this.children[this.children.length - 1].preprendGeneratedCode) {
          this.children[this.children.length - 1].preprendGeneratedCode(generatedCode);
        } else {
          this.children.unshift(new CodeNode(generatedCode));
        }
      } else if (generatedCode.getMappings && generatedCode.getGeneratedCode) {
        this.children.unshift(generatedCode);
      } else if (generatedCode.children) {
        generatedCode.children.slice().reverse().forEach(function (sln) {
          this.children.unshift(sln);
        }, this);
      } else {
        throw new Error("Invalid arguments to SourceListMap.protfotype.prerend: Expected string, Node or SourceListMap");
      }
    }
  }, {
    key: "mapGeneratedCode",
    value: function mapGeneratedCode(fn) {
      var normalizedNodes = [];
      this.children.forEach(function (sln) {
        sln.getNormalizedNodes().forEach(function (newNode) {
          normalizedNodes.push(newNode);
        });
      });
      var optimizedNodes = [];
      normalizedNodes.forEach(function (sln) {
        sln = sln.mapGeneratedCode(fn);

        if (optimizedNodes.length === 0) {
          optimizedNodes.push(sln);
        } else {
          var last = optimizedNodes[optimizedNodes.length - 1];
          var mergedNode = last.merge(sln);

          if (mergedNode) {
            optimizedNodes[optimizedNodes.length - 1] = mergedNode;
          } else {
            optimizedNodes.push(sln);
          }
        }
      });
      return new SourceListMap(optimizedNodes);
    }
  }, {
    key: "toString",
    value: function toString() {
      return this.children.map(function (sln) {
        return sln.getGeneratedCode();
      }).join("");
    }
  }, {
    key: "toStringWithSourceMap",
    value: function toStringWithSourceMap(options) {
      var mappingsContext = new MappingsContext();
      var source = this.children.map(function (sln) {
        return sln.getGeneratedCode();
      }).join("");
      var mappings = this.children.map(function (sln) {
        return sln.getMappings(mappingsContext);
      }).join("");
      var arrays = mappingsContext.getArrays();
      return {
        source: source,
        map: {
          version: 3,
          file: options && options.file,
          sources: arrays.sources,
          sourcesContent: mappingsContext.hasSourceContent ? arrays.sourcesContent : undefined,
          mappings: mappings
        }
      };
    }
  }]);

  return SourceListMap;
}();

module.exports = SourceListMap;

/***/ }),

/***/ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/SourceNode.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/_source-list-map@2.0.1@source-list-map/lib/SourceNode.js ***!
  \*******************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/


function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var base64VLQ = __webpack_require__(/*! ./base64-vlq */ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/base64-vlq.js");

var getNumberOfLines = __webpack_require__(/*! ./helpers */ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/helpers.js").getNumberOfLines;

var getUnfinishedLine = __webpack_require__(/*! ./helpers */ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/helpers.js").getUnfinishedLine;

var LINE_MAPPING = ";AACA";

var SourceNode = /*#__PURE__*/function () {
  function SourceNode(generatedCode, source, originalSource, startingLine) {
    _classCallCheck(this, SourceNode);

    this.generatedCode = generatedCode;
    this.originalSource = originalSource;
    this.source = source;
    this.startingLine = startingLine || 1;
    this._numberOfLines = getNumberOfLines(this.generatedCode);
    this._endsWithNewLine = generatedCode[generatedCode.length - 1] === "\n";
  }

  _createClass(SourceNode, [{
    key: "clone",
    value: function clone() {
      return new SourceNode(this.generatedCode, this.source, this.originalSource, this.startingLine);
    }
  }, {
    key: "getGeneratedCode",
    value: function getGeneratedCode() {
      return this.generatedCode;
    }
  }, {
    key: "addGeneratedCode",
    value: function addGeneratedCode(code) {
      this.generatedCode += code;
      this._numberOfLines += getNumberOfLines(code);
      this._endsWithNewLine = code[code.length - 1] === "\n";
    }
  }, {
    key: "getMappings",
    value: function getMappings(mappingsContext) {
      if (!this.generatedCode) return "";
      var lines = this._numberOfLines;
      var sourceIdx = mappingsContext.ensureSource(this.source, this.originalSource);
      var mappings = "A"; // generated column 0

      if (mappingsContext.unfinishedGeneratedLine) mappings = "," + base64VLQ.encode(mappingsContext.unfinishedGeneratedLine);
      mappings += base64VLQ.encode(sourceIdx - mappingsContext.currentSource); // source index

      mappings += base64VLQ.encode(this.startingLine - mappingsContext.currentOriginalLine); // original line index

      mappings += "A"; // original column 0

      mappingsContext.currentSource = sourceIdx;
      mappingsContext.currentOriginalLine = this.startingLine + lines - 1;
      var unfinishedGeneratedLine = mappingsContext.unfinishedGeneratedLine = getUnfinishedLine(this.generatedCode);
      mappings += Array(lines).join(LINE_MAPPING);

      if (unfinishedGeneratedLine === 0) {
        mappings += ";";
      } else {
        if (lines !== 0) {
          mappings += LINE_MAPPING;
        }

        mappingsContext.currentOriginalLine++;
      }

      return mappings;
    }
  }, {
    key: "mapGeneratedCode",
    value: function mapGeneratedCode(fn) {
      throw new Error("Cannot map generated code on a SourceMap. Normalize to SingleLineNode first.");
    }
  }, {
    key: "getNormalizedNodes",
    value: function getNormalizedNodes() {
      var results = [];
      var currentLine = this.startingLine;
      var generatedCode = this.generatedCode;
      var index = 0;
      var indexEnd = generatedCode.length;

      while (index < indexEnd) {
        // get one generated line
        var nextLine = generatedCode.indexOf("\n", index) + 1;
        if (nextLine === 0) nextLine = indexEnd;
        var lineGenerated = generatedCode.substr(index, nextLine - index);
        results.push(new SingleLineNode(lineGenerated, this.source, this.originalSource, currentLine)); // move cursors

        index = nextLine;
        currentLine++;
      }

      return results;
    }
  }, {
    key: "merge",
    value: function merge(otherNode) {
      if (otherNode instanceof SourceNode) {
        return this.mergeSourceNode(otherNode);
      } else if (otherNode instanceof SingleLineNode) {
        return this.mergeSingleLineNode(otherNode);
      }

      return false;
    }
  }, {
    key: "mergeSourceNode",
    value: function mergeSourceNode(otherNode) {
      if (this.source === otherNode.source && this._endsWithNewLine && this.startingLine + this._numberOfLines === otherNode.startingLine) {
        this.generatedCode += otherNode.generatedCode;
        this._numberOfLines += otherNode._numberOfLines;
        this._endsWithNewLine = otherNode._endsWithNewLine;
        return this;
      }

      return false;
    }
  }, {
    key: "mergeSingleLineNode",
    value: function mergeSingleLineNode(otherNode) {
      if (this.source === otherNode.source && this._endsWithNewLine && this.startingLine + this._numberOfLines === otherNode.line && otherNode._numberOfLines <= 1) {
        this.addSingleLineNode(otherNode);
        return this;
      }

      return false;
    }
  }, {
    key: "addSingleLineNode",
    value: function addSingleLineNode(otherNode) {
      this.generatedCode += otherNode.generatedCode;
      this._numberOfLines += otherNode._numberOfLines;
      this._endsWithNewLine = otherNode._endsWithNewLine;
    }
  }]);

  return SourceNode;
}();

module.exports = SourceNode;

var SingleLineNode = __webpack_require__(/*! ./SingleLineNode */ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/SingleLineNode.js"); // circular dependency

/***/ }),

/***/ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/base64-vlq.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/_source-list-map@2.0.1@source-list-map/lib/base64-vlq.js ***!
  \*******************************************************************************/
/***/ ((__unused_webpack_module, exports) => {

/* -*- Mode: js; js-indent-level: 2; -*- */

/*
 * Copyright 2011 Mozilla Foundation and contributors
 * Licensed under the New BSD license. See LICENSE or:
 * http://opensource.org/licenses/BSD-3-Clause
 *
 * Based on the Base 64 VLQ implementation in Closure Compiler:
 * https://code.google.com/p/closure-compiler/source/browse/trunk/src/com/google/debugging/sourcemap/Base64VLQ.java
 *
 * Copyright 2011 The Closure Compiler Authors. All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the following
 *    disclaimer in the documentation and/or other materials provided
 *    with the distribution.
 *  * Neither the name of Google Inc. nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*eslint no-bitwise:0,quotes:0,global-strict:0*/
var charToIntMap = {};
var intToCharMap = {};
'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'.split('').forEach(function (ch, index) {
  charToIntMap[ch] = index;
  intToCharMap[index] = ch;
});
var base64 = {};
/**
 * Encode an integer in the range of 0 to 63 to a single base 64 digit.
 */

base64.encode = function base64_encode(aNumber) {
  if (aNumber in intToCharMap) {
    return intToCharMap[aNumber];
  }

  throw new TypeError("Must be between 0 and 63: " + aNumber);
};
/**
 * Decode a single base 64 digit to an integer.
 */


base64.decode = function base64_decode(aChar) {
  if (aChar in charToIntMap) {
    return charToIntMap[aChar];
  }

  throw new TypeError("Not a valid base 64 digit: " + aChar);
}; // A single base 64 digit can contain 6 bits of data. For the base 64 variable
// length quantities we use in the source map spec, the first bit is the sign,
// the next four bits are the actual value, and the 6th bit is the
// continuation bit. The continuation bit tells us whether there are more
// digits in this value following this digit.
//
//   Continuation
//   |    Sign
//   |    |
//   V    V
//   101011


var VLQ_BASE_SHIFT = 5; // binary: 100000

var VLQ_BASE = 1 << VLQ_BASE_SHIFT; // binary: 011111

var VLQ_BASE_MASK = VLQ_BASE - 1; // binary: 100000

var VLQ_CONTINUATION_BIT = VLQ_BASE;
/**
 * Converts from a two-complement value to a value where the sign bit is
 * placed in the least significant bit.  For example, as decimals:
 *   1 becomes 2 (10 binary), -1 becomes 3 (11 binary)
 *   2 becomes 4 (100 binary), -2 becomes 5 (101 binary)
 */

function toVLQSigned(aValue) {
  return aValue < 0 ? (-aValue << 1) + 1 : (aValue << 1) + 0;
}
/**
 * Converts to a two-complement value from a value where the sign bit is
 * placed in the least significant bit.  For example, as decimals:
 *   2 (10 binary) becomes 1, 3 (11 binary) becomes -1
 *   4 (100 binary) becomes 2, 5 (101 binary) becomes -2
 */


function fromVLQSigned(aValue) {
  var isNegative = (aValue & 1) === 1;
  var shifted = aValue >> 1;
  return isNegative ? -shifted : shifted;
}
/**
 * Returns the base 64 VLQ encoded value.
 */


exports.encode = function base64VLQ_encode(aValue) {
  var encoded = "";
  var digit;
  var vlq = toVLQSigned(aValue);

  do {
    digit = vlq & VLQ_BASE_MASK;
    vlq >>>= VLQ_BASE_SHIFT;

    if (vlq > 0) {
      // There are still more digits in this value, so we must make sure the
      // continuation bit is marked.
      digit |= VLQ_CONTINUATION_BIT;
    }

    encoded += base64.encode(digit);
  } while (vlq > 0);

  return encoded;
};
/**
 * Decodes the next base 64 VLQ value from the given string and returns the
 * value and the rest of the string via the out parameter.
 */


exports.decode = function base64VLQ_decode(aStr, aOutParam) {
  var i = 0;
  var strLen = aStr.length;
  var result = 0;
  var shift = 0;
  var continuation, digit;

  do {
    if (i >= strLen) {
      throw new Error("Expected more digits in base 64 VLQ value.");
    }

    digit = base64.decode(aStr.charAt(i++));
    continuation = !!(digit & VLQ_CONTINUATION_BIT);
    digit &= VLQ_BASE_MASK;
    result = result + (digit << shift);
    shift += VLQ_BASE_SHIFT;
  } while (continuation);

  aOutParam.value = fromVLQSigned(result);
  aOutParam.rest = aStr.slice(i);
};

/***/ }),

/***/ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/fromStringWithSourceMap.js":
/*!********************************************************************************************!*\
  !*** ./node_modules/_source-list-map@2.0.1@source-list-map/lib/fromStringWithSourceMap.js ***!
  \********************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/


var base64VLQ = __webpack_require__(/*! ./base64-vlq */ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/base64-vlq.js");

var SourceNode = __webpack_require__(/*! ./SourceNode */ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/SourceNode.js");

var CodeNode = __webpack_require__(/*! ./CodeNode */ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/CodeNode.js");

var SourceListMap = __webpack_require__(/*! ./SourceListMap */ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/SourceListMap.js");

module.exports = function fromStringWithSourceMap(code, map) {
  var sources = map.sources;
  var sourcesContent = map.sourcesContent;
  var mappings = map.mappings.split(";");
  var lines = code.split("\n");
  var nodes = [];
  var currentNode = null;
  var currentLine = 1;
  var currentSourceIdx = 0;
  var currentSourceNodeLine;

  function addCode(generatedCode) {
    if (currentNode && currentNode instanceof CodeNode) {
      currentNode.addGeneratedCode(generatedCode);
    } else if (currentNode && currentNode instanceof SourceNode && !generatedCode.trim()) {
      currentNode.addGeneratedCode(generatedCode);
      currentSourceNodeLine++;
    } else {
      currentNode = new CodeNode(generatedCode);
      nodes.push(currentNode);
    }
  }

  function addSource(generatedCode, source, originalSource, linePosition) {
    if (currentNode && currentNode instanceof SourceNode && currentNode.source === source && currentSourceNodeLine === linePosition) {
      currentNode.addGeneratedCode(generatedCode);
      currentSourceNodeLine++;
    } else {
      currentNode = new SourceNode(generatedCode, source, originalSource, linePosition);
      currentSourceNodeLine = linePosition + 1;
      nodes.push(currentNode);
    }
  }

  mappings.forEach(function (mapping, idx) {
    var line = lines[idx];
    if (typeof line === 'undefined') return;
    if (idx !== lines.length - 1) line += "\n";
    if (!mapping) return addCode(line);
    mapping = {
      value: 0,
      rest: mapping
    };
    var lineAdded = false;

    while (mapping.rest) {
      lineAdded = processMapping(mapping, line, lineAdded) || lineAdded;
    }

    if (!lineAdded) addCode(line);
  });

  if (mappings.length < lines.length) {
    var idx = mappings.length;

    while (!lines[idx].trim() && idx < lines.length - 1) {
      addCode(lines[idx] + "\n");
      idx++;
    }

    addCode(lines.slice(idx).join("\n"));
  }

  return new SourceListMap(nodes);

  function processMapping(mapping, line, ignore) {
    if (mapping.rest && mapping.rest[0] !== ",") {
      base64VLQ.decode(mapping.rest, mapping);
    }

    if (!mapping.rest) return false;

    if (mapping.rest[0] === ",") {
      mapping.rest = mapping.rest.substr(1);
      return false;
    }

    base64VLQ.decode(mapping.rest, mapping);
    var sourceIdx = mapping.value + currentSourceIdx;
    currentSourceIdx = sourceIdx;
    var linePosition;

    if (mapping.rest && mapping.rest[0] !== ",") {
      base64VLQ.decode(mapping.rest, mapping);
      linePosition = mapping.value + currentLine;
      currentLine = linePosition;
    } else {
      linePosition = currentLine;
    }

    if (mapping.rest) {
      var next = mapping.rest.indexOf(",");
      mapping.rest = next === -1 ? "" : mapping.rest.substr(next);
    }

    if (!ignore) {
      addSource(line, sources ? sources[sourceIdx] : null, sourcesContent ? sourcesContent[sourceIdx] : null, linePosition);
      return true;
    }
  }
};

/***/ }),

/***/ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/helpers.js":
/*!****************************************************************************!*\
  !*** ./node_modules/_source-list-map@2.0.1@source-list-map/lib/helpers.js ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, exports) => {

"use strict";
/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/


exports.getNumberOfLines = function getNumberOfLines(str) {
  var nr = -1;
  var idx = -1;

  do {
    nr++;
    idx = str.indexOf("\n", idx + 1);
  } while (idx >= 0);

  return nr;
};

exports.getUnfinishedLine = function getUnfinishedLine(str) {
  var idx = str.lastIndexOf("\n");
  if (idx === -1) return str.length;else return str.length - idx - 1;
};

/***/ }),

/***/ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/index.js":
/*!**************************************************************************!*\
  !*** ./node_modules/_source-list-map@2.0.1@source-list-map/lib/index.js ***!
  \**************************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

exports.SourceListMap = __webpack_require__(/*! ./SourceListMap */ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/SourceListMap.js");
exports.SourceNode = __webpack_require__(/*! ./SourceNode */ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/SourceNode.js");
exports.SingleLineNode = __webpack_require__(/*! ./SingleLineNode */ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/SingleLineNode.js");
exports.CodeNode = __webpack_require__(/*! ./CodeNode */ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/CodeNode.js");
exports.MappingsContext = __webpack_require__(/*! ./MappingsContext */ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/MappingsContext.js");
exports.fromStringWithSourceMap = __webpack_require__(/*! ./fromStringWithSourceMap */ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/fromStringWithSourceMap.js");

/***/ }),

/***/ "./node_modules/_source-map@0.6.1@source-map/lib/array-set.js":
/*!********************************************************************!*\
  !*** ./node_modules/_source-map@0.6.1@source-map/lib/array-set.js ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

/* -*- Mode: js; js-indent-level: 2; -*- */

/*
 * Copyright 2011 Mozilla Foundation and contributors
 * Licensed under the New BSD license. See LICENSE or:
 * http://opensource.org/licenses/BSD-3-Clause
 */
var util = __webpack_require__(/*! ./util */ "./node_modules/_source-map@0.6.1@source-map/lib/util.js");

var has = Object.prototype.hasOwnProperty;
var hasNativeMap = typeof Map !== "undefined";
/**
 * A data structure which is a combination of an array and a set. Adding a new
 * member is O(1), testing for membership is O(1), and finding the index of an
 * element is O(1). Removing elements from the set is not supported. Only
 * strings are supported for membership.
 */

function ArraySet() {
  this._array = [];
  this._set = hasNativeMap ? new Map() : Object.create(null);
}
/**
 * Static method for creating ArraySet instances from an existing array.
 */


ArraySet.fromArray = function ArraySet_fromArray(aArray, aAllowDuplicates) {
  var set = new ArraySet();

  for (var i = 0, len = aArray.length; i < len; i++) {
    set.add(aArray[i], aAllowDuplicates);
  }

  return set;
};
/**
 * Return how many unique items are in this ArraySet. If duplicates have been
 * added, than those do not count towards the size.
 *
 * @returns Number
 */


ArraySet.prototype.size = function ArraySet_size() {
  return hasNativeMap ? this._set.size : Object.getOwnPropertyNames(this._set).length;
};
/**
 * Add the given string to this set.
 *
 * @param String aStr
 */


ArraySet.prototype.add = function ArraySet_add(aStr, aAllowDuplicates) {
  var sStr = hasNativeMap ? aStr : util.toSetString(aStr);
  var isDuplicate = hasNativeMap ? this.has(aStr) : has.call(this._set, sStr);
  var idx = this._array.length;

  if (!isDuplicate || aAllowDuplicates) {
    this._array.push(aStr);
  }

  if (!isDuplicate) {
    if (hasNativeMap) {
      this._set.set(aStr, idx);
    } else {
      this._set[sStr] = idx;
    }
  }
};
/**
 * Is the given string a member of this set?
 *
 * @param String aStr
 */


ArraySet.prototype.has = function ArraySet_has(aStr) {
  if (hasNativeMap) {
    return this._set.has(aStr);
  } else {
    var sStr = util.toSetString(aStr);
    return has.call(this._set, sStr);
  }
};
/**
 * What is the index of the given string in the array?
 *
 * @param String aStr
 */


ArraySet.prototype.indexOf = function ArraySet_indexOf(aStr) {
  if (hasNativeMap) {
    var idx = this._set.get(aStr);

    if (idx >= 0) {
      return idx;
    }
  } else {
    var sStr = util.toSetString(aStr);

    if (has.call(this._set, sStr)) {
      return this._set[sStr];
    }
  }

  throw new Error('"' + aStr + '" is not in the set.');
};
/**
 * What is the element at the given index?
 *
 * @param Number aIdx
 */


ArraySet.prototype.at = function ArraySet_at(aIdx) {
  if (aIdx >= 0 && aIdx < this._array.length) {
    return this._array[aIdx];
  }

  throw new Error('No element indexed by ' + aIdx);
};
/**
 * Returns the array representation of this set (which has the proper indices
 * indicated by indexOf). Note that this is a copy of the internal array used
 * for storing the members so that no one can mess with internal state.
 */


ArraySet.prototype.toArray = function ArraySet_toArray() {
  return this._array.slice();
};

exports.ArraySet = ArraySet;

/***/ }),

/***/ "./node_modules/_source-map@0.6.1@source-map/lib/base64-vlq.js":
/*!*********************************************************************!*\
  !*** ./node_modules/_source-map@0.6.1@source-map/lib/base64-vlq.js ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

/* -*- Mode: js; js-indent-level: 2; -*- */

/*
 * Copyright 2011 Mozilla Foundation and contributors
 * Licensed under the New BSD license. See LICENSE or:
 * http://opensource.org/licenses/BSD-3-Clause
 *
 * Based on the Base 64 VLQ implementation in Closure Compiler:
 * https://code.google.com/p/closure-compiler/source/browse/trunk/src/com/google/debugging/sourcemap/Base64VLQ.java
 *
 * Copyright 2011 The Closure Compiler Authors. All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the following
 *    disclaimer in the documentation and/or other materials provided
 *    with the distribution.
 *  * Neither the name of Google Inc. nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
var base64 = __webpack_require__(/*! ./base64 */ "./node_modules/_source-map@0.6.1@source-map/lib/base64.js"); // A single base 64 digit can contain 6 bits of data. For the base 64 variable
// length quantities we use in the source map spec, the first bit is the sign,
// the next four bits are the actual value, and the 6th bit is the
// continuation bit. The continuation bit tells us whether there are more
// digits in this value following this digit.
//
//   Continuation
//   |    Sign
//   |    |
//   V    V
//   101011


var VLQ_BASE_SHIFT = 5; // binary: 100000

var VLQ_BASE = 1 << VLQ_BASE_SHIFT; // binary: 011111

var VLQ_BASE_MASK = VLQ_BASE - 1; // binary: 100000

var VLQ_CONTINUATION_BIT = VLQ_BASE;
/**
 * Converts from a two-complement value to a value where the sign bit is
 * placed in the least significant bit.  For example, as decimals:
 *   1 becomes 2 (10 binary), -1 becomes 3 (11 binary)
 *   2 becomes 4 (100 binary), -2 becomes 5 (101 binary)
 */

function toVLQSigned(aValue) {
  return aValue < 0 ? (-aValue << 1) + 1 : (aValue << 1) + 0;
}
/**
 * Converts to a two-complement value from a value where the sign bit is
 * placed in the least significant bit.  For example, as decimals:
 *   2 (10 binary) becomes 1, 3 (11 binary) becomes -1
 *   4 (100 binary) becomes 2, 5 (101 binary) becomes -2
 */


function fromVLQSigned(aValue) {
  var isNegative = (aValue & 1) === 1;
  var shifted = aValue >> 1;
  return isNegative ? -shifted : shifted;
}
/**
 * Returns the base 64 VLQ encoded value.
 */


exports.encode = function base64VLQ_encode(aValue) {
  var encoded = "";
  var digit;
  var vlq = toVLQSigned(aValue);

  do {
    digit = vlq & VLQ_BASE_MASK;
    vlq >>>= VLQ_BASE_SHIFT;

    if (vlq > 0) {
      // There are still more digits in this value, so we must make sure the
      // continuation bit is marked.
      digit |= VLQ_CONTINUATION_BIT;
    }

    encoded += base64.encode(digit);
  } while (vlq > 0);

  return encoded;
};
/**
 * Decodes the next base 64 VLQ value from the given string and returns the
 * value and the rest of the string via the out parameter.
 */


exports.decode = function base64VLQ_decode(aStr, aIndex, aOutParam) {
  var strLen = aStr.length;
  var result = 0;
  var shift = 0;
  var continuation, digit;

  do {
    if (aIndex >= strLen) {
      throw new Error("Expected more digits in base 64 VLQ value.");
    }

    digit = base64.decode(aStr.charCodeAt(aIndex++));

    if (digit === -1) {
      throw new Error("Invalid base64 digit: " + aStr.charAt(aIndex - 1));
    }

    continuation = !!(digit & VLQ_CONTINUATION_BIT);
    digit &= VLQ_BASE_MASK;
    result = result + (digit << shift);
    shift += VLQ_BASE_SHIFT;
  } while (continuation);

  aOutParam.value = fromVLQSigned(result);
  aOutParam.rest = aIndex;
};

/***/ }),

/***/ "./node_modules/_source-map@0.6.1@source-map/lib/base64.js":
/*!*****************************************************************!*\
  !*** ./node_modules/_source-map@0.6.1@source-map/lib/base64.js ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, exports) => {

/* -*- Mode: js; js-indent-level: 2; -*- */

/*
 * Copyright 2011 Mozilla Foundation and contributors
 * Licensed under the New BSD license. See LICENSE or:
 * http://opensource.org/licenses/BSD-3-Clause
 */
var intToCharMap = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'.split('');
/**
 * Encode an integer in the range of 0 to 63 to a single base 64 digit.
 */

exports.encode = function (number) {
  if (0 <= number && number < intToCharMap.length) {
    return intToCharMap[number];
  }

  throw new TypeError("Must be between 0 and 63: " + number);
};
/**
 * Decode a single base 64 character code digit to an integer. Returns -1 on
 * failure.
 */


exports.decode = function (charCode) {
  var bigA = 65; // 'A'

  var bigZ = 90; // 'Z'

  var littleA = 97; // 'a'

  var littleZ = 122; // 'z'

  var zero = 48; // '0'

  var nine = 57; // '9'

  var plus = 43; // '+'

  var slash = 47; // '/'

  var littleOffset = 26;
  var numberOffset = 52; // 0 - 25: ABCDEFGHIJKLMNOPQRSTUVWXYZ

  if (bigA <= charCode && charCode <= bigZ) {
    return charCode - bigA;
  } // 26 - 51: abcdefghijklmnopqrstuvwxyz


  if (littleA <= charCode && charCode <= littleZ) {
    return charCode - littleA + littleOffset;
  } // 52 - 61: 0123456789


  if (zero <= charCode && charCode <= nine) {
    return charCode - zero + numberOffset;
  } // 62: +


  if (charCode == plus) {
    return 62;
  } // 63: /


  if (charCode == slash) {
    return 63;
  } // Invalid base64 digit.


  return -1;
};

/***/ }),

/***/ "./node_modules/_source-map@0.6.1@source-map/lib/binary-search.js":
/*!************************************************************************!*\
  !*** ./node_modules/_source-map@0.6.1@source-map/lib/binary-search.js ***!
  \************************************************************************/
/***/ ((__unused_webpack_module, exports) => {

/* -*- Mode: js; js-indent-level: 2; -*- */

/*
 * Copyright 2011 Mozilla Foundation and contributors
 * Licensed under the New BSD license. See LICENSE or:
 * http://opensource.org/licenses/BSD-3-Clause
 */
exports.GREATEST_LOWER_BOUND = 1;
exports.LEAST_UPPER_BOUND = 2;
/**
 * Recursive implementation of binary search.
 *
 * @param aLow Indices here and lower do not contain the needle.
 * @param aHigh Indices here and higher do not contain the needle.
 * @param aNeedle The element being searched for.
 * @param aHaystack The non-empty array being searched.
 * @param aCompare Function which takes two elements and returns -1, 0, or 1.
 * @param aBias Either 'binarySearch.GREATEST_LOWER_BOUND' or
 *     'binarySearch.LEAST_UPPER_BOUND'. Specifies whether to return the
 *     closest element that is smaller than or greater than the one we are
 *     searching for, respectively, if the exact element cannot be found.
 */

function recursiveSearch(aLow, aHigh, aNeedle, aHaystack, aCompare, aBias) {
  // This function terminates when one of the following is true:
  //
  //   1. We find the exact element we are looking for.
  //
  //   2. We did not find the exact element, but we can return the index of
  //      the next-closest element.
  //
  //   3. We did not find the exact element, and there is no next-closest
  //      element than the one we are searching for, so we return -1.
  var mid = Math.floor((aHigh - aLow) / 2) + aLow;
  var cmp = aCompare(aNeedle, aHaystack[mid], true);

  if (cmp === 0) {
    // Found the element we are looking for.
    return mid;
  } else if (cmp > 0) {
    // Our needle is greater than aHaystack[mid].
    if (aHigh - mid > 1) {
      // The element is in the upper half.
      return recursiveSearch(mid, aHigh, aNeedle, aHaystack, aCompare, aBias);
    } // The exact needle element was not found in this haystack. Determine if
    // we are in termination case (3) or (2) and return the appropriate thing.


    if (aBias == exports.LEAST_UPPER_BOUND) {
      return aHigh < aHaystack.length ? aHigh : -1;
    } else {
      return mid;
    }
  } else {
    // Our needle is less than aHaystack[mid].
    if (mid - aLow > 1) {
      // The element is in the lower half.
      return recursiveSearch(aLow, mid, aNeedle, aHaystack, aCompare, aBias);
    } // we are in termination case (3) or (2) and return the appropriate thing.


    if (aBias == exports.LEAST_UPPER_BOUND) {
      return mid;
    } else {
      return aLow < 0 ? -1 : aLow;
    }
  }
}
/**
 * This is an implementation of binary search which will always try and return
 * the index of the closest element if there is no exact hit. This is because
 * mappings between original and generated line/col pairs are single points,
 * and there is an implicit region between each of them, so a miss just means
 * that you aren't on the very start of a region.
 *
 * @param aNeedle The element you are looking for.
 * @param aHaystack The array that is being searched.
 * @param aCompare A function which takes the needle and an element in the
 *     array and returns -1, 0, or 1 depending on whether the needle is less
 *     than, equal to, or greater than the element, respectively.
 * @param aBias Either 'binarySearch.GREATEST_LOWER_BOUND' or
 *     'binarySearch.LEAST_UPPER_BOUND'. Specifies whether to return the
 *     closest element that is smaller than or greater than the one we are
 *     searching for, respectively, if the exact element cannot be found.
 *     Defaults to 'binarySearch.GREATEST_LOWER_BOUND'.
 */


exports.search = function search(aNeedle, aHaystack, aCompare, aBias) {
  if (aHaystack.length === 0) {
    return -1;
  }

  var index = recursiveSearch(-1, aHaystack.length, aNeedle, aHaystack, aCompare, aBias || exports.GREATEST_LOWER_BOUND);

  if (index < 0) {
    return -1;
  } // We have found either the exact element, or the next-closest element than
  // the one we are searching for. However, there may be more than one such
  // element. Make sure we always return the smallest of these.


  while (index - 1 >= 0) {
    if (aCompare(aHaystack[index], aHaystack[index - 1], true) !== 0) {
      break;
    }

    --index;
  }

  return index;
};

/***/ }),

/***/ "./node_modules/_source-map@0.6.1@source-map/lib/mapping-list.js":
/*!***********************************************************************!*\
  !*** ./node_modules/_source-map@0.6.1@source-map/lib/mapping-list.js ***!
  \***********************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

/* -*- Mode: js; js-indent-level: 2; -*- */

/*
 * Copyright 2014 Mozilla Foundation and contributors
 * Licensed under the New BSD license. See LICENSE or:
 * http://opensource.org/licenses/BSD-3-Clause
 */
var util = __webpack_require__(/*! ./util */ "./node_modules/_source-map@0.6.1@source-map/lib/util.js");
/**
 * Determine whether mappingB is after mappingA with respect to generated
 * position.
 */


function generatedPositionAfter(mappingA, mappingB) {
  // Optimized for most common case
  var lineA = mappingA.generatedLine;
  var lineB = mappingB.generatedLine;
  var columnA = mappingA.generatedColumn;
  var columnB = mappingB.generatedColumn;
  return lineB > lineA || lineB == lineA && columnB >= columnA || util.compareByGeneratedPositionsInflated(mappingA, mappingB) <= 0;
}
/**
 * A data structure to provide a sorted view of accumulated mappings in a
 * performance conscious manner. It trades a neglibable overhead in general
 * case for a large speedup in case of mappings being added in order.
 */


function MappingList() {
  this._array = [];
  this._sorted = true; // Serves as infimum

  this._last = {
    generatedLine: -1,
    generatedColumn: 0
  };
}
/**
 * Iterate through internal items. This method takes the same arguments that
 * `Array.prototype.forEach` takes.
 *
 * NOTE: The order of the mappings is NOT guaranteed.
 */


MappingList.prototype.unsortedForEach = function MappingList_forEach(aCallback, aThisArg) {
  this._array.forEach(aCallback, aThisArg);
};
/**
 * Add the given source mapping.
 *
 * @param Object aMapping
 */


MappingList.prototype.add = function MappingList_add(aMapping) {
  if (generatedPositionAfter(this._last, aMapping)) {
    this._last = aMapping;

    this._array.push(aMapping);
  } else {
    this._sorted = false;

    this._array.push(aMapping);
  }
};
/**
 * Returns the flat, sorted array of mappings. The mappings are sorted by
 * generated position.
 *
 * WARNING: This method returns internal data without copying, for
 * performance. The return value must NOT be mutated, and should be treated as
 * an immutable borrow. If you want to take ownership, you must make your own
 * copy.
 */


MappingList.prototype.toArray = function MappingList_toArray() {
  if (!this._sorted) {
    this._array.sort(util.compareByGeneratedPositionsInflated);

    this._sorted = true;
  }

  return this._array;
};

exports.MappingList = MappingList;

/***/ }),

/***/ "./node_modules/_source-map@0.6.1@source-map/lib/quick-sort.js":
/*!*********************************************************************!*\
  !*** ./node_modules/_source-map@0.6.1@source-map/lib/quick-sort.js ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, exports) => {

/* -*- Mode: js; js-indent-level: 2; -*- */

/*
 * Copyright 2011 Mozilla Foundation and contributors
 * Licensed under the New BSD license. See LICENSE or:
 * http://opensource.org/licenses/BSD-3-Clause
 */
// It turns out that some (most?) JavaScript engines don't self-host
// `Array.prototype.sort`. This makes sense because C++ will likely remain
// faster than JS when doing raw CPU-intensive sorting. However, when using a
// custom comparator function, calling back and forth between the VM's C++ and
// JIT'd JS is rather slow *and* loses JIT type information, resulting in
// worse generated code for the comparator function than would be optimal. In
// fact, when sorting with a comparator, these costs outweigh the benefits of
// sorting in C++. By using our own JS-implemented Quick Sort (below), we get
// a ~3500ms mean speed-up in `bench/bench.html`.

/**
 * Swap the elements indexed by `x` and `y` in the array `ary`.
 *
 * @param {Array} ary
 *        The array.
 * @param {Number} x
 *        The index of the first item.
 * @param {Number} y
 *        The index of the second item.
 */
function swap(ary, x, y) {
  var temp = ary[x];
  ary[x] = ary[y];
  ary[y] = temp;
}
/**
 * Returns a random integer within the range `low .. high` inclusive.
 *
 * @param {Number} low
 *        The lower bound on the range.
 * @param {Number} high
 *        The upper bound on the range.
 */


function randomIntInRange(low, high) {
  return Math.round(low + Math.random() * (high - low));
}
/**
 * The Quick Sort algorithm.
 *
 * @param {Array} ary
 *        An array to sort.
 * @param {function} comparator
 *        Function to use to compare two items.
 * @param {Number} p
 *        Start index of the array
 * @param {Number} r
 *        End index of the array
 */


function doQuickSort(ary, comparator, p, r) {
  // If our lower bound is less than our upper bound, we (1) partition the
  // array into two pieces and (2) recurse on each half. If it is not, this is
  // the empty array and our base case.
  if (p < r) {
    // (1) Partitioning.
    //
    // The partitioning chooses a pivot between `p` and `r` and moves all
    // elements that are less than or equal to the pivot to the before it, and
    // all the elements that are greater than it after it. The effect is that
    // once partition is done, the pivot is in the exact place it will be when
    // the array is put in sorted order, and it will not need to be moved
    // again. This runs in O(n) time.
    // Always choose a random pivot so that an input array which is reverse
    // sorted does not cause O(n^2) running time.
    var pivotIndex = randomIntInRange(p, r);
    var i = p - 1;
    swap(ary, pivotIndex, r);
    var pivot = ary[r]; // Immediately after `j` is incremented in this loop, the following hold
    // true:
    //
    //   * Every element in `ary[p .. i]` is less than or equal to the pivot.
    //
    //   * Every element in `ary[i+1 .. j-1]` is greater than the pivot.

    for (var j = p; j < r; j++) {
      if (comparator(ary[j], pivot) <= 0) {
        i += 1;
        swap(ary, i, j);
      }
    }

    swap(ary, i + 1, j);
    var q = i + 1; // (2) Recurse on each half.

    doQuickSort(ary, comparator, p, q - 1);
    doQuickSort(ary, comparator, q + 1, r);
  }
}
/**
 * Sort the given array in-place with the given comparator function.
 *
 * @param {Array} ary
 *        An array to sort.
 * @param {function} comparator
 *        Function to use to compare two items.
 */


exports.quickSort = function (ary, comparator) {
  doQuickSort(ary, comparator, 0, ary.length - 1);
};

/***/ }),

/***/ "./node_modules/_source-map@0.6.1@source-map/lib/source-map-consumer.js":
/*!******************************************************************************!*\
  !*** ./node_modules/_source-map@0.6.1@source-map/lib/source-map-consumer.js ***!
  \******************************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

/* -*- Mode: js; js-indent-level: 2; -*- */

/*
 * Copyright 2011 Mozilla Foundation and contributors
 * Licensed under the New BSD license. See LICENSE or:
 * http://opensource.org/licenses/BSD-3-Clause
 */
var util = __webpack_require__(/*! ./util */ "./node_modules/_source-map@0.6.1@source-map/lib/util.js");

var binarySearch = __webpack_require__(/*! ./binary-search */ "./node_modules/_source-map@0.6.1@source-map/lib/binary-search.js");

var ArraySet = __webpack_require__(/*! ./array-set */ "./node_modules/_source-map@0.6.1@source-map/lib/array-set.js").ArraySet;

var base64VLQ = __webpack_require__(/*! ./base64-vlq */ "./node_modules/_source-map@0.6.1@source-map/lib/base64-vlq.js");

var quickSort = __webpack_require__(/*! ./quick-sort */ "./node_modules/_source-map@0.6.1@source-map/lib/quick-sort.js").quickSort;

function SourceMapConsumer(aSourceMap, aSourceMapURL) {
  var sourceMap = aSourceMap;

  if (typeof aSourceMap === 'string') {
    sourceMap = util.parseSourceMapInput(aSourceMap);
  }

  return sourceMap.sections != null ? new IndexedSourceMapConsumer(sourceMap, aSourceMapURL) : new BasicSourceMapConsumer(sourceMap, aSourceMapURL);
}

SourceMapConsumer.fromSourceMap = function (aSourceMap, aSourceMapURL) {
  return BasicSourceMapConsumer.fromSourceMap(aSourceMap, aSourceMapURL);
};
/**
 * The version of the source mapping spec that we are consuming.
 */


SourceMapConsumer.prototype._version = 3; // `__generatedMappings` and `__originalMappings` are arrays that hold the
// parsed mapping coordinates from the source map's "mappings" attribute. They
// are lazily instantiated, accessed via the `_generatedMappings` and
// `_originalMappings` getters respectively, and we only parse the mappings
// and create these arrays once queried for a source location. We jump through
// these hoops because there can be many thousands of mappings, and parsing
// them is expensive, so we only want to do it if we must.
//
// Each object in the arrays is of the form:
//
//     {
//       generatedLine: The line number in the generated code,
//       generatedColumn: The column number in the generated code,
//       source: The path to the original source file that generated this
//               chunk of code,
//       originalLine: The line number in the original source that
//                     corresponds to this chunk of generated code,
//       originalColumn: The column number in the original source that
//                       corresponds to this chunk of generated code,
//       name: The name of the original symbol which generated this chunk of
//             code.
//     }
//
// All properties except for `generatedLine` and `generatedColumn` can be
// `null`.
//
// `_generatedMappings` is ordered by the generated positions.
//
// `_originalMappings` is ordered by the original positions.

SourceMapConsumer.prototype.__generatedMappings = null;
Object.defineProperty(SourceMapConsumer.prototype, '_generatedMappings', {
  configurable: true,
  enumerable: true,
  get: function get() {
    if (!this.__generatedMappings) {
      this._parseMappings(this._mappings, this.sourceRoot);
    }

    return this.__generatedMappings;
  }
});
SourceMapConsumer.prototype.__originalMappings = null;
Object.defineProperty(SourceMapConsumer.prototype, '_originalMappings', {
  configurable: true,
  enumerable: true,
  get: function get() {
    if (!this.__originalMappings) {
      this._parseMappings(this._mappings, this.sourceRoot);
    }

    return this.__originalMappings;
  }
});

SourceMapConsumer.prototype._charIsMappingSeparator = function SourceMapConsumer_charIsMappingSeparator(aStr, index) {
  var c = aStr.charAt(index);
  return c === ";" || c === ",";
};
/**
 * Parse the mappings in a string in to a data structure which we can easily
 * query (the ordered arrays in the `this.__generatedMappings` and
 * `this.__originalMappings` properties).
 */


SourceMapConsumer.prototype._parseMappings = function SourceMapConsumer_parseMappings(aStr, aSourceRoot) {
  throw new Error("Subclasses must implement _parseMappings");
};

SourceMapConsumer.GENERATED_ORDER = 1;
SourceMapConsumer.ORIGINAL_ORDER = 2;
SourceMapConsumer.GREATEST_LOWER_BOUND = 1;
SourceMapConsumer.LEAST_UPPER_BOUND = 2;
/**
 * Iterate over each mapping between an original source/line/column and a
 * generated line/column in this source map.
 *
 * @param Function aCallback
 *        The function that is called with each mapping.
 * @param Object aContext
 *        Optional. If specified, this object will be the value of `this` every
 *        time that `aCallback` is called.
 * @param aOrder
 *        Either `SourceMapConsumer.GENERATED_ORDER` or
 *        `SourceMapConsumer.ORIGINAL_ORDER`. Specifies whether you want to
 *        iterate over the mappings sorted by the generated file's line/column
 *        order or the original's source/line/column order, respectively. Defaults to
 *        `SourceMapConsumer.GENERATED_ORDER`.
 */

SourceMapConsumer.prototype.eachMapping = function SourceMapConsumer_eachMapping(aCallback, aContext, aOrder) {
  var context = aContext || null;
  var order = aOrder || SourceMapConsumer.GENERATED_ORDER;
  var mappings;

  switch (order) {
    case SourceMapConsumer.GENERATED_ORDER:
      mappings = this._generatedMappings;
      break;

    case SourceMapConsumer.ORIGINAL_ORDER:
      mappings = this._originalMappings;
      break;

    default:
      throw new Error("Unknown order of iteration.");
  }

  var sourceRoot = this.sourceRoot;
  mappings.map(function (mapping) {
    var source = mapping.source === null ? null : this._sources.at(mapping.source);
    source = util.computeSourceURL(sourceRoot, source, this._sourceMapURL);
    return {
      source: source,
      generatedLine: mapping.generatedLine,
      generatedColumn: mapping.generatedColumn,
      originalLine: mapping.originalLine,
      originalColumn: mapping.originalColumn,
      name: mapping.name === null ? null : this._names.at(mapping.name)
    };
  }, this).forEach(aCallback, context);
};
/**
 * Returns all generated line and column information for the original source,
 * line, and column provided. If no column is provided, returns all mappings
 * corresponding to a either the line we are searching for or the next
 * closest line that has any mappings. Otherwise, returns all mappings
 * corresponding to the given line and either the column we are searching for
 * or the next closest column that has any offsets.
 *
 * The only argument is an object with the following properties:
 *
 *   - source: The filename of the original source.
 *   - line: The line number in the original source.  The line number is 1-based.
 *   - column: Optional. the column number in the original source.
 *    The column number is 0-based.
 *
 * and an array of objects is returned, each with the following properties:
 *
 *   - line: The line number in the generated source, or null.  The
 *    line number is 1-based.
 *   - column: The column number in the generated source, or null.
 *    The column number is 0-based.
 */


SourceMapConsumer.prototype.allGeneratedPositionsFor = function SourceMapConsumer_allGeneratedPositionsFor(aArgs) {
  var line = util.getArg(aArgs, 'line'); // When there is no exact match, BasicSourceMapConsumer.prototype._findMapping
  // returns the index of the closest mapping less than the needle. By
  // setting needle.originalColumn to 0, we thus find the last mapping for
  // the given line, provided such a mapping exists.

  var needle = {
    source: util.getArg(aArgs, 'source'),
    originalLine: line,
    originalColumn: util.getArg(aArgs, 'column', 0)
  };
  needle.source = this._findSourceIndex(needle.source);

  if (needle.source < 0) {
    return [];
  }

  var mappings = [];

  var index = this._findMapping(needle, this._originalMappings, "originalLine", "originalColumn", util.compareByOriginalPositions, binarySearch.LEAST_UPPER_BOUND);

  if (index >= 0) {
    var mapping = this._originalMappings[index];

    if (aArgs.column === undefined) {
      var originalLine = mapping.originalLine; // Iterate until either we run out of mappings, or we run into
      // a mapping for a different line than the one we found. Since
      // mappings are sorted, this is guaranteed to find all mappings for
      // the line we found.

      while (mapping && mapping.originalLine === originalLine) {
        mappings.push({
          line: util.getArg(mapping, 'generatedLine', null),
          column: util.getArg(mapping, 'generatedColumn', null),
          lastColumn: util.getArg(mapping, 'lastGeneratedColumn', null)
        });
        mapping = this._originalMappings[++index];
      }
    } else {
      var originalColumn = mapping.originalColumn; // Iterate until either we run out of mappings, or we run into
      // a mapping for a different line than the one we were searching for.
      // Since mappings are sorted, this is guaranteed to find all mappings for
      // the line we are searching for.

      while (mapping && mapping.originalLine === line && mapping.originalColumn == originalColumn) {
        mappings.push({
          line: util.getArg(mapping, 'generatedLine', null),
          column: util.getArg(mapping, 'generatedColumn', null),
          lastColumn: util.getArg(mapping, 'lastGeneratedColumn', null)
        });
        mapping = this._originalMappings[++index];
      }
    }
  }

  return mappings;
};

exports.SourceMapConsumer = SourceMapConsumer;
/**
 * A BasicSourceMapConsumer instance represents a parsed source map which we can
 * query for information about the original file positions by giving it a file
 * position in the generated source.
 *
 * The first parameter is the raw source map (either as a JSON string, or
 * already parsed to an object). According to the spec, source maps have the
 * following attributes:
 *
 *   - version: Which version of the source map spec this map is following.
 *   - sources: An array of URLs to the original source files.
 *   - names: An array of identifiers which can be referrenced by individual mappings.
 *   - sourceRoot: Optional. The URL root from which all sources are relative.
 *   - sourcesContent: Optional. An array of contents of the original source files.
 *   - mappings: A string of base64 VLQs which contain the actual mappings.
 *   - file: Optional. The generated file this source map is associated with.
 *
 * Here is an example source map, taken from the source map spec[0]:
 *
 *     {
 *       version : 3,
 *       file: "out.js",
 *       sourceRoot : "",
 *       sources: ["foo.js", "bar.js"],
 *       names: ["src", "maps", "are", "fun"],
 *       mappings: "AA,AB;;ABCDE;"
 *     }
 *
 * The second parameter, if given, is a string whose value is the URL
 * at which the source map was found.  This URL is used to compute the
 * sources array.
 *
 * [0]: https://docs.google.com/document/d/1U1RGAehQwRypUTovF1KRlpiOFze0b-_2gc6fAH0KY0k/edit?pli=1#
 */

function BasicSourceMapConsumer(aSourceMap, aSourceMapURL) {
  var sourceMap = aSourceMap;

  if (typeof aSourceMap === 'string') {
    sourceMap = util.parseSourceMapInput(aSourceMap);
  }

  var version = util.getArg(sourceMap, 'version');
  var sources = util.getArg(sourceMap, 'sources'); // Sass 3.3 leaves out the 'names' array, so we deviate from the spec (which
  // requires the array) to play nice here.

  var names = util.getArg(sourceMap, 'names', []);
  var sourceRoot = util.getArg(sourceMap, 'sourceRoot', null);
  var sourcesContent = util.getArg(sourceMap, 'sourcesContent', null);
  var mappings = util.getArg(sourceMap, 'mappings');
  var file = util.getArg(sourceMap, 'file', null); // Once again, Sass deviates from the spec and supplies the version as a
  // string rather than a number, so we use loose equality checking here.

  if (version != this._version) {
    throw new Error('Unsupported version: ' + version);
  }

  if (sourceRoot) {
    sourceRoot = util.normalize(sourceRoot);
  }

  sources = sources.map(String) // Some source maps produce relative source paths like "./foo.js" instead of
  // "foo.js".  Normalize these first so that future comparisons will succeed.
  // See bugzil.la/1090768.
  .map(util.normalize) // Always ensure that absolute sources are internally stored relative to
  // the source root, if the source root is absolute. Not doing this would
  // be particularly problematic when the source root is a prefix of the
  // source (valid, but why??). See github issue #199 and bugzil.la/1188982.
  .map(function (source) {
    return sourceRoot && util.isAbsolute(sourceRoot) && util.isAbsolute(source) ? util.relative(sourceRoot, source) : source;
  }); // Pass `true` below to allow duplicate names and sources. While source maps
  // are intended to be compressed and deduplicated, the TypeScript compiler
  // sometimes generates source maps with duplicates in them. See Github issue
  // #72 and bugzil.la/889492.

  this._names = ArraySet.fromArray(names.map(String), true);
  this._sources = ArraySet.fromArray(sources, true);
  this._absoluteSources = this._sources.toArray().map(function (s) {
    return util.computeSourceURL(sourceRoot, s, aSourceMapURL);
  });
  this.sourceRoot = sourceRoot;
  this.sourcesContent = sourcesContent;
  this._mappings = mappings;
  this._sourceMapURL = aSourceMapURL;
  this.file = file;
}

BasicSourceMapConsumer.prototype = Object.create(SourceMapConsumer.prototype);
BasicSourceMapConsumer.prototype.consumer = SourceMapConsumer;
/**
 * Utility function to find the index of a source.  Returns -1 if not
 * found.
 */

BasicSourceMapConsumer.prototype._findSourceIndex = function (aSource) {
  var relativeSource = aSource;

  if (this.sourceRoot != null) {
    relativeSource = util.relative(this.sourceRoot, relativeSource);
  }

  if (this._sources.has(relativeSource)) {
    return this._sources.indexOf(relativeSource);
  } // Maybe aSource is an absolute URL as returned by |sources|.  In
  // this case we can't simply undo the transform.


  var i;

  for (i = 0; i < this._absoluteSources.length; ++i) {
    if (this._absoluteSources[i] == aSource) {
      return i;
    }
  }

  return -1;
};
/**
 * Create a BasicSourceMapConsumer from a SourceMapGenerator.
 *
 * @param SourceMapGenerator aSourceMap
 *        The source map that will be consumed.
 * @param String aSourceMapURL
 *        The URL at which the source map can be found (optional)
 * @returns BasicSourceMapConsumer
 */


BasicSourceMapConsumer.fromSourceMap = function SourceMapConsumer_fromSourceMap(aSourceMap, aSourceMapURL) {
  var smc = Object.create(BasicSourceMapConsumer.prototype);
  var names = smc._names = ArraySet.fromArray(aSourceMap._names.toArray(), true);
  var sources = smc._sources = ArraySet.fromArray(aSourceMap._sources.toArray(), true);
  smc.sourceRoot = aSourceMap._sourceRoot;
  smc.sourcesContent = aSourceMap._generateSourcesContent(smc._sources.toArray(), smc.sourceRoot);
  smc.file = aSourceMap._file;
  smc._sourceMapURL = aSourceMapURL;
  smc._absoluteSources = smc._sources.toArray().map(function (s) {
    return util.computeSourceURL(smc.sourceRoot, s, aSourceMapURL);
  }); // Because we are modifying the entries (by converting string sources and
  // names to indices into the sources and names ArraySets), we have to make
  // a copy of the entry or else bad things happen. Shared mutable state
  // strikes again! See github issue #191.

  var generatedMappings = aSourceMap._mappings.toArray().slice();

  var destGeneratedMappings = smc.__generatedMappings = [];
  var destOriginalMappings = smc.__originalMappings = [];

  for (var i = 0, length = generatedMappings.length; i < length; i++) {
    var srcMapping = generatedMappings[i];
    var destMapping = new Mapping();
    destMapping.generatedLine = srcMapping.generatedLine;
    destMapping.generatedColumn = srcMapping.generatedColumn;

    if (srcMapping.source) {
      destMapping.source = sources.indexOf(srcMapping.source);
      destMapping.originalLine = srcMapping.originalLine;
      destMapping.originalColumn = srcMapping.originalColumn;

      if (srcMapping.name) {
        destMapping.name = names.indexOf(srcMapping.name);
      }

      destOriginalMappings.push(destMapping);
    }

    destGeneratedMappings.push(destMapping);
  }

  quickSort(smc.__originalMappings, util.compareByOriginalPositions);
  return smc;
};
/**
 * The version of the source mapping spec that we are consuming.
 */


BasicSourceMapConsumer.prototype._version = 3;
/**
 * The list of original sources.
 */

Object.defineProperty(BasicSourceMapConsumer.prototype, 'sources', {
  get: function get() {
    return this._absoluteSources.slice();
  }
});
/**
 * Provide the JIT with a nice shape / hidden class.
 */

function Mapping() {
  this.generatedLine = 0;
  this.generatedColumn = 0;
  this.source = null;
  this.originalLine = null;
  this.originalColumn = null;
  this.name = null;
}
/**
 * Parse the mappings in a string in to a data structure which we can easily
 * query (the ordered arrays in the `this.__generatedMappings` and
 * `this.__originalMappings` properties).
 */


BasicSourceMapConsumer.prototype._parseMappings = function SourceMapConsumer_parseMappings(aStr, aSourceRoot) {
  var generatedLine = 1;
  var previousGeneratedColumn = 0;
  var previousOriginalLine = 0;
  var previousOriginalColumn = 0;
  var previousSource = 0;
  var previousName = 0;
  var length = aStr.length;
  var index = 0;
  var cachedSegments = {};
  var temp = {};
  var originalMappings = [];
  var generatedMappings = [];
  var mapping, str, segment, end, value;

  while (index < length) {
    if (aStr.charAt(index) === ';') {
      generatedLine++;
      index++;
      previousGeneratedColumn = 0;
    } else if (aStr.charAt(index) === ',') {
      index++;
    } else {
      mapping = new Mapping();
      mapping.generatedLine = generatedLine; // Because each offset is encoded relative to the previous one,
      // many segments often have the same encoding. We can exploit this
      // fact by caching the parsed variable length fields of each segment,
      // allowing us to avoid a second parse if we encounter the same
      // segment again.

      for (end = index; end < length; end++) {
        if (this._charIsMappingSeparator(aStr, end)) {
          break;
        }
      }

      str = aStr.slice(index, end);
      segment = cachedSegments[str];

      if (segment) {
        index += str.length;
      } else {
        segment = [];

        while (index < end) {
          base64VLQ.decode(aStr, index, temp);
          value = temp.value;
          index = temp.rest;
          segment.push(value);
        }

        if (segment.length === 2) {
          throw new Error('Found a source, but no line and column');
        }

        if (segment.length === 3) {
          throw new Error('Found a source and line, but no column');
        }

        cachedSegments[str] = segment;
      } // Generated column.


      mapping.generatedColumn = previousGeneratedColumn + segment[0];
      previousGeneratedColumn = mapping.generatedColumn;

      if (segment.length > 1) {
        // Original source.
        mapping.source = previousSource + segment[1];
        previousSource += segment[1]; // Original line.

        mapping.originalLine = previousOriginalLine + segment[2];
        previousOriginalLine = mapping.originalLine; // Lines are stored 0-based

        mapping.originalLine += 1; // Original column.

        mapping.originalColumn = previousOriginalColumn + segment[3];
        previousOriginalColumn = mapping.originalColumn;

        if (segment.length > 4) {
          // Original name.
          mapping.name = previousName + segment[4];
          previousName += segment[4];
        }
      }

      generatedMappings.push(mapping);

      if (typeof mapping.originalLine === 'number') {
        originalMappings.push(mapping);
      }
    }
  }

  quickSort(generatedMappings, util.compareByGeneratedPositionsDeflated);
  this.__generatedMappings = generatedMappings;
  quickSort(originalMappings, util.compareByOriginalPositions);
  this.__originalMappings = originalMappings;
};
/**
 * Find the mapping that best matches the hypothetical "needle" mapping that
 * we are searching for in the given "haystack" of mappings.
 */


BasicSourceMapConsumer.prototype._findMapping = function SourceMapConsumer_findMapping(aNeedle, aMappings, aLineName, aColumnName, aComparator, aBias) {
  // To return the position we are searching for, we must first find the
  // mapping for the given position and then return the opposite position it
  // points to. Because the mappings are sorted, we can use binary search to
  // find the best mapping.
  if (aNeedle[aLineName] <= 0) {
    throw new TypeError('Line must be greater than or equal to 1, got ' + aNeedle[aLineName]);
  }

  if (aNeedle[aColumnName] < 0) {
    throw new TypeError('Column must be greater than or equal to 0, got ' + aNeedle[aColumnName]);
  }

  return binarySearch.search(aNeedle, aMappings, aComparator, aBias);
};
/**
 * Compute the last column for each generated mapping. The last column is
 * inclusive.
 */


BasicSourceMapConsumer.prototype.computeColumnSpans = function SourceMapConsumer_computeColumnSpans() {
  for (var index = 0; index < this._generatedMappings.length; ++index) {
    var mapping = this._generatedMappings[index]; // Mappings do not contain a field for the last generated columnt. We
    // can come up with an optimistic estimate, however, by assuming that
    // mappings are contiguous (i.e. given two consecutive mappings, the
    // first mapping ends where the second one starts).

    if (index + 1 < this._generatedMappings.length) {
      var nextMapping = this._generatedMappings[index + 1];

      if (mapping.generatedLine === nextMapping.generatedLine) {
        mapping.lastGeneratedColumn = nextMapping.generatedColumn - 1;
        continue;
      }
    } // The last mapping for each line spans the entire line.


    mapping.lastGeneratedColumn = Infinity;
  }
};
/**
 * Returns the original source, line, and column information for the generated
 * source's line and column positions provided. The only argument is an object
 * with the following properties:
 *
 *   - line: The line number in the generated source.  The line number
 *     is 1-based.
 *   - column: The column number in the generated source.  The column
 *     number is 0-based.
 *   - bias: Either 'SourceMapConsumer.GREATEST_LOWER_BOUND' or
 *     'SourceMapConsumer.LEAST_UPPER_BOUND'. Specifies whether to return the
 *     closest element that is smaller than or greater than the one we are
 *     searching for, respectively, if the exact element cannot be found.
 *     Defaults to 'SourceMapConsumer.GREATEST_LOWER_BOUND'.
 *
 * and an object is returned with the following properties:
 *
 *   - source: The original source file, or null.
 *   - line: The line number in the original source, or null.  The
 *     line number is 1-based.
 *   - column: The column number in the original source, or null.  The
 *     column number is 0-based.
 *   - name: The original identifier, or null.
 */


BasicSourceMapConsumer.prototype.originalPositionFor = function SourceMapConsumer_originalPositionFor(aArgs) {
  var needle = {
    generatedLine: util.getArg(aArgs, 'line'),
    generatedColumn: util.getArg(aArgs, 'column')
  };

  var index = this._findMapping(needle, this._generatedMappings, "generatedLine", "generatedColumn", util.compareByGeneratedPositionsDeflated, util.getArg(aArgs, 'bias', SourceMapConsumer.GREATEST_LOWER_BOUND));

  if (index >= 0) {
    var mapping = this._generatedMappings[index];

    if (mapping.generatedLine === needle.generatedLine) {
      var source = util.getArg(mapping, 'source', null);

      if (source !== null) {
        source = this._sources.at(source);
        source = util.computeSourceURL(this.sourceRoot, source, this._sourceMapURL);
      }

      var name = util.getArg(mapping, 'name', null);

      if (name !== null) {
        name = this._names.at(name);
      }

      return {
        source: source,
        line: util.getArg(mapping, 'originalLine', null),
        column: util.getArg(mapping, 'originalColumn', null),
        name: name
      };
    }
  }

  return {
    source: null,
    line: null,
    column: null,
    name: null
  };
};
/**
 * Return true if we have the source content for every source in the source
 * map, false otherwise.
 */


BasicSourceMapConsumer.prototype.hasContentsOfAllSources = function BasicSourceMapConsumer_hasContentsOfAllSources() {
  if (!this.sourcesContent) {
    return false;
  }

  return this.sourcesContent.length >= this._sources.size() && !this.sourcesContent.some(function (sc) {
    return sc == null;
  });
};
/**
 * Returns the original source content. The only argument is the url of the
 * original source file. Returns null if no original source content is
 * available.
 */


BasicSourceMapConsumer.prototype.sourceContentFor = function SourceMapConsumer_sourceContentFor(aSource, nullOnMissing) {
  if (!this.sourcesContent) {
    return null;
  }

  var index = this._findSourceIndex(aSource);

  if (index >= 0) {
    return this.sourcesContent[index];
  }

  var relativeSource = aSource;

  if (this.sourceRoot != null) {
    relativeSource = util.relative(this.sourceRoot, relativeSource);
  }

  var url;

  if (this.sourceRoot != null && (url = util.urlParse(this.sourceRoot))) {
    // XXX: file:// URIs and absolute paths lead to unexpected behavior for
    // many users. We can help them out when they expect file:// URIs to
    // behave like it would if they were running a local HTTP server. See
    // https://bugzilla.mozilla.org/show_bug.cgi?id=885597.
    var fileUriAbsPath = relativeSource.replace(/^file:\/\//, "");

    if (url.scheme == "file" && this._sources.has(fileUriAbsPath)) {
      return this.sourcesContent[this._sources.indexOf(fileUriAbsPath)];
    }

    if ((!url.path || url.path == "/") && this._sources.has("/" + relativeSource)) {
      return this.sourcesContent[this._sources.indexOf("/" + relativeSource)];
    }
  } // This function is used recursively from
  // IndexedSourceMapConsumer.prototype.sourceContentFor. In that case, we
  // don't want to throw if we can't find the source - we just want to
  // return null, so we provide a flag to exit gracefully.


  if (nullOnMissing) {
    return null;
  } else {
    throw new Error('"' + relativeSource + '" is not in the SourceMap.');
  }
};
/**
 * Returns the generated line and column information for the original source,
 * line, and column positions provided. The only argument is an object with
 * the following properties:
 *
 *   - source: The filename of the original source.
 *   - line: The line number in the original source.  The line number
 *     is 1-based.
 *   - column: The column number in the original source.  The column
 *     number is 0-based.
 *   - bias: Either 'SourceMapConsumer.GREATEST_LOWER_BOUND' or
 *     'SourceMapConsumer.LEAST_UPPER_BOUND'. Specifies whether to return the
 *     closest element that is smaller than or greater than the one we are
 *     searching for, respectively, if the exact element cannot be found.
 *     Defaults to 'SourceMapConsumer.GREATEST_LOWER_BOUND'.
 *
 * and an object is returned with the following properties:
 *
 *   - line: The line number in the generated source, or null.  The
 *     line number is 1-based.
 *   - column: The column number in the generated source, or null.
 *     The column number is 0-based.
 */


BasicSourceMapConsumer.prototype.generatedPositionFor = function SourceMapConsumer_generatedPositionFor(aArgs) {
  var source = util.getArg(aArgs, 'source');
  source = this._findSourceIndex(source);

  if (source < 0) {
    return {
      line: null,
      column: null,
      lastColumn: null
    };
  }

  var needle = {
    source: source,
    originalLine: util.getArg(aArgs, 'line'),
    originalColumn: util.getArg(aArgs, 'column')
  };

  var index = this._findMapping(needle, this._originalMappings, "originalLine", "originalColumn", util.compareByOriginalPositions, util.getArg(aArgs, 'bias', SourceMapConsumer.GREATEST_LOWER_BOUND));

  if (index >= 0) {
    var mapping = this._originalMappings[index];

    if (mapping.source === needle.source) {
      return {
        line: util.getArg(mapping, 'generatedLine', null),
        column: util.getArg(mapping, 'generatedColumn', null),
        lastColumn: util.getArg(mapping, 'lastGeneratedColumn', null)
      };
    }
  }

  return {
    line: null,
    column: null,
    lastColumn: null
  };
};

exports.BasicSourceMapConsumer = BasicSourceMapConsumer;
/**
 * An IndexedSourceMapConsumer instance represents a parsed source map which
 * we can query for information. It differs from BasicSourceMapConsumer in
 * that it takes "indexed" source maps (i.e. ones with a "sections" field) as
 * input.
 *
 * The first parameter is a raw source map (either as a JSON string, or already
 * parsed to an object). According to the spec for indexed source maps, they
 * have the following attributes:
 *
 *   - version: Which version of the source map spec this map is following.
 *   - file: Optional. The generated file this source map is associated with.
 *   - sections: A list of section definitions.
 *
 * Each value under the "sections" field has two fields:
 *   - offset: The offset into the original specified at which this section
 *       begins to apply, defined as an object with a "line" and "column"
 *       field.
 *   - map: A source map definition. This source map could also be indexed,
 *       but doesn't have to be.
 *
 * Instead of the "map" field, it's also possible to have a "url" field
 * specifying a URL to retrieve a source map from, but that's currently
 * unsupported.
 *
 * Here's an example source map, taken from the source map spec[0], but
 * modified to omit a section which uses the "url" field.
 *
 *  {
 *    version : 3,
 *    file: "app.js",
 *    sections: [{
 *      offset: {line:100, column:10},
 *      map: {
 *        version : 3,
 *        file: "section.js",
 *        sources: ["foo.js", "bar.js"],
 *        names: ["src", "maps", "are", "fun"],
 *        mappings: "AAAA,E;;ABCDE;"
 *      }
 *    }],
 *  }
 *
 * The second parameter, if given, is a string whose value is the URL
 * at which the source map was found.  This URL is used to compute the
 * sources array.
 *
 * [0]: https://docs.google.com/document/d/1U1RGAehQwRypUTovF1KRlpiOFze0b-_2gc6fAH0KY0k/edit#heading=h.535es3xeprgt
 */

function IndexedSourceMapConsumer(aSourceMap, aSourceMapURL) {
  var sourceMap = aSourceMap;

  if (typeof aSourceMap === 'string') {
    sourceMap = util.parseSourceMapInput(aSourceMap);
  }

  var version = util.getArg(sourceMap, 'version');
  var sections = util.getArg(sourceMap, 'sections');

  if (version != this._version) {
    throw new Error('Unsupported version: ' + version);
  }

  this._sources = new ArraySet();
  this._names = new ArraySet();
  var lastOffset = {
    line: -1,
    column: 0
  };
  this._sections = sections.map(function (s) {
    if (s.url) {
      // The url field will require support for asynchronicity.
      // See https://github.com/mozilla/source-map/issues/16
      throw new Error('Support for url field in sections not implemented.');
    }

    var offset = util.getArg(s, 'offset');
    var offsetLine = util.getArg(offset, 'line');
    var offsetColumn = util.getArg(offset, 'column');

    if (offsetLine < lastOffset.line || offsetLine === lastOffset.line && offsetColumn < lastOffset.column) {
      throw new Error('Section offsets must be ordered and non-overlapping.');
    }

    lastOffset = offset;
    return {
      generatedOffset: {
        // The offset fields are 0-based, but we use 1-based indices when
        // encoding/decoding from VLQ.
        generatedLine: offsetLine + 1,
        generatedColumn: offsetColumn + 1
      },
      consumer: new SourceMapConsumer(util.getArg(s, 'map'), aSourceMapURL)
    };
  });
}

IndexedSourceMapConsumer.prototype = Object.create(SourceMapConsumer.prototype);
IndexedSourceMapConsumer.prototype.constructor = SourceMapConsumer;
/**
 * The version of the source mapping spec that we are consuming.
 */

IndexedSourceMapConsumer.prototype._version = 3;
/**
 * The list of original sources.
 */

Object.defineProperty(IndexedSourceMapConsumer.prototype, 'sources', {
  get: function get() {
    var sources = [];

    for (var i = 0; i < this._sections.length; i++) {
      for (var j = 0; j < this._sections[i].consumer.sources.length; j++) {
        sources.push(this._sections[i].consumer.sources[j]);
      }
    }

    return sources;
  }
});
/**
 * Returns the original source, line, and column information for the generated
 * source's line and column positions provided. The only argument is an object
 * with the following properties:
 *
 *   - line: The line number in the generated source.  The line number
 *     is 1-based.
 *   - column: The column number in the generated source.  The column
 *     number is 0-based.
 *
 * and an object is returned with the following properties:
 *
 *   - source: The original source file, or null.
 *   - line: The line number in the original source, or null.  The
 *     line number is 1-based.
 *   - column: The column number in the original source, or null.  The
 *     column number is 0-based.
 *   - name: The original identifier, or null.
 */

IndexedSourceMapConsumer.prototype.originalPositionFor = function IndexedSourceMapConsumer_originalPositionFor(aArgs) {
  var needle = {
    generatedLine: util.getArg(aArgs, 'line'),
    generatedColumn: util.getArg(aArgs, 'column')
  }; // Find the section containing the generated position we're trying to map
  // to an original position.

  var sectionIndex = binarySearch.search(needle, this._sections, function (needle, section) {
    var cmp = needle.generatedLine - section.generatedOffset.generatedLine;

    if (cmp) {
      return cmp;
    }

    return needle.generatedColumn - section.generatedOffset.generatedColumn;
  });
  var section = this._sections[sectionIndex];

  if (!section) {
    return {
      source: null,
      line: null,
      column: null,
      name: null
    };
  }

  return section.consumer.originalPositionFor({
    line: needle.generatedLine - (section.generatedOffset.generatedLine - 1),
    column: needle.generatedColumn - (section.generatedOffset.generatedLine === needle.generatedLine ? section.generatedOffset.generatedColumn - 1 : 0),
    bias: aArgs.bias
  });
};
/**
 * Return true if we have the source content for every source in the source
 * map, false otherwise.
 */


IndexedSourceMapConsumer.prototype.hasContentsOfAllSources = function IndexedSourceMapConsumer_hasContentsOfAllSources() {
  return this._sections.every(function (s) {
    return s.consumer.hasContentsOfAllSources();
  });
};
/**
 * Returns the original source content. The only argument is the url of the
 * original source file. Returns null if no original source content is
 * available.
 */


IndexedSourceMapConsumer.prototype.sourceContentFor = function IndexedSourceMapConsumer_sourceContentFor(aSource, nullOnMissing) {
  for (var i = 0; i < this._sections.length; i++) {
    var section = this._sections[i];
    var content = section.consumer.sourceContentFor(aSource, true);

    if (content) {
      return content;
    }
  }

  if (nullOnMissing) {
    return null;
  } else {
    throw new Error('"' + aSource + '" is not in the SourceMap.');
  }
};
/**
 * Returns the generated line and column information for the original source,
 * line, and column positions provided. The only argument is an object with
 * the following properties:
 *
 *   - source: The filename of the original source.
 *   - line: The line number in the original source.  The line number
 *     is 1-based.
 *   - column: The column number in the original source.  The column
 *     number is 0-based.
 *
 * and an object is returned with the following properties:
 *
 *   - line: The line number in the generated source, or null.  The
 *     line number is 1-based. 
 *   - column: The column number in the generated source, or null.
 *     The column number is 0-based.
 */


IndexedSourceMapConsumer.prototype.generatedPositionFor = function IndexedSourceMapConsumer_generatedPositionFor(aArgs) {
  for (var i = 0; i < this._sections.length; i++) {
    var section = this._sections[i]; // Only consider this section if the requested source is in the list of
    // sources of the consumer.

    if (section.consumer._findSourceIndex(util.getArg(aArgs, 'source')) === -1) {
      continue;
    }

    var generatedPosition = section.consumer.generatedPositionFor(aArgs);

    if (generatedPosition) {
      var ret = {
        line: generatedPosition.line + (section.generatedOffset.generatedLine - 1),
        column: generatedPosition.column + (section.generatedOffset.generatedLine === generatedPosition.line ? section.generatedOffset.generatedColumn - 1 : 0)
      };
      return ret;
    }
  }

  return {
    line: null,
    column: null
  };
};
/**
 * Parse the mappings in a string in to a data structure which we can easily
 * query (the ordered arrays in the `this.__generatedMappings` and
 * `this.__originalMappings` properties).
 */


IndexedSourceMapConsumer.prototype._parseMappings = function IndexedSourceMapConsumer_parseMappings(aStr, aSourceRoot) {
  this.__generatedMappings = [];
  this.__originalMappings = [];

  for (var i = 0; i < this._sections.length; i++) {
    var section = this._sections[i];
    var sectionMappings = section.consumer._generatedMappings;

    for (var j = 0; j < sectionMappings.length; j++) {
      var mapping = sectionMappings[j];

      var source = section.consumer._sources.at(mapping.source);

      source = util.computeSourceURL(section.consumer.sourceRoot, source, this._sourceMapURL);

      this._sources.add(source);

      source = this._sources.indexOf(source);
      var name = null;

      if (mapping.name) {
        name = section.consumer._names.at(mapping.name);

        this._names.add(name);

        name = this._names.indexOf(name);
      } // The mappings coming from the consumer for the section have
      // generated positions relative to the start of the section, so we
      // need to offset them to be relative to the start of the concatenated
      // generated file.


      var adjustedMapping = {
        source: source,
        generatedLine: mapping.generatedLine + (section.generatedOffset.generatedLine - 1),
        generatedColumn: mapping.generatedColumn + (section.generatedOffset.generatedLine === mapping.generatedLine ? section.generatedOffset.generatedColumn - 1 : 0),
        originalLine: mapping.originalLine,
        originalColumn: mapping.originalColumn,
        name: name
      };

      this.__generatedMappings.push(adjustedMapping);

      if (typeof adjustedMapping.originalLine === 'number') {
        this.__originalMappings.push(adjustedMapping);
      }
    }
  }

  quickSort(this.__generatedMappings, util.compareByGeneratedPositionsDeflated);
  quickSort(this.__originalMappings, util.compareByOriginalPositions);
};

exports.IndexedSourceMapConsumer = IndexedSourceMapConsumer;

/***/ }),

/***/ "./node_modules/_source-map@0.6.1@source-map/lib/source-map-generator.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/_source-map@0.6.1@source-map/lib/source-map-generator.js ***!
  \*******************************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

/* -*- Mode: js; js-indent-level: 2; -*- */

/*
 * Copyright 2011 Mozilla Foundation and contributors
 * Licensed under the New BSD license. See LICENSE or:
 * http://opensource.org/licenses/BSD-3-Clause
 */
var base64VLQ = __webpack_require__(/*! ./base64-vlq */ "./node_modules/_source-map@0.6.1@source-map/lib/base64-vlq.js");

var util = __webpack_require__(/*! ./util */ "./node_modules/_source-map@0.6.1@source-map/lib/util.js");

var ArraySet = __webpack_require__(/*! ./array-set */ "./node_modules/_source-map@0.6.1@source-map/lib/array-set.js").ArraySet;

var MappingList = __webpack_require__(/*! ./mapping-list */ "./node_modules/_source-map@0.6.1@source-map/lib/mapping-list.js").MappingList;
/**
 * An instance of the SourceMapGenerator represents a source map which is
 * being built incrementally. You may pass an object with the following
 * properties:
 *
 *   - file: The filename of the generated source.
 *   - sourceRoot: A root for all relative URLs in this source map.
 */


function SourceMapGenerator(aArgs) {
  if (!aArgs) {
    aArgs = {};
  }

  this._file = util.getArg(aArgs, 'file', null);
  this._sourceRoot = util.getArg(aArgs, 'sourceRoot', null);
  this._skipValidation = util.getArg(aArgs, 'skipValidation', false);
  this._sources = new ArraySet();
  this._names = new ArraySet();
  this._mappings = new MappingList();
  this._sourcesContents = null;
}

SourceMapGenerator.prototype._version = 3;
/**
 * Creates a new SourceMapGenerator based on a SourceMapConsumer
 *
 * @param aSourceMapConsumer The SourceMap.
 */

SourceMapGenerator.fromSourceMap = function SourceMapGenerator_fromSourceMap(aSourceMapConsumer) {
  var sourceRoot = aSourceMapConsumer.sourceRoot;
  var generator = new SourceMapGenerator({
    file: aSourceMapConsumer.file,
    sourceRoot: sourceRoot
  });
  aSourceMapConsumer.eachMapping(function (mapping) {
    var newMapping = {
      generated: {
        line: mapping.generatedLine,
        column: mapping.generatedColumn
      }
    };

    if (mapping.source != null) {
      newMapping.source = mapping.source;

      if (sourceRoot != null) {
        newMapping.source = util.relative(sourceRoot, newMapping.source);
      }

      newMapping.original = {
        line: mapping.originalLine,
        column: mapping.originalColumn
      };

      if (mapping.name != null) {
        newMapping.name = mapping.name;
      }
    }

    generator.addMapping(newMapping);
  });
  aSourceMapConsumer.sources.forEach(function (sourceFile) {
    var sourceRelative = sourceFile;

    if (sourceRoot !== null) {
      sourceRelative = util.relative(sourceRoot, sourceFile);
    }

    if (!generator._sources.has(sourceRelative)) {
      generator._sources.add(sourceRelative);
    }

    var content = aSourceMapConsumer.sourceContentFor(sourceFile);

    if (content != null) {
      generator.setSourceContent(sourceFile, content);
    }
  });
  return generator;
};
/**
 * Add a single mapping from original source line and column to the generated
 * source's line and column for this source map being created. The mapping
 * object should have the following properties:
 *
 *   - generated: An object with the generated line and column positions.
 *   - original: An object with the original line and column positions.
 *   - source: The original source file (relative to the sourceRoot).
 *   - name: An optional original token name for this mapping.
 */


SourceMapGenerator.prototype.addMapping = function SourceMapGenerator_addMapping(aArgs) {
  var generated = util.getArg(aArgs, 'generated');
  var original = util.getArg(aArgs, 'original', null);
  var source = util.getArg(aArgs, 'source', null);
  var name = util.getArg(aArgs, 'name', null);

  if (!this._skipValidation) {
    this._validateMapping(generated, original, source, name);
  }

  if (source != null) {
    source = String(source);

    if (!this._sources.has(source)) {
      this._sources.add(source);
    }
  }

  if (name != null) {
    name = String(name);

    if (!this._names.has(name)) {
      this._names.add(name);
    }
  }

  this._mappings.add({
    generatedLine: generated.line,
    generatedColumn: generated.column,
    originalLine: original != null && original.line,
    originalColumn: original != null && original.column,
    source: source,
    name: name
  });
};
/**
 * Set the source content for a source file.
 */


SourceMapGenerator.prototype.setSourceContent = function SourceMapGenerator_setSourceContent(aSourceFile, aSourceContent) {
  var source = aSourceFile;

  if (this._sourceRoot != null) {
    source = util.relative(this._sourceRoot, source);
  }

  if (aSourceContent != null) {
    // Add the source content to the _sourcesContents map.
    // Create a new _sourcesContents map if the property is null.
    if (!this._sourcesContents) {
      this._sourcesContents = Object.create(null);
    }

    this._sourcesContents[util.toSetString(source)] = aSourceContent;
  } else if (this._sourcesContents) {
    // Remove the source file from the _sourcesContents map.
    // If the _sourcesContents map is empty, set the property to null.
    delete this._sourcesContents[util.toSetString(source)];

    if (Object.keys(this._sourcesContents).length === 0) {
      this._sourcesContents = null;
    }
  }
};
/**
 * Applies the mappings of a sub-source-map for a specific source file to the
 * source map being generated. Each mapping to the supplied source file is
 * rewritten using the supplied source map. Note: The resolution for the
 * resulting mappings is the minimium of this map and the supplied map.
 *
 * @param aSourceMapConsumer The source map to be applied.
 * @param aSourceFile Optional. The filename of the source file.
 *        If omitted, SourceMapConsumer's file property will be used.
 * @param aSourceMapPath Optional. The dirname of the path to the source map
 *        to be applied. If relative, it is relative to the SourceMapConsumer.
 *        This parameter is needed when the two source maps aren't in the same
 *        directory, and the source map to be applied contains relative source
 *        paths. If so, those relative source paths need to be rewritten
 *        relative to the SourceMapGenerator.
 */


SourceMapGenerator.prototype.applySourceMap = function SourceMapGenerator_applySourceMap(aSourceMapConsumer, aSourceFile, aSourceMapPath) {
  var sourceFile = aSourceFile; // If aSourceFile is omitted, we will use the file property of the SourceMap

  if (aSourceFile == null) {
    if (aSourceMapConsumer.file == null) {
      throw new Error('SourceMapGenerator.prototype.applySourceMap requires either an explicit source file, ' + 'or the source map\'s "file" property. Both were omitted.');
    }

    sourceFile = aSourceMapConsumer.file;
  }

  var sourceRoot = this._sourceRoot; // Make "sourceFile" relative if an absolute Url is passed.

  if (sourceRoot != null) {
    sourceFile = util.relative(sourceRoot, sourceFile);
  } // Applying the SourceMap can add and remove items from the sources and
  // the names array.


  var newSources = new ArraySet();
  var newNames = new ArraySet(); // Find mappings for the "sourceFile"

  this._mappings.unsortedForEach(function (mapping) {
    if (mapping.source === sourceFile && mapping.originalLine != null) {
      // Check if it can be mapped by the source map, then update the mapping.
      var original = aSourceMapConsumer.originalPositionFor({
        line: mapping.originalLine,
        column: mapping.originalColumn
      });

      if (original.source != null) {
        // Copy mapping
        mapping.source = original.source;

        if (aSourceMapPath != null) {
          mapping.source = util.join(aSourceMapPath, mapping.source);
        }

        if (sourceRoot != null) {
          mapping.source = util.relative(sourceRoot, mapping.source);
        }

        mapping.originalLine = original.line;
        mapping.originalColumn = original.column;

        if (original.name != null) {
          mapping.name = original.name;
        }
      }
    }

    var source = mapping.source;

    if (source != null && !newSources.has(source)) {
      newSources.add(source);
    }

    var name = mapping.name;

    if (name != null && !newNames.has(name)) {
      newNames.add(name);
    }
  }, this);

  this._sources = newSources;
  this._names = newNames; // Copy sourcesContents of applied map.

  aSourceMapConsumer.sources.forEach(function (sourceFile) {
    var content = aSourceMapConsumer.sourceContentFor(sourceFile);

    if (content != null) {
      if (aSourceMapPath != null) {
        sourceFile = util.join(aSourceMapPath, sourceFile);
      }

      if (sourceRoot != null) {
        sourceFile = util.relative(sourceRoot, sourceFile);
      }

      this.setSourceContent(sourceFile, content);
    }
  }, this);
};
/**
 * A mapping can have one of the three levels of data:
 *
 *   1. Just the generated position.
 *   2. The Generated position, original position, and original source.
 *   3. Generated and original position, original source, as well as a name
 *      token.
 *
 * To maintain consistency, we validate that any new mapping being added falls
 * in to one of these categories.
 */


SourceMapGenerator.prototype._validateMapping = function SourceMapGenerator_validateMapping(aGenerated, aOriginal, aSource, aName) {
  // When aOriginal is truthy but has empty values for .line and .column,
  // it is most likely a programmer error. In this case we throw a very
  // specific error message to try to guide them the right way.
  // For example: https://github.com/Polymer/polymer-bundler/pull/519
  if (aOriginal && typeof aOriginal.line !== 'number' && typeof aOriginal.column !== 'number') {
    throw new Error('original.line and original.column are not numbers -- you probably meant to omit ' + 'the original mapping entirely and only map the generated position. If so, pass ' + 'null for the original mapping instead of an object with empty or null values.');
  }

  if (aGenerated && 'line' in aGenerated && 'column' in aGenerated && aGenerated.line > 0 && aGenerated.column >= 0 && !aOriginal && !aSource && !aName) {
    // Case 1.
    return;
  } else if (aGenerated && 'line' in aGenerated && 'column' in aGenerated && aOriginal && 'line' in aOriginal && 'column' in aOriginal && aGenerated.line > 0 && aGenerated.column >= 0 && aOriginal.line > 0 && aOriginal.column >= 0 && aSource) {
    // Cases 2 and 3.
    return;
  } else {
    throw new Error('Invalid mapping: ' + JSON.stringify({
      generated: aGenerated,
      source: aSource,
      original: aOriginal,
      name: aName
    }));
  }
};
/**
 * Serialize the accumulated mappings in to the stream of base 64 VLQs
 * specified by the source map format.
 */


SourceMapGenerator.prototype._serializeMappings = function SourceMapGenerator_serializeMappings() {
  var previousGeneratedColumn = 0;
  var previousGeneratedLine = 1;
  var previousOriginalColumn = 0;
  var previousOriginalLine = 0;
  var previousName = 0;
  var previousSource = 0;
  var result = '';
  var next;
  var mapping;
  var nameIdx;
  var sourceIdx;

  var mappings = this._mappings.toArray();

  for (var i = 0, len = mappings.length; i < len; i++) {
    mapping = mappings[i];
    next = '';

    if (mapping.generatedLine !== previousGeneratedLine) {
      previousGeneratedColumn = 0;

      while (mapping.generatedLine !== previousGeneratedLine) {
        next += ';';
        previousGeneratedLine++;
      }
    } else {
      if (i > 0) {
        if (!util.compareByGeneratedPositionsInflated(mapping, mappings[i - 1])) {
          continue;
        }

        next += ',';
      }
    }

    next += base64VLQ.encode(mapping.generatedColumn - previousGeneratedColumn);
    previousGeneratedColumn = mapping.generatedColumn;

    if (mapping.source != null) {
      sourceIdx = this._sources.indexOf(mapping.source);
      next += base64VLQ.encode(sourceIdx - previousSource);
      previousSource = sourceIdx; // lines are stored 0-based in SourceMap spec version 3

      next += base64VLQ.encode(mapping.originalLine - 1 - previousOriginalLine);
      previousOriginalLine = mapping.originalLine - 1;
      next += base64VLQ.encode(mapping.originalColumn - previousOriginalColumn);
      previousOriginalColumn = mapping.originalColumn;

      if (mapping.name != null) {
        nameIdx = this._names.indexOf(mapping.name);
        next += base64VLQ.encode(nameIdx - previousName);
        previousName = nameIdx;
      }
    }

    result += next;
  }

  return result;
};

SourceMapGenerator.prototype._generateSourcesContent = function SourceMapGenerator_generateSourcesContent(aSources, aSourceRoot) {
  return aSources.map(function (source) {
    if (!this._sourcesContents) {
      return null;
    }

    if (aSourceRoot != null) {
      source = util.relative(aSourceRoot, source);
    }

    var key = util.toSetString(source);
    return Object.prototype.hasOwnProperty.call(this._sourcesContents, key) ? this._sourcesContents[key] : null;
  }, this);
};
/**
 * Externalize the source map.
 */


SourceMapGenerator.prototype.toJSON = function SourceMapGenerator_toJSON() {
  var map = {
    version: this._version,
    sources: this._sources.toArray(),
    names: this._names.toArray(),
    mappings: this._serializeMappings()
  };

  if (this._file != null) {
    map.file = this._file;
  }

  if (this._sourceRoot != null) {
    map.sourceRoot = this._sourceRoot;
  }

  if (this._sourcesContents) {
    map.sourcesContent = this._generateSourcesContent(map.sources, map.sourceRoot);
  }

  return map;
};
/**
 * Render the source map being generated to a string.
 */


SourceMapGenerator.prototype.toString = function SourceMapGenerator_toString() {
  return JSON.stringify(this.toJSON());
};

exports.SourceMapGenerator = SourceMapGenerator;

/***/ }),

/***/ "./node_modules/_source-map@0.6.1@source-map/lib/source-node.js":
/*!**********************************************************************!*\
  !*** ./node_modules/_source-map@0.6.1@source-map/lib/source-node.js ***!
  \**********************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

/* -*- Mode: js; js-indent-level: 2; -*- */

/*
 * Copyright 2011 Mozilla Foundation and contributors
 * Licensed under the New BSD license. See LICENSE or:
 * http://opensource.org/licenses/BSD-3-Clause
 */
var SourceMapGenerator = __webpack_require__(/*! ./source-map-generator */ "./node_modules/_source-map@0.6.1@source-map/lib/source-map-generator.js").SourceMapGenerator;

var util = __webpack_require__(/*! ./util */ "./node_modules/_source-map@0.6.1@source-map/lib/util.js"); // Matches a Windows-style `\r\n` newline or a `\n` newline used by all other
// operating systems these days (capturing the result).


var REGEX_NEWLINE = /(\r?\n)/; // Newline character code for charCodeAt() comparisons

var NEWLINE_CODE = 10; // Private symbol for identifying `SourceNode`s when multiple versions of
// the source-map library are loaded. This MUST NOT CHANGE across
// versions!

var isSourceNode = "$$$isSourceNode$$$";
/**
 * SourceNodes provide a way to abstract over interpolating/concatenating
 * snippets of generated JavaScript source code while maintaining the line and
 * column information associated with the original source code.
 *
 * @param aLine The original line number.
 * @param aColumn The original column number.
 * @param aSource The original source's filename.
 * @param aChunks Optional. An array of strings which are snippets of
 *        generated JS, or other SourceNodes.
 * @param aName The original identifier.
 */

function SourceNode(aLine, aColumn, aSource, aChunks, aName) {
  this.children = [];
  this.sourceContents = {};
  this.line = aLine == null ? null : aLine;
  this.column = aColumn == null ? null : aColumn;
  this.source = aSource == null ? null : aSource;
  this.name = aName == null ? null : aName;
  this[isSourceNode] = true;
  if (aChunks != null) this.add(aChunks);
}
/**
 * Creates a SourceNode from generated code and a SourceMapConsumer.
 *
 * @param aGeneratedCode The generated code
 * @param aSourceMapConsumer The SourceMap for the generated code
 * @param aRelativePath Optional. The path that relative sources in the
 *        SourceMapConsumer should be relative to.
 */


SourceNode.fromStringWithSourceMap = function SourceNode_fromStringWithSourceMap(aGeneratedCode, aSourceMapConsumer, aRelativePath) {
  // The SourceNode we want to fill with the generated code
  // and the SourceMap
  var node = new SourceNode(); // All even indices of this array are one line of the generated code,
  // while all odd indices are the newlines between two adjacent lines
  // (since `REGEX_NEWLINE` captures its match).
  // Processed fragments are accessed by calling `shiftNextLine`.

  var remainingLines = aGeneratedCode.split(REGEX_NEWLINE);
  var remainingLinesIndex = 0;

  var shiftNextLine = function shiftNextLine() {
    var lineContents = getNextLine(); // The last line of a file might not have a newline.

    var newLine = getNextLine() || "";
    return lineContents + newLine;

    function getNextLine() {
      return remainingLinesIndex < remainingLines.length ? remainingLines[remainingLinesIndex++] : undefined;
    }
  }; // We need to remember the position of "remainingLines"


  var lastGeneratedLine = 1,
      lastGeneratedColumn = 0; // The generate SourceNodes we need a code range.
  // To extract it current and last mapping is used.
  // Here we store the last mapping.

  var lastMapping = null;
  aSourceMapConsumer.eachMapping(function (mapping) {
    if (lastMapping !== null) {
      // We add the code from "lastMapping" to "mapping":
      // First check if there is a new line in between.
      if (lastGeneratedLine < mapping.generatedLine) {
        // Associate first line with "lastMapping"
        addMappingWithCode(lastMapping, shiftNextLine());
        lastGeneratedLine++;
        lastGeneratedColumn = 0; // The remaining code is added without mapping
      } else {
        // There is no new line in between.
        // Associate the code between "lastGeneratedColumn" and
        // "mapping.generatedColumn" with "lastMapping"
        var nextLine = remainingLines[remainingLinesIndex] || '';
        var code = nextLine.substr(0, mapping.generatedColumn - lastGeneratedColumn);
        remainingLines[remainingLinesIndex] = nextLine.substr(mapping.generatedColumn - lastGeneratedColumn);
        lastGeneratedColumn = mapping.generatedColumn;
        addMappingWithCode(lastMapping, code); // No more remaining code, continue

        lastMapping = mapping;
        return;
      }
    } // We add the generated code until the first mapping
    // to the SourceNode without any mapping.
    // Each line is added as separate string.


    while (lastGeneratedLine < mapping.generatedLine) {
      node.add(shiftNextLine());
      lastGeneratedLine++;
    }

    if (lastGeneratedColumn < mapping.generatedColumn) {
      var nextLine = remainingLines[remainingLinesIndex] || '';
      node.add(nextLine.substr(0, mapping.generatedColumn));
      remainingLines[remainingLinesIndex] = nextLine.substr(mapping.generatedColumn);
      lastGeneratedColumn = mapping.generatedColumn;
    }

    lastMapping = mapping;
  }, this); // We have processed all mappings.

  if (remainingLinesIndex < remainingLines.length) {
    if (lastMapping) {
      // Associate the remaining code in the current line with "lastMapping"
      addMappingWithCode(lastMapping, shiftNextLine());
    } // and add the remaining lines without any mapping


    node.add(remainingLines.splice(remainingLinesIndex).join(""));
  } // Copy sourcesContent into SourceNode


  aSourceMapConsumer.sources.forEach(function (sourceFile) {
    var content = aSourceMapConsumer.sourceContentFor(sourceFile);

    if (content != null) {
      if (aRelativePath != null) {
        sourceFile = util.join(aRelativePath, sourceFile);
      }

      node.setSourceContent(sourceFile, content);
    }
  });
  return node;

  function addMappingWithCode(mapping, code) {
    if (mapping === null || mapping.source === undefined) {
      node.add(code);
    } else {
      var source = aRelativePath ? util.join(aRelativePath, mapping.source) : mapping.source;
      node.add(new SourceNode(mapping.originalLine, mapping.originalColumn, source, code, mapping.name));
    }
  }
};
/**
 * Add a chunk of generated JS to this source node.
 *
 * @param aChunk A string snippet of generated JS code, another instance of
 *        SourceNode, or an array where each member is one of those things.
 */


SourceNode.prototype.add = function SourceNode_add(aChunk) {
  if (Array.isArray(aChunk)) {
    aChunk.forEach(function (chunk) {
      this.add(chunk);
    }, this);
  } else if (aChunk[isSourceNode] || typeof aChunk === "string") {
    if (aChunk) {
      this.children.push(aChunk);
    }
  } else {
    throw new TypeError("Expected a SourceNode, string, or an array of SourceNodes and strings. Got " + aChunk);
  }

  return this;
};
/**
 * Add a chunk of generated JS to the beginning of this source node.
 *
 * @param aChunk A string snippet of generated JS code, another instance of
 *        SourceNode, or an array where each member is one of those things.
 */


SourceNode.prototype.prepend = function SourceNode_prepend(aChunk) {
  if (Array.isArray(aChunk)) {
    for (var i = aChunk.length - 1; i >= 0; i--) {
      this.prepend(aChunk[i]);
    }
  } else if (aChunk[isSourceNode] || typeof aChunk === "string") {
    this.children.unshift(aChunk);
  } else {
    throw new TypeError("Expected a SourceNode, string, or an array of SourceNodes and strings. Got " + aChunk);
  }

  return this;
};
/**
 * Walk over the tree of JS snippets in this node and its children. The
 * walking function is called once for each snippet of JS and is passed that
 * snippet and the its original associated source's line/column location.
 *
 * @param aFn The traversal function.
 */


SourceNode.prototype.walk = function SourceNode_walk(aFn) {
  var chunk;

  for (var i = 0, len = this.children.length; i < len; i++) {
    chunk = this.children[i];

    if (chunk[isSourceNode]) {
      chunk.walk(aFn);
    } else {
      if (chunk !== '') {
        aFn(chunk, {
          source: this.source,
          line: this.line,
          column: this.column,
          name: this.name
        });
      }
    }
  }
};
/**
 * Like `String.prototype.join` except for SourceNodes. Inserts `aStr` between
 * each of `this.children`.
 *
 * @param aSep The separator.
 */


SourceNode.prototype.join = function SourceNode_join(aSep) {
  var newChildren;
  var i;
  var len = this.children.length;

  if (len > 0) {
    newChildren = [];

    for (i = 0; i < len - 1; i++) {
      newChildren.push(this.children[i]);
      newChildren.push(aSep);
    }

    newChildren.push(this.children[i]);
    this.children = newChildren;
  }

  return this;
};
/**
 * Call String.prototype.replace on the very right-most source snippet. Useful
 * for trimming whitespace from the end of a source node, etc.
 *
 * @param aPattern The pattern to replace.
 * @param aReplacement The thing to replace the pattern with.
 */


SourceNode.prototype.replaceRight = function SourceNode_replaceRight(aPattern, aReplacement) {
  var lastChild = this.children[this.children.length - 1];

  if (lastChild[isSourceNode]) {
    lastChild.replaceRight(aPattern, aReplacement);
  } else if (typeof lastChild === 'string') {
    this.children[this.children.length - 1] = lastChild.replace(aPattern, aReplacement);
  } else {
    this.children.push(''.replace(aPattern, aReplacement));
  }

  return this;
};
/**
 * Set the source content for a source file. This will be added to the SourceMapGenerator
 * in the sourcesContent field.
 *
 * @param aSourceFile The filename of the source file
 * @param aSourceContent The content of the source file
 */


SourceNode.prototype.setSourceContent = function SourceNode_setSourceContent(aSourceFile, aSourceContent) {
  this.sourceContents[util.toSetString(aSourceFile)] = aSourceContent;
};
/**
 * Walk over the tree of SourceNodes. The walking function is called for each
 * source file content and is passed the filename and source content.
 *
 * @param aFn The traversal function.
 */


SourceNode.prototype.walkSourceContents = function SourceNode_walkSourceContents(aFn) {
  for (var i = 0, len = this.children.length; i < len; i++) {
    if (this.children[i][isSourceNode]) {
      this.children[i].walkSourceContents(aFn);
    }
  }

  var sources = Object.keys(this.sourceContents);

  for (var i = 0, len = sources.length; i < len; i++) {
    aFn(util.fromSetString(sources[i]), this.sourceContents[sources[i]]);
  }
};
/**
 * Return the string representation of this source node. Walks over the tree
 * and concatenates all the various snippets together to one string.
 */


SourceNode.prototype.toString = function SourceNode_toString() {
  var str = "";
  this.walk(function (chunk) {
    str += chunk;
  });
  return str;
};
/**
 * Returns the string representation of this source node along with a source
 * map.
 */


SourceNode.prototype.toStringWithSourceMap = function SourceNode_toStringWithSourceMap(aArgs) {
  var generated = {
    code: "",
    line: 1,
    column: 0
  };
  var map = new SourceMapGenerator(aArgs);
  var sourceMappingActive = false;
  var lastOriginalSource = null;
  var lastOriginalLine = null;
  var lastOriginalColumn = null;
  var lastOriginalName = null;
  this.walk(function (chunk, original) {
    generated.code += chunk;

    if (original.source !== null && original.line !== null && original.column !== null) {
      if (lastOriginalSource !== original.source || lastOriginalLine !== original.line || lastOriginalColumn !== original.column || lastOriginalName !== original.name) {
        map.addMapping({
          source: original.source,
          original: {
            line: original.line,
            column: original.column
          },
          generated: {
            line: generated.line,
            column: generated.column
          },
          name: original.name
        });
      }

      lastOriginalSource = original.source;
      lastOriginalLine = original.line;
      lastOriginalColumn = original.column;
      lastOriginalName = original.name;
      sourceMappingActive = true;
    } else if (sourceMappingActive) {
      map.addMapping({
        generated: {
          line: generated.line,
          column: generated.column
        }
      });
      lastOriginalSource = null;
      sourceMappingActive = false;
    }

    for (var idx = 0, length = chunk.length; idx < length; idx++) {
      if (chunk.charCodeAt(idx) === NEWLINE_CODE) {
        generated.line++;
        generated.column = 0; // Mappings end at eol

        if (idx + 1 === length) {
          lastOriginalSource = null;
          sourceMappingActive = false;
        } else if (sourceMappingActive) {
          map.addMapping({
            source: original.source,
            original: {
              line: original.line,
              column: original.column
            },
            generated: {
              line: generated.line,
              column: generated.column
            },
            name: original.name
          });
        }
      } else {
        generated.column++;
      }
    }
  });
  this.walkSourceContents(function (sourceFile, sourceContent) {
    map.setSourceContent(sourceFile, sourceContent);
  });
  return {
    code: generated.code,
    map: map
  };
};

exports.SourceNode = SourceNode;

/***/ }),

/***/ "./node_modules/_source-map@0.6.1@source-map/lib/util.js":
/*!***************************************************************!*\
  !*** ./node_modules/_source-map@0.6.1@source-map/lib/util.js ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, exports) => {

/* -*- Mode: js; js-indent-level: 2; -*- */

/*
 * Copyright 2011 Mozilla Foundation and contributors
 * Licensed under the New BSD license. See LICENSE or:
 * http://opensource.org/licenses/BSD-3-Clause
 */

/**
 * This is a helper function for getting values from parameter/options
 * objects.
 *
 * @param args The object we are extracting values from
 * @param name The name of the property we are getting.
 * @param defaultValue An optional value to return if the property is missing
 * from the object. If this is not specified and the property is missing, an
 * error will be thrown.
 */
function getArg(aArgs, aName, aDefaultValue) {
  if (aName in aArgs) {
    return aArgs[aName];
  } else if (arguments.length === 3) {
    return aDefaultValue;
  } else {
    throw new Error('"' + aName + '" is a required argument.');
  }
}

exports.getArg = getArg;
var urlRegexp = /^(?:([\w+\-.]+):)?\/\/(?:(\w+:\w+)@)?([\w.-]*)(?::(\d+))?(.*)$/;
var dataUrlRegexp = /^data:.+\,.+$/;

function urlParse(aUrl) {
  var match = aUrl.match(urlRegexp);

  if (!match) {
    return null;
  }

  return {
    scheme: match[1],
    auth: match[2],
    host: match[3],
    port: match[4],
    path: match[5]
  };
}

exports.urlParse = urlParse;

function urlGenerate(aParsedUrl) {
  var url = '';

  if (aParsedUrl.scheme) {
    url += aParsedUrl.scheme + ':';
  }

  url += '//';

  if (aParsedUrl.auth) {
    url += aParsedUrl.auth + '@';
  }

  if (aParsedUrl.host) {
    url += aParsedUrl.host;
  }

  if (aParsedUrl.port) {
    url += ":" + aParsedUrl.port;
  }

  if (aParsedUrl.path) {
    url += aParsedUrl.path;
  }

  return url;
}

exports.urlGenerate = urlGenerate;
/**
 * Normalizes a path, or the path portion of a URL:
 *
 * - Replaces consecutive slashes with one slash.
 * - Removes unnecessary '.' parts.
 * - Removes unnecessary '<dir>/..' parts.
 *
 * Based on code in the Node.js 'path' core module.
 *
 * @param aPath The path or url to normalize.
 */

function normalize(aPath) {
  var path = aPath;
  var url = urlParse(aPath);

  if (url) {
    if (!url.path) {
      return aPath;
    }

    path = url.path;
  }

  var isAbsolute = exports.isAbsolute(path);
  var parts = path.split(/\/+/);

  for (var part, up = 0, i = parts.length - 1; i >= 0; i--) {
    part = parts[i];

    if (part === '.') {
      parts.splice(i, 1);
    } else if (part === '..') {
      up++;
    } else if (up > 0) {
      if (part === '') {
        // The first part is blank if the path is absolute. Trying to go
        // above the root is a no-op. Therefore we can remove all '..' parts
        // directly after the root.
        parts.splice(i + 1, up);
        up = 0;
      } else {
        parts.splice(i, 2);
        up--;
      }
    }
  }

  path = parts.join('/');

  if (path === '') {
    path = isAbsolute ? '/' : '.';
  }

  if (url) {
    url.path = path;
    return urlGenerate(url);
  }

  return path;
}

exports.normalize = normalize;
/**
 * Joins two paths/URLs.
 *
 * @param aRoot The root path or URL.
 * @param aPath The path or URL to be joined with the root.
 *
 * - If aPath is a URL or a data URI, aPath is returned, unless aPath is a
 *   scheme-relative URL: Then the scheme of aRoot, if any, is prepended
 *   first.
 * - Otherwise aPath is a path. If aRoot is a URL, then its path portion
 *   is updated with the result and aRoot is returned. Otherwise the result
 *   is returned.
 *   - If aPath is absolute, the result is aPath.
 *   - Otherwise the two paths are joined with a slash.
 * - Joining for example 'http://' and 'www.example.com' is also supported.
 */

function join(aRoot, aPath) {
  if (aRoot === "") {
    aRoot = ".";
  }

  if (aPath === "") {
    aPath = ".";
  }

  var aPathUrl = urlParse(aPath);
  var aRootUrl = urlParse(aRoot);

  if (aRootUrl) {
    aRoot = aRootUrl.path || '/';
  } // `join(foo, '//www.example.org')`


  if (aPathUrl && !aPathUrl.scheme) {
    if (aRootUrl) {
      aPathUrl.scheme = aRootUrl.scheme;
    }

    return urlGenerate(aPathUrl);
  }

  if (aPathUrl || aPath.match(dataUrlRegexp)) {
    return aPath;
  } // `join('http://', 'www.example.com')`


  if (aRootUrl && !aRootUrl.host && !aRootUrl.path) {
    aRootUrl.host = aPath;
    return urlGenerate(aRootUrl);
  }

  var joined = aPath.charAt(0) === '/' ? aPath : normalize(aRoot.replace(/\/+$/, '') + '/' + aPath);

  if (aRootUrl) {
    aRootUrl.path = joined;
    return urlGenerate(aRootUrl);
  }

  return joined;
}

exports.join = join;

exports.isAbsolute = function (aPath) {
  return aPath.charAt(0) === '/' || urlRegexp.test(aPath);
};
/**
 * Make a path relative to a URL or another path.
 *
 * @param aRoot The root path or URL.
 * @param aPath The path or URL to be made relative to aRoot.
 */


function relative(aRoot, aPath) {
  if (aRoot === "") {
    aRoot = ".";
  }

  aRoot = aRoot.replace(/\/$/, ''); // It is possible for the path to be above the root. In this case, simply
  // checking whether the root is a prefix of the path won't work. Instead, we
  // need to remove components from the root one by one, until either we find
  // a prefix that fits, or we run out of components to remove.

  var level = 0;

  while (aPath.indexOf(aRoot + '/') !== 0) {
    var index = aRoot.lastIndexOf("/");

    if (index < 0) {
      return aPath;
    } // If the only part of the root that is left is the scheme (i.e. http://,
    // file:///, etc.), one or more slashes (/), or simply nothing at all, we
    // have exhausted all components, so the path is not relative to the root.


    aRoot = aRoot.slice(0, index);

    if (aRoot.match(/^([^\/]+:\/)?\/*$/)) {
      return aPath;
    }

    ++level;
  } // Make sure we add a "../" for each component we removed from the root.


  return Array(level + 1).join("../") + aPath.substr(aRoot.length + 1);
}

exports.relative = relative;

var supportsNullProto = function () {
  var obj = Object.create(null);
  return !('__proto__' in obj);
}();

function identity(s) {
  return s;
}
/**
 * Because behavior goes wacky when you set `__proto__` on objects, we
 * have to prefix all the strings in our set with an arbitrary character.
 *
 * See https://github.com/mozilla/source-map/pull/31 and
 * https://github.com/mozilla/source-map/issues/30
 *
 * @param String aStr
 */


function toSetString(aStr) {
  if (isProtoString(aStr)) {
    return '$' + aStr;
  }

  return aStr;
}

exports.toSetString = supportsNullProto ? identity : toSetString;

function fromSetString(aStr) {
  if (isProtoString(aStr)) {
    return aStr.slice(1);
  }

  return aStr;
}

exports.fromSetString = supportsNullProto ? identity : fromSetString;

function isProtoString(s) {
  if (!s) {
    return false;
  }

  var length = s.length;

  if (length < 9
  /* "__proto__".length */
  ) {
      return false;
    }

  if (s.charCodeAt(length - 1) !== 95
  /* '_' */
  || s.charCodeAt(length - 2) !== 95
  /* '_' */
  || s.charCodeAt(length - 3) !== 111
  /* 'o' */
  || s.charCodeAt(length - 4) !== 116
  /* 't' */
  || s.charCodeAt(length - 5) !== 111
  /* 'o' */
  || s.charCodeAt(length - 6) !== 114
  /* 'r' */
  || s.charCodeAt(length - 7) !== 112
  /* 'p' */
  || s.charCodeAt(length - 8) !== 95
  /* '_' */
  || s.charCodeAt(length - 9) !== 95
  /* '_' */
  ) {
      return false;
    }

  for (var i = length - 10; i >= 0; i--) {
    if (s.charCodeAt(i) !== 36
    /* '$' */
    ) {
        return false;
      }
  }

  return true;
}
/**
 * Comparator between two mappings where the original positions are compared.
 *
 * Optionally pass in `true` as `onlyCompareGenerated` to consider two
 * mappings with the same original source/line/column, but different generated
 * line and column the same. Useful when searching for a mapping with a
 * stubbed out mapping.
 */


function compareByOriginalPositions(mappingA, mappingB, onlyCompareOriginal) {
  var cmp = strcmp(mappingA.source, mappingB.source);

  if (cmp !== 0) {
    return cmp;
  }

  cmp = mappingA.originalLine - mappingB.originalLine;

  if (cmp !== 0) {
    return cmp;
  }

  cmp = mappingA.originalColumn - mappingB.originalColumn;

  if (cmp !== 0 || onlyCompareOriginal) {
    return cmp;
  }

  cmp = mappingA.generatedColumn - mappingB.generatedColumn;

  if (cmp !== 0) {
    return cmp;
  }

  cmp = mappingA.generatedLine - mappingB.generatedLine;

  if (cmp !== 0) {
    return cmp;
  }

  return strcmp(mappingA.name, mappingB.name);
}

exports.compareByOriginalPositions = compareByOriginalPositions;
/**
 * Comparator between two mappings with deflated source and name indices where
 * the generated positions are compared.
 *
 * Optionally pass in `true` as `onlyCompareGenerated` to consider two
 * mappings with the same generated line and column, but different
 * source/name/original line and column the same. Useful when searching for a
 * mapping with a stubbed out mapping.
 */

function compareByGeneratedPositionsDeflated(mappingA, mappingB, onlyCompareGenerated) {
  var cmp = mappingA.generatedLine - mappingB.generatedLine;

  if (cmp !== 0) {
    return cmp;
  }

  cmp = mappingA.generatedColumn - mappingB.generatedColumn;

  if (cmp !== 0 || onlyCompareGenerated) {
    return cmp;
  }

  cmp = strcmp(mappingA.source, mappingB.source);

  if (cmp !== 0) {
    return cmp;
  }

  cmp = mappingA.originalLine - mappingB.originalLine;

  if (cmp !== 0) {
    return cmp;
  }

  cmp = mappingA.originalColumn - mappingB.originalColumn;

  if (cmp !== 0) {
    return cmp;
  }

  return strcmp(mappingA.name, mappingB.name);
}

exports.compareByGeneratedPositionsDeflated = compareByGeneratedPositionsDeflated;

function strcmp(aStr1, aStr2) {
  if (aStr1 === aStr2) {
    return 0;
  }

  if (aStr1 === null) {
    return 1; // aStr2 !== null
  }

  if (aStr2 === null) {
    return -1; // aStr1 !== null
  }

  if (aStr1 > aStr2) {
    return 1;
  }

  return -1;
}
/**
 * Comparator between two mappings with inflated source and name strings where
 * the generated positions are compared.
 */


function compareByGeneratedPositionsInflated(mappingA, mappingB) {
  var cmp = mappingA.generatedLine - mappingB.generatedLine;

  if (cmp !== 0) {
    return cmp;
  }

  cmp = mappingA.generatedColumn - mappingB.generatedColumn;

  if (cmp !== 0) {
    return cmp;
  }

  cmp = strcmp(mappingA.source, mappingB.source);

  if (cmp !== 0) {
    return cmp;
  }

  cmp = mappingA.originalLine - mappingB.originalLine;

  if (cmp !== 0) {
    return cmp;
  }

  cmp = mappingA.originalColumn - mappingB.originalColumn;

  if (cmp !== 0) {
    return cmp;
  }

  return strcmp(mappingA.name, mappingB.name);
}

exports.compareByGeneratedPositionsInflated = compareByGeneratedPositionsInflated;
/**
 * Strip any JSON XSSI avoidance prefix from the string (as documented
 * in the source maps specification), and then parse the string as
 * JSON.
 */

function parseSourceMapInput(str) {
  return JSON.parse(str.replace(/^\)]}'[^\n]*\n/, ''));
}

exports.parseSourceMapInput = parseSourceMapInput;
/**
 * Compute the URL of a source given the the source root, the source's
 * URL, and the source map's URL.
 */

function computeSourceURL(sourceRoot, sourceURL, sourceMapURL) {
  sourceURL = sourceURL || '';

  if (sourceRoot) {
    // This follows what Chrome does.
    if (sourceRoot[sourceRoot.length - 1] !== '/' && sourceURL[0] !== '/') {
      sourceRoot += '/';
    } // The spec says:
    //   Line 4: An optional source root, useful for relocating source
    //   files on a server or removing repeated values in the
    //   “sources” entry.  This value is prepended to the individual
    //   entries in the “source” field.


    sourceURL = sourceRoot + sourceURL;
  } // Historically, SourceMapConsumer did not take the sourceMapURL as
  // a parameter.  This mode is still somewhat supported, which is why
  // this code block is conditional.  However, it's preferable to pass
  // the source map URL to SourceMapConsumer, so that this function
  // can implement the source URL resolution algorithm as outlined in
  // the spec.  This block is basically the equivalent of:
  //    new URL(sourceURL, sourceMapURL).toString()
  // ... except it avoids using URL, which wasn't available in the
  // older releases of node still supported by this library.
  //
  // The spec says:
  //   If the sources are not absolute URLs after prepending of the
  //   “sourceRoot”, the sources are resolved relative to the
  //   SourceMap (like resolving script src in a html document).


  if (sourceMapURL) {
    var parsed = urlParse(sourceMapURL);

    if (!parsed) {
      throw new Error("sourceMapURL could not be parsed");
    }

    if (parsed.path) {
      // Strip the last path component, but keep the "/".
      var index = parsed.path.lastIndexOf('/');

      if (index >= 0) {
        parsed.path = parsed.path.substring(0, index + 1);
      }
    }

    sourceURL = join(urlGenerate(parsed), sourceURL);
  }

  return normalize(sourceURL);
}

exports.computeSourceURL = computeSourceURL;

/***/ }),

/***/ "./node_modules/_source-map@0.6.1@source-map/source-map.js":
/*!*****************************************************************!*\
  !*** ./node_modules/_source-map@0.6.1@source-map/source-map.js ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

/*
 * Copyright 2009-2011 Mozilla Foundation and contributors
 * Licensed under the New BSD license. See LICENSE.txt or:
 * http://opensource.org/licenses/BSD-3-Clause
 */
exports.SourceMapGenerator = __webpack_require__(/*! ./lib/source-map-generator */ "./node_modules/_source-map@0.6.1@source-map/lib/source-map-generator.js").SourceMapGenerator;
exports.SourceMapConsumer = __webpack_require__(/*! ./lib/source-map-consumer */ "./node_modules/_source-map@0.6.1@source-map/lib/source-map-consumer.js").SourceMapConsumer;
exports.SourceNode = __webpack_require__(/*! ./lib/source-node */ "./node_modules/_source-map@0.6.1@source-map/lib/source-node.js").SourceNode;

/***/ }),

/***/ "./node_modules/_source-map@0.7.3@source-map/lib/array-set.js":
/*!********************************************************************!*\
  !*** ./node_modules/_source-map@0.7.3@source-map/lib/array-set.js ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, exports) => {

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/* -*- Mode: js; js-indent-level: 2; -*- */

/*
 * Copyright 2011 Mozilla Foundation and contributors
 * Licensed under the New BSD license. See LICENSE or:
 * http://opensource.org/licenses/BSD-3-Clause
 */

/**
 * A data structure which is a combination of an array and a set. Adding a new
 * member is O(1), testing for membership is O(1), and finding the index of an
 * element is O(1). Removing elements from the set is not supported. Only
 * strings are supported for membership.
 */
var ArraySet = /*#__PURE__*/function () {
  function ArraySet() {
    _classCallCheck(this, ArraySet);

    this._array = [];
    this._set = new Map();
  }
  /**
   * Static method for creating ArraySet instances from an existing array.
   */


  _createClass(ArraySet, [{
    key: "size",
    value:
    /**
     * Return how many unique items are in this ArraySet. If duplicates have been
     * added, than those do not count towards the size.
     *
     * @returns Number
     */
    function size() {
      return this._set.size;
    }
    /**
     * Add the given string to this set.
     *
     * @param String aStr
     */

  }, {
    key: "add",
    value: function add(aStr, aAllowDuplicates) {
      var isDuplicate = this.has(aStr);
      var idx = this._array.length;

      if (!isDuplicate || aAllowDuplicates) {
        this._array.push(aStr);
      }

      if (!isDuplicate) {
        this._set.set(aStr, idx);
      }
    }
    /**
     * Is the given string a member of this set?
     *
     * @param String aStr
     */

  }, {
    key: "has",
    value: function has(aStr) {
      return this._set.has(aStr);
    }
    /**
     * What is the index of the given string in the array?
     *
     * @param String aStr
     */

  }, {
    key: "indexOf",
    value: function indexOf(aStr) {
      var idx = this._set.get(aStr);

      if (idx >= 0) {
        return idx;
      }

      throw new Error('"' + aStr + '" is not in the set.');
    }
    /**
     * What is the element at the given index?
     *
     * @param Number aIdx
     */

  }, {
    key: "at",
    value: function at(aIdx) {
      if (aIdx >= 0 && aIdx < this._array.length) {
        return this._array[aIdx];
      }

      throw new Error("No element indexed by " + aIdx);
    }
    /**
     * Returns the array representation of this set (which has the proper indices
     * indicated by indexOf). Note that this is a copy of the internal array used
     * for storing the members so that no one can mess with internal state.
     */

  }, {
    key: "toArray",
    value: function toArray() {
      return this._array.slice();
    }
  }], [{
    key: "fromArray",
    value: function fromArray(aArray, aAllowDuplicates) {
      var set = new ArraySet();

      for (var i = 0, len = aArray.length; i < len; i++) {
        set.add(aArray[i], aAllowDuplicates);
      }

      return set;
    }
  }]);

  return ArraySet;
}();

exports.ArraySet = ArraySet;

/***/ }),

/***/ "./node_modules/_source-map@0.7.3@source-map/lib/base64-vlq.js":
/*!*********************************************************************!*\
  !*** ./node_modules/_source-map@0.7.3@source-map/lib/base64-vlq.js ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

/* -*- Mode: js; js-indent-level: 2; -*- */

/*
 * Copyright 2011 Mozilla Foundation and contributors
 * Licensed under the New BSD license. See LICENSE or:
 * http://opensource.org/licenses/BSD-3-Clause
 *
 * Based on the Base 64 VLQ implementation in Closure Compiler:
 * https://code.google.com/p/closure-compiler/source/browse/trunk/src/com/google/debugging/sourcemap/Base64VLQ.java
 *
 * Copyright 2011 The Closure Compiler Authors. All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the following
 *    disclaimer in the documentation and/or other materials provided
 *    with the distribution.
 *  * Neither the name of Google Inc. nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
var base64 = __webpack_require__(/*! ./base64 */ "./node_modules/_source-map@0.7.3@source-map/lib/base64.js"); // A single base 64 digit can contain 6 bits of data. For the base 64 variable
// length quantities we use in the source map spec, the first bit is the sign,
// the next four bits are the actual value, and the 6th bit is the
// continuation bit. The continuation bit tells us whether there are more
// digits in this value following this digit.
//
//   Continuation
//   |    Sign
//   |    |
//   V    V
//   101011


var VLQ_BASE_SHIFT = 5; // binary: 100000

var VLQ_BASE = 1 << VLQ_BASE_SHIFT; // binary: 011111

var VLQ_BASE_MASK = VLQ_BASE - 1; // binary: 100000

var VLQ_CONTINUATION_BIT = VLQ_BASE;
/**
 * Converts from a two-complement value to a value where the sign bit is
 * placed in the least significant bit.  For example, as decimals:
 *   1 becomes 2 (10 binary), -1 becomes 3 (11 binary)
 *   2 becomes 4 (100 binary), -2 becomes 5 (101 binary)
 */

function toVLQSigned(aValue) {
  return aValue < 0 ? (-aValue << 1) + 1 : (aValue << 1) + 0;
}
/**
 * Converts to a two-complement value from a value where the sign bit is
 * placed in the least significant bit.  For example, as decimals:
 *   2 (10 binary) becomes 1, 3 (11 binary) becomes -1
 *   4 (100 binary) becomes 2, 5 (101 binary) becomes -2
 */
// eslint-disable-next-line no-unused-vars


function fromVLQSigned(aValue) {
  var isNegative = (aValue & 1) === 1;
  var shifted = aValue >> 1;
  return isNegative ? -shifted : shifted;
}
/**
 * Returns the base 64 VLQ encoded value.
 */


exports.encode = function base64VLQ_encode(aValue) {
  var encoded = "";
  var digit;
  var vlq = toVLQSigned(aValue);

  do {
    digit = vlq & VLQ_BASE_MASK;
    vlq >>>= VLQ_BASE_SHIFT;

    if (vlq > 0) {
      // There are still more digits in this value, so we must make sure the
      // continuation bit is marked.
      digit |= VLQ_CONTINUATION_BIT;
    }

    encoded += base64.encode(digit);
  } while (vlq > 0);

  return encoded;
};

/***/ }),

/***/ "./node_modules/_source-map@0.7.3@source-map/lib/base64.js":
/*!*****************************************************************!*\
  !*** ./node_modules/_source-map@0.7.3@source-map/lib/base64.js ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, exports) => {

/* -*- Mode: js; js-indent-level: 2; -*- */

/*
 * Copyright 2011 Mozilla Foundation and contributors
 * Licensed under the New BSD license. See LICENSE or:
 * http://opensource.org/licenses/BSD-3-Clause
 */
var intToCharMap = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".split("");
/**
 * Encode an integer in the range of 0 to 63 to a single base 64 digit.
 */

exports.encode = function (number) {
  if (0 <= number && number < intToCharMap.length) {
    return intToCharMap[number];
  }

  throw new TypeError("Must be between 0 and 63: " + number);
};

/***/ }),

/***/ "./node_modules/_source-map@0.7.3@source-map/lib/binary-search.js":
/*!************************************************************************!*\
  !*** ./node_modules/_source-map@0.7.3@source-map/lib/binary-search.js ***!
  \************************************************************************/
/***/ ((__unused_webpack_module, exports) => {

/* -*- Mode: js; js-indent-level: 2; -*- */

/*
 * Copyright 2011 Mozilla Foundation and contributors
 * Licensed under the New BSD license. See LICENSE or:
 * http://opensource.org/licenses/BSD-3-Clause
 */
exports.GREATEST_LOWER_BOUND = 1;
exports.LEAST_UPPER_BOUND = 2;
/**
 * Recursive implementation of binary search.
 *
 * @param aLow Indices here and lower do not contain the needle.
 * @param aHigh Indices here and higher do not contain the needle.
 * @param aNeedle The element being searched for.
 * @param aHaystack The non-empty array being searched.
 * @param aCompare Function which takes two elements and returns -1, 0, or 1.
 * @param aBias Either 'binarySearch.GREATEST_LOWER_BOUND' or
 *     'binarySearch.LEAST_UPPER_BOUND'. Specifies whether to return the
 *     closest element that is smaller than or greater than the one we are
 *     searching for, respectively, if the exact element cannot be found.
 */

function recursiveSearch(aLow, aHigh, aNeedle, aHaystack, aCompare, aBias) {
  // This function terminates when one of the following is true:
  //
  //   1. We find the exact element we are looking for.
  //
  //   2. We did not find the exact element, but we can return the index of
  //      the next-closest element.
  //
  //   3. We did not find the exact element, and there is no next-closest
  //      element than the one we are searching for, so we return -1.
  var mid = Math.floor((aHigh - aLow) / 2) + aLow;
  var cmp = aCompare(aNeedle, aHaystack[mid], true);

  if (cmp === 0) {
    // Found the element we are looking for.
    return mid;
  } else if (cmp > 0) {
    // Our needle is greater than aHaystack[mid].
    if (aHigh - mid > 1) {
      // The element is in the upper half.
      return recursiveSearch(mid, aHigh, aNeedle, aHaystack, aCompare, aBias);
    } // The exact needle element was not found in this haystack. Determine if
    // we are in termination case (3) or (2) and return the appropriate thing.


    if (aBias == exports.LEAST_UPPER_BOUND) {
      return aHigh < aHaystack.length ? aHigh : -1;
    }

    return mid;
  } // Our needle is less than aHaystack[mid].


  if (mid - aLow > 1) {
    // The element is in the lower half.
    return recursiveSearch(aLow, mid, aNeedle, aHaystack, aCompare, aBias);
  } // we are in termination case (3) or (2) and return the appropriate thing.


  if (aBias == exports.LEAST_UPPER_BOUND) {
    return mid;
  }

  return aLow < 0 ? -1 : aLow;
}
/**
 * This is an implementation of binary search which will always try and return
 * the index of the closest element if there is no exact hit. This is because
 * mappings between original and generated line/col pairs are single points,
 * and there is an implicit region between each of them, so a miss just means
 * that you aren't on the very start of a region.
 *
 * @param aNeedle The element you are looking for.
 * @param aHaystack The array that is being searched.
 * @param aCompare A function which takes the needle and an element in the
 *     array and returns -1, 0, or 1 depending on whether the needle is less
 *     than, equal to, or greater than the element, respectively.
 * @param aBias Either 'binarySearch.GREATEST_LOWER_BOUND' or
 *     'binarySearch.LEAST_UPPER_BOUND'. Specifies whether to return the
 *     closest element that is smaller than or greater than the one we are
 *     searching for, respectively, if the exact element cannot be found.
 *     Defaults to 'binarySearch.GREATEST_LOWER_BOUND'.
 */


exports.search = function search(aNeedle, aHaystack, aCompare, aBias) {
  if (aHaystack.length === 0) {
    return -1;
  }

  var index = recursiveSearch(-1, aHaystack.length, aNeedle, aHaystack, aCompare, aBias || exports.GREATEST_LOWER_BOUND);

  if (index < 0) {
    return -1;
  } // We have found either the exact element, or the next-closest element than
  // the one we are searching for. However, there may be more than one such
  // element. Make sure we always return the smallest of these.


  while (index - 1 >= 0) {
    if (aCompare(aHaystack[index], aHaystack[index - 1], true) !== 0) {
      break;
    }

    --index;
  }

  return index;
};

/***/ }),

/***/ "./node_modules/_source-map@0.7.3@source-map/lib/mapping-list.js":
/*!***********************************************************************!*\
  !*** ./node_modules/_source-map@0.7.3@source-map/lib/mapping-list.js ***!
  \***********************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/* -*- Mode: js; js-indent-level: 2; -*- */

/*
 * Copyright 2014 Mozilla Foundation and contributors
 * Licensed under the New BSD license. See LICENSE or:
 * http://opensource.org/licenses/BSD-3-Clause
 */
var util = __webpack_require__(/*! ./util */ "./node_modules/_source-map@0.7.3@source-map/lib/util.js");
/**
 * Determine whether mappingB is after mappingA with respect to generated
 * position.
 */


function generatedPositionAfter(mappingA, mappingB) {
  // Optimized for most common case
  var lineA = mappingA.generatedLine;
  var lineB = mappingB.generatedLine;
  var columnA = mappingA.generatedColumn;
  var columnB = mappingB.generatedColumn;
  return lineB > lineA || lineB == lineA && columnB >= columnA || util.compareByGeneratedPositionsInflated(mappingA, mappingB) <= 0;
}
/**
 * A data structure to provide a sorted view of accumulated mappings in a
 * performance conscious manner. It trades a negligible overhead in general
 * case for a large speedup in case of mappings being added in order.
 */


var MappingList = /*#__PURE__*/function () {
  function MappingList() {
    _classCallCheck(this, MappingList);

    this._array = [];
    this._sorted = true; // Serves as infimum

    this._last = {
      generatedLine: -1,
      generatedColumn: 0
    };
  }
  /**
   * Iterate through internal items. This method takes the same arguments that
   * `Array.prototype.forEach` takes.
   *
   * NOTE: The order of the mappings is NOT guaranteed.
   */


  _createClass(MappingList, [{
    key: "unsortedForEach",
    value: function unsortedForEach(aCallback, aThisArg) {
      this._array.forEach(aCallback, aThisArg);
    }
    /**
     * Add the given source mapping.
     *
     * @param Object aMapping
     */

  }, {
    key: "add",
    value: function add(aMapping) {
      if (generatedPositionAfter(this._last, aMapping)) {
        this._last = aMapping;

        this._array.push(aMapping);
      } else {
        this._sorted = false;

        this._array.push(aMapping);
      }
    }
    /**
     * Returns the flat, sorted array of mappings. The mappings are sorted by
     * generated position.
     *
     * WARNING: This method returns internal data without copying, for
     * performance. The return value must NOT be mutated, and should be treated as
     * an immutable borrow. If you want to take ownership, you must make your own
     * copy.
     */

  }, {
    key: "toArray",
    value: function toArray() {
      if (!this._sorted) {
        this._array.sort(util.compareByGeneratedPositionsInflated);

        this._sorted = true;
      }

      return this._array;
    }
  }]);

  return MappingList;
}();

exports.MappingList = MappingList;

/***/ }),

/***/ "./node_modules/_source-map@0.7.3@source-map/lib/read-wasm.js":
/*!********************************************************************!*\
  !*** ./node_modules/_source-map@0.7.3@source-map/lib/read-wasm.js ***!
  \********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var __dirname = "/";
if (typeof fetch === "function") {
  // Web version of reading a wasm file into an array buffer.
  var mappingsWasmUrl = null;

  module.exports = function readWasm() {
    if (typeof mappingsWasmUrl !== "string") {
      throw new Error("You must provide the URL of lib/mappings.wasm by calling " + "SourceMapConsumer.initialize({ 'lib/mappings.wasm': ... }) " + "before using SourceMapConsumer");
    }

    return fetch(mappingsWasmUrl).then(function (response) {
      return response.arrayBuffer();
    });
  };

  module.exports.initialize = function (url) {
    return mappingsWasmUrl = url;
  };
} else {
  // Node version of reading a wasm file into an array buffer.
  var fs = __webpack_require__(/*! fs */ "?e413");

  var path = __webpack_require__(/*! path */ "?5bde");

  module.exports = function readWasm() {
    return new Promise(function (resolve, reject) {
      var wasmPath = path.join(__dirname, "mappings.wasm");
      fs.readFile(wasmPath, null, function (error, data) {
        if (error) {
          reject(error);
          return;
        }

        resolve(data.buffer);
      });
    });
  };

  module.exports.initialize = function (_) {
    console.debug("SourceMapConsumer.initialize is a no-op when running in node.js");
  };
}

/***/ }),

/***/ "./node_modules/_source-map@0.7.3@source-map/lib/source-map-consumer.js":
/*!******************************************************************************!*\
  !*** ./node_modules/_source-map@0.7.3@source-map/lib/source-map-consumer.js ***!
  \******************************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/* -*- Mode: js; js-indent-level: 2; -*- */

/*
 * Copyright 2011 Mozilla Foundation and contributors
 * Licensed under the New BSD license. See LICENSE or:
 * http://opensource.org/licenses/BSD-3-Clause
 */
var util = __webpack_require__(/*! ./util */ "./node_modules/_source-map@0.7.3@source-map/lib/util.js");

var binarySearch = __webpack_require__(/*! ./binary-search */ "./node_modules/_source-map@0.7.3@source-map/lib/binary-search.js");

var ArraySet = __webpack_require__(/*! ./array-set */ "./node_modules/_source-map@0.7.3@source-map/lib/array-set.js").ArraySet;

var base64VLQ = __webpack_require__(/*! ./base64-vlq */ "./node_modules/_source-map@0.7.3@source-map/lib/base64-vlq.js"); // eslint-disable-line no-unused-vars


var readWasm = __webpack_require__(/*! ../lib/read-wasm */ "./node_modules/_source-map@0.7.3@source-map/lib/read-wasm.js");

var wasm = __webpack_require__(/*! ./wasm */ "./node_modules/_source-map@0.7.3@source-map/lib/wasm.js");

var INTERNAL = Symbol("smcInternal");

var SourceMapConsumer = /*#__PURE__*/function () {
  function SourceMapConsumer(aSourceMap, aSourceMapURL) {
    _classCallCheck(this, SourceMapConsumer);

    // If the constructor was called by super(), just return Promise<this>.
    // Yes, this is a hack to retain the pre-existing API of the base-class
    // constructor also being an async factory function.
    if (aSourceMap == INTERNAL) {
      return Promise.resolve(this);
    }

    return _factory(aSourceMap, aSourceMapURL);
  }

  _createClass(SourceMapConsumer, [{
    key: "_parseMappings",
    value:
    /**
     * Parse the mappings in a string in to a data structure which we can easily
     * query (the ordered arrays in the `this.__generatedMappings` and
     * `this.__originalMappings` properties).
     */
    function _parseMappings(aStr, aSourceRoot) {
      throw new Error("Subclasses must implement _parseMappings");
    }
    /**
     * Iterate over each mapping between an original source/line/column and a
     * generated line/column in this source map.
     *
     * @param Function aCallback
     *        The function that is called with each mapping.
     * @param Object aContext
     *        Optional. If specified, this object will be the value of `this` every
     *        time that `aCallback` is called.
     * @param aOrder
     *        Either `SourceMapConsumer.GENERATED_ORDER` or
     *        `SourceMapConsumer.ORIGINAL_ORDER`. Specifies whether you want to
     *        iterate over the mappings sorted by the generated file's line/column
     *        order or the original's source/line/column order, respectively. Defaults to
     *        `SourceMapConsumer.GENERATED_ORDER`.
     */

  }, {
    key: "eachMapping",
    value: function eachMapping(aCallback, aContext, aOrder) {
      throw new Error("Subclasses must implement eachMapping");
    }
    /**
     * Returns all generated line and column information for the original source,
     * line, and column provided. If no column is provided, returns all mappings
     * corresponding to a either the line we are searching for or the next
     * closest line that has any mappings. Otherwise, returns all mappings
     * corresponding to the given line and either the column we are searching for
     * or the next closest column that has any offsets.
     *
     * The only argument is an object with the following properties:
     *
     *   - source: The filename of the original source.
     *   - line: The line number in the original source.  The line number is 1-based.
     *   - column: Optional. the column number in the original source.
     *    The column number is 0-based.
     *
     * and an array of objects is returned, each with the following properties:
     *
     *   - line: The line number in the generated source, or null.  The
     *    line number is 1-based.
     *   - column: The column number in the generated source, or null.
     *    The column number is 0-based.
     */

  }, {
    key: "allGeneratedPositionsFor",
    value: function allGeneratedPositionsFor(aArgs) {
      throw new Error("Subclasses must implement allGeneratedPositionsFor");
    }
  }, {
    key: "destroy",
    value: function destroy() {
      throw new Error("Subclasses must implement destroy");
    }
  }], [{
    key: "initialize",
    value: function initialize(opts) {
      readWasm.initialize(opts["lib/mappings.wasm"]);
    }
  }, {
    key: "fromSourceMap",
    value: function fromSourceMap(aSourceMap, aSourceMapURL) {
      return _factoryBSM(aSourceMap, aSourceMapURL);
    }
    /**
     * Construct a new `SourceMapConsumer` from `rawSourceMap` and `sourceMapUrl`
     * (see the `SourceMapConsumer` constructor for details. Then, invoke the `async
     * function f(SourceMapConsumer) -> T` with the newly constructed consumer, wait
     * for `f` to complete, call `destroy` on the consumer, and return `f`'s return
     * value.
     *
     * You must not use the consumer after `f` completes!
     *
     * By using `with`, you do not have to remember to manually call `destroy` on
     * the consumer, since it will be called automatically once `f` completes.
     *
     * ```js
     * const xSquared = await SourceMapConsumer.with(
     *   myRawSourceMap,
     *   null,
     *   async function (consumer) {
     *     // Use `consumer` inside here and don't worry about remembering
     *     // to call `destroy`.
     *
     *     const x = await whatever(consumer);
     *     return x * x;
     *   }
     * );
     *
     * // You may not use that `consumer` anymore out here; it has
     * // been destroyed. But you can use `xSquared`.
     * console.log(xSquared);
     * ```
     */

  }, {
    key: "with",
    value: function _with(rawSourceMap, sourceMapUrl, f) {
      // Note: The `acorn` version that `webpack` currently depends on doesn't
      // support `async` functions, and the nodes that we support don't all have
      // `.finally`. Therefore, this is written a bit more convolutedly than it
      // should really be.
      var consumer = null;
      var promise = new SourceMapConsumer(rawSourceMap, sourceMapUrl);
      return promise.then(function (c) {
        consumer = c;
        return f(c);
      }).then(function (x) {
        if (consumer) {
          consumer.destroy();
        }

        return x;
      }, function (e) {
        if (consumer) {
          consumer.destroy();
        }

        throw e;
      });
    }
  }]);

  return SourceMapConsumer;
}();
/**
 * The version of the source mapping spec that we are consuming.
 */


SourceMapConsumer.prototype._version = 3;
SourceMapConsumer.GENERATED_ORDER = 1;
SourceMapConsumer.ORIGINAL_ORDER = 2;
SourceMapConsumer.GREATEST_LOWER_BOUND = 1;
SourceMapConsumer.LEAST_UPPER_BOUND = 2;
exports.SourceMapConsumer = SourceMapConsumer;
/**
 * A BasicSourceMapConsumer instance represents a parsed source map which we can
 * query for information about the original file positions by giving it a file
 * position in the generated source.
 *
 * The first parameter is the raw source map (either as a JSON string, or
 * already parsed to an object). According to the spec, source maps have the
 * following attributes:
 *
 *   - version: Which version of the source map spec this map is following.
 *   - sources: An array of URLs to the original source files.
 *   - names: An array of identifiers which can be referenced by individual mappings.
 *   - sourceRoot: Optional. The URL root from which all sources are relative.
 *   - sourcesContent: Optional. An array of contents of the original source files.
 *   - mappings: A string of base64 VLQs which contain the actual mappings.
 *   - file: Optional. The generated file this source map is associated with.
 *
 * Here is an example source map, taken from the source map spec[0]:
 *
 *     {
 *       version : 3,
 *       file: "out.js",
 *       sourceRoot : "",
 *       sources: ["foo.js", "bar.js"],
 *       names: ["src", "maps", "are", "fun"],
 *       mappings: "AA,AB;;ABCDE;"
 *     }
 *
 * The second parameter, if given, is a string whose value is the URL
 * at which the source map was found.  This URL is used to compute the
 * sources array.
 *
 * [0]: https://docs.google.com/document/d/1U1RGAehQwRypUTovF1KRlpiOFze0b-_2gc6fAH0KY0k/edit?pli=1#
 */

var BasicSourceMapConsumer = /*#__PURE__*/function (_SourceMapConsumer) {
  _inherits(BasicSourceMapConsumer, _SourceMapConsumer);

  var _super = _createSuper(BasicSourceMapConsumer);

  function BasicSourceMapConsumer(aSourceMap, aSourceMapURL) {
    var _this;

    _classCallCheck(this, BasicSourceMapConsumer);

    return _possibleConstructorReturn(_this, (_this = _super.call(this, INTERNAL)).then(function (that) {
      var sourceMap = aSourceMap;

      if (typeof aSourceMap === "string") {
        sourceMap = util.parseSourceMapInput(aSourceMap);
      }

      var version = util.getArg(sourceMap, "version");
      var sources = util.getArg(sourceMap, "sources"); // Sass 3.3 leaves out the 'names' array, so we deviate from the spec (which
      // requires the array) to play nice here.

      var names = util.getArg(sourceMap, "names", []);
      var sourceRoot = util.getArg(sourceMap, "sourceRoot", null);
      var sourcesContent = util.getArg(sourceMap, "sourcesContent", null);
      var mappings = util.getArg(sourceMap, "mappings");
      var file = util.getArg(sourceMap, "file", null); // Once again, Sass deviates from the spec and supplies the version as a
      // string rather than a number, so we use loose equality checking here.

      if (version != that._version) {
        throw new Error("Unsupported version: " + version);
      }

      if (sourceRoot) {
        sourceRoot = util.normalize(sourceRoot);
      }

      sources = sources.map(String) // Some source maps produce relative source paths like "./foo.js" instead of
      // "foo.js".  Normalize these first so that future comparisons will succeed.
      // See bugzil.la/1090768.
      .map(util.normalize) // Always ensure that absolute sources are internally stored relative to
      // the source root, if the source root is absolute. Not doing this would
      // be particularly problematic when the source root is a prefix of the
      // source (valid, but why??). See github issue #199 and bugzil.la/1188982.
      .map(function (source) {
        return sourceRoot && util.isAbsolute(sourceRoot) && util.isAbsolute(source) ? util.relative(sourceRoot, source) : source;
      }); // Pass `true` below to allow duplicate names and sources. While source maps
      // are intended to be compressed and deduplicated, the TypeScript compiler
      // sometimes generates source maps with duplicates in them. See Github issue
      // #72 and bugzil.la/889492.

      that._names = ArraySet.fromArray(names.map(String), true);
      that._sources = ArraySet.fromArray(sources, true);
      that._absoluteSources = that._sources.toArray().map(function (s) {
        return util.computeSourceURL(sourceRoot, s, aSourceMapURL);
      });
      that.sourceRoot = sourceRoot;
      that.sourcesContent = sourcesContent;
      that._mappings = mappings;
      that._sourceMapURL = aSourceMapURL;
      that.file = file;
      that._computedColumnSpans = false;
      that._mappingsPtr = 0;
      that._wasm = null;
      return wasm().then(function (w) {
        that._wasm = w;
        return that;
      });
    }));
  }
  /**
   * Utility function to find the index of a source.  Returns -1 if not
   * found.
   */


  _createClass(BasicSourceMapConsumer, [{
    key: "_findSourceIndex",
    value: function _findSourceIndex(aSource) {
      var relativeSource = aSource;

      if (this.sourceRoot != null) {
        relativeSource = util.relative(this.sourceRoot, relativeSource);
      }

      if (this._sources.has(relativeSource)) {
        return this._sources.indexOf(relativeSource);
      } // Maybe aSource is an absolute URL as returned by |sources|.  In
      // this case we can't simply undo the transform.


      for (var i = 0; i < this._absoluteSources.length; ++i) {
        if (this._absoluteSources[i] == aSource) {
          return i;
        }
      }

      return -1;
    }
    /**
     * Create a BasicSourceMapConsumer from a SourceMapGenerator.
     *
     * @param SourceMapGenerator aSourceMap
     *        The source map that will be consumed.
     * @param String aSourceMapURL
     *        The URL at which the source map can be found (optional)
     * @returns BasicSourceMapConsumer
     */

  }, {
    key: "sources",
    get: function get() {
      return this._absoluteSources.slice();
    }
  }, {
    key: "_getMappingsPtr",
    value: function _getMappingsPtr() {
      if (this._mappingsPtr === 0) {
        this._parseMappings(this._mappings, this.sourceRoot);
      }

      return this._mappingsPtr;
    }
    /**
     * Parse the mappings in a string in to a data structure which we can easily
     * query (the ordered arrays in the `this.__generatedMappings` and
     * `this.__originalMappings` properties).
     */

  }, {
    key: "_parseMappings",
    value: function _parseMappings(aStr, aSourceRoot) {
      var size = aStr.length;

      var mappingsBufPtr = this._wasm.exports.allocate_mappings(size);

      var mappingsBuf = new Uint8Array(this._wasm.exports.memory.buffer, mappingsBufPtr, size);

      for (var i = 0; i < size; i++) {
        mappingsBuf[i] = aStr.charCodeAt(i);
      }

      var mappingsPtr = this._wasm.exports.parse_mappings(mappingsBufPtr);

      if (!mappingsPtr) {
        var error = this._wasm.exports.get_last_error();

        var msg = "Error parsing mappings (code ".concat(error, "): "); // XXX: keep these error codes in sync with `fitzgen/source-map-mappings`.

        switch (error) {
          case 1:
            msg += "the mappings contained a negative line, column, source index, or name index";
            break;

          case 2:
            msg += "the mappings contained a number larger than 2**32";
            break;

          case 3:
            msg += "reached EOF while in the middle of parsing a VLQ";
            break;

          case 4:
            msg += "invalid base 64 character while parsing a VLQ";
            break;

          default:
            msg += "unknown error code";
            break;
        }

        throw new Error(msg);
      }

      this._mappingsPtr = mappingsPtr;
    }
  }, {
    key: "eachMapping",
    value: function eachMapping(aCallback, aContext, aOrder) {
      var _this2 = this;

      var context = aContext || null;
      var order = aOrder || SourceMapConsumer.GENERATED_ORDER;
      var sourceRoot = this.sourceRoot;

      this._wasm.withMappingCallback(function (mapping) {
        if (mapping.source !== null) {
          mapping.source = _this2._sources.at(mapping.source);
          mapping.source = util.computeSourceURL(sourceRoot, mapping.source, _this2._sourceMapURL);

          if (mapping.name !== null) {
            mapping.name = _this2._names.at(mapping.name);
          }
        }

        aCallback.call(context, mapping);
      }, function () {
        switch (order) {
          case SourceMapConsumer.GENERATED_ORDER:
            _this2._wasm.exports.by_generated_location(_this2._getMappingsPtr());

            break;

          case SourceMapConsumer.ORIGINAL_ORDER:
            _this2._wasm.exports.by_original_location(_this2._getMappingsPtr());

            break;

          default:
            throw new Error("Unknown order of iteration.");
        }
      });
    }
  }, {
    key: "allGeneratedPositionsFor",
    value: function allGeneratedPositionsFor(aArgs) {
      var _this3 = this;

      var source = util.getArg(aArgs, "source");
      var originalLine = util.getArg(aArgs, "line");
      var originalColumn = aArgs.column || 0;
      source = this._findSourceIndex(source);

      if (source < 0) {
        return [];
      }

      if (originalLine < 1) {
        throw new Error("Line numbers must be >= 1");
      }

      if (originalColumn < 0) {
        throw new Error("Column numbers must be >= 0");
      }

      var mappings = [];

      this._wasm.withMappingCallback(function (m) {
        var lastColumn = m.lastGeneratedColumn;

        if (_this3._computedColumnSpans && lastColumn === null) {
          lastColumn = Infinity;
        }

        mappings.push({
          line: m.generatedLine,
          column: m.generatedColumn,
          lastColumn: lastColumn
        });
      }, function () {
        _this3._wasm.exports.all_generated_locations_for(_this3._getMappingsPtr(), source, originalLine - 1, "column" in aArgs, originalColumn);
      });

      return mappings;
    }
  }, {
    key: "destroy",
    value: function destroy() {
      if (this._mappingsPtr !== 0) {
        this._wasm.exports.free_mappings(this._mappingsPtr);

        this._mappingsPtr = 0;
      }
    }
    /**
     * Compute the last column for each generated mapping. The last column is
     * inclusive.
     */

  }, {
    key: "computeColumnSpans",
    value: function computeColumnSpans() {
      if (this._computedColumnSpans) {
        return;
      }

      this._wasm.exports.compute_column_spans(this._getMappingsPtr());

      this._computedColumnSpans = true;
    }
    /**
     * Returns the original source, line, and column information for the generated
     * source's line and column positions provided. The only argument is an object
     * with the following properties:
     *
     *   - line: The line number in the generated source.  The line number
     *     is 1-based.
     *   - column: The column number in the generated source.  The column
     *     number is 0-based.
     *   - bias: Either 'SourceMapConsumer.GREATEST_LOWER_BOUND' or
     *     'SourceMapConsumer.LEAST_UPPER_BOUND'. Specifies whether to return the
     *     closest element that is smaller than or greater than the one we are
     *     searching for, respectively, if the exact element cannot be found.
     *     Defaults to 'SourceMapConsumer.GREATEST_LOWER_BOUND'.
     *
     * and an object is returned with the following properties:
     *
     *   - source: The original source file, or null.
     *   - line: The line number in the original source, or null.  The
     *     line number is 1-based.
     *   - column: The column number in the original source, or null.  The
     *     column number is 0-based.
     *   - name: The original identifier, or null.
     */

  }, {
    key: "originalPositionFor",
    value: function originalPositionFor(aArgs) {
      var _this4 = this;

      var needle = {
        generatedLine: util.getArg(aArgs, "line"),
        generatedColumn: util.getArg(aArgs, "column")
      };

      if (needle.generatedLine < 1) {
        throw new Error("Line numbers must be >= 1");
      }

      if (needle.generatedColumn < 0) {
        throw new Error("Column numbers must be >= 0");
      }

      var bias = util.getArg(aArgs, "bias", SourceMapConsumer.GREATEST_LOWER_BOUND);

      if (bias == null) {
        bias = SourceMapConsumer.GREATEST_LOWER_BOUND;
      }

      var mapping;

      this._wasm.withMappingCallback(function (m) {
        return mapping = m;
      }, function () {
        _this4._wasm.exports.original_location_for(_this4._getMappingsPtr(), needle.generatedLine - 1, needle.generatedColumn, bias);
      });

      if (mapping) {
        if (mapping.generatedLine === needle.generatedLine) {
          var source = util.getArg(mapping, "source", null);

          if (source !== null) {
            source = this._sources.at(source);
            source = util.computeSourceURL(this.sourceRoot, source, this._sourceMapURL);
          }

          var name = util.getArg(mapping, "name", null);

          if (name !== null) {
            name = this._names.at(name);
          }

          return {
            source: source,
            line: util.getArg(mapping, "originalLine", null),
            column: util.getArg(mapping, "originalColumn", null),
            name: name
          };
        }
      }

      return {
        source: null,
        line: null,
        column: null,
        name: null
      };
    }
    /**
     * Return true if we have the source content for every source in the source
     * map, false otherwise.
     */

  }, {
    key: "hasContentsOfAllSources",
    value: function hasContentsOfAllSources() {
      if (!this.sourcesContent) {
        return false;
      }

      return this.sourcesContent.length >= this._sources.size() && !this.sourcesContent.some(function (sc) {
        return sc == null;
      });
    }
    /**
     * Returns the original source content. The only argument is the url of the
     * original source file. Returns null if no original source content is
     * available.
     */

  }, {
    key: "sourceContentFor",
    value: function sourceContentFor(aSource, nullOnMissing) {
      if (!this.sourcesContent) {
        return null;
      }

      var index = this._findSourceIndex(aSource);

      if (index >= 0) {
        return this.sourcesContent[index];
      }

      var relativeSource = aSource;

      if (this.sourceRoot != null) {
        relativeSource = util.relative(this.sourceRoot, relativeSource);
      }

      var url;

      if (this.sourceRoot != null && (url = util.urlParse(this.sourceRoot))) {
        // XXX: file:// URIs and absolute paths lead to unexpected behavior for
        // many users. We can help them out when they expect file:// URIs to
        // behave like it would if they were running a local HTTP server. See
        // https://bugzilla.mozilla.org/show_bug.cgi?id=885597.
        var fileUriAbsPath = relativeSource.replace(/^file:\/\//, "");

        if (url.scheme == "file" && this._sources.has(fileUriAbsPath)) {
          return this.sourcesContent[this._sources.indexOf(fileUriAbsPath)];
        }

        if ((!url.path || url.path == "/") && this._sources.has("/" + relativeSource)) {
          return this.sourcesContent[this._sources.indexOf("/" + relativeSource)];
        }
      } // This function is used recursively from
      // IndexedSourceMapConsumer.prototype.sourceContentFor. In that case, we
      // don't want to throw if we can't find the source - we just want to
      // return null, so we provide a flag to exit gracefully.


      if (nullOnMissing) {
        return null;
      }

      throw new Error('"' + relativeSource + '" is not in the SourceMap.');
    }
    /**
     * Returns the generated line and column information for the original source,
     * line, and column positions provided. The only argument is an object with
     * the following properties:
     *
     *   - source: The filename of the original source.
     *   - line: The line number in the original source.  The line number
     *     is 1-based.
     *   - column: The column number in the original source.  The column
     *     number is 0-based.
     *   - bias: Either 'SourceMapConsumer.GREATEST_LOWER_BOUND' or
     *     'SourceMapConsumer.LEAST_UPPER_BOUND'. Specifies whether to return the
     *     closest element that is smaller than or greater than the one we are
     *     searching for, respectively, if the exact element cannot be found.
     *     Defaults to 'SourceMapConsumer.GREATEST_LOWER_BOUND'.
     *
     * and an object is returned with the following properties:
     *
     *   - line: The line number in the generated source, or null.  The
     *     line number is 1-based.
     *   - column: The column number in the generated source, or null.
     *     The column number is 0-based.
     */

  }, {
    key: "generatedPositionFor",
    value: function generatedPositionFor(aArgs) {
      var _this5 = this;

      var source = util.getArg(aArgs, "source");
      source = this._findSourceIndex(source);

      if (source < 0) {
        return {
          line: null,
          column: null,
          lastColumn: null
        };
      }

      var needle = {
        source: source,
        originalLine: util.getArg(aArgs, "line"),
        originalColumn: util.getArg(aArgs, "column")
      };

      if (needle.originalLine < 1) {
        throw new Error("Line numbers must be >= 1");
      }

      if (needle.originalColumn < 0) {
        throw new Error("Column numbers must be >= 0");
      }

      var bias = util.getArg(aArgs, "bias", SourceMapConsumer.GREATEST_LOWER_BOUND);

      if (bias == null) {
        bias = SourceMapConsumer.GREATEST_LOWER_BOUND;
      }

      var mapping;

      this._wasm.withMappingCallback(function (m) {
        return mapping = m;
      }, function () {
        _this5._wasm.exports.generated_location_for(_this5._getMappingsPtr(), needle.source, needle.originalLine - 1, needle.originalColumn, bias);
      });

      if (mapping) {
        if (mapping.source === needle.source) {
          var lastColumn = mapping.lastGeneratedColumn;

          if (this._computedColumnSpans && lastColumn === null) {
            lastColumn = Infinity;
          }

          return {
            line: util.getArg(mapping, "generatedLine", null),
            column: util.getArg(mapping, "generatedColumn", null),
            lastColumn: lastColumn
          };
        }
      }

      return {
        line: null,
        column: null,
        lastColumn: null
      };
    }
  }], [{
    key: "fromSourceMap",
    value: function fromSourceMap(aSourceMap, aSourceMapURL) {
      return new BasicSourceMapConsumer(aSourceMap.toString());
    }
  }]);

  return BasicSourceMapConsumer;
}(SourceMapConsumer);

BasicSourceMapConsumer.prototype.consumer = SourceMapConsumer;
exports.BasicSourceMapConsumer = BasicSourceMapConsumer;
/**
 * An IndexedSourceMapConsumer instance represents a parsed source map which
 * we can query for information. It differs from BasicSourceMapConsumer in
 * that it takes "indexed" source maps (i.e. ones with a "sections" field) as
 * input.
 *
 * The first parameter is a raw source map (either as a JSON string, or already
 * parsed to an object). According to the spec for indexed source maps, they
 * have the following attributes:
 *
 *   - version: Which version of the source map spec this map is following.
 *   - file: Optional. The generated file this source map is associated with.
 *   - sections: A list of section definitions.
 *
 * Each value under the "sections" field has two fields:
 *   - offset: The offset into the original specified at which this section
 *       begins to apply, defined as an object with a "line" and "column"
 *       field.
 *   - map: A source map definition. This source map could also be indexed,
 *       but doesn't have to be.
 *
 * Instead of the "map" field, it's also possible to have a "url" field
 * specifying a URL to retrieve a source map from, but that's currently
 * unsupported.
 *
 * Here's an example source map, taken from the source map spec[0], but
 * modified to omit a section which uses the "url" field.
 *
 *  {
 *    version : 3,
 *    file: "app.js",
 *    sections: [{
 *      offset: {line:100, column:10},
 *      map: {
 *        version : 3,
 *        file: "section.js",
 *        sources: ["foo.js", "bar.js"],
 *        names: ["src", "maps", "are", "fun"],
 *        mappings: "AAAA,E;;ABCDE;"
 *      }
 *    }],
 *  }
 *
 * The second parameter, if given, is a string whose value is the URL
 * at which the source map was found.  This URL is used to compute the
 * sources array.
 *
 * [0]: https://docs.google.com/document/d/1U1RGAehQwRypUTovF1KRlpiOFze0b-_2gc6fAH0KY0k/edit#heading=h.535es3xeprgt
 */

var IndexedSourceMapConsumer = /*#__PURE__*/function (_SourceMapConsumer2) {
  _inherits(IndexedSourceMapConsumer, _SourceMapConsumer2);

  var _super2 = _createSuper(IndexedSourceMapConsumer);

  function IndexedSourceMapConsumer(aSourceMap, aSourceMapURL) {
    var _this6;

    _classCallCheck(this, IndexedSourceMapConsumer);

    return _possibleConstructorReturn(_this6, (_this6 = _super2.call(this, INTERNAL)).then(function (that) {
      var sourceMap = aSourceMap;

      if (typeof aSourceMap === "string") {
        sourceMap = util.parseSourceMapInput(aSourceMap);
      }

      var version = util.getArg(sourceMap, "version");
      var sections = util.getArg(sourceMap, "sections");

      if (version != that._version) {
        throw new Error("Unsupported version: " + version);
      }

      that._sources = new ArraySet();
      that._names = new ArraySet();
      that.__generatedMappings = null;
      that.__originalMappings = null;
      that.__generatedMappingsUnsorted = null;
      that.__originalMappingsUnsorted = null;
      var lastOffset = {
        line: -1,
        column: 0
      };
      return Promise.all(sections.map(function (s) {
        if (s.url) {
          // The url field will require support for asynchronicity.
          // See https://github.com/mozilla/source-map/issues/16
          throw new Error("Support for url field in sections not implemented.");
        }

        var offset = util.getArg(s, "offset");
        var offsetLine = util.getArg(offset, "line");
        var offsetColumn = util.getArg(offset, "column");

        if (offsetLine < lastOffset.line || offsetLine === lastOffset.line && offsetColumn < lastOffset.column) {
          throw new Error("Section offsets must be ordered and non-overlapping.");
        }

        lastOffset = offset;
        var cons = new SourceMapConsumer(util.getArg(s, "map"), aSourceMapURL);
        return cons.then(function (consumer) {
          return {
            generatedOffset: {
              // The offset fields are 0-based, but we use 1-based indices when
              // encoding/decoding from VLQ.
              generatedLine: offsetLine + 1,
              generatedColumn: offsetColumn + 1
            },
            consumer: consumer
          };
        });
      })).then(function (s) {
        that._sections = s;
        return that;
      });
    }));
  } // `__generatedMappings` and `__originalMappings` are arrays that hold the
  // parsed mapping coordinates from the source map's "mappings" attribute. They
  // are lazily instantiated, accessed via the `_generatedMappings` and
  // `_originalMappings` getters respectively, and we only parse the mappings
  // and create these arrays once queried for a source location. We jump through
  // these hoops because there can be many thousands of mappings, and parsing
  // them is expensive, so we only want to do it if we must.
  //
  // Each object in the arrays is of the form:
  //
  //     {
  //       generatedLine: The line number in the generated code,
  //       generatedColumn: The column number in the generated code,
  //       source: The path to the original source file that generated this
  //               chunk of code,
  //       originalLine: The line number in the original source that
  //                     corresponds to this chunk of generated code,
  //       originalColumn: The column number in the original source that
  //                       corresponds to this chunk of generated code,
  //       name: The name of the original symbol which generated this chunk of
  //             code.
  //     }
  //
  // All properties except for `generatedLine` and `generatedColumn` can be
  // `null`.
  //
  // `_generatedMappings` is ordered by the generated positions.
  //
  // `_originalMappings` is ordered by the original positions.


  _createClass(IndexedSourceMapConsumer, [{
    key: "_generatedMappings",
    get: function get() {
      if (!this.__generatedMappings) {
        this._sortGeneratedMappings();
      }

      return this.__generatedMappings;
    }
  }, {
    key: "_originalMappings",
    get: function get() {
      if (!this.__originalMappings) {
        this._sortOriginalMappings();
      }

      return this.__originalMappings;
    }
  }, {
    key: "_generatedMappingsUnsorted",
    get: function get() {
      if (!this.__generatedMappingsUnsorted) {
        this._parseMappings(this._mappings, this.sourceRoot);
      }

      return this.__generatedMappingsUnsorted;
    }
  }, {
    key: "_originalMappingsUnsorted",
    get: function get() {
      if (!this.__originalMappingsUnsorted) {
        this._parseMappings(this._mappings, this.sourceRoot);
      }

      return this.__originalMappingsUnsorted;
    }
  }, {
    key: "_sortGeneratedMappings",
    value: function _sortGeneratedMappings() {
      var mappings = this._generatedMappingsUnsorted;
      mappings.sort(util.compareByGeneratedPositionsDeflated);
      this.__generatedMappings = mappings;
    }
  }, {
    key: "_sortOriginalMappings",
    value: function _sortOriginalMappings() {
      var mappings = this._originalMappingsUnsorted;
      mappings.sort(util.compareByOriginalPositions);
      this.__originalMappings = mappings;
    }
    /**
     * The list of original sources.
     */

  }, {
    key: "sources",
    get: function get() {
      var sources = [];

      for (var i = 0; i < this._sections.length; i++) {
        for (var j = 0; j < this._sections[i].consumer.sources.length; j++) {
          sources.push(this._sections[i].consumer.sources[j]);
        }
      }

      return sources;
    }
    /**
     * Returns the original source, line, and column information for the generated
     * source's line and column positions provided. The only argument is an object
     * with the following properties:
     *
     *   - line: The line number in the generated source.  The line number
     *     is 1-based.
     *   - column: The column number in the generated source.  The column
     *     number is 0-based.
     *
     * and an object is returned with the following properties:
     *
     *   - source: The original source file, or null.
     *   - line: The line number in the original source, or null.  The
     *     line number is 1-based.
     *   - column: The column number in the original source, or null.  The
     *     column number is 0-based.
     *   - name: The original identifier, or null.
     */

  }, {
    key: "originalPositionFor",
    value: function originalPositionFor(aArgs) {
      var needle = {
        generatedLine: util.getArg(aArgs, "line"),
        generatedColumn: util.getArg(aArgs, "column")
      }; // Find the section containing the generated position we're trying to map
      // to an original position.

      var sectionIndex = binarySearch.search(needle, this._sections, function (aNeedle, section) {
        var cmp = aNeedle.generatedLine - section.generatedOffset.generatedLine;

        if (cmp) {
          return cmp;
        }

        return aNeedle.generatedColumn - section.generatedOffset.generatedColumn;
      });
      var section = this._sections[sectionIndex];

      if (!section) {
        return {
          source: null,
          line: null,
          column: null,
          name: null
        };
      }

      return section.consumer.originalPositionFor({
        line: needle.generatedLine - (section.generatedOffset.generatedLine - 1),
        column: needle.generatedColumn - (section.generatedOffset.generatedLine === needle.generatedLine ? section.generatedOffset.generatedColumn - 1 : 0),
        bias: aArgs.bias
      });
    }
    /**
     * Return true if we have the source content for every source in the source
     * map, false otherwise.
     */

  }, {
    key: "hasContentsOfAllSources",
    value: function hasContentsOfAllSources() {
      return this._sections.every(function (s) {
        return s.consumer.hasContentsOfAllSources();
      });
    }
    /**
     * Returns the original source content. The only argument is the url of the
     * original source file. Returns null if no original source content is
     * available.
     */

  }, {
    key: "sourceContentFor",
    value: function sourceContentFor(aSource, nullOnMissing) {
      for (var i = 0; i < this._sections.length; i++) {
        var section = this._sections[i];
        var content = section.consumer.sourceContentFor(aSource, true);

        if (content) {
          return content;
        }
      }

      if (nullOnMissing) {
        return null;
      }

      throw new Error('"' + aSource + '" is not in the SourceMap.');
    }
    /**
     * Returns the generated line and column information for the original source,
     * line, and column positions provided. The only argument is an object with
     * the following properties:
     *
     *   - source: The filename of the original source.
     *   - line: The line number in the original source.  The line number
     *     is 1-based.
     *   - column: The column number in the original source.  The column
     *     number is 0-based.
     *
     * and an object is returned with the following properties:
     *
     *   - line: The line number in the generated source, or null.  The
     *     line number is 1-based.
     *   - column: The column number in the generated source, or null.
     *     The column number is 0-based.
     */

  }, {
    key: "generatedPositionFor",
    value: function generatedPositionFor(aArgs) {
      for (var i = 0; i < this._sections.length; i++) {
        var section = this._sections[i]; // Only consider this section if the requested source is in the list of
        // sources of the consumer.

        if (section.consumer._findSourceIndex(util.getArg(aArgs, "source")) === -1) {
          continue;
        }

        var generatedPosition = section.consumer.generatedPositionFor(aArgs);

        if (generatedPosition) {
          var ret = {
            line: generatedPosition.line + (section.generatedOffset.generatedLine - 1),
            column: generatedPosition.column + (section.generatedOffset.generatedLine === generatedPosition.line ? section.generatedOffset.generatedColumn - 1 : 0)
          };
          return ret;
        }
      }

      return {
        line: null,
        column: null
      };
    }
    /**
     * Parse the mappings in a string in to a data structure which we can easily
     * query (the ordered arrays in the `this.__generatedMappings` and
     * `this.__originalMappings` properties).
     */

  }, {
    key: "_parseMappings",
    value: function _parseMappings(aStr, aSourceRoot) {
      var _this7 = this;

      var generatedMappings = this.__generatedMappingsUnsorted = [];
      var originalMappings = this.__originalMappingsUnsorted = [];

      var _loop = function _loop(i) {
        var section = _this7._sections[i];
        var sectionMappings = [];
        section.consumer.eachMapping(function (m) {
          return sectionMappings.push(m);
        });

        for (var j = 0; j < sectionMappings.length; j++) {
          var mapping = sectionMappings[j]; // TODO: test if null is correct here.  The original code used
          // `source`, which would actually have gotten used as null because
          // var's get hoisted.
          // See: https://github.com/mozilla/source-map/issues/333

          var source = util.computeSourceURL(section.consumer.sourceRoot, null, _this7._sourceMapURL);

          _this7._sources.add(source);

          source = _this7._sources.indexOf(source);
          var name = null;

          if (mapping.name) {
            _this7._names.add(mapping.name);

            name = _this7._names.indexOf(mapping.name);
          } // The mappings coming from the consumer for the section have
          // generated positions relative to the start of the section, so we
          // need to offset them to be relative to the start of the concatenated
          // generated file.


          var adjustedMapping = {
            source: source,
            generatedLine: mapping.generatedLine + (section.generatedOffset.generatedLine - 1),
            generatedColumn: mapping.generatedColumn + (section.generatedOffset.generatedLine === mapping.generatedLine ? section.generatedOffset.generatedColumn - 1 : 0),
            originalLine: mapping.originalLine,
            originalColumn: mapping.originalColumn,
            name: name
          };
          generatedMappings.push(adjustedMapping);

          if (typeof adjustedMapping.originalLine === "number") {
            originalMappings.push(adjustedMapping);
          }
        }
      };

      for (var i = 0; i < this._sections.length; i++) {
        _loop(i);
      }
    }
  }, {
    key: "eachMapping",
    value: function eachMapping(aCallback, aContext, aOrder) {
      var context = aContext || null;
      var order = aOrder || SourceMapConsumer.GENERATED_ORDER;
      var mappings;

      switch (order) {
        case SourceMapConsumer.GENERATED_ORDER:
          mappings = this._generatedMappings;
          break;

        case SourceMapConsumer.ORIGINAL_ORDER:
          mappings = this._originalMappings;
          break;

        default:
          throw new Error("Unknown order of iteration.");
      }

      var sourceRoot = this.sourceRoot;
      mappings.map(function (mapping) {
        var source = null;

        if (mapping.source !== null) {
          source = this._sources.at(mapping.source);
          source = util.computeSourceURL(sourceRoot, source, this._sourceMapURL);
        }

        return {
          source: source,
          generatedLine: mapping.generatedLine,
          generatedColumn: mapping.generatedColumn,
          originalLine: mapping.originalLine,
          originalColumn: mapping.originalColumn,
          name: mapping.name === null ? null : this._names.at(mapping.name)
        };
      }, this).forEach(aCallback, context);
    }
    /**
     * Find the mapping that best matches the hypothetical "needle" mapping that
     * we are searching for in the given "haystack" of mappings.
     */

  }, {
    key: "_findMapping",
    value: function _findMapping(aNeedle, aMappings, aLineName, aColumnName, aComparator, aBias) {
      // To return the position we are searching for, we must first find the
      // mapping for the given position and then return the opposite position it
      // points to. Because the mappings are sorted, we can use binary search to
      // find the best mapping.
      if (aNeedle[aLineName] <= 0) {
        throw new TypeError("Line must be greater than or equal to 1, got " + aNeedle[aLineName]);
      }

      if (aNeedle[aColumnName] < 0) {
        throw new TypeError("Column must be greater than or equal to 0, got " + aNeedle[aColumnName]);
      }

      return binarySearch.search(aNeedle, aMappings, aComparator, aBias);
    }
  }, {
    key: "allGeneratedPositionsFor",
    value: function allGeneratedPositionsFor(aArgs) {
      var line = util.getArg(aArgs, "line"); // When there is no exact match, BasicSourceMapConsumer.prototype._findMapping
      // returns the index of the closest mapping less than the needle. By
      // setting needle.originalColumn to 0, we thus find the last mapping for
      // the given line, provided such a mapping exists.

      var needle = {
        source: util.getArg(aArgs, "source"),
        originalLine: line,
        originalColumn: util.getArg(aArgs, "column", 0)
      };
      needle.source = this._findSourceIndex(needle.source);

      if (needle.source < 0) {
        return [];
      }

      if (needle.originalLine < 1) {
        throw new Error("Line numbers must be >= 1");
      }

      if (needle.originalColumn < 0) {
        throw new Error("Column numbers must be >= 0");
      }

      var mappings = [];

      var index = this._findMapping(needle, this._originalMappings, "originalLine", "originalColumn", util.compareByOriginalPositions, binarySearch.LEAST_UPPER_BOUND);

      if (index >= 0) {
        var mapping = this._originalMappings[index];

        if (aArgs.column === undefined) {
          var originalLine = mapping.originalLine; // Iterate until either we run out of mappings, or we run into
          // a mapping for a different line than the one we found. Since
          // mappings are sorted, this is guaranteed to find all mappings for
          // the line we found.

          while (mapping && mapping.originalLine === originalLine) {
            var lastColumn = mapping.lastGeneratedColumn;

            if (this._computedColumnSpans && lastColumn === null) {
              lastColumn = Infinity;
            }

            mappings.push({
              line: util.getArg(mapping, "generatedLine", null),
              column: util.getArg(mapping, "generatedColumn", null),
              lastColumn: lastColumn
            });
            mapping = this._originalMappings[++index];
          }
        } else {
          var originalColumn = mapping.originalColumn; // Iterate until either we run out of mappings, or we run into
          // a mapping for a different line than the one we were searching for.
          // Since mappings are sorted, this is guaranteed to find all mappings for
          // the line we are searching for.

          while (mapping && mapping.originalLine === line && mapping.originalColumn == originalColumn) {
            var _lastColumn = mapping.lastGeneratedColumn;

            if (this._computedColumnSpans && _lastColumn === null) {
              _lastColumn = Infinity;
            }

            mappings.push({
              line: util.getArg(mapping, "generatedLine", null),
              column: util.getArg(mapping, "generatedColumn", null),
              lastColumn: _lastColumn
            });
            mapping = this._originalMappings[++index];
          }
        }
      }

      return mappings;
    }
  }, {
    key: "destroy",
    value: function destroy() {
      for (var i = 0; i < this._sections.length; i++) {
        this._sections[i].consumer.destroy();
      }
    }
  }]);

  return IndexedSourceMapConsumer;
}(SourceMapConsumer);

exports.IndexedSourceMapConsumer = IndexedSourceMapConsumer;
/*
 * Cheat to get around inter-twingled classes.  `factory()` can be at the end
 * where it has access to non-hoisted classes, but it gets hoisted itself.
 */

function _factory(aSourceMap, aSourceMapURL) {
  var sourceMap = aSourceMap;

  if (typeof aSourceMap === "string") {
    sourceMap = util.parseSourceMapInput(aSourceMap);
  }

  var consumer = sourceMap.sections != null ? new IndexedSourceMapConsumer(sourceMap, aSourceMapURL) : new BasicSourceMapConsumer(sourceMap, aSourceMapURL);
  return Promise.resolve(consumer);
}

function _factoryBSM(aSourceMap, aSourceMapURL) {
  return BasicSourceMapConsumer.fromSourceMap(aSourceMap, aSourceMapURL);
}

/***/ }),

/***/ "./node_modules/_source-map@0.7.3@source-map/lib/source-map-generator.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/_source-map@0.7.3@source-map/lib/source-map-generator.js ***!
  \*******************************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/* -*- Mode: js; js-indent-level: 2; -*- */

/*
 * Copyright 2011 Mozilla Foundation and contributors
 * Licensed under the New BSD license. See LICENSE or:
 * http://opensource.org/licenses/BSD-3-Clause
 */
var base64VLQ = __webpack_require__(/*! ./base64-vlq */ "./node_modules/_source-map@0.7.3@source-map/lib/base64-vlq.js");

var util = __webpack_require__(/*! ./util */ "./node_modules/_source-map@0.7.3@source-map/lib/util.js");

var ArraySet = __webpack_require__(/*! ./array-set */ "./node_modules/_source-map@0.7.3@source-map/lib/array-set.js").ArraySet;

var MappingList = __webpack_require__(/*! ./mapping-list */ "./node_modules/_source-map@0.7.3@source-map/lib/mapping-list.js").MappingList;
/**
 * An instance of the SourceMapGenerator represents a source map which is
 * being built incrementally. You may pass an object with the following
 * properties:
 *
 *   - file: The filename of the generated source.
 *   - sourceRoot: A root for all relative URLs in this source map.
 */


var SourceMapGenerator = /*#__PURE__*/function () {
  function SourceMapGenerator(aArgs) {
    _classCallCheck(this, SourceMapGenerator);

    if (!aArgs) {
      aArgs = {};
    }

    this._file = util.getArg(aArgs, "file", null);
    this._sourceRoot = util.getArg(aArgs, "sourceRoot", null);
    this._skipValidation = util.getArg(aArgs, "skipValidation", false);
    this._sources = new ArraySet();
    this._names = new ArraySet();
    this._mappings = new MappingList();
    this._sourcesContents = null;
  }
  /**
   * Creates a new SourceMapGenerator based on a SourceMapConsumer
   *
   * @param aSourceMapConsumer The SourceMap.
   */


  _createClass(SourceMapGenerator, [{
    key: "addMapping",
    value:
    /**
     * Add a single mapping from original source line and column to the generated
     * source's line and column for this source map being created. The mapping
     * object should have the following properties:
     *
     *   - generated: An object with the generated line and column positions.
     *   - original: An object with the original line and column positions.
     *   - source: The original source file (relative to the sourceRoot).
     *   - name: An optional original token name for this mapping.
     */
    function addMapping(aArgs) {
      var generated = util.getArg(aArgs, "generated");
      var original = util.getArg(aArgs, "original", null);
      var source = util.getArg(aArgs, "source", null);
      var name = util.getArg(aArgs, "name", null);

      if (!this._skipValidation) {
        this._validateMapping(generated, original, source, name);
      }

      if (source != null) {
        source = String(source);

        if (!this._sources.has(source)) {
          this._sources.add(source);
        }
      }

      if (name != null) {
        name = String(name);

        if (!this._names.has(name)) {
          this._names.add(name);
        }
      }

      this._mappings.add({
        generatedLine: generated.line,
        generatedColumn: generated.column,
        originalLine: original != null && original.line,
        originalColumn: original != null && original.column,
        source: source,
        name: name
      });
    }
    /**
     * Set the source content for a source file.
     */

  }, {
    key: "setSourceContent",
    value: function setSourceContent(aSourceFile, aSourceContent) {
      var source = aSourceFile;

      if (this._sourceRoot != null) {
        source = util.relative(this._sourceRoot, source);
      }

      if (aSourceContent != null) {
        // Add the source content to the _sourcesContents map.
        // Create a new _sourcesContents map if the property is null.
        if (!this._sourcesContents) {
          this._sourcesContents = Object.create(null);
        }

        this._sourcesContents[util.toSetString(source)] = aSourceContent;
      } else if (this._sourcesContents) {
        // Remove the source file from the _sourcesContents map.
        // If the _sourcesContents map is empty, set the property to null.
        delete this._sourcesContents[util.toSetString(source)];

        if (Object.keys(this._sourcesContents).length === 0) {
          this._sourcesContents = null;
        }
      }
    }
    /**
     * Applies the mappings of a sub-source-map for a specific source file to the
     * source map being generated. Each mapping to the supplied source file is
     * rewritten using the supplied source map. Note: The resolution for the
     * resulting mappings is the minimium of this map and the supplied map.
     *
     * @param aSourceMapConsumer The source map to be applied.
     * @param aSourceFile Optional. The filename of the source file.
     *        If omitted, SourceMapConsumer's file property will be used.
     * @param aSourceMapPath Optional. The dirname of the path to the source map
     *        to be applied. If relative, it is relative to the SourceMapConsumer.
     *        This parameter is needed when the two source maps aren't in the same
     *        directory, and the source map to be applied contains relative source
     *        paths. If so, those relative source paths need to be rewritten
     *        relative to the SourceMapGenerator.
     */

  }, {
    key: "applySourceMap",
    value: function applySourceMap(aSourceMapConsumer, aSourceFile, aSourceMapPath) {
      var sourceFile = aSourceFile; // If aSourceFile is omitted, we will use the file property of the SourceMap

      if (aSourceFile == null) {
        if (aSourceMapConsumer.file == null) {
          throw new Error("SourceMapGenerator.prototype.applySourceMap requires either an explicit source file, " + 'or the source map\'s "file" property. Both were omitted.');
        }

        sourceFile = aSourceMapConsumer.file;
      }

      var sourceRoot = this._sourceRoot; // Make "sourceFile" relative if an absolute Url is passed.

      if (sourceRoot != null) {
        sourceFile = util.relative(sourceRoot, sourceFile);
      } // Applying the SourceMap can add and remove items from the sources and
      // the names array.


      var newSources = this._mappings.toArray().length > 0 ? new ArraySet() : this._sources;
      var newNames = new ArraySet(); // Find mappings for the "sourceFile"

      this._mappings.unsortedForEach(function (mapping) {
        if (mapping.source === sourceFile && mapping.originalLine != null) {
          // Check if it can be mapped by the source map, then update the mapping.
          var original = aSourceMapConsumer.originalPositionFor({
            line: mapping.originalLine,
            column: mapping.originalColumn
          });

          if (original.source != null) {
            // Copy mapping
            mapping.source = original.source;

            if (aSourceMapPath != null) {
              mapping.source = util.join(aSourceMapPath, mapping.source);
            }

            if (sourceRoot != null) {
              mapping.source = util.relative(sourceRoot, mapping.source);
            }

            mapping.originalLine = original.line;
            mapping.originalColumn = original.column;

            if (original.name != null) {
              mapping.name = original.name;
            }
          }
        }

        var source = mapping.source;

        if (source != null && !newSources.has(source)) {
          newSources.add(source);
        }

        var name = mapping.name;

        if (name != null && !newNames.has(name)) {
          newNames.add(name);
        }
      }, this);

      this._sources = newSources;
      this._names = newNames; // Copy sourcesContents of applied map.

      aSourceMapConsumer.sources.forEach(function (srcFile) {
        var content = aSourceMapConsumer.sourceContentFor(srcFile);

        if (content != null) {
          if (aSourceMapPath != null) {
            srcFile = util.join(aSourceMapPath, srcFile);
          }

          if (sourceRoot != null) {
            srcFile = util.relative(sourceRoot, srcFile);
          }

          this.setSourceContent(srcFile, content);
        }
      }, this);
    }
    /**
     * A mapping can have one of the three levels of data:
     *
     *   1. Just the generated position.
     *   2. The Generated position, original position, and original source.
     *   3. Generated and original position, original source, as well as a name
     *      token.
     *
     * To maintain consistency, we validate that any new mapping being added falls
     * in to one of these categories.
     */

  }, {
    key: "_validateMapping",
    value: function _validateMapping(aGenerated, aOriginal, aSource, aName) {
      // When aOriginal is truthy but has empty values for .line and .column,
      // it is most likely a programmer error. In this case we throw a very
      // specific error message to try to guide them the right way.
      // For example: https://github.com/Polymer/polymer-bundler/pull/519
      if (aOriginal && typeof aOriginal.line !== "number" && typeof aOriginal.column !== "number") {
        throw new Error("original.line and original.column are not numbers -- you probably meant to omit " + "the original mapping entirely and only map the generated position. If so, pass " + "null for the original mapping instead of an object with empty or null values.");
      }

      if (aGenerated && "line" in aGenerated && "column" in aGenerated && aGenerated.line > 0 && aGenerated.column >= 0 && !aOriginal && !aSource && !aName) {// Case 1.
      } else if (aGenerated && "line" in aGenerated && "column" in aGenerated && aOriginal && "line" in aOriginal && "column" in aOriginal && aGenerated.line > 0 && aGenerated.column >= 0 && aOriginal.line > 0 && aOriginal.column >= 0 && aSource) {// Cases 2 and 3.
      } else {
        throw new Error("Invalid mapping: " + JSON.stringify({
          generated: aGenerated,
          source: aSource,
          original: aOriginal,
          name: aName
        }));
      }
    }
    /**
     * Serialize the accumulated mappings in to the stream of base 64 VLQs
     * specified by the source map format.
     */

  }, {
    key: "_serializeMappings",
    value: function _serializeMappings() {
      var previousGeneratedColumn = 0;
      var previousGeneratedLine = 1;
      var previousOriginalColumn = 0;
      var previousOriginalLine = 0;
      var previousName = 0;
      var previousSource = 0;
      var result = "";
      var next;
      var mapping;
      var nameIdx;
      var sourceIdx;

      var mappings = this._mappings.toArray();

      for (var i = 0, len = mappings.length; i < len; i++) {
        mapping = mappings[i];
        next = "";

        if (mapping.generatedLine !== previousGeneratedLine) {
          previousGeneratedColumn = 0;

          while (mapping.generatedLine !== previousGeneratedLine) {
            next += ";";
            previousGeneratedLine++;
          }
        } else if (i > 0) {
          if (!util.compareByGeneratedPositionsInflated(mapping, mappings[i - 1])) {
            continue;
          }

          next += ",";
        }

        next += base64VLQ.encode(mapping.generatedColumn - previousGeneratedColumn);
        previousGeneratedColumn = mapping.generatedColumn;

        if (mapping.source != null) {
          sourceIdx = this._sources.indexOf(mapping.source);
          next += base64VLQ.encode(sourceIdx - previousSource);
          previousSource = sourceIdx; // lines are stored 0-based in SourceMap spec version 3

          next += base64VLQ.encode(mapping.originalLine - 1 - previousOriginalLine);
          previousOriginalLine = mapping.originalLine - 1;
          next += base64VLQ.encode(mapping.originalColumn - previousOriginalColumn);
          previousOriginalColumn = mapping.originalColumn;

          if (mapping.name != null) {
            nameIdx = this._names.indexOf(mapping.name);
            next += base64VLQ.encode(nameIdx - previousName);
            previousName = nameIdx;
          }
        }

        result += next;
      }

      return result;
    }
  }, {
    key: "_generateSourcesContent",
    value: function _generateSourcesContent(aSources, aSourceRoot) {
      return aSources.map(function (source) {
        if (!this._sourcesContents) {
          return null;
        }

        if (aSourceRoot != null) {
          source = util.relative(aSourceRoot, source);
        }

        var key = util.toSetString(source);
        return Object.prototype.hasOwnProperty.call(this._sourcesContents, key) ? this._sourcesContents[key] : null;
      }, this);
    }
    /**
     * Externalize the source map.
     */

  }, {
    key: "toJSON",
    value: function toJSON() {
      var map = {
        version: this._version,
        sources: this._sources.toArray(),
        names: this._names.toArray(),
        mappings: this._serializeMappings()
      };

      if (this._file != null) {
        map.file = this._file;
      }

      if (this._sourceRoot != null) {
        map.sourceRoot = this._sourceRoot;
      }

      if (this._sourcesContents) {
        map.sourcesContent = this._generateSourcesContent(map.sources, map.sourceRoot);
      }

      return map;
    }
    /**
     * Render the source map being generated to a string.
     */

  }, {
    key: "toString",
    value: function toString() {
      return JSON.stringify(this.toJSON());
    }
  }], [{
    key: "fromSourceMap",
    value: function fromSourceMap(aSourceMapConsumer) {
      var sourceRoot = aSourceMapConsumer.sourceRoot;
      var generator = new SourceMapGenerator({
        file: aSourceMapConsumer.file,
        sourceRoot: sourceRoot
      });
      aSourceMapConsumer.eachMapping(function (mapping) {
        var newMapping = {
          generated: {
            line: mapping.generatedLine,
            column: mapping.generatedColumn
          }
        };

        if (mapping.source != null) {
          newMapping.source = mapping.source;

          if (sourceRoot != null) {
            newMapping.source = util.relative(sourceRoot, newMapping.source);
          }

          newMapping.original = {
            line: mapping.originalLine,
            column: mapping.originalColumn
          };

          if (mapping.name != null) {
            newMapping.name = mapping.name;
          }
        }

        generator.addMapping(newMapping);
      });
      aSourceMapConsumer.sources.forEach(function (sourceFile) {
        var sourceRelative = sourceFile;

        if (sourceRoot !== null) {
          sourceRelative = util.relative(sourceRoot, sourceFile);
        }

        if (!generator._sources.has(sourceRelative)) {
          generator._sources.add(sourceRelative);
        }

        var content = aSourceMapConsumer.sourceContentFor(sourceFile);

        if (content != null) {
          generator.setSourceContent(sourceFile, content);
        }
      });
      return generator;
    }
  }]);

  return SourceMapGenerator;
}();

SourceMapGenerator.prototype._version = 3;
exports.SourceMapGenerator = SourceMapGenerator;

/***/ }),

/***/ "./node_modules/_source-map@0.7.3@source-map/lib/source-node.js":
/*!**********************************************************************!*\
  !*** ./node_modules/_source-map@0.7.3@source-map/lib/source-node.js ***!
  \**********************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/* -*- Mode: js; js-indent-level: 2; -*- */

/*
 * Copyright 2011 Mozilla Foundation and contributors
 * Licensed under the New BSD license. See LICENSE or:
 * http://opensource.org/licenses/BSD-3-Clause
 */
var SourceMapGenerator = __webpack_require__(/*! ./source-map-generator */ "./node_modules/_source-map@0.7.3@source-map/lib/source-map-generator.js").SourceMapGenerator;

var util = __webpack_require__(/*! ./util */ "./node_modules/_source-map@0.7.3@source-map/lib/util.js"); // Matches a Windows-style `\r\n` newline or a `\n` newline used by all other
// operating systems these days (capturing the result).


var REGEX_NEWLINE = /(\r?\n)/; // Newline character code for charCodeAt() comparisons

var NEWLINE_CODE = 10; // Private symbol for identifying `SourceNode`s when multiple versions of
// the source-map library are loaded. This MUST NOT CHANGE across
// versions!

var isSourceNode = "$$$isSourceNode$$$";
/**
 * SourceNodes provide a way to abstract over interpolating/concatenating
 * snippets of generated JavaScript source code while maintaining the line and
 * column information associated with the original source code.
 *
 * @param aLine The original line number.
 * @param aColumn The original column number.
 * @param aSource The original source's filename.
 * @param aChunks Optional. An array of strings which are snippets of
 *        generated JS, or other SourceNodes.
 * @param aName The original identifier.
 */

var SourceNode = /*#__PURE__*/function () {
  function SourceNode(aLine, aColumn, aSource, aChunks, aName) {
    _classCallCheck(this, SourceNode);

    this.children = [];
    this.sourceContents = {};
    this.line = aLine == null ? null : aLine;
    this.column = aColumn == null ? null : aColumn;
    this.source = aSource == null ? null : aSource;
    this.name = aName == null ? null : aName;
    this[isSourceNode] = true;
    if (aChunks != null) this.add(aChunks);
  }
  /**
   * Creates a SourceNode from generated code and a SourceMapConsumer.
   *
   * @param aGeneratedCode The generated code
   * @param aSourceMapConsumer The SourceMap for the generated code
   * @param aRelativePath Optional. The path that relative sources in the
   *        SourceMapConsumer should be relative to.
   */


  _createClass(SourceNode, [{
    key: "add",
    value:
    /**
     * Add a chunk of generated JS to this source node.
     *
     * @param aChunk A string snippet of generated JS code, another instance of
     *        SourceNode, or an array where each member is one of those things.
     */
    function add(aChunk) {
      if (Array.isArray(aChunk)) {
        aChunk.forEach(function (chunk) {
          this.add(chunk);
        }, this);
      } else if (aChunk[isSourceNode] || typeof aChunk === "string") {
        if (aChunk) {
          this.children.push(aChunk);
        }
      } else {
        throw new TypeError("Expected a SourceNode, string, or an array of SourceNodes and strings. Got " + aChunk);
      }

      return this;
    }
    /**
     * Add a chunk of generated JS to the beginning of this source node.
     *
     * @param aChunk A string snippet of generated JS code, another instance of
     *        SourceNode, or an array where each member is one of those things.
     */

  }, {
    key: "prepend",
    value: function prepend(aChunk) {
      if (Array.isArray(aChunk)) {
        for (var i = aChunk.length - 1; i >= 0; i--) {
          this.prepend(aChunk[i]);
        }
      } else if (aChunk[isSourceNode] || typeof aChunk === "string") {
        this.children.unshift(aChunk);
      } else {
        throw new TypeError("Expected a SourceNode, string, or an array of SourceNodes and strings. Got " + aChunk);
      }

      return this;
    }
    /**
     * Walk over the tree of JS snippets in this node and its children. The
     * walking function is called once for each snippet of JS and is passed that
     * snippet and the its original associated source's line/column location.
     *
     * @param aFn The traversal function.
     */

  }, {
    key: "walk",
    value: function walk(aFn) {
      var chunk;

      for (var i = 0, len = this.children.length; i < len; i++) {
        chunk = this.children[i];

        if (chunk[isSourceNode]) {
          chunk.walk(aFn);
        } else if (chunk !== "") {
          aFn(chunk, {
            source: this.source,
            line: this.line,
            column: this.column,
            name: this.name
          });
        }
      }
    }
    /**
     * Like `String.prototype.join` except for SourceNodes. Inserts `aStr` between
     * each of `this.children`.
     *
     * @param aSep The separator.
     */

  }, {
    key: "join",
    value: function join(aSep) {
      var newChildren;
      var i;
      var len = this.children.length;

      if (len > 0) {
        newChildren = [];

        for (i = 0; i < len - 1; i++) {
          newChildren.push(this.children[i]);
          newChildren.push(aSep);
        }

        newChildren.push(this.children[i]);
        this.children = newChildren;
      }

      return this;
    }
    /**
     * Call String.prototype.replace on the very right-most source snippet. Useful
     * for trimming whitespace from the end of a source node, etc.
     *
     * @param aPattern The pattern to replace.
     * @param aReplacement The thing to replace the pattern with.
     */

  }, {
    key: "replaceRight",
    value: function replaceRight(aPattern, aReplacement) {
      var lastChild = this.children[this.children.length - 1];

      if (lastChild[isSourceNode]) {
        lastChild.replaceRight(aPattern, aReplacement);
      } else if (typeof lastChild === "string") {
        this.children[this.children.length - 1] = lastChild.replace(aPattern, aReplacement);
      } else {
        this.children.push("".replace(aPattern, aReplacement));
      }

      return this;
    }
    /**
     * Set the source content for a source file. This will be added to the SourceMapGenerator
     * in the sourcesContent field.
     *
     * @param aSourceFile The filename of the source file
     * @param aSourceContent The content of the source file
     */

  }, {
    key: "setSourceContent",
    value: function setSourceContent(aSourceFile, aSourceContent) {
      this.sourceContents[util.toSetString(aSourceFile)] = aSourceContent;
    }
    /**
     * Walk over the tree of SourceNodes. The walking function is called for each
     * source file content and is passed the filename and source content.
     *
     * @param aFn The traversal function.
     */

  }, {
    key: "walkSourceContents",
    value: function walkSourceContents(aFn) {
      for (var i = 0, len = this.children.length; i < len; i++) {
        if (this.children[i][isSourceNode]) {
          this.children[i].walkSourceContents(aFn);
        }
      }

      var sources = Object.keys(this.sourceContents);

      for (var _i = 0, _len = sources.length; _i < _len; _i++) {
        aFn(util.fromSetString(sources[_i]), this.sourceContents[sources[_i]]);
      }
    }
    /**
     * Return the string representation of this source node. Walks over the tree
     * and concatenates all the various snippets together to one string.
     */

  }, {
    key: "toString",
    value: function toString() {
      var str = "";
      this.walk(function (chunk) {
        str += chunk;
      });
      return str;
    }
    /**
     * Returns the string representation of this source node along with a source
     * map.
     */

  }, {
    key: "toStringWithSourceMap",
    value: function toStringWithSourceMap(aArgs) {
      var generated = {
        code: "",
        line: 1,
        column: 0
      };
      var map = new SourceMapGenerator(aArgs);
      var sourceMappingActive = false;
      var lastOriginalSource = null;
      var lastOriginalLine = null;
      var lastOriginalColumn = null;
      var lastOriginalName = null;
      this.walk(function (chunk, original) {
        generated.code += chunk;

        if (original.source !== null && original.line !== null && original.column !== null) {
          if (lastOriginalSource !== original.source || lastOriginalLine !== original.line || lastOriginalColumn !== original.column || lastOriginalName !== original.name) {
            map.addMapping({
              source: original.source,
              original: {
                line: original.line,
                column: original.column
              },
              generated: {
                line: generated.line,
                column: generated.column
              },
              name: original.name
            });
          }

          lastOriginalSource = original.source;
          lastOriginalLine = original.line;
          lastOriginalColumn = original.column;
          lastOriginalName = original.name;
          sourceMappingActive = true;
        } else if (sourceMappingActive) {
          map.addMapping({
            generated: {
              line: generated.line,
              column: generated.column
            }
          });
          lastOriginalSource = null;
          sourceMappingActive = false;
        }

        for (var idx = 0, length = chunk.length; idx < length; idx++) {
          if (chunk.charCodeAt(idx) === NEWLINE_CODE) {
            generated.line++;
            generated.column = 0; // Mappings end at eol

            if (idx + 1 === length) {
              lastOriginalSource = null;
              sourceMappingActive = false;
            } else if (sourceMappingActive) {
              map.addMapping({
                source: original.source,
                original: {
                  line: original.line,
                  column: original.column
                },
                generated: {
                  line: generated.line,
                  column: generated.column
                },
                name: original.name
              });
            }
          } else {
            generated.column++;
          }
        }
      });
      this.walkSourceContents(function (sourceFile, sourceContent) {
        map.setSourceContent(sourceFile, sourceContent);
      });
      return {
        code: generated.code,
        map: map
      };
    }
  }], [{
    key: "fromStringWithSourceMap",
    value: function fromStringWithSourceMap(aGeneratedCode, aSourceMapConsumer, aRelativePath) {
      // The SourceNode we want to fill with the generated code
      // and the SourceMap
      var node = new SourceNode(); // All even indices of this array are one line of the generated code,
      // while all odd indices are the newlines between two adjacent lines
      // (since `REGEX_NEWLINE` captures its match).
      // Processed fragments are accessed by calling `shiftNextLine`.

      var remainingLines = aGeneratedCode.split(REGEX_NEWLINE);
      var remainingLinesIndex = 0;

      var shiftNextLine = function shiftNextLine() {
        var lineContents = getNextLine(); // The last line of a file might not have a newline.

        var newLine = getNextLine() || "";
        return lineContents + newLine;

        function getNextLine() {
          return remainingLinesIndex < remainingLines.length ? remainingLines[remainingLinesIndex++] : undefined;
        }
      }; // We need to remember the position of "remainingLines"


      var lastGeneratedLine = 1,
          lastGeneratedColumn = 0; // The generate SourceNodes we need a code range.
      // To extract it current and last mapping is used.
      // Here we store the last mapping.

      var lastMapping = null;
      var nextLine;
      aSourceMapConsumer.eachMapping(function (mapping) {
        if (lastMapping !== null) {
          // We add the code from "lastMapping" to "mapping":
          // First check if there is a new line in between.
          if (lastGeneratedLine < mapping.generatedLine) {
            // Associate first line with "lastMapping"
            addMappingWithCode(lastMapping, shiftNextLine());
            lastGeneratedLine++;
            lastGeneratedColumn = 0; // The remaining code is added without mapping
          } else {
            // There is no new line in between.
            // Associate the code between "lastGeneratedColumn" and
            // "mapping.generatedColumn" with "lastMapping"
            nextLine = remainingLines[remainingLinesIndex] || "";
            var code = nextLine.substr(0, mapping.generatedColumn - lastGeneratedColumn);
            remainingLines[remainingLinesIndex] = nextLine.substr(mapping.generatedColumn - lastGeneratedColumn);
            lastGeneratedColumn = mapping.generatedColumn;
            addMappingWithCode(lastMapping, code); // No more remaining code, continue

            lastMapping = mapping;
            return;
          }
        } // We add the generated code until the first mapping
        // to the SourceNode without any mapping.
        // Each line is added as separate string.


        while (lastGeneratedLine < mapping.generatedLine) {
          node.add(shiftNextLine());
          lastGeneratedLine++;
        }

        if (lastGeneratedColumn < mapping.generatedColumn) {
          nextLine = remainingLines[remainingLinesIndex] || "";
          node.add(nextLine.substr(0, mapping.generatedColumn));
          remainingLines[remainingLinesIndex] = nextLine.substr(mapping.generatedColumn);
          lastGeneratedColumn = mapping.generatedColumn;
        }

        lastMapping = mapping;
      }, this); // We have processed all mappings.

      if (remainingLinesIndex < remainingLines.length) {
        if (lastMapping) {
          // Associate the remaining code in the current line with "lastMapping"
          addMappingWithCode(lastMapping, shiftNextLine());
        } // and add the remaining lines without any mapping


        node.add(remainingLines.splice(remainingLinesIndex).join(""));
      } // Copy sourcesContent into SourceNode


      aSourceMapConsumer.sources.forEach(function (sourceFile) {
        var content = aSourceMapConsumer.sourceContentFor(sourceFile);

        if (content != null) {
          if (aRelativePath != null) {
            sourceFile = util.join(aRelativePath, sourceFile);
          }

          node.setSourceContent(sourceFile, content);
        }
      });
      return node;

      function addMappingWithCode(mapping, code) {
        if (mapping === null || mapping.source === undefined) {
          node.add(code);
        } else {
          var source = aRelativePath ? util.join(aRelativePath, mapping.source) : mapping.source;
          node.add(new SourceNode(mapping.originalLine, mapping.originalColumn, source, code, mapping.name));
        }
      }
    }
  }]);

  return SourceNode;
}();

exports.SourceNode = SourceNode;

/***/ }),

/***/ "./node_modules/_source-map@0.7.3@source-map/lib/util.js":
/*!***************************************************************!*\
  !*** ./node_modules/_source-map@0.7.3@source-map/lib/util.js ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, exports) => {

/* -*- Mode: js; js-indent-level: 2; -*- */

/*
 * Copyright 2011 Mozilla Foundation and contributors
 * Licensed under the New BSD license. See LICENSE or:
 * http://opensource.org/licenses/BSD-3-Clause
 */

/**
 * This is a helper function for getting values from parameter/options
 * objects.
 *
 * @param args The object we are extracting values from
 * @param name The name of the property we are getting.
 * @param defaultValue An optional value to return if the property is missing
 * from the object. If this is not specified and the property is missing, an
 * error will be thrown.
 */
function getArg(aArgs, aName, aDefaultValue) {
  if (aName in aArgs) {
    return aArgs[aName];
  } else if (arguments.length === 3) {
    return aDefaultValue;
  }

  throw new Error('"' + aName + '" is a required argument.');
}

exports.getArg = getArg;
var urlRegexp = /^(?:([\w+\-.]+):)?\/\/(?:(\w+:\w+)@)?([\w.-]*)(?::(\d+))?(.*)$/;
var dataUrlRegexp = /^data:.+\,.+$/;

function urlParse(aUrl) {
  var match = aUrl.match(urlRegexp);

  if (!match) {
    return null;
  }

  return {
    scheme: match[1],
    auth: match[2],
    host: match[3],
    port: match[4],
    path: match[5]
  };
}

exports.urlParse = urlParse;

function urlGenerate(aParsedUrl) {
  var url = "";

  if (aParsedUrl.scheme) {
    url += aParsedUrl.scheme + ":";
  }

  url += "//";

  if (aParsedUrl.auth) {
    url += aParsedUrl.auth + "@";
  }

  if (aParsedUrl.host) {
    url += aParsedUrl.host;
  }

  if (aParsedUrl.port) {
    url += ":" + aParsedUrl.port;
  }

  if (aParsedUrl.path) {
    url += aParsedUrl.path;
  }

  return url;
}

exports.urlGenerate = urlGenerate;
var MAX_CACHED_INPUTS = 32;
/**
 * Takes some function `f(input) -> result` and returns a memoized version of
 * `f`.
 *
 * We keep at most `MAX_CACHED_INPUTS` memoized results of `f` alive. The
 * memoization is a dumb-simple, linear least-recently-used cache.
 */

function lruMemoize(f) {
  var cache = [];
  return function (input) {
    for (var i = 0; i < cache.length; i++) {
      if (cache[i].input === input) {
        var temp = cache[0];
        cache[0] = cache[i];
        cache[i] = temp;
        return cache[0].result;
      }
    }

    var result = f(input);
    cache.unshift({
      input: input,
      result: result
    });

    if (cache.length > MAX_CACHED_INPUTS) {
      cache.pop();
    }

    return result;
  };
}
/**
 * Normalizes a path, or the path portion of a URL:
 *
 * - Replaces consecutive slashes with one slash.
 * - Removes unnecessary '.' parts.
 * - Removes unnecessary '<dir>/..' parts.
 *
 * Based on code in the Node.js 'path' core module.
 *
 * @param aPath The path or url to normalize.
 */


var normalize = lruMemoize(function normalize(aPath) {
  var path = aPath;
  var url = urlParse(aPath);

  if (url) {
    if (!url.path) {
      return aPath;
    }

    path = url.path;
  }

  var isAbsolute = exports.isAbsolute(path); // Split the path into parts between `/` characters. This is much faster than
  // using `.split(/\/+/g)`.

  var parts = [];
  var start = 0;
  var i = 0;

  while (true) {
    start = i;
    i = path.indexOf("/", start);

    if (i === -1) {
      parts.push(path.slice(start));
      break;
    } else {
      parts.push(path.slice(start, i));

      while (i < path.length && path[i] === "/") {
        i++;
      }
    }
  }

  var up = 0;

  for (i = parts.length - 1; i >= 0; i--) {
    var part = parts[i];

    if (part === ".") {
      parts.splice(i, 1);
    } else if (part === "..") {
      up++;
    } else if (up > 0) {
      if (part === "") {
        // The first part is blank if the path is absolute. Trying to go
        // above the root is a no-op. Therefore we can remove all '..' parts
        // directly after the root.
        parts.splice(i + 1, up);
        up = 0;
      } else {
        parts.splice(i, 2);
        up--;
      }
    }
  }

  path = parts.join("/");

  if (path === "") {
    path = isAbsolute ? "/" : ".";
  }

  if (url) {
    url.path = path;
    return urlGenerate(url);
  }

  return path;
});
exports.normalize = normalize;
/**
 * Joins two paths/URLs.
 *
 * @param aRoot The root path or URL.
 * @param aPath The path or URL to be joined with the root.
 *
 * - If aPath is a URL or a data URI, aPath is returned, unless aPath is a
 *   scheme-relative URL: Then the scheme of aRoot, if any, is prepended
 *   first.
 * - Otherwise aPath is a path. If aRoot is a URL, then its path portion
 *   is updated with the result and aRoot is returned. Otherwise the result
 *   is returned.
 *   - If aPath is absolute, the result is aPath.
 *   - Otherwise the two paths are joined with a slash.
 * - Joining for example 'http://' and 'www.example.com' is also supported.
 */

function join(aRoot, aPath) {
  if (aRoot === "") {
    aRoot = ".";
  }

  if (aPath === "") {
    aPath = ".";
  }

  var aPathUrl = urlParse(aPath);
  var aRootUrl = urlParse(aRoot);

  if (aRootUrl) {
    aRoot = aRootUrl.path || "/";
  } // `join(foo, '//www.example.org')`


  if (aPathUrl && !aPathUrl.scheme) {
    if (aRootUrl) {
      aPathUrl.scheme = aRootUrl.scheme;
    }

    return urlGenerate(aPathUrl);
  }

  if (aPathUrl || aPath.match(dataUrlRegexp)) {
    return aPath;
  } // `join('http://', 'www.example.com')`


  if (aRootUrl && !aRootUrl.host && !aRootUrl.path) {
    aRootUrl.host = aPath;
    return urlGenerate(aRootUrl);
  }

  var joined = aPath.charAt(0) === "/" ? aPath : normalize(aRoot.replace(/\/+$/, "") + "/" + aPath);

  if (aRootUrl) {
    aRootUrl.path = joined;
    return urlGenerate(aRootUrl);
  }

  return joined;
}

exports.join = join;

exports.isAbsolute = function (aPath) {
  return aPath.charAt(0) === "/" || urlRegexp.test(aPath);
};
/**
 * Make a path relative to a URL or another path.
 *
 * @param aRoot The root path or URL.
 * @param aPath The path or URL to be made relative to aRoot.
 */


function relative(aRoot, aPath) {
  if (aRoot === "") {
    aRoot = ".";
  }

  aRoot = aRoot.replace(/\/$/, ""); // It is possible for the path to be above the root. In this case, simply
  // checking whether the root is a prefix of the path won't work. Instead, we
  // need to remove components from the root one by one, until either we find
  // a prefix that fits, or we run out of components to remove.

  var level = 0;

  while (aPath.indexOf(aRoot + "/") !== 0) {
    var index = aRoot.lastIndexOf("/");

    if (index < 0) {
      return aPath;
    } // If the only part of the root that is left is the scheme (i.e. http://,
    // file:///, etc.), one or more slashes (/), or simply nothing at all, we
    // have exhausted all components, so the path is not relative to the root.


    aRoot = aRoot.slice(0, index);

    if (aRoot.match(/^([^\/]+:\/)?\/*$/)) {
      return aPath;
    }

    ++level;
  } // Make sure we add a "../" for each component we removed from the root.


  return Array(level + 1).join("../") + aPath.substr(aRoot.length + 1);
}

exports.relative = relative;

var supportsNullProto = function () {
  var obj = Object.create(null);
  return !("__proto__" in obj);
}();

function identity(s) {
  return s;
}
/**
 * Because behavior goes wacky when you set `__proto__` on objects, we
 * have to prefix all the strings in our set with an arbitrary character.
 *
 * See https://github.com/mozilla/source-map/pull/31 and
 * https://github.com/mozilla/source-map/issues/30
 *
 * @param String aStr
 */


function toSetString(aStr) {
  if (isProtoString(aStr)) {
    return "$" + aStr;
  }

  return aStr;
}

exports.toSetString = supportsNullProto ? identity : toSetString;

function fromSetString(aStr) {
  if (isProtoString(aStr)) {
    return aStr.slice(1);
  }

  return aStr;
}

exports.fromSetString = supportsNullProto ? identity : fromSetString;

function isProtoString(s) {
  if (!s) {
    return false;
  }

  var length = s.length;

  if (length < 9
  /* "__proto__".length */
  ) {
      return false;
    }
  /* eslint-disable no-multi-spaces */


  if (s.charCodeAt(length - 1) !== 95
  /* '_' */
  || s.charCodeAt(length - 2) !== 95
  /* '_' */
  || s.charCodeAt(length - 3) !== 111
  /* 'o' */
  || s.charCodeAt(length - 4) !== 116
  /* 't' */
  || s.charCodeAt(length - 5) !== 111
  /* 'o' */
  || s.charCodeAt(length - 6) !== 114
  /* 'r' */
  || s.charCodeAt(length - 7) !== 112
  /* 'p' */
  || s.charCodeAt(length - 8) !== 95
  /* '_' */
  || s.charCodeAt(length - 9) !== 95
  /* '_' */
  ) {
      return false;
    }
  /* eslint-enable no-multi-spaces */


  for (var i = length - 10; i >= 0; i--) {
    if (s.charCodeAt(i) !== 36
    /* '$' */
    ) {
        return false;
      }
  }

  return true;
}
/**
 * Comparator between two mappings where the original positions are compared.
 *
 * Optionally pass in `true` as `onlyCompareGenerated` to consider two
 * mappings with the same original source/line/column, but different generated
 * line and column the same. Useful when searching for a mapping with a
 * stubbed out mapping.
 */


function compareByOriginalPositions(mappingA, mappingB, onlyCompareOriginal) {
  var cmp = strcmp(mappingA.source, mappingB.source);

  if (cmp !== 0) {
    return cmp;
  }

  cmp = mappingA.originalLine - mappingB.originalLine;

  if (cmp !== 0) {
    return cmp;
  }

  cmp = mappingA.originalColumn - mappingB.originalColumn;

  if (cmp !== 0 || onlyCompareOriginal) {
    return cmp;
  }

  cmp = mappingA.generatedColumn - mappingB.generatedColumn;

  if (cmp !== 0) {
    return cmp;
  }

  cmp = mappingA.generatedLine - mappingB.generatedLine;

  if (cmp !== 0) {
    return cmp;
  }

  return strcmp(mappingA.name, mappingB.name);
}

exports.compareByOriginalPositions = compareByOriginalPositions;
/**
 * Comparator between two mappings with deflated source and name indices where
 * the generated positions are compared.
 *
 * Optionally pass in `true` as `onlyCompareGenerated` to consider two
 * mappings with the same generated line and column, but different
 * source/name/original line and column the same. Useful when searching for a
 * mapping with a stubbed out mapping.
 */

function compareByGeneratedPositionsDeflated(mappingA, mappingB, onlyCompareGenerated) {
  var cmp = mappingA.generatedLine - mappingB.generatedLine;

  if (cmp !== 0) {
    return cmp;
  }

  cmp = mappingA.generatedColumn - mappingB.generatedColumn;

  if (cmp !== 0 || onlyCompareGenerated) {
    return cmp;
  }

  cmp = strcmp(mappingA.source, mappingB.source);

  if (cmp !== 0) {
    return cmp;
  }

  cmp = mappingA.originalLine - mappingB.originalLine;

  if (cmp !== 0) {
    return cmp;
  }

  cmp = mappingA.originalColumn - mappingB.originalColumn;

  if (cmp !== 0) {
    return cmp;
  }

  return strcmp(mappingA.name, mappingB.name);
}

exports.compareByGeneratedPositionsDeflated = compareByGeneratedPositionsDeflated;

function strcmp(aStr1, aStr2) {
  if (aStr1 === aStr2) {
    return 0;
  }

  if (aStr1 === null) {
    return 1; // aStr2 !== null
  }

  if (aStr2 === null) {
    return -1; // aStr1 !== null
  }

  if (aStr1 > aStr2) {
    return 1;
  }

  return -1;
}
/**
 * Comparator between two mappings with inflated source and name strings where
 * the generated positions are compared.
 */


function compareByGeneratedPositionsInflated(mappingA, mappingB) {
  var cmp = mappingA.generatedLine - mappingB.generatedLine;

  if (cmp !== 0) {
    return cmp;
  }

  cmp = mappingA.generatedColumn - mappingB.generatedColumn;

  if (cmp !== 0) {
    return cmp;
  }

  cmp = strcmp(mappingA.source, mappingB.source);

  if (cmp !== 0) {
    return cmp;
  }

  cmp = mappingA.originalLine - mappingB.originalLine;

  if (cmp !== 0) {
    return cmp;
  }

  cmp = mappingA.originalColumn - mappingB.originalColumn;

  if (cmp !== 0) {
    return cmp;
  }

  return strcmp(mappingA.name, mappingB.name);
}

exports.compareByGeneratedPositionsInflated = compareByGeneratedPositionsInflated;
/**
 * Strip any JSON XSSI avoidance prefix from the string (as documented
 * in the source maps specification), and then parse the string as
 * JSON.
 */

function parseSourceMapInput(str) {
  return JSON.parse(str.replace(/^\)]}'[^\n]*\n/, ""));
}

exports.parseSourceMapInput = parseSourceMapInput;
/**
 * Compute the URL of a source given the the source root, the source's
 * URL, and the source map's URL.
 */

function computeSourceURL(sourceRoot, sourceURL, sourceMapURL) {
  sourceURL = sourceURL || "";

  if (sourceRoot) {
    // This follows what Chrome does.
    if (sourceRoot[sourceRoot.length - 1] !== "/" && sourceURL[0] !== "/") {
      sourceRoot += "/";
    } // The spec says:
    //   Line 4: An optional source root, useful for relocating source
    //   files on a server or removing repeated values in the
    //   “sources” entry.  This value is prepended to the individual
    //   entries in the “source” field.


    sourceURL = sourceRoot + sourceURL;
  } // Historically, SourceMapConsumer did not take the sourceMapURL as
  // a parameter.  This mode is still somewhat supported, which is why
  // this code block is conditional.  However, it's preferable to pass
  // the source map URL to SourceMapConsumer, so that this function
  // can implement the source URL resolution algorithm as outlined in
  // the spec.  This block is basically the equivalent of:
  //    new URL(sourceURL, sourceMapURL).toString()
  // ... except it avoids using URL, which wasn't available in the
  // older releases of node still supported by this library.
  //
  // The spec says:
  //   If the sources are not absolute URLs after prepending of the
  //   “sourceRoot”, the sources are resolved relative to the
  //   SourceMap (like resolving script src in a html document).


  if (sourceMapURL) {
    var parsed = urlParse(sourceMapURL);

    if (!parsed) {
      throw new Error("sourceMapURL could not be parsed");
    }

    if (parsed.path) {
      // Strip the last path component, but keep the "/".
      var index = parsed.path.lastIndexOf("/");

      if (index >= 0) {
        parsed.path = parsed.path.substring(0, index + 1);
      }
    }

    sourceURL = join(urlGenerate(parsed), sourceURL);
  }

  return normalize(sourceURL);
}

exports.computeSourceURL = computeSourceURL;

/***/ }),

/***/ "./node_modules/_source-map@0.7.3@source-map/lib/wasm.js":
/*!***************************************************************!*\
  !*** ./node_modules/_source-map@0.7.3@source-map/lib/wasm.js ***!
  \***************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var readWasm = __webpack_require__(/*! ../lib/read-wasm */ "./node_modules/_source-map@0.7.3@source-map/lib/read-wasm.js");
/**
 * Provide the JIT with a nice shape / hidden class.
 */


function Mapping() {
  this.generatedLine = 0;
  this.generatedColumn = 0;
  this.lastGeneratedColumn = null;
  this.source = null;
  this.originalLine = null;
  this.originalColumn = null;
  this.name = null;
}

var cachedWasm = null;

module.exports = function wasm() {
  if (cachedWasm) {
    return cachedWasm;
  }

  var callbackStack = [];
  cachedWasm = readWasm().then(function (buffer) {
    return WebAssembly.instantiate(buffer, {
      env: {
        mapping_callback: function mapping_callback(generatedLine, generatedColumn, hasLastGeneratedColumn, lastGeneratedColumn, hasOriginal, source, originalLine, originalColumn, hasName, name) {
          var mapping = new Mapping(); // JS uses 1-based line numbers, wasm uses 0-based.

          mapping.generatedLine = generatedLine + 1;
          mapping.generatedColumn = generatedColumn;

          if (hasLastGeneratedColumn) {
            // JS uses inclusive last generated column, wasm uses exclusive.
            mapping.lastGeneratedColumn = lastGeneratedColumn - 1;
          }

          if (hasOriginal) {
            mapping.source = source; // JS uses 1-based line numbers, wasm uses 0-based.

            mapping.originalLine = originalLine + 1;
            mapping.originalColumn = originalColumn;

            if (hasName) {
              mapping.name = name;
            }
          }

          callbackStack[callbackStack.length - 1](mapping);
        },
        start_all_generated_locations_for: function start_all_generated_locations_for() {
          console.time("all_generated_locations_for");
        },
        end_all_generated_locations_for: function end_all_generated_locations_for() {
          console.timeEnd("all_generated_locations_for");
        },
        start_compute_column_spans: function start_compute_column_spans() {
          console.time("compute_column_spans");
        },
        end_compute_column_spans: function end_compute_column_spans() {
          console.timeEnd("compute_column_spans");
        },
        start_generated_location_for: function start_generated_location_for() {
          console.time("generated_location_for");
        },
        end_generated_location_for: function end_generated_location_for() {
          console.timeEnd("generated_location_for");
        },
        start_original_location_for: function start_original_location_for() {
          console.time("original_location_for");
        },
        end_original_location_for: function end_original_location_for() {
          console.timeEnd("original_location_for");
        },
        start_parse_mappings: function start_parse_mappings() {
          console.time("parse_mappings");
        },
        end_parse_mappings: function end_parse_mappings() {
          console.timeEnd("parse_mappings");
        },
        start_sort_by_generated_location: function start_sort_by_generated_location() {
          console.time("sort_by_generated_location");
        },
        end_sort_by_generated_location: function end_sort_by_generated_location() {
          console.timeEnd("sort_by_generated_location");
        },
        start_sort_by_original_location: function start_sort_by_original_location() {
          console.time("sort_by_original_location");
        },
        end_sort_by_original_location: function end_sort_by_original_location() {
          console.timeEnd("sort_by_original_location");
        }
      }
    });
  }).then(function (Wasm) {
    return {
      exports: Wasm.instance.exports,
      withMappingCallback: function withMappingCallback(mappingCallback, f) {
        callbackStack.push(mappingCallback);

        try {
          f();
        } finally {
          callbackStack.pop();
        }
      }
    };
  }).then(null, function (e) {
    cachedWasm = null;
    throw e;
  });
  return cachedWasm;
};

/***/ }),

/***/ "./node_modules/_source-map@0.7.3@source-map/source-map.js":
/*!*****************************************************************!*\
  !*** ./node_modules/_source-map@0.7.3@source-map/source-map.js ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

/*
 * Copyright 2009-2011 Mozilla Foundation and contributors
 * Licensed under the New BSD license. See LICENSE.txt or:
 * http://opensource.org/licenses/BSD-3-Clause
 */
exports.SourceMapGenerator = __webpack_require__(/*! ./lib/source-map-generator */ "./node_modules/_source-map@0.7.3@source-map/lib/source-map-generator.js").SourceMapGenerator;
exports.SourceMapConsumer = __webpack_require__(/*! ./lib/source-map-consumer */ "./node_modules/_source-map@0.7.3@source-map/lib/source-map-consumer.js").SourceMapConsumer;
exports.SourceNode = __webpack_require__(/*! ./lib/source-node */ "./node_modules/_source-map@0.7.3@source-map/lib/source-node.js").SourceNode;

/***/ }),

/***/ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/CachedSource.js":
/*!*********************************************************************************!*\
  !*** ./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/CachedSource.js ***!
  \*********************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/


function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it.return != null) it.return(); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var Source = __webpack_require__(/*! ./Source */ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/Source.js");

var _require = __webpack_require__(/*! source-map */ "./node_modules/_source-map@0.6.1@source-map/source-map.js"),
    SourceMapConsumer = _require.SourceMapConsumer,
    SourceNode = _require.SourceNode;

var _require2 = __webpack_require__(/*! source-list-map */ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/index.js"),
    SourceListMap = _require2.SourceListMap,
    fromStringWithSourceMap = _require2.fromStringWithSourceMap;

var LIST_MAP_OPTIONS = {
  columns: false
};

var mapToBufferedMap = function mapToBufferedMap(map) {
  if (_typeof(map) !== "object" || !map) return map;
  var bufferedMap = Object.assign({}, map);

  if (map.mappings) {
    bufferedMap.mappings = Buffer.from(map.mappings, "utf-8");
  }

  if (map.sourcesContent) {
    bufferedMap.sourcesContent = map.sourcesContent.map(function (str) {
      return str && Buffer.from(str, "utf-8");
    });
  }

  return bufferedMap;
};

var bufferedMapToMap = function bufferedMapToMap(bufferedMap) {
  if (_typeof(bufferedMap) !== "object" || !bufferedMap) return bufferedMap;
  var map = Object.assign({}, bufferedMap);

  if (bufferedMap.mappings) {
    map.mappings = bufferedMap.mappings.toString("utf-8");
  }

  if (bufferedMap.sourcesContent) {
    map.sourcesContent = bufferedMap.sourcesContent.map(function (buffer) {
      return buffer && buffer.toString("utf-8");
    });
  }

  return map;
};

var sourceAndMapToNode = function sourceAndMapToNode(source, map) {
  if (map) {
    return SourceNode.fromStringWithSourceMap(source, new SourceMapConsumer(map));
  } else {
    return new SourceNode(null, null, null, source);
  }
};

var sourceAndMapToListMap = function sourceAndMapToListMap(source, map) {
  if (map) {
    return fromStringWithSourceMap(source, map);
  } else {
    return new SourceListMap(source);
  }
};

var CachedSource = /*#__PURE__*/function (_Source) {
  _inherits(CachedSource, _Source);

  var _super = _createSuper(CachedSource);

  function CachedSource(source, cachedData) {
    var _this;

    _classCallCheck(this, CachedSource);

    _this = _super.call(this);
    _this._source = source;
    _this._cachedSourceType = cachedData ? cachedData.source : undefined;
    _this._cachedSource = undefined;
    _this._cachedBuffer = cachedData ? cachedData.buffer : undefined;
    _this._cachedSize = cachedData ? cachedData.size : undefined;
    _this._cachedMaps = cachedData ? cachedData.maps : new Map();
    _this._cachedHashUpdate = cachedData ? cachedData.hash : undefined;
    return _this;
  }

  _createClass(CachedSource, [{
    key: "getCachedData",
    value: function getCachedData() {
      var bufferedMaps = new Map();

      var _iterator = _createForOfIteratorHelper(this._cachedMaps),
          _step;

      try {
        for (_iterator.s(); !(_step = _iterator.n()).done;) {
          var pair = _step.value;
          var cacheEntry = pair[1];

          if (cacheEntry.bufferedMap === undefined) {
            cacheEntry.bufferedMap = mapToBufferedMap(this._getMapFromCacheEntry(cacheEntry));
          }

          bufferedMaps.set(pair[0], {
            map: undefined,
            node: undefined,
            listMap: undefined,
            bufferedMap: cacheEntry.bufferedMap
          });
        } // We don't want to cache strings
        // So if we have a caches sources
        // create a buffer from it and only store
        // if it was a Buffer or string

      } catch (err) {
        _iterator.e(err);
      } finally {
        _iterator.f();
      }

      if (this._cachedSource) {
        this.buffer();
      }

      return {
        buffer: this._cachedBuffer,
        source: this._cachedSourceType !== undefined ? this._cachedSourceType : typeof this._cachedSource === "string" ? true : Buffer.isBuffer(this._cachedSource) ? false : undefined,
        size: this._cachedSize,
        maps: bufferedMaps,
        hash: this._cachedHashUpdate
      };
    }
  }, {
    key: "originalLazy",
    value: function originalLazy() {
      return this._source;
    }
  }, {
    key: "original",
    value: function original() {
      if (typeof this._source === "function") this._source = this._source();
      return this._source;
    }
  }, {
    key: "source",
    value: function source() {
      var source = this._getCachedSource();

      if (source !== undefined) return source;
      return this._cachedSource = this.original().source();
    }
  }, {
    key: "_getMapFromCacheEntry",
    value: function _getMapFromCacheEntry(cacheEntry) {
      if (cacheEntry.map !== undefined) {
        return cacheEntry.map;
      } else if (cacheEntry.bufferedMap !== undefined) {
        return cacheEntry.map = bufferedMapToMap(cacheEntry.bufferedMap);
      } else if (cacheEntry.node !== undefined) {
        var result = cacheEntry.node.toStringWithSourceMap({
          file: "x"
        });
        if (this._cachedSource === undefined) this._cachedSource = result.code;
        return cacheEntry.map = result.map.toJSON();
      } else if (cacheEntry.listMap !== undefined) {
        var _result = cacheEntry.listMap.toStringWithSourceMap({
          file: "x"
        });

        if (this._cachedSource === undefined) this._cachedSource = _result.source;
        return cacheEntry.map = _result.map;
      }
    }
  }, {
    key: "_getCachedSource",
    value: function _getCachedSource() {
      if (this._cachedSource !== undefined) return this._cachedSource;

      if (this._cachedBuffer && this._cachedSourceType !== undefined) {
        return this._cachedSource = this._cachedSourceType ? this._cachedBuffer.toString("utf-8") : this._cachedBuffer;
      }

      var _iterator2 = _createForOfIteratorHelper(this._cachedMaps.values()),
          _step2;

      try {
        for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
          var cacheEntry = _step2.value;

          if (cacheEntry.node !== undefined) {
            return this._cachedSource = cacheEntry.node.toString();
          }

          if (cacheEntry.listMap !== undefined) {
            return this._cachedSource = cacheEntry.listMap.toString();
          }
        }
      } catch (err) {
        _iterator2.e(err);
      } finally {
        _iterator2.f();
      }
    }
  }, {
    key: "buffer",
    value: function buffer() {
      if (this._cachedBuffer !== undefined) return this._cachedBuffer;

      if (this._cachedSource !== undefined) {
        if (Buffer.isBuffer(this._cachedSource)) {
          return this._cachedBuffer = this._cachedSource;
        }

        return this._cachedBuffer = Buffer.from(this._cachedSource, "utf-8");
      }

      if (typeof this.original().buffer === "function") {
        return this._cachedBuffer = this.original().buffer();
      }

      var bufferOrString = this.source();

      if (Buffer.isBuffer(bufferOrString)) {
        return this._cachedBuffer = bufferOrString;
      }

      return this._cachedBuffer = Buffer.from(bufferOrString, "utf-8");
    }
  }, {
    key: "size",
    value: function size() {
      if (this._cachedSize !== undefined) return this._cachedSize;

      if (this._cachedBuffer !== undefined) {
        return this._cachedSize = this._cachedBuffer.length;
      }

      var source = this._getCachedSource();

      if (source !== undefined) {
        return this._cachedSize = Buffer.byteLength(source);
      }

      return this._cachedSize = this.original().size();
    }
  }, {
    key: "sourceAndMap",
    value: function sourceAndMap(options) {
      var key = options ? JSON.stringify(options) : "{}";

      var cacheEntry = this._cachedMaps.get(key); // Look for a cached map


      if (cacheEntry !== undefined) {
        // We have a cached map in some representation
        var _map = this._getMapFromCacheEntry(cacheEntry); // Either get the cached source or compute it


        return {
          source: this.source(),
          map: _map
        };
      } // Look for a cached source


      var source = this._getCachedSource(); // Compute the map


      var map;

      if (source !== undefined) {
        map = this.original().map(options);
      } else {
        // Compute the source and map together.
        var sourceAndMap = this.original().sourceAndMap(options);
        source = sourceAndMap.source;
        map = sourceAndMap.map;
        this._cachedSource = source;
      }

      this._cachedMaps.set(key, {
        map: map,
        node: undefined,
        listMap: undefined,
        bufferedMap: undefined
      });

      return {
        source: source,
        map: map
      };
    }
  }, {
    key: "node",
    value: function node(options) {
      var key = options ? JSON.stringify(options) : "{}";

      var cacheEntry = this._cachedMaps.get(key); // Check cache


      if (cacheEntry !== undefined) {
        // Directly cached
        if (cacheEntry.node) return cacheEntry.node; // Construct from cached map

        var map = this._getMapFromCacheEntry(cacheEntry);

        var source = this.source();

        var _node = sourceAndMapToNode(source, map);

        cacheEntry.node = _node;
        return _node;
      }

      var node;
      var original = this.original();

      if (typeof original.node === "function") {
        node = original.node(options);

        this._cachedMaps.set(key, {
          map: undefined,
          node: node,
          listMap: undefined,
          bufferedMap: undefined
        });
      } else {
        var sourceAndMap = this.sourceAndMap(options);
        node = sourceAndMapToNode(sourceAndMap.source, sourceAndMap.map);
        this._cachedMaps.get(key).node = node;
      }

      return node;
    }
  }, {
    key: "listMap",
    value: function listMap(options) {
      var key; // Enforce options must include columns: false

      if (!options) {
        key = '{"columns":false}';
        options = LIST_MAP_OPTIONS;
      } else {
        if (options.columns !== false) {
          options = Object.assign({}, options, LIST_MAP_OPTIONS);
        }

        key = JSON.stringify(options);
      } // Check cache


      var cacheEntry = this._cachedMaps.get(key);

      if (cacheEntry !== undefined) {
        // Directly cached
        if (cacheEntry.listMap) return cacheEntry.listMap; // Construct from cached map

        var map = this._getMapFromCacheEntry(cacheEntry);

        var source = this.source();

        var _listMap = sourceAndMapToListMap(source, map);

        cacheEntry.listMap = _listMap;
        return _listMap;
      }

      var listMap;
      var original = this.original();

      if (typeof original.listMap === "function") {
        listMap = original.listMap(options);

        this._cachedMaps.set(key, {
          map: undefined,
          node: undefined,
          listMap: listMap,
          bufferedMap: undefined
        });
      } else {
        var sourceAndMap = this.sourceAndMap(options);
        listMap = sourceAndMapToListMap(sourceAndMap.source, sourceAndMap.map);
        this._cachedMaps.get(key).listMap = listMap;
      }

      return listMap;
    }
  }, {
    key: "map",
    value: function map(options) {
      var key = options ? JSON.stringify(options) : "{}";

      var cacheEntry = this._cachedMaps.get(key);

      if (cacheEntry !== undefined) {
        return this._getMapFromCacheEntry(cacheEntry);
      }

      var map = this.original().map(options);

      this._cachedMaps.set(key, {
        map: map,
        node: undefined,
        listMap: undefined,
        bufferedMap: undefined
      });

      return map;
    }
  }, {
    key: "updateHash",
    value: function updateHash(hash) {
      if (this._cachedHashUpdate !== undefined) {
        var _iterator3 = _createForOfIteratorHelper(this._cachedHashUpdate),
            _step3;

        try {
          for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
            var item = _step3.value;
            hash.update(item);
          }
        } catch (err) {
          _iterator3.e(err);
        } finally {
          _iterator3.f();
        }

        return;
      }

      var _update = [];
      var currentString = undefined;
      var tracker = {
        update: function update(item) {
          if (typeof item === "string" && item.length < 10240) {
            if (currentString === undefined) {
              currentString = item;
            } else {
              currentString += item;

              if (currentString > 102400) {
                _update.push(Buffer.from(currentString));

                currentString = undefined;
              }
            }
          } else {
            if (currentString !== undefined) {
              _update.push(Buffer.from(currentString));

              currentString = undefined;
            }

            _update.push(item);
          }
        }
      };
      this.original().updateHash(tracker);

      if (currentString !== undefined) {
        _update.push(Buffer.from(currentString));
      }

      for (var _i = 0, _update2 = _update; _i < _update2.length; _i++) {
        var _item = _update2[_i];
        hash.update(_item);
      }

      this._cachedHashUpdate = _update;
    }
  }]);

  return CachedSource;
}(Source);

module.exports = CachedSource;

/***/ }),

/***/ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/CompatSource.js":
/*!*********************************************************************************!*\
  !*** ./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/CompatSource.js ***!
  \*********************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/


function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var Source = __webpack_require__(/*! ./Source */ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/Source.js");

var CompatSource = /*#__PURE__*/function (_Source) {
  _inherits(CompatSource, _Source);

  var _super = _createSuper(CompatSource);

  function CompatSource(sourceLike) {
    var _this;

    _classCallCheck(this, CompatSource);

    _this = _super.call(this);
    _this._sourceLike = sourceLike;
    return _this;
  }

  _createClass(CompatSource, [{
    key: "source",
    value: function source() {
      return this._sourceLike.source();
    }
  }, {
    key: "buffer",
    value: function buffer() {
      if (typeof this._sourceLike.buffer === "function") {
        return this._sourceLike.buffer();
      }

      return _get(_getPrototypeOf(CompatSource.prototype), "buffer", this).call(this);
    }
  }, {
    key: "size",
    value: function size() {
      if (typeof this._sourceLike.size === "function") {
        return this._sourceLike.size();
      }

      return _get(_getPrototypeOf(CompatSource.prototype), "size", this).call(this);
    }
  }, {
    key: "map",
    value: function map(options) {
      if (typeof this._sourceLike.map === "function") {
        return this._sourceLike.map(options);
      }

      return _get(_getPrototypeOf(CompatSource.prototype), "map", this).call(this, options);
    }
  }, {
    key: "sourceAndMap",
    value: function sourceAndMap(options) {
      if (typeof this._sourceLike.sourceAndMap === "function") {
        return this._sourceLike.sourceAndMap(options);
      }

      return _get(_getPrototypeOf(CompatSource.prototype), "sourceAndMap", this).call(this, options);
    }
  }, {
    key: "updateHash",
    value: function updateHash(hash) {
      if (typeof this._sourceLike.updateHash === "function") {
        return this._sourceLike.updateHash(hash);
      }

      if (typeof this._sourceLike.map === "function") {
        throw new Error("A Source-like object with a 'map' method must also provide an 'updateHash' method");
      }

      hash.update(this.buffer());
    }
  }], [{
    key: "from",
    value: function from(sourceLike) {
      return sourceLike instanceof Source ? sourceLike : new CompatSource(sourceLike);
    }
  }]);

  return CompatSource;
}(Source);

module.exports = CompatSource;

/***/ }),

/***/ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/ConcatSource.js":
/*!*********************************************************************************!*\
  !*** ./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/ConcatSource.js ***!
  \*********************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/


function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it.return != null) it.return(); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var Source = __webpack_require__(/*! ./Source */ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/Source.js");

var RawSource = __webpack_require__(/*! ./RawSource */ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/RawSource.js");

var _require = __webpack_require__(/*! source-map */ "./node_modules/_source-map@0.6.1@source-map/source-map.js"),
    SourceNode = _require.SourceNode,
    SourceMapConsumer = _require.SourceMapConsumer;

var _require2 = __webpack_require__(/*! source-list-map */ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/index.js"),
    SourceListMap = _require2.SourceListMap,
    fromStringWithSourceMap = _require2.fromStringWithSourceMap;

var _require3 = __webpack_require__(/*! ./helpers */ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/helpers.js"),
    getSourceAndMap = _require3.getSourceAndMap,
    getMap = _require3.getMap;

var stringsAsRawSources = new WeakSet();

var ConcatSource = /*#__PURE__*/function (_Source) {
  _inherits(ConcatSource, _Source);

  var _super = _createSuper(ConcatSource);

  function ConcatSource() {
    var _this;

    _classCallCheck(this, ConcatSource);

    _this = _super.call(this);
    _this._children = [];

    for (var i = 0; i < arguments.length; i++) {
      var item = arguments[i];

      if (item instanceof ConcatSource) {
        var _iterator = _createForOfIteratorHelper(item._children),
            _step;

        try {
          for (_iterator.s(); !(_step = _iterator.n()).done;) {
            var child = _step.value;

            _this._children.push(child);
          }
        } catch (err) {
          _iterator.e(err);
        } finally {
          _iterator.f();
        }
      } else {
        _this._children.push(item);
      }
    }

    _this._isOptimized = arguments.length === 0;
    return _this;
  }

  _createClass(ConcatSource, [{
    key: "getChildren",
    value: function getChildren() {
      if (!this._isOptimized) this._optimize();
      return this._children;
    }
  }, {
    key: "add",
    value: function add(item) {
      if (item instanceof ConcatSource) {
        var _iterator2 = _createForOfIteratorHelper(item._children),
            _step2;

        try {
          for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
            var child = _step2.value;

            this._children.push(child);
          }
        } catch (err) {
          _iterator2.e(err);
        } finally {
          _iterator2.f();
        }
      } else {
        this._children.push(item);
      }

      this._isOptimized = false;
    }
  }, {
    key: "addAllSkipOptimizing",
    value: function addAllSkipOptimizing(items) {
      var _iterator3 = _createForOfIteratorHelper(items),
          _step3;

      try {
        for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
          var item = _step3.value;

          this._children.push(item);
        }
      } catch (err) {
        _iterator3.e(err);
      } finally {
        _iterator3.f();
      }
    }
  }, {
    key: "buffer",
    value: function buffer() {
      if (!this._isOptimized) this._optimize();
      var buffers = [];

      var _iterator4 = _createForOfIteratorHelper(this._children),
          _step4;

      try {
        for (_iterator4.s(); !(_step4 = _iterator4.n()).done;) {
          var child = _step4.value;

          if (typeof child.buffer === "function") {
            buffers.push(child.buffer());
          } else {
            var bufferOrString = child.source();

            if (Buffer.isBuffer(bufferOrString)) {
              buffers.push(bufferOrString);
            } else {
              buffers.push(Buffer.from(bufferOrString, "utf-8"));
            }
          }
        }
      } catch (err) {
        _iterator4.e(err);
      } finally {
        _iterator4.f();
      }

      return Buffer.concat(buffers);
    }
  }, {
    key: "source",
    value: function source() {
      if (!this._isOptimized) this._optimize();
      var source = "";

      var _iterator5 = _createForOfIteratorHelper(this._children),
          _step5;

      try {
        for (_iterator5.s(); !(_step5 = _iterator5.n()).done;) {
          var child = _step5.value;
          source += child.source();
        }
      } catch (err) {
        _iterator5.e(err);
      } finally {
        _iterator5.f();
      }

      return source;
    }
  }, {
    key: "size",
    value: function size() {
      if (!this._isOptimized) this._optimize();
      var size = 0;

      var _iterator6 = _createForOfIteratorHelper(this._children),
          _step6;

      try {
        for (_iterator6.s(); !(_step6 = _iterator6.n()).done;) {
          var child = _step6.value;
          size += child.size();
        }
      } catch (err) {
        _iterator6.e(err);
      } finally {
        _iterator6.f();
      }

      return size;
    }
  }, {
    key: "map",
    value: function map(options) {
      return getMap(this, options);
    }
  }, {
    key: "sourceAndMap",
    value: function sourceAndMap(options) {
      return getSourceAndMap(this, options);
    }
  }, {
    key: "node",
    value: function node(options) {
      if (!this._isOptimized) this._optimize();
      var node = new SourceNode(null, null, null, this._children.map(function (item) {
        if (typeof item.node === "function") return item.node(options);
        var sourceAndMap = item.sourceAndMap(options);

        if (sourceAndMap.map) {
          return SourceNode.fromStringWithSourceMap(sourceAndMap.source, new SourceMapConsumer(sourceAndMap.map));
        } else {
          return sourceAndMap.source;
        }
      }));
      return node;
    }
  }, {
    key: "listMap",
    value: function listMap(options) {
      if (!this._isOptimized) this._optimize();
      var map = new SourceListMap();

      var _iterator7 = _createForOfIteratorHelper(this._children),
          _step7;

      try {
        for (_iterator7.s(); !(_step7 = _iterator7.n()).done;) {
          var item = _step7.value;

          if (typeof item === "string") {
            map.add(item);
          } else if (typeof item.listMap === "function") {
            map.add(item.listMap(options));
          } else {
            var sourceAndMap = item.sourceAndMap(options);

            if (sourceAndMap.map) {
              map.add(fromStringWithSourceMap(sourceAndMap.source, sourceAndMap.map));
            } else {
              map.add(sourceAndMap.source);
            }
          }
        }
      } catch (err) {
        _iterator7.e(err);
      } finally {
        _iterator7.f();
      }

      return map;
    }
  }, {
    key: "updateHash",
    value: function updateHash(hash) {
      if (!this._isOptimized) this._optimize();
      hash.update("ConcatSource");

      var _iterator8 = _createForOfIteratorHelper(this._children),
          _step8;

      try {
        for (_iterator8.s(); !(_step8 = _iterator8.n()).done;) {
          var item = _step8.value;
          item.updateHash(hash);
        }
      } catch (err) {
        _iterator8.e(err);
      } finally {
        _iterator8.f();
      }
    }
  }, {
    key: "_optimize",
    value: function _optimize() {
      var newChildren = [];
      var currentString = undefined;
      var currentRawSources = undefined;

      var addStringToRawSources = function addStringToRawSources(string) {
        if (currentRawSources === undefined) {
          currentRawSources = string;
        } else if (Array.isArray(currentRawSources)) {
          currentRawSources.push(string);
        } else {
          currentRawSources = [typeof currentRawSources === "string" ? currentRawSources : currentRawSources.source(), string];
        }
      };

      var addSourceToRawSources = function addSourceToRawSources(source) {
        if (currentRawSources === undefined) {
          currentRawSources = source;
        } else if (Array.isArray(currentRawSources)) {
          currentRawSources.push(source.source());
        } else {
          currentRawSources = [typeof currentRawSources === "string" ? currentRawSources : currentRawSources.source(), source.source()];
        }
      };

      var mergeRawSources = function mergeRawSources() {
        if (Array.isArray(currentRawSources)) {
          var rawSource = new RawSource(currentRawSources.join(""));
          stringsAsRawSources.add(rawSource);
          newChildren.push(rawSource);
        } else if (typeof currentRawSources === "string") {
          var _rawSource = new RawSource(currentRawSources);

          stringsAsRawSources.add(_rawSource);
          newChildren.push(_rawSource);
        } else {
          newChildren.push(currentRawSources);
        }
      };

      var _iterator9 = _createForOfIteratorHelper(this._children),
          _step9;

      try {
        for (_iterator9.s(); !(_step9 = _iterator9.n()).done;) {
          var child = _step9.value;

          if (typeof child === "string") {
            if (currentString === undefined) {
              currentString = child;
            } else {
              currentString += child;
            }
          } else {
            if (currentString !== undefined) {
              addStringToRawSources(currentString);
              currentString = undefined;
            }

            if (stringsAsRawSources.has(child)) {
              addSourceToRawSources(child);
            } else {
              if (currentRawSources !== undefined) {
                mergeRawSources();
                currentRawSources = undefined;
              }

              newChildren.push(child);
            }
          }
        }
      } catch (err) {
        _iterator9.e(err);
      } finally {
        _iterator9.f();
      }

      if (currentString !== undefined) {
        addStringToRawSources(currentString);
      }

      if (currentRawSources !== undefined) {
        mergeRawSources();
      }

      this._children = newChildren;
      this._isOptimized = true;
    }
  }]);

  return ConcatSource;
}(Source);

module.exports = ConcatSource;

/***/ }),

/***/ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/OriginalSource.js":
/*!***********************************************************************************!*\
  !*** ./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/OriginalSource.js ***!
  \***********************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/


function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var Source = __webpack_require__(/*! ./Source */ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/Source.js");

var _require = __webpack_require__(/*! source-map */ "./node_modules/_source-map@0.6.1@source-map/source-map.js"),
    SourceNode = _require.SourceNode;

var _require2 = __webpack_require__(/*! source-list-map */ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/index.js"),
    SourceListMap = _require2.SourceListMap;

var _require3 = __webpack_require__(/*! ./helpers */ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/helpers.js"),
    getSourceAndMap = _require3.getSourceAndMap,
    getMap = _require3.getMap;

var SPLIT_REGEX = /(?!$)[^\n\r;{}]*[\n\r;{}]*/g;

function _splitCode(code) {
  return code.match(SPLIT_REGEX) || [];
}

var OriginalSource = /*#__PURE__*/function (_Source) {
  _inherits(OriginalSource, _Source);

  var _super = _createSuper(OriginalSource);

  function OriginalSource(value, name) {
    var _this;

    _classCallCheck(this, OriginalSource);

    _this = _super.call(this);
    var isBuffer = Buffer.isBuffer(value);
    _this._value = isBuffer ? undefined : value;
    _this._valueAsBuffer = isBuffer ? value : undefined;
    _this._name = name;
    return _this;
  }

  _createClass(OriginalSource, [{
    key: "getName",
    value: function getName() {
      return this._name;
    }
  }, {
    key: "source",
    value: function source() {
      if (this._value === undefined) {
        this._value = this._valueAsBuffer.toString("utf-8");
      }

      return this._value;
    }
  }, {
    key: "buffer",
    value: function buffer() {
      if (this._valueAsBuffer === undefined) {
        this._valueAsBuffer = Buffer.from(this._value, "utf-8");
      }

      return this._valueAsBuffer;
    }
  }, {
    key: "map",
    value: function map(options) {
      return getMap(this, options);
    }
  }, {
    key: "sourceAndMap",
    value: function sourceAndMap(options) {
      return getSourceAndMap(this, options);
    }
  }, {
    key: "node",
    value: function node(options) {
      if (this._value === undefined) {
        this._value = this._valueAsBuffer.toString("utf-8");
      }

      var value = this._value;
      var name = this._name;
      var lines = value.split("\n");
      var node = new SourceNode(null, null, null, lines.map(function (line, idx) {
        var pos = 0;

        if (options && options.columns === false) {
          var content = line + (idx !== lines.length - 1 ? "\n" : "");
          return new SourceNode(idx + 1, 0, name, content);
        }

        return new SourceNode(null, null, null, _splitCode(line + (idx !== lines.length - 1 ? "\n" : "")).map(function (item) {
          if (/^\s*$/.test(item)) {
            pos += item.length;
            return item;
          }

          var res = new SourceNode(idx + 1, pos, name, item);
          pos += item.length;
          return res;
        }));
      }));
      node.setSourceContent(name, value);
      return node;
    }
  }, {
    key: "listMap",
    value: function listMap(options) {
      if (this._value === undefined) {
        this._value = this._valueAsBuffer.toString("utf-8");
      }

      return new SourceListMap(this._value, this._name, this._value);
    }
  }, {
    key: "updateHash",
    value: function updateHash(hash) {
      if (this._valueAsBuffer === undefined) {
        this._valueAsBuffer = Buffer.from(this._value, "utf-8");
      }

      hash.update("OriginalSource");
      hash.update(this._valueAsBuffer);
      hash.update(this._name || "");
    }
  }]);

  return OriginalSource;
}(Source);

module.exports = OriginalSource;

/***/ }),

/***/ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/PrefixSource.js":
/*!*********************************************************************************!*\
  !*** ./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/PrefixSource.js ***!
  \*********************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/


function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var Source = __webpack_require__(/*! ./Source */ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/Source.js");

var RawSource = __webpack_require__(/*! ./RawSource */ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/RawSource.js");

var _require = __webpack_require__(/*! source-map */ "./node_modules/_source-map@0.6.1@source-map/source-map.js"),
    SourceNode = _require.SourceNode;

var _require2 = __webpack_require__(/*! ./helpers */ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/helpers.js"),
    getSourceAndMap = _require2.getSourceAndMap,
    getMap = _require2.getMap;

var REPLACE_REGEX = /\n(?=.|\s)/g;

var PrefixSource = /*#__PURE__*/function (_Source) {
  _inherits(PrefixSource, _Source);

  var _super = _createSuper(PrefixSource);

  function PrefixSource(prefix, source) {
    var _this;

    _classCallCheck(this, PrefixSource);

    _this = _super.call(this);
    _this._source = typeof source === "string" || Buffer.isBuffer(source) ? new RawSource(source, true) : source;
    _this._prefix = prefix;
    return _this;
  }

  _createClass(PrefixSource, [{
    key: "getPrefix",
    value: function getPrefix() {
      return this._prefix;
    }
  }, {
    key: "original",
    value: function original() {
      return this._source;
    }
  }, {
    key: "source",
    value: function source() {
      var node = this._source.source();

      var prefix = this._prefix;
      return prefix + node.replace(REPLACE_REGEX, "\n" + prefix);
    } // TODO efficient buffer() implementation

  }, {
    key: "map",
    value: function map(options) {
      return getMap(this, options);
    }
  }, {
    key: "sourceAndMap",
    value: function sourceAndMap(options) {
      return getSourceAndMap(this, options);
    }
  }, {
    key: "node",
    value: function node(options) {
      var node = this._source.node(options);

      var prefix = this._prefix;
      var output = [];
      var result = new SourceNode();
      node.walkSourceContents(function (source, content) {
        result.setSourceContent(source, content);
      });
      var needPrefix = true;
      node.walk(function (chunk, mapping) {
        var parts = chunk.split(/(\n)/);

        for (var i = 0; i < parts.length; i += 2) {
          var nl = i + 1 < parts.length;
          var part = parts[i] + (nl ? "\n" : "");

          if (part) {
            if (needPrefix) {
              output.push(prefix);
            }

            output.push(new SourceNode(mapping.line, mapping.column, mapping.source, part, mapping.name));
            needPrefix = nl;
          }
        }
      });
      result.add(output);
      return result;
    }
  }, {
    key: "listMap",
    value: function listMap(options) {
      var prefix = this._prefix;

      var map = this._source.listMap(options);

      var prefixNextLine = true;
      return map.mapGeneratedCode(function (code) {
        var updatedCode = code.replace(REPLACE_REGEX, "\n" + prefix);
        if (prefixNextLine) updatedCode = prefix + updatedCode;
        prefixNextLine = code.charCodeAt(code.length - 1) === 10; // === /\n$/.test(code)

        return updatedCode;
      });
    }
  }, {
    key: "updateHash",
    value: function updateHash(hash) {
      hash.update("PrefixSource");

      this._source.updateHash(hash);

      hash.update(this._prefix);
    }
  }]);

  return PrefixSource;
}(Source);

module.exports = PrefixSource;

/***/ }),

/***/ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/RawSource.js":
/*!******************************************************************************!*\
  !*** ./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/RawSource.js ***!
  \******************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/


function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var Source = __webpack_require__(/*! ./Source */ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/Source.js");

var _require = __webpack_require__(/*! source-map */ "./node_modules/_source-map@0.6.1@source-map/source-map.js"),
    SourceNode = _require.SourceNode;

var _require2 = __webpack_require__(/*! source-list-map */ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/index.js"),
    SourceListMap = _require2.SourceListMap;

var RawSource = /*#__PURE__*/function (_Source) {
  _inherits(RawSource, _Source);

  var _super = _createSuper(RawSource);

  function RawSource(value) {
    var _this;

    var convertToString = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

    _classCallCheck(this, RawSource);

    _this = _super.call(this);
    var isBuffer = Buffer.isBuffer(value);

    if (!isBuffer && typeof value !== "string") {
      throw new TypeError("argument 'value' must be either string of Buffer");
    }

    _this._valueIsBuffer = !convertToString && isBuffer;
    _this._value = convertToString && isBuffer ? undefined : value;
    _this._valueAsBuffer = isBuffer ? value : undefined;
    return _this;
  }

  _createClass(RawSource, [{
    key: "isBuffer",
    value: function isBuffer() {
      return this._valueIsBuffer;
    }
  }, {
    key: "source",
    value: function source() {
      if (this._value === undefined) {
        this._value = this._valueAsBuffer.toString("utf-8");
      }

      return this._value;
    }
  }, {
    key: "buffer",
    value: function buffer() {
      if (this._valueAsBuffer === undefined) {
        this._valueAsBuffer = Buffer.from(this._value, "utf-8");
      }

      return this._valueAsBuffer;
    }
  }, {
    key: "map",
    value: function map(options) {
      return null;
    }
  }, {
    key: "node",
    value: function node(options) {
      if (this._value === undefined) {
        this._value = this._valueAsBuffer.toString("utf-8");
      }

      return new SourceNode(null, null, null, this._value);
    }
  }, {
    key: "listMap",
    value: function listMap(options) {
      if (this._value === undefined) {
        this._value = this._valueAsBuffer.toString("utf-8");
      }

      return new SourceListMap(this._value);
    }
  }, {
    key: "updateHash",
    value: function updateHash(hash) {
      if (this._valueAsBuffer === undefined) {
        this._valueAsBuffer = Buffer.from(this._value, "utf-8");
      }

      hash.update("RawSource");
      hash.update(this._valueAsBuffer);
    }
  }]);

  return RawSource;
}(Source);

module.exports = RawSource;

/***/ }),

/***/ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/ReplaceSource.js":
/*!**********************************************************************************!*\
  !*** ./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/ReplaceSource.js ***!
  \**********************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/


function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it.return != null) it.return(); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Source = __webpack_require__(/*! ./Source */ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/Source.js");

var _require = __webpack_require__(/*! source-map */ "./node_modules/_source-map@0.6.1@source-map/source-map.js"),
    SourceNode = _require.SourceNode;

var _require2 = __webpack_require__(/*! ./helpers */ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/helpers.js"),
    getSourceAndMap = _require2.getSourceAndMap,
    getMap = _require2.getMap,
    getNode = _require2.getNode,
    getListMap = _require2.getListMap;

var Replacement = function Replacement(start, end, content, insertIndex, name) {
  _classCallCheck(this, Replacement);

  this.start = start;
  this.end = end;
  this.content = content;
  this.insertIndex = insertIndex;
  this.name = name;
};

var ReplaceSource = /*#__PURE__*/function (_Source) {
  _inherits(ReplaceSource, _Source);

  var _super = _createSuper(ReplaceSource);

  function ReplaceSource(source, name) {
    var _this;

    _classCallCheck(this, ReplaceSource);

    _this = _super.call(this);
    _this._source = source;
    _this._name = name;
    /** @type {Replacement[]} */

    _this._replacements = [];
    _this._isSorted = true;
    return _this;
  }

  _createClass(ReplaceSource, [{
    key: "getName",
    value: function getName() {
      return this._name;
    }
  }, {
    key: "getReplacements",
    value: function getReplacements() {
      var replacements = Array.from(this._replacements);
      replacements.sort(function (a, b) {
        return a.insertIndex - b.insertIndex;
      });
      return replacements;
    }
  }, {
    key: "replace",
    value: function replace(start, end, newValue, name) {
      if (typeof newValue !== "string") throw new Error("insertion must be a string, but is a " + _typeof(newValue));

      this._replacements.push(new Replacement(start, end, newValue, this._replacements.length, name));

      this._isSorted = false;
    }
  }, {
    key: "insert",
    value: function insert(pos, newValue, name) {
      if (typeof newValue !== "string") throw new Error("insertion must be a string, but is a " + _typeof(newValue) + ": " + newValue);

      this._replacements.push(new Replacement(pos, pos - 1, newValue, this._replacements.length, name));

      this._isSorted = false;
    }
  }, {
    key: "source",
    value: function source() {
      return this._replaceString(this._source.source());
    }
  }, {
    key: "map",
    value: function map(options) {
      if (this._replacements.length === 0) {
        return this._source.map(options);
      }

      return getMap(this, options);
    }
  }, {
    key: "sourceAndMap",
    value: function sourceAndMap(options) {
      if (this._replacements.length === 0) {
        return this._source.sourceAndMap(options);
      }

      return getSourceAndMap(this, options);
    }
  }, {
    key: "original",
    value: function original() {
      return this._source;
    }
  }, {
    key: "_sortReplacements",
    value: function _sortReplacements() {
      if (this._isSorted) return;

      this._replacements.sort(function (a, b) {
        var diff1 = b.end - a.end;
        if (diff1 !== 0) return diff1;
        var diff2 = b.start - a.start;
        if (diff2 !== 0) return diff2;
        return b.insertIndex - a.insertIndex;
      });

      this._isSorted = true;
    }
  }, {
    key: "_replaceString",
    value: function _replaceString(str) {
      if (typeof str !== "string") throw new Error("str must be a string, but is a " + _typeof(str) + ": " + str);

      this._sortReplacements();

      var result = [str];

      this._replacements.forEach(function (repl) {
        var remSource = result.pop();

        var splitted1 = this._splitString(remSource, Math.floor(repl.end + 1));

        var splitted2 = this._splitString(splitted1[0], Math.floor(repl.start));

        result.push(splitted1[1], repl.content, splitted2[0]);
      }, this); // write out result array in reverse order


      var resultStr = "";

      for (var i = result.length - 1; i >= 0; --i) {
        resultStr += result[i];
      }

      return resultStr;
    }
  }, {
    key: "node",
    value: function node(options) {
      var node = getNode(this._source, options);

      if (this._replacements.length === 0) {
        return node;
      }

      this._sortReplacements();

      var replace = new ReplacementEnumerator(this._replacements);
      var output = [];
      var position = 0;
      var sources = Object.create(null);
      var sourcesInLines = Object.create(null); // We build a new list of SourceNodes in "output"
      // from the original mapping data

      var result = new SourceNode(); // We need to add source contents manually
      // because "walk" will not handle it

      node.walkSourceContents(function (sourceFile, sourceContent) {
        result.setSourceContent(sourceFile, sourceContent);
        sources["$" + sourceFile] = sourceContent;
      });

      var replaceInStringNode = this._replaceInStringNode.bind(this, output, replace, function getOriginalSource(mapping) {
        var key = "$" + mapping.source;
        var lines = sourcesInLines[key];

        if (!lines) {
          var source = sources[key];
          if (!source) return null;
          lines = source.split("\n").map(function (line) {
            return line + "\n";
          });
          sourcesInLines[key] = lines;
        } // line is 1-based


        if (mapping.line > lines.length) return null;
        var line = lines[mapping.line - 1];
        return line.substr(mapping.column);
      });

      node.walk(function (chunk, mapping) {
        position = replaceInStringNode(chunk, position, mapping);
      }); // If any replacements occur after the end of the original file, then we append them
      // directly to the end of the output

      var remaining = replace.footer();

      if (remaining) {
        output.push(remaining);
      }

      result.add(output);
      return result;
    }
  }, {
    key: "listMap",
    value: function listMap(options) {
      var map = getListMap(this._source, options);

      this._sortReplacements();

      var currentIndex = 0;
      var replacements = this._replacements;
      var idxReplacement = replacements.length - 1;
      var removeChars = 0;
      map = map.mapGeneratedCode(function (str) {
        var newCurrentIndex = currentIndex + str.length;

        if (removeChars > str.length) {
          removeChars -= str.length;
          str = "";
        } else {
          if (removeChars > 0) {
            str = str.substr(removeChars);
            currentIndex += removeChars;
            removeChars = 0;
          }

          var finalStr = "";

          while (idxReplacement >= 0 && replacements[idxReplacement].start < newCurrentIndex) {
            var repl = replacements[idxReplacement];
            var start = Math.floor(repl.start);
            var end = Math.floor(repl.end + 1);
            var before = str.substr(0, Math.max(0, start - currentIndex));

            if (end <= newCurrentIndex) {
              var after = str.substr(Math.max(0, end - currentIndex));
              finalStr += before + repl.content;
              str = after;
              currentIndex = Math.max(currentIndex, end);
            } else {
              finalStr += before + repl.content;
              str = "";
              removeChars = end - newCurrentIndex;
            }

            idxReplacement--;
          }

          str = finalStr + str;
        }

        currentIndex = newCurrentIndex;
        return str;
      });
      var extraCode = "";

      while (idxReplacement >= 0) {
        extraCode += replacements[idxReplacement].content;
        idxReplacement--;
      }

      if (extraCode) {
        map.add(extraCode);
      }

      return map;
    }
  }, {
    key: "_splitString",
    value: function _splitString(str, position) {
      return position <= 0 ? ["", str] : [str.substr(0, position), str.substr(position)];
    }
  }, {
    key: "_replaceInStringNode",
    value: function _replaceInStringNode(output, replace, getOriginalSource, node, position, mapping) {
      var original = undefined;

      do {
        var splitPosition = replace.position - position; // If multiple replaces occur in the same location then the splitPosition may be
        // before the current position for the subsequent splits. Ensure it is >= 0

        if (splitPosition < 0) {
          splitPosition = 0;
        }

        if (splitPosition >= node.length || replace.done) {
          if (replace.emit) {
            var nodeEnd = new SourceNode(mapping.line, mapping.column, mapping.source, node, mapping.name);
            output.push(nodeEnd);
          }

          return position + node.length;
        }

        var originalColumn = mapping.column; // Try to figure out if generated code matches original code of this segement
        // If this is the case we assume that it's allowed to move mapping.column
        // Because getOriginalSource can be expensive we only do it when neccessary

        var nodePart = void 0;

        if (splitPosition > 0) {
          nodePart = node.slice(0, splitPosition);

          if (original === undefined) {
            original = getOriginalSource(mapping);
          }

          if (original && original.length >= splitPosition && original.startsWith(nodePart)) {
            mapping.column += splitPosition;
            original = original.substr(splitPosition);
          }
        }

        var emit = replace.next();

        if (!emit) {
          // Stop emitting when we have found the beginning of the string to replace.
          // Emit the part of the string before splitPosition
          if (splitPosition > 0) {
            var nodeStart = new SourceNode(mapping.line, originalColumn, mapping.source, nodePart, mapping.name);
            output.push(nodeStart);
          } // Emit the replacement value


          if (replace.value) {
            output.push(new SourceNode(mapping.line, mapping.column, mapping.source, replace.value, mapping.name || replace.name));
          }
        } // Recurse with remainder of the string as there may be multiple replaces within a single node


        node = node.substr(splitPosition);
        position += splitPosition; // eslint-disable-next-line no-constant-condition
      } while (true);
    }
  }, {
    key: "updateHash",
    value: function updateHash(hash) {
      this._sortReplacements();

      hash.update("ReplaceSource");

      this._source.updateHash(hash);

      hash.update(this._name || "");

      var _iterator = _createForOfIteratorHelper(this._replacements),
          _step;

      try {
        for (_iterator.s(); !(_step = _iterator.n()).done;) {
          var repl = _step.value;
          hash.update("".concat(repl.start));
          hash.update("".concat(repl.end));
          hash.update("".concat(repl.content));
          hash.update("".concat(repl.insertIndex));
          hash.update("".concat(repl.name));
        }
      } catch (err) {
        _iterator.e(err);
      } finally {
        _iterator.f();
      }
    }
  }]);

  return ReplaceSource;
}(Source);

var ReplacementEnumerator = /*#__PURE__*/function () {
  /**
   * @param {Replacement[]} replacements list of replacements
   */
  function ReplacementEnumerator(replacements) {
    _classCallCheck(this, ReplacementEnumerator);

    this.replacements = replacements || [];
    this.index = this.replacements.length;
    this.done = false;
    this.emit = false; // Set initial start position

    this.next();
  }

  _createClass(ReplacementEnumerator, [{
    key: "next",
    value: function next() {
      if (this.done) return true;

      if (this.emit) {
        // Start point found. stop emitting. set position to find end
        var repl = this.replacements[this.index];
        var end = Math.floor(repl.end + 1);
        this.position = end;
        this.value = repl.content;
        this.name = repl.name;
      } else {
        // End point found. start emitting. set position to find next start
        this.index--;

        if (this.index < 0) {
          this.done = true;
        } else {
          var nextRepl = this.replacements[this.index];
          var start = Math.floor(nextRepl.start);
          this.position = start;
        }
      }

      if (this.position < 0) this.position = 0;
      this.emit = !this.emit;
      return this.emit;
    }
  }, {
    key: "footer",
    value: function footer() {
      if (!this.done && !this.emit) this.next(); // If we finished _replaceInNode mid emit we advance to next entry

      if (this.done) {
        return [];
      } else {
        var resultStr = "";

        for (var i = this.index; i >= 0; i--) {
          var repl = this.replacements[i]; // this doesn't need to handle repl.name, because in SourceMaps generated code
          // without pointer to original source can't have a name

          resultStr += repl.content;
        }

        return resultStr;
      }
    }
  }]);

  return ReplacementEnumerator;
}();

module.exports = ReplaceSource;

/***/ }),

/***/ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/SizeOnlySource.js":
/*!***********************************************************************************!*\
  !*** ./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/SizeOnlySource.js ***!
  \***********************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/


function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var Source = __webpack_require__(/*! ./Source */ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/Source.js");

var SizeOnlySource = /*#__PURE__*/function (_Source) {
  _inherits(SizeOnlySource, _Source);

  var _super = _createSuper(SizeOnlySource);

  function SizeOnlySource(size) {
    var _this;

    _classCallCheck(this, SizeOnlySource);

    _this = _super.call(this);
    _this._size = size;
    return _this;
  }

  _createClass(SizeOnlySource, [{
    key: "_error",
    value: function _error() {
      return new Error("Content and Map of this Source is not available (only size() is supported)");
    }
  }, {
    key: "size",
    value: function size() {
      return this._size;
    }
  }, {
    key: "source",
    value: function source() {
      throw this._error();
    }
  }, {
    key: "buffer",
    value: function buffer() {
      throw this._error();
    }
  }, {
    key: "map",
    value: function map(options) {
      throw this._error();
    }
  }, {
    key: "updateHash",
    value: function updateHash() {
      throw this._error();
    }
  }]);

  return SizeOnlySource;
}(Source);

module.exports = SizeOnlySource;

/***/ }),

/***/ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/Source.js":
/*!***************************************************************************!*\
  !*** ./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/Source.js ***!
  \***************************************************************************/
/***/ ((module) => {

"use strict";
/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/


function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Source = /*#__PURE__*/function () {
  function Source() {
    _classCallCheck(this, Source);
  }

  _createClass(Source, [{
    key: "source",
    value: function source() {
      throw new Error("Abstract");
    }
  }, {
    key: "buffer",
    value: function buffer() {
      var source = this.source();
      if (Buffer.isBuffer(source)) return source;
      return Buffer.from(source, "utf-8");
    }
  }, {
    key: "size",
    value: function size() {
      return this.buffer().length;
    }
  }, {
    key: "map",
    value: function map(options) {
      return null;
    }
  }, {
    key: "sourceAndMap",
    value: function sourceAndMap(options) {
      return {
        source: this.source(),
        map: this.map(options)
      };
    }
  }, {
    key: "updateHash",
    value: function updateHash(hash) {
      throw new Error("Abstract");
    }
  }]);

  return Source;
}();

module.exports = Source;

/***/ }),

/***/ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/SourceMapSource.js":
/*!************************************************************************************!*\
  !*** ./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/SourceMapSource.js ***!
  \************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/


function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var Source = __webpack_require__(/*! ./Source */ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/Source.js");

var _require = __webpack_require__(/*! source-map */ "./node_modules/_source-map@0.6.1@source-map/source-map.js"),
    SourceNode = _require.SourceNode,
    SourceMapConsumer = _require.SourceMapConsumer;

var _require2 = __webpack_require__(/*! source-list-map */ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/index.js"),
    SourceListMap = _require2.SourceListMap,
    fromStringWithSourceMap = _require2.fromStringWithSourceMap;

var _require3 = __webpack_require__(/*! ./helpers */ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/helpers.js"),
    getSourceAndMap = _require3.getSourceAndMap,
    getMap = _require3.getMap;

var applySourceMap = __webpack_require__(/*! ./applySourceMap */ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/applySourceMap.js");

var SourceMapSource = /*#__PURE__*/function (_Source) {
  _inherits(SourceMapSource, _Source);

  var _super = _createSuper(SourceMapSource);

  function SourceMapSource(value, name, sourceMap, originalSource, innerSourceMap, removeOriginalSource) {
    var _this;

    _classCallCheck(this, SourceMapSource);

    _this = _super.call(this);
    var valueIsBuffer = Buffer.isBuffer(value);
    _this._valueAsString = valueIsBuffer ? undefined : value;
    _this._valueAsBuffer = valueIsBuffer ? value : undefined;
    _this._name = name;
    _this._hasSourceMap = !!sourceMap;
    var sourceMapIsBuffer = Buffer.isBuffer(sourceMap);
    var sourceMapIsString = typeof sourceMap === "string";
    _this._sourceMapAsObject = sourceMapIsBuffer || sourceMapIsString ? undefined : sourceMap;
    _this._sourceMapAsString = sourceMapIsString ? sourceMap : undefined;
    _this._sourceMapAsBuffer = sourceMapIsBuffer ? sourceMap : undefined;
    _this._hasOriginalSource = !!originalSource;
    var originalSourceIsBuffer = Buffer.isBuffer(originalSource);
    _this._originalSourceAsString = originalSourceIsBuffer ? undefined : originalSource;
    _this._originalSourceAsBuffer = originalSourceIsBuffer ? originalSource : undefined;
    _this._hasInnerSourceMap = !!innerSourceMap;
    var innerSourceMapIsBuffer = Buffer.isBuffer(innerSourceMap);
    var innerSourceMapIsString = typeof innerSourceMap === "string";
    _this._innerSourceMapAsObject = innerSourceMapIsBuffer || innerSourceMapIsString ? undefined : innerSourceMap;
    _this._innerSourceMapAsString = innerSourceMapIsString ? innerSourceMap : undefined;
    _this._innerSourceMapAsBuffer = innerSourceMapIsBuffer ? innerSourceMap : undefined;
    _this._removeOriginalSource = removeOriginalSource;
    return _this;
  }

  _createClass(SourceMapSource, [{
    key: "_ensureValueBuffer",
    value: function _ensureValueBuffer() {
      if (this._valueAsBuffer === undefined) {
        this._valueAsBuffer = Buffer.from(this._valueAsString, "utf-8");
      }
    }
  }, {
    key: "_ensureValueString",
    value: function _ensureValueString() {
      if (this._valueAsString === undefined) {
        this._valueAsString = this._valueAsBuffer.toString("utf-8");
      }
    }
  }, {
    key: "_ensureOriginalSourceBuffer",
    value: function _ensureOriginalSourceBuffer() {
      if (this._originalSourceAsBuffer === undefined && this._hasOriginalSource) {
        this._originalSourceAsBuffer = Buffer.from(this._originalSourceAsString, "utf-8");
      }
    }
  }, {
    key: "_ensureOriginalSourceString",
    value: function _ensureOriginalSourceString() {
      if (this._originalSourceAsString === undefined && this._hasOriginalSource) {
        this._originalSourceAsString = this._originalSourceAsBuffer.toString("utf-8");
      }
    }
  }, {
    key: "_ensureInnerSourceMapObject",
    value: function _ensureInnerSourceMapObject() {
      if (this._innerSourceMapAsObject === undefined && this._hasInnerSourceMap) {
        this._ensureInnerSourceMapString();

        this._innerSourceMapAsObject = JSON.parse(this._innerSourceMapAsString);
      }
    }
  }, {
    key: "_ensureInnerSourceMapBuffer",
    value: function _ensureInnerSourceMapBuffer() {
      if (this._innerSourceMapAsBuffer === undefined && this._hasInnerSourceMap) {
        this._ensureInnerSourceMapString();

        this._innerSourceMapAsBuffer = Buffer.from(this._innerSourceMapAsString, "utf-8");
      }
    }
  }, {
    key: "_ensureInnerSourceMapString",
    value: function _ensureInnerSourceMapString() {
      if (this._innerSourceMapAsString === undefined && this._hasInnerSourceMap) {
        if (this._innerSourceMapAsBuffer !== undefined) {
          this._innerSourceMapAsString = this._innerSourceMapAsBuffer.toString("utf-8");
        } else {
          this._innerSourceMapAsString = JSON.stringify(this._innerSourceMapAsObject);
        }
      }
    }
  }, {
    key: "_ensureSourceMapObject",
    value: function _ensureSourceMapObject() {
      if (this._sourceMapAsObject === undefined) {
        this._ensureSourceMapString();

        this._sourceMapAsObject = JSON.parse(this._sourceMapAsString);
      }
    }
  }, {
    key: "_ensureSourceMapBuffer",
    value: function _ensureSourceMapBuffer() {
      if (this._sourceMapAsBuffer === undefined) {
        this._ensureSourceMapString();

        this._sourceMapAsBuffer = Buffer.from(this._sourceMapAsString, "utf-8");
      }
    }
  }, {
    key: "_ensureSourceMapString",
    value: function _ensureSourceMapString() {
      if (this._sourceMapAsString === undefined) {
        if (this._sourceMapAsBuffer !== undefined) {
          this._sourceMapAsString = this._sourceMapAsBuffer.toString("utf-8");
        } else {
          this._sourceMapAsString = JSON.stringify(this._sourceMapAsObject);
        }
      }
    }
  }, {
    key: "getArgsAsBuffers",
    value: function getArgsAsBuffers() {
      this._ensureValueBuffer();

      this._ensureSourceMapBuffer();

      this._ensureOriginalSourceBuffer();

      this._ensureInnerSourceMapBuffer();

      return [this._valueAsBuffer, this._name, this._sourceMapAsBuffer, this._originalSourceAsBuffer, this._innerSourceMapAsBuffer, this._removeOriginalSource];
    }
  }, {
    key: "source",
    value: function source() {
      this._ensureValueString();

      return this._valueAsString;
    }
  }, {
    key: "map",
    value: function map(options) {
      if (!this._hasInnerSourceMap) {
        this._ensureSourceMapObject();

        return this._sourceMapAsObject;
      }

      return getMap(this, options);
    }
  }, {
    key: "sourceAndMap",
    value: function sourceAndMap(options) {
      if (!this._hasInnerSourceMap) {
        this._ensureValueString();

        this._ensureSourceMapObject();

        return {
          source: this._valueAsString,
          map: this._sourceMapAsObject
        };
      }

      return getSourceAndMap(this, options);
    }
  }, {
    key: "node",
    value: function node(options) {
      this._ensureValueString();

      this._ensureSourceMapObject();

      this._ensureOriginalSourceString();

      var node = SourceNode.fromStringWithSourceMap(this._valueAsString, new SourceMapConsumer(this._sourceMapAsObject));
      node.setSourceContent(this._name, this._originalSourceAsString);

      if (this._hasInnerSourceMap) {
        this._ensureInnerSourceMapObject();

        node = applySourceMap(node, new SourceMapConsumer(this._innerSourceMapAsObject), this._name, this._removeOriginalSource);
      }

      return node;
    }
  }, {
    key: "listMap",
    value: function listMap(options) {
      this._ensureValueString();

      this._ensureSourceMapObject();

      options = options || {};
      if (options.module === false) return new SourceListMap(this._valueAsString, this._name, this._valueAsString);
      return fromStringWithSourceMap(this._valueAsString, this._sourceMapAsObject);
    }
  }, {
    key: "updateHash",
    value: function updateHash(hash) {
      this._ensureValueBuffer();

      this._ensureSourceMapBuffer();

      this._ensureOriginalSourceBuffer();

      this._ensureInnerSourceMapBuffer();

      hash.update("SourceMapSource");
      hash.update(this._valueAsBuffer);
      hash.update(this._sourceMapAsBuffer);

      if (this._hasOriginalSource) {
        hash.update(this._originalSourceAsBuffer);
      }

      if (this._hasInnerSourceMap) {
        hash.update(this._innerSourceMapAsBuffer);
      }

      hash.update(this._removeOriginalSource ? "true" : "false");
    }
  }]);

  return SourceMapSource;
}(Source);

module.exports = SourceMapSource;

/***/ }),

/***/ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/applySourceMap.js":
/*!***********************************************************************************!*\
  !*** ./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/applySourceMap.js ***!
  \***********************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/


var SourceNode = __webpack_require__(/*! source-map */ "./node_modules/_source-map@0.6.1@source-map/source-map.js").SourceNode;

var SourceMapConsumer = __webpack_require__(/*! source-map */ "./node_modules/_source-map@0.6.1@source-map/source-map.js").SourceMapConsumer;

var applySourceMap = function applySourceMap(sourceNode, sourceMapConsumer, sourceFile, removeGeneratedCodeForSourceFile) {
  // The following notations are used to name stuff:
  // Left <------------> Middle <-------------------> Right
  // Input arguments:
  //        sourceNode                                       - Code mapping from Left to Middle
  //                   sourceFile                            - Name of a Middle file
  //                              sourceMapConsumer          - Code mapping from Middle to Right
  // Variables:
  //           l2m                      m2r
  // Left <-----------------------------------------> Right
  // Variables:
  //                       l2r
  var l2rResult = new SourceNode();
  var l2rOutput = [];
  var middleSourceContents = {};
  var m2rMappingsByLine = {};
  var rightSourceContentsSet = {};
  var rightSourceContentsLines = {}; // Store all mappings by generated line

  sourceMapConsumer.eachMapping(function (mapping) {
    (m2rMappingsByLine[mapping.generatedLine] = m2rMappingsByLine[mapping.generatedLine] || []).push(mapping);
  }, null, SourceMapConsumer.GENERATED_ORDER);

  var findM2rMapping = function findM2rMapping(line, column) {
    var m2rMappings = m2rMappingsByLine[line];
    var l = 0;
    var r = m2rMappings.length;

    while (l < r) {
      var m = l + r >> 1;

      if (m2rMappings[m].generatedColumn <= column) {
        l = m + 1;
      } else {
        r = m;
      }
    }

    if (l === 0) return undefined;
    return m2rMappings[l - 1];
  }; // Store all source contents


  sourceNode.walkSourceContents(function (source, content) {
    middleSourceContents["$" + source] = content;
  });
  var middleSource = middleSourceContents["$" + sourceFile];
  var middleSourceLines = middleSource ? middleSource.split("\n") : undefined; // Walk all left to middle mappings

  sourceNode.walk(function (chunk, middleMapping) {
    // Find a mapping from middle to right
    if (middleMapping.source === sourceFile && middleMapping.line && m2rMappingsByLine[middleMapping.line]) {
      // For minimized sources this is performance-relevant,
      // since all mappings are in a single line, use a binary search
      var m2rBestFit = findM2rMapping(middleMapping.line, middleMapping.column);

      if (m2rBestFit) {
        var allowMiddleName = false;
        var middleLine;
        var rightSourceContent;
        var rightSourceContentLines;
        var rightSource = m2rBestFit.source; // Check if we have middle and right source for this mapping
        // Then we could have an "identify" mapping

        if (middleSourceLines && rightSource && (middleLine = middleSourceLines[m2rBestFit.generatedLine - 1]) && ((rightSourceContentLines = rightSourceContentsLines[rightSource]) || (rightSourceContent = sourceMapConsumer.sourceContentFor(rightSource, true)))) {
          if (!rightSourceContentLines) {
            rightSourceContentLines = rightSourceContentsLines[rightSource] = rightSourceContent.split("\n");
          }

          var rightLine = rightSourceContentLines[m2rBestFit.originalLine - 1];

          if (rightLine) {
            var offset = middleMapping.column - m2rBestFit.generatedColumn;

            if (offset > 0) {
              var middlePart = middleLine.slice(m2rBestFit.generatedColumn, middleMapping.column);
              var rightPart = rightLine.slice(m2rBestFit.originalColumn, m2rBestFit.originalColumn + offset);

              if (middlePart === rightPart) {
                // When original and generated code is equal we assume we have an "identity" mapping
                // In this case we can offset the original position
                m2rBestFit = Object.assign({}, m2rBestFit, {
                  originalColumn: m2rBestFit.originalColumn + offset,
                  generatedColumn: middleMapping.column,
                  name: undefined
                });
              }
            }

            if (!m2rBestFit.name && middleMapping.name) {
              allowMiddleName = rightLine.slice(m2rBestFit.originalColumn, m2rBestFit.originalColumn + middleMapping.name.length) === middleMapping.name;
            }
          }
        } // Construct a left to right node from the found middle to right mapping


        var _source = m2rBestFit.source; // Workaround for bug in source-map
        // null sources are incorrectly normalized to "."

        if (_source && _source !== ".") {
          l2rOutput.push(new SourceNode(m2rBestFit.originalLine, m2rBestFit.originalColumn, _source, chunk, allowMiddleName ? middleMapping.name : m2rBestFit.name)); // Set the source contents once

          if (!("$" + _source in rightSourceContentsSet)) {
            rightSourceContentsSet["$" + _source] = true;
            var sourceContent = sourceMapConsumer.sourceContentFor(_source, true);

            if (sourceContent) {
              l2rResult.setSourceContent(_source, sourceContent);
            }
          }

          return;
        }
      }
    }

    if (removeGeneratedCodeForSourceFile && middleMapping.source === sourceFile || !middleMapping.source) {
      // Construct a left to middle node with only generated code
      // Because user do not want mappings to middle sources
      // Or this chunk has no mapping
      l2rOutput.push(chunk);
      return;
    } // Construct a left to middle node


    var source = middleMapping.source;
    l2rOutput.push(new SourceNode(middleMapping.line, middleMapping.column, source, chunk, middleMapping.name));

    if ("$" + source in middleSourceContents) {
      if (!("$" + source in rightSourceContentsSet)) {
        l2rResult.setSourceContent(source, middleSourceContents["$" + source]);
        delete middleSourceContents["$" + source];
      }
    }
  }); // Put output into the resulting SourceNode

  l2rResult.add(l2rOutput);
  return l2rResult;
};

module.exports = applySourceMap;

/***/ }),

/***/ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/helpers.js":
/*!****************************************************************************!*\
  !*** ./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/helpers.js ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";
/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/


var _require = __webpack_require__(/*! source-map */ "./node_modules/_source-map@0.6.1@source-map/source-map.js"),
    SourceNode = _require.SourceNode,
    SourceMapConsumer = _require.SourceMapConsumer;

var _require2 = __webpack_require__(/*! source-list-map */ "./node_modules/_source-list-map@2.0.1@source-list-map/lib/index.js"),
    SourceListMap = _require2.SourceListMap,
    fromStringWithSourceMap = _require2.fromStringWithSourceMap;

exports.getSourceAndMap = function (inputSource, options) {
  var source;
  var map;

  if (options && options.columns === false) {
    var res = inputSource.listMap(options).toStringWithSourceMap({
      file: "x"
    });
    source = res.source;
    map = res.map;
  } else {
    var _res = inputSource.node(options).toStringWithSourceMap({
      file: "x"
    });

    source = _res.code;
    map = _res.map.toJSON();
  }

  if (!map || !map.sources || map.sources.length === 0) map = null;
  return {
    source: source,
    map: map
  };
};

exports.getMap = function (source, options) {
  var map;

  if (options && options.columns === false) {
    map = source.listMap(options).toStringWithSourceMap({
      file: "x"
    }).map;
  } else {
    map = source.node(options).toStringWithSourceMap({
      file: "x"
    }).map.toJSON();
  }

  if (!map || !map.sources || map.sources.length === 0) return null;
  return map;
};

exports.getNode = function (source, options) {
  if (typeof source.node === "function") {
    return source.node(options);
  } else {
    var sourceAndMap = source.sourceAndMap(options);

    if (sourceAndMap.map) {
      return SourceNode.fromStringWithSourceMap(sourceAndMap.source, new SourceMapConsumer(sourceAndMap.map));
    } else {
      return new SourceNode(null, null, null, sourceAndMap.source);
    }
  }
};

exports.getListMap = function (source, options) {
  if (typeof source.listMap === "function") {
    return source.listMap(options);
  } else {
    var sourceAndMap = source.sourceAndMap(options);

    if (sourceAndMap.map) {
      return fromStringWithSourceMap(sourceAndMap.source, sourceAndMap.map);
    } else {
      return new SourceListMap(sourceAndMap.source);
    }
  }
};

/***/ }),

/***/ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/index.js":
/*!**************************************************************************!*\
  !*** ./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/index.js ***!
  \**************************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
var defineExport = function defineExport(name, fn) {
  var value;
  Object.defineProperty(exports, name, {
    get: function get() {
      if (fn !== undefined) {
        value = fn();
        fn = undefined;
      }

      return value;
    },
    configurable: true
  });
};

defineExport("Source", function () {
  return __webpack_require__(/*! ./Source */ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/Source.js");
});
defineExport("RawSource", function () {
  return __webpack_require__(/*! ./RawSource */ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/RawSource.js");
});
defineExport("OriginalSource", function () {
  return __webpack_require__(/*! ./OriginalSource */ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/OriginalSource.js");
});
defineExport("SourceMapSource", function () {
  return __webpack_require__(/*! ./SourceMapSource */ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/SourceMapSource.js");
});
defineExport("CachedSource", function () {
  return __webpack_require__(/*! ./CachedSource */ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/CachedSource.js");
});
defineExport("ConcatSource", function () {
  return __webpack_require__(/*! ./ConcatSource */ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/ConcatSource.js");
});
defineExport("ReplaceSource", function () {
  return __webpack_require__(/*! ./ReplaceSource */ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/ReplaceSource.js");
});
defineExport("PrefixSource", function () {
  return __webpack_require__(/*! ./PrefixSource */ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/PrefixSource.js");
});
defineExport("SizeOnlySource", function () {
  return __webpack_require__(/*! ./SizeOnlySource */ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/SizeOnlySource.js");
});
defineExport("CompatSource", function () {
  return __webpack_require__(/*! ./CompatSource */ "./node_modules/_webpack-sources@2.3.0@webpack-sources/lib/CompatSource.js");
});

/***/ }),

/***/ "?e413":
/*!********************!*\
  !*** fs (ignored) ***!
  \********************/
/***/ (() => {

/* (ignored) */

/***/ }),

/***/ "?5bde":
/*!**********************!*\
  !*** path (ignored) ***!
  \**********************/
/***/ (() => {

/* (ignored) */

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module is referenced by other modules so it can't be inlined
/******/ 	var __webpack_exports__ = __webpack_require__("./main.js");
/******/ 	
/******/ 	return __webpack_exports__;
/******/ })()
;
});
//# sourceMappingURL=webpack-sources.js.map