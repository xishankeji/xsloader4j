(function(root) {
	root.CustomerFunction = function(scriptStr) {
		transformScript("function checkVueExpressionFun(){\n" + scriptStr + "\n}");
	}

	const POLYFILL_PATH = $jsBridge$.getPolyfillPath();
	const CONTEXT_PATH = $jsBridge$.getContextPath();

	let api = {};
	root.XsloaderServer = api;

	let extend = function(target) {
		for (let i = 1; i < arguments.length; i++) {
			let obj = arguments[i];
			if (!obj) {
				continue;
			}
			for (let x in obj) {
				let value = obj[x];
				if (value === undefined) {
					continue;
				}
				target[x] = obj[x];
			}
		}
		return target;
	};

	String.prototype.replaceAll = function(str, replace) {
		if (!(typeof str == "string")) {
			return this;
		} else {
			let as = [];

			let index = 0;
			while (true) {
				let index2 = this.indexOf(str, index);
				if (index2 != -1) {
					as.push(this.substring(index, index2));
					as.push(replace);
					index = index2 + str.length;
				} else {
					as.push(this.substring(index));
					break;
				}
			}

			return as.join("");
		}
	};

	function tagExec(tag, str, isMulti, offset) {
		offset = offset || 0;

		let startReg = new RegExp(`(<\\s*${tag}\\s*)([^>]*)>`);
		let endReg = new RegExp(`</\\s*${tag}\\s*>`);


		let startStack = [];
		let currentIndex = 0;
		let firstStartItem;
		let lastEndItem;
		while (currentIndex < str.length) {
			let startResult = startReg.exec(str.substring(currentIndex));
			let endResult = endReg.exec(str.substring(currentIndex));
			if (startResult && endResult && endResult.index < startResult.index) {
				startResult = null;
			}

			if (startResult) { //下一个标签是开始标签
				let item = {
					result: startResult,
					index: currentIndex + startResult.index,
					nextIndex: currentIndex + startResult.index + startResult[0].length,
				};
				startStack.push(item);

				if (!firstStartItem) {
					firstStartItem = item;
				}

				currentIndex = currentIndex + startResult.index + startResult[0].length;
			} else if (endResult) { //下一个是结束标签
				if (startStack.length) {
					startStack.pop();
					lastEndItem = {
						result: endResult,
						index: currentIndex + endResult.index,
						nextIndex: currentIndex + endResult.index + endResult[0].length,
					}
				}

				currentIndex = currentIndex + endResult.index + endResult[0].length;

				if (firstStartItem && !startStack.length) {
					break;
				}
			} else {
				break;
			}
		}

		if (firstStartItem && lastEndItem) {
			let content = str.substring(firstStartItem.nextIndex, lastEndItem.index);
			let attrsStr = firstStartItem.result[2];
			let attrs = {};
			currentIndex = firstStartItem.index + firstStartItem.result[1].length;

			while (attrsStr) {
				let regResult2 = /([a-zA-Z0-9\|\$:@#!\*\&\^%_\.-]+)(\s*=\s*['"])([^'"]+)(['"])/.exec(attrsStr)
				if (regResult2) {
					currentIndex += regResult2.index;
					let k = regResult2[1]
					attrs[k] = {
						k: k,
						s1: regResult2[2],
						content: regResult2[3],
						s2: regResult2[4],
						fullContent: regResult2[0],
						index: currentIndex //在整个str中的偏移量
					};
					attrsStr = attrsStr.substring(regResult2.index + regResult2[0].length);
					currentIndex += regResult2[0].length;
				} else {
					break;
				}
			}

			let cindex = firstStartItem.index + firstStartItem.result[1].length + firstStartItem.result[2].length +
				1;

			let tLength = lastEndItem.nextIndex - firstStartItem.index; //整个内容的长度（包括标签）

			return {
				content, //标签里的内容
				lang: attrs.lang ? attrs.lang.content : null, //lang属性
				tag, //标签名字
				index: firstStartItem.index + offset, //标签开始位置
				end: firstStartItem.index + tLength + offset, //结束位置
				length: tLength,
				attrs, //所有标签上的属性
				cindex: cindex + offset, //标签里的内容的开始位置
				cend: cindex + content.length + offset //结束标签的开始位置
			};
		}

		// let q = isMulti ? "?" : ""; //当isMulti为true时，转换为非贪婪匹配模式
		// let reg = new RegExp(`(<\\s*${tag}\\s*)([^>]*)>([\\w\\W]*${q})</\\s*${tag}\\s*>`);
		// let regResult = reg.exec(str);
		// if (regResult) {
		// 	let content = regResult[3];
		// 	let attrsStr = regResult[2];
		// 	let attrs = {};
		// 	let currentIndex = regResult.index + regResult[1].length;
		// 	while (attrsStr) {
		// 		let regResult2 = /([a-zA-Z0-9\|\$:@#!\*\&\^%_\.-]+)(\s*=\s*['"])([^'"]+)(['"])/.exec(attrsStr)
		// 		if (regResult2) {
		// 			currentIndex += regResult2.index;
		// 			let k = regResult2[1]
		// 			attrs[k] = {
		// 				k: k,
		// 				s1: regResult2[2],
		// 				content: regResult2[3],
		// 				s2: regResult2[4],
		// 				fullContent: regResult2[0],
		// 				index: currentIndex //在整个str中的偏移量
		// 			};
		// 			attrsStr = attrsStr.substring(regResult2.index + regResult2[0].length);
		// 			currentIndex += regResult2[0].length;
		// 		} else {
		// 			break;
		// 		}
		// 	}

		// 	let cindex = regResult.index + regResult[1].length + regResult[2].length + 1;

		// 	return {
		// 		content, //标签里的内容
		// 		lang: attrs.lang ? attrs.lang.content : null, //lang属性
		// 		tag, //标签名字
		// 		index: regResult.index + offset, //标签开始位置
		// 		end: regResult.index + regResult[0].length + offset, //结束位置
		// 		length: regResult[0].length, //整个内容的长度（包括标签）
		// 		attrs, //所有标签上的属性
		// 		cindex: cindex + offset, //标签里的内容的开始位置
		// 		cend: cindex + content.length + offset //结束标签的开始位置
		// 	};
		// }
	};

	api.compileVueTemplate = function(currentUrl, filepath, template, hasSourceMap, otherOption) {
		if (!template) {
			return {};
		}

		otherOption = extend({}, otherOption, {
			strictMode: false,
			isInline: true,
			doStaticInclude: false,
			doStaticVueTemplate: false
		});

		//进行编译，转换内部es6表达式等,使得低版本浏览器支持es6的表达式
		function parseScript(scriptStr) {
			let rs = api.parseEs6(currentUrl, filepath, scriptStr, null, hasSourceMap, otherOption);
			rs = rs.code.trim();
			if (rs.startsWith('"use strict";')) {
				rs = rs.substring('"use strict";'.length);
			}
			return rs.trim();
		}

		let res = Vue.compile(template, {
			warn(str) {
				console.warn(str);
			},
			createFunction(code, errors) {
				let funName = "__" + $jsBridge$.shortId();
				let parsed = parseScript(`function ${funName}(){\n${code}\n}`);
				return `(function(){${parsed}; return ${funName};})()`;
			}
		});

		//String:res.render,res.staticRenderFns
		let result = {};
		result.render = res.render;
		if (res.staticRenderFns) {
			let arr = [];
			for (let i = 0; i < res.staticRenderFns.length; i++) {
				arr.push(res.staticRenderFns[i]);
			}
			result.staticRenderFns = "[" + res.staticRenderFns.join(",") + "]";
		}

		return result;
	}

	const KEEP_VAR_NAMES = [
		"origin__beforeCreate",
		"origin__created",
		"origin__mounted",
		"origin__destroyed",
		"exports",
		"thiz",
		"module",
		"require",
		"define",
		"xsloader",
		"vtemplate",
		"invoker",
		"__ImporT__"
	];


	function transformScript(scriptContent, otherOption, requireModules) {
		otherOption = extend({
			strictMode: true,
			isInline: false,
			hasSourceMap: false,
			currentUrl: undefined,
			inlineSourceMap: false,
			browserType: null,
			browserMajorVersion: null,
			scriptLang: undefined,
		}, otherOption);

		let option = {
			ast: false,
			minified: false,
			comments: false,
			compact: false,
			parserOpts: {
				strictMode: otherOption.strictMode,
			},
			sourceType: "module",
			sourceMaps: otherOption.inlineSourceMap ? "inline" : (!otherOption.isInline && otherOption
				.hasSourceMap ? true :
				false),
			filename: otherOption.currentUrl,
			presets: [
				"es2015",
				"es2016",
				"es2017"
			],
			plugins: [
				//["transform-modules-commonjs"], //需要转换import "...";
				function(utils) {
					const t = utils.types;
					return {
						visitor: {
							// ImportDeclaration: function(path) { //需要转换import "...";
							// 	if (requireModules) {
							// 		let id = "_import_" + $jsBridge$.shortId() + "_mod";
							// 		let varname = path.scope.generateUid("mod");
							// 		requireModules.push({
							// 			name: path.node.source.value,
							// 			id,
							// 			varname
							// 		});
							// 		path.node.source.value = id;
							// 	}
							// },
							CallExpression: function(path) {
								const callee = path.node.callee;
								const args = path.node.arguments;

								if (callee.type == "Import") {
									//将import(...)替换成__ImporT__("...")
									let newExp = t.callExpression(t.identifier("__ImporT__"), path.node
										.arguments);
									path.replaceWith(newExp);
								} else if (callee.type == "Identifier" && callee.name == "require" &&
									args.length == 1 && args[0].type == "StringLiteral"
								) { //需要转换import xxxx "...";export xxxx from "...";
									path.node.callee.name = "__getRequireDep";
									if (requireModules) {
										requireModules.push({
											name: args[0].value,
										});
										// let varname = path.scope.generateUid("mod");
										// let id = "_import_" + varname + "_" + $jsBridge$.shortId();
										// requireModules.push({
										// 	name: args[0].value,
										// 	id,
										// 	varname
										// });
										// args[0].value = id;
									}
								}
							}
						}
					}
				},
			]
		}; //!!!!!!!!!!!!!!先顺序执行插件，接着逆序执行预设

		if (otherOption.browserType && otherOption.browserType != "no" &&
			otherOption.browserMajorVersion && otherOption.browserMajorVersion != "no") {
			option.presets = [
				['env', {
					"targets": {
						"browsers": [otherOption.browserType.toLowerCase() + " " + otherOption
							.browserMajorVersion.toLowerCase()
						]
					}
				}]
			];

		}

		// if (otherOption.browserType == "ie") {
		// 	option.plugins.push(
		// 		['transform-async-to-generator'], //es2017
		// 		['proposal-object-rest-spread'], //es2018
		// 		["transform-destructuring"], //解构
		// 		['proposal-async-generator-functions'], //es2018
		// 		["transform-dotall-regex"], //es2018
		// 		["transform-named-capturing-groups-regex"], //es2018
		// 		["proposal-optional-catch-binding"], //es2018
		// 		["proposal-unicode-property-regex", {
		// 			"useUnicodeFlag": false
		// 		}] //es2018
		// 	);
		// }

		let currentPath = otherOption.currentPath;

		let isTypeScript = otherOption.scriptLang == "typescript" || otherOption.scriptLang == "ts" ||
			currentPath && (currentPath.endsWith(".ts") || currentPath.endsWith(".jtr"));
		let isTSFile = currentPath && currentPath.endsWith(".ts");
		let isReact = otherOption.htmr_jsr || currentPath && (currentPath.endsWith(".jsr") || currentPath.endsWith(
			".jtr"));

		if (isTypeScript) {
			option.plugins.push(['transform-typescript', {
				isTSX: isReact || !isTSFile,
				jsxPragma: isReact ? undefined : "__internals__.jsx(this)",
				allowNamespaces: true,
				allowDeclareFields: true,
				onlyRemoveTypeImports: true,
				allExtensions: true,
			}]);

			option.plugins.push(['transform-react-jsx', {
				pragma: isReact ? undefined : "__internals__.jsx(this)",
				throwIfNamespace: true,
				runtime: 'classic',
			}]);
		} else {
			option.plugins.push(['transform-react-jsx', {
				pragma: isReact ? undefined : "__internals__.jsx(this)",
				throwIfNamespace: true,
				runtime: 'classic',
			}]);
		}

		option.plugins.push(
			//["transform-regenerator"],
			["proposal-export-default-from"],
			["proposal-decorators", {
				"legacy": true
			}],
			["proposal-class-properties", {
				"loose": true
			}],
			["proposal-private-methods", {
				"loose": true
			}],
			["proposal-private-property-in-object", {
				"loose": true
			}],
			["proposal-nullish-coalescing-operator"],
			["proposal-optional-chaining"],
			["proposal-numeric-separator"],
			["proposal-throw-expressions"],
			["proposal-logical-assignment-operators"],
			["proposal-do-expressions"]);

		let rs = Babel.transform(scriptContent, option);
		return rs;
	}

	api.parseEs6Script = function(name, scriptContent, otherOption) {
		otherOption = extend({
			hasSourceMap: false,
			currentUrl: name
		}, otherOption)
		let rs = transformScript(scriptContent, otherOption);
		return rs.code;
	}

	/**
	 * 转换es6的代码。
	 * 特殊指令：
	 * staticInclude(relativePath)：在编译前导入静态页面内容（且被导入的页面依然支持静态导入）,relativePath相对于当前页面文件路径，如staticInclude("./include/a.js")
	 * template:staticVueTemplate(`templateContent`):在服务器端进行vue的template编译，返回：“render:function(){...},staticRenderFns:[...]”
	 * @param {Object} currentUrl
	 * @param {Object} filepath
	 * @param {Object} scriptContent
	 * @param {Object} customerScriptPart
	 * @param {Object} hasSourceMap
	 * @param {Object} otherOption
	 */
	api.parseEs6 = function(currentUrl, filepath, scriptContent, customerScriptPart, hasSourceMap,
		otherOption) {
		otherOption = extend({
			strictMode: true,
			isInline: false,
			doStaticInclude: true,
			doStaticVueTemplate: true,
			scriptLang: undefined,
			reactAutojs: true,
		}, otherOption);

		let __unstrictFunMap = {};
		if (otherOption.doStaticInclude) {
			scriptContent = $jsBridge$.staticInclude(filepath, scriptContent);
		}

		if (otherOption.doStaticVueTemplate) {
			scriptContent = $jsBridge$.staticVueTemplate(currentUrl, filepath, scriptContent, hasSourceMap,
				"__unstrictFunMap",
				__unstrictFunMap);
		}

		let currentPath;
		if (currentUrl) {
			let index = currentUrl.indexOf("://");
			if (index > 0) {
				index = currentUrl.indexOf("/", index + 3);
				if (index > 0) {
					currentPath = currentUrl.substring(index);
				}
			}
		}
		otherOption.currentPath = currentPath;

		customerScriptPart = customerScriptPart || "";
		const requireModules = [];

		let rs = transformScript(scriptContent, extend({
			hasSourceMap,
			currentUrl
		}, otherOption), requireModules);

		let parsedCode = rs.code;
		const innerDeps = [];
		const getIndexes = {};

		const isReact = otherOption.htmr_jsr || currentPath && (currentPath.endsWith(".jsr") || currentPath
			.endsWith(".jtr"));
		const hasReactDep = isReact && otherOption.reactAutojs;

		requireModules.forEach((item, index) => {
			let name = item.name;
			innerDeps.push(`"${name}"`);
			getIndexes[name] = index + (hasReactDep ? 4 : 2);
		});

		let sourceMap = rs.map ? JSON.stringify(rs.map) : null;

		if (otherOption.isInline) {
			return {
				code: parsedCode,
				sourceMap
			};
		}

		let scriptPrefix =
			`${currentPath?'xsloader.__currentPath="'+currentPath+'";':''}
			xsloader.__ignoreCurrentRequireDep=true;
			xsloader.define(/*__DEF_TAG_NAME__*/['exports','exists!xsloader4j-server-bridge or server-bridge']
				.concat(
					xsloader.hasDefine('react')?${JSON.stringify(isReact?['react','react-dom']:[])}:
					${JSON.stringify(isReact?[!otherOption.reactAutojs?'react':'name!react=>>'+CONTEXT_PATH+'/react.react-inner.js',!otherOption.reactAutojs?'react-dom':'name!react-dom=>>'+CONTEXT_PATH+'/react-dom.react-inner.js']:[])}
				)
				/*__DEF_INNER__*/.concat([${innerDeps.join(',')}])/*__DEF_INNER__*/
				.concat(window.__hasPolyfill?[]:[${POLYFILL_PATH?"'"+POLYFILL_PATH+"'":""}])
				,function(exports,__serverBridge__${hasReactDep?',React,ReactDOM':''}){
				var setReadonlyProp = function(obj, key, value) {
					if (obj && xsloader.isObject(obj)) {
						Object.defineProperty(obj, key, {
							value: value,
							enumerable: false,
							configurable: false,
						});
					}
				};
				var L=xsloader;
				var thiz=this;
				var module={};
				var __internals__={};
				setReadonlyProp(__internals__,'require',__serverBridge__.getRequire(thiz));
				setReadonlyProp(__internals__,'styleBuilder',__serverBridge__.getStyleBuilder(thiz));
				setReadonlyProp(__internals__,'decodeBase64',__serverBridge__.decodeBase64);
				setReadonlyProp(__internals__,'compileVue',__serverBridge__.getVueCompiler(thiz));
				setReadonlyProp(__internals__,'jsx',__serverBridge__.renderJsx);
				setReadonlyProp(__internals__,'rmaps',/*__get_maps__*/{});
				setReadonlyProp(__internals__,'getIndexes',${JSON.stringify(getIndexes)});
				setReadonlyProp(__internals__,'deps',new Array(arguments.length));
				setReadonlyProp(__internals__,'defEsModuleProp',function(obj){
					if(!obj||typeof obj != 'object'||obj.__esModule||(obj instanceof Function)){return};
					Object.defineProperty(obj,'__esModule',{value: true});
					for(var x in obj){__internals__.defEsModuleProp(obj[x])}
				});
				
				var require=function(){
					if(arguments.length==1&&xsloader.isString(arguments[0])){
						if(arguments[0]==="exports"){
							throw new Error("you should use 'exports' directly");
						}else{
							return require.get(arguments[0]);
						}
					}
					
					return __internals__.require.apply(this,arguments);
				};
				
				require.get=function(name){
					if(name==="exports"){
						throw new Error("you should use 'exports' directly");
					}else if(__internals__.rmaps[name]){
						name=__internals__.rmaps[name];
					}
					return __internals__.require.get.call(this,name);
				};
				
				for(var __i=0;__i<arguments.length;__i++){
					__internals__.deps[__i]=arguments[__i];
				}
				
				var __getRequireDep=function(dep){
					var index=__internals__.getIndexes[dep];
					if(index!==undefined){
						return __internals__.deps[index];
					}else{
						return require.get(dep);
					}
				};
				
				L.asyncCall(function(){
					while(__internals__.deps.length){
						__internals__.deps.pop();
					}
					
					var keys=[];
					for(var k in __internals__.getIndexes){
						keys.push(k);
					}
					
					for(var i=0;i<keys.length;i++){
						delete __internals__.getIndexes[keys[i]];
					}
				});
				
				var define=__serverBridge__.getDefine?__serverBridge__.getDefine(thiz):thiz.define;
				var invoker=__serverBridge__.getInvoker(thiz);
				var vtemplate=__serverBridge__.getVtemplate(thiz);
				var __ImporT__=__serverBridge__.getImporter(thiz);
				var __require_get__ = require.get;
				__serverBridge__=null;
				L=null;
				setReadonlyProp=null;
				__internals__.defEsModuleProp(exports);
				(function(__unstrictFunMap){/*__DEF_PREFIX__*/`;

		scriptPrefix = scriptPrefix.replace(/[\r\n]+[\t\b ]+/g, ' ');

		let scriptSuffix = "/*__DEF_SUFFIX__*/})(" + (function() {
				let as = [];
				for (let k in __unstrictFunMap) {
					as.push("'" + k + "':" + __unstrictFunMap[k]);
				}
				let rs = "{" + as.join(",\n") + "}";
				return rs;
			})() + ");" + customerScriptPart +
			`if(module.exports){return module.exports;} }).then({default:true});`;

		scriptSuffix = scriptSuffix.replace(/[\r\n]/g, "");

		let finalCode = scriptPrefix + parsedCode + scriptSuffix;
		return {
			code: finalCode,
			sourceMap,
			suffixLns: countSubstr(scriptSuffix, "\n"),
		};
	};

	/**
	 * @param {Object} url
	 * @param {Object} filepath
	 * @param {Object} sassContent
	 * @param {Object} hasSourceMap
	 */
	api.parseSass = function(url, filepath, sassContent, hasSourceMap) {
		let $parseSass = $jsBridge$.parseSass;
		return $parseSass(url, filepath, sassContent, hasSourceMap);
	};

	api.parseLess = function(url, filepath, lessContent, hasSourceMap) {
		let $parseLess = $jsBridge$.parseLess;
		return $parseLess(url, filepath, lessContent, hasSourceMap);
	}

	/**
	 * 转换vue文件。
	 * 基本格式为：
	 * /////////
	 * <template></template>
	 * <script></script>
	 * <style></style>
	 * <style></style>
	 * //////////
	 * 说明：
	 * 1、<template>：内部只能有一个根元素，表达式支持es6表达式，在服务器端编译。
	 * 2、<script>：支持ES6代码。支持import,vue异步加载组件用'()=>import(...)'
	 * 3、<style>：lang属性支持default、scss(推荐)、less；可包含多个style标签；scoped:true(scoped),false
	 * 4、支持:staticInclude(relativePath)
	 * 5、htmr文件无template，可以js代码可以省略script标签，style标签必须放在js代码后面，js代码里不能出现<style>标签与html注释。
	 * 
	 * @param {Object} url
	 * @param {Object} filepath
	 * @param {Object} content
	 * @param {Object} hasSourceMap
	 */
	api.transformVue = function(url, filepath, content, hasSourceMap, otherOption) {
		content = $jsBridge$.staticInclude(filepath, content);

		//获取script的内容
		let scriptContent = undefined;
		let scriptLang = undefined;
		let htmr_jsr = otherOption.htmr_jsr;
		let scriptResult;
		if (!htmr_jsr) {
			let templateResult = tagExec("template", content);
			scriptResult = tagExec("script", content, false);
			if (templateResult && scriptResult && scriptResult.index < templateResult.index) {
				templateResult = null;
			}

			if (templateResult) {
				scriptResult = tagExec("script", content.substring(templateResult.end),
					false, templateResult.end);
			}
		} else {
			let commontIndex = content.lastIndexOf("-->\n"); //注释需要换行结尾
			let styleIndex = content.indexOf("\n<style"); //样式需要换行开头
			let cindex, cend;
			if (commontIndex > 0 && styleIndex > 0) { //同时含有注释与样式
				cindex = commontIndex + 4;
				cend = styleIndex;
			} else if (commontIndex > 0) {
				cindex = commontIndex + 4;
				cend = content.length;
			} else if (styleIndex > 0) {
				cindex = 0;
				cend = styleIndex;
			} else {
				cindex = 0;
				cend = content.length;
			}

			let scontent = content.substring(cindex, cend);

			scriptResult = {
				content: scontent, //标签里的内容
				lang: "", //lang属性
				tag: "", //标签名字
				index: cindex, //标签开始位置
				end: cend,
				length: scontent.length, //整个内容的长度（包括标签）
				attrs: {}, //所有标签上的属性
				cindex, //标签里的内容的开始位置
				cend //结束标签的开始位置
			}
		}

		let appendPrefixComment = function(str, markedComments, offset) {
			if (!str) {
				return "";
			}

			if (offset === undefined) {
				offset = 0;
			}

			let strs = str.split("\n");
			for (var i = 0; i < strs.length; i++) {
				strs[i] = "//" + strs[i];
			}
			markedComments.push({
				offset,
				length: strs.length
			});
			return strs.join("\n");
		}

		let charCount = function(str, c) {
			let n = 0;
			for (var i = 0; i < str.length; i++) {
				if (str.charAt(i) == c) {
					n++;
				}
			}
			return n;
		}

		let markedComments = []; //标记添加的前缀注释
		if (scriptResult) {
			let isR = scriptResult.content.indexOf("\r\n") >= 0;
			scriptContent = appendPrefixComment(content.substring(0, scriptResult.cindex), markedComments);
			if (!(scriptResult.content.charAt(0) == '\r' || scriptResult.content.charAt(0) == '\n')) {
				scriptContent += isR ? "\r\n" : "\n";
			}

			scriptContent += scriptResult.content;
			if (!(scriptResult.content.charAt(scriptResult.content.length - 1) == '\n')) {
				scriptContent += isR ? "\r\n" : "\n";
			}

			scriptContent += appendPrefixComment(content.substring(scriptResult.cend), markedComments,
				charCount(scriptContent,
					'\n'));
			scriptLang = scriptResult.lang;

			content = content.substring(0, scriptResult.index) + content.substring(scriptResult.index +
				scriptResult.length);

			if (!scriptResult.content || !scriptResult.content.trim()) {
				scriptContent += "\n'empty';" //添加一行，解决embed模式下、script脚本为空时sourcemap丢失文件的问题
			}
		} else {
			scriptLang = "default";
			scriptContent = appendPrefixComment(content, markedComments) + "\n'empty';";
		}

		//去掉注释
		while (true) {
			let index = content.indexOf("<!--");
			if (index == -1) {
				break;
			}
			let index2 = content.indexOf("-->", index + 4);
			if (index2 != -1) {
				content = content.substring(0, index) + content.substring(index2 + 3);
			}
		}

		let templateResult = htmr_jsr ? null : tagExec("template", content);
		if (templateResult) { //防止转换template内部的style标签
			content = content.substring(0, templateResult.index) + content.substring(templateResult.index +
				templateResult.length);
		}

		let finalCssContents = [];
		let scopedClasses = [];
		let styleNames = [];
		//处理style

		while (true) {
			let styleResult = tagExec("style", content, true);
			if (!styleResult) {
				break;
			} else {
				let finalCssContent;
				content = content.substring(0, styleResult.index) + content.substring(styleResult.index +
					styleResult.length);
				let lang = styleResult.lang || "scss";
				let scoped = styleResult.attrs.scoped === undefined ||
					styleResult.attrs.scoped === null ||
					styleResult.attrs.scoped === "" ? "true" : styleResult.attrs.scoped.content;
				let cssContent = styleResult.content;
				let name = styleResult.attrs.name;
				let scopedClass = "";

				if (scoped === "scoped") {
					scoped = "true";
				}


				if (lang == "sass" || lang == "scss") {
					if (scoped == "true") {
						scopedClass = "scoped-scss-" + $jsBridge$.shortId();
						cssContent = "." + scopedClass + "{\n" + cssContent + "\n}";
					}
					finalCssContent = this.parseSass(url, filepath, cssContent, hasSourceMap);
				} else if (lang == "less") {
					if (scoped == "true") {
						scopedClass = "scoped-less-" + $jsBridge$.shortId();
						cssContent = "." + scopedClass + "{\n" + cssContent + "\n}";
					}
					finalCssContent = this.parseLess(url, filepath, cssContent, hasSourceMap);
				} else {
					finalCssContent = cssContent;
				}
				finalCssContents.push(finalCssContent);
				scopedClass && scopedClasses.push(scopedClass);
				name && styleNames.push({
					name: name.content,
					clazz: scopedClass
				});
			}
		}

		//获取template
		let template = templateResult ? templateResult.content : "";

		let encodeBase64 = function(str) {
			if (str) {
				return root.encodeBase64(str);
			} else {
				return "";
			}
		}

		let theFinalCssContent = finalCssContents.join("\n");

		//替换"$style--name"
		for (let i = 0; i < styleNames.length; i++) {
			let name = styleNames[i].name;
			let clazz = styleNames[i].clazz;
			if (template) {
				template = template.replaceAll("$style--" + name, clazz);
			}
			if (theFinalCssContent) {
				theFinalCssContent = theFinalCssContent.replaceAll("$style--" + name, clazz);
			}
			if (scriptContent) {
				scriptContent = scriptContent.replaceAll("$style--" + name, clazz);
			}
		}

		let filename = "";
		if (filepath) {
			let dirCount = 4;
			let path = filepath.replaceAll("\\", "/");
			let index = path.indexOf("/webapp/");
			if (index > 0) {
				path = path.substring(index);
			}

			let names = path.split("/");
			while (names.length > dirCount) {
				names.shift();
			}
			filename = "__" + names.join("$").replace(/['\"\.:\*\s]/g, "_");
		}

		let customerScriptPart = `\n
		exports.default=exports.default||{};
		var exportsDefault=exports.default;
		if(xsloader.isFunction(exports.default)&&xsloader.isFunction(exports.default.extend)){
			exportsDefault={};
		}
		\n` +
			'var __cssBase64="' + encodeBase64(theFinalCssContent) + '";\n' +
			`var __styleObj=__internals__.styleBuilder(__internals__.decodeBase64(__cssBase64),'${filename}');\n`
		if (htmr_jsr) {
			customerScriptPart += `__styleObj&&__styleObj.init();\n` +
				(() => {
					let strs = [];
					if (scopedClasses.length) {
						let scopedClass = scopedClasses.join(" ");
						strs.push(`try{` +
							`var reactAppDom = document.getElementById('react-app');` +
							`reactAppDom&&reactAppDom.classList.add(${JSON.stringify(scopedClass)});` +
							`}catch(e){console.error(e);}\n`);
					}
					return strs.join("");
				})();
		} else {
			customerScriptPart +=
				`var origin__beforeCreate;
				var origin__created;
				var origin_beforeMount;
				var origin__mounted;
				var origin__destroyed;\n` +
				(() => { //服务端编译<template>
					let strs = [];

					if (scopedClasses.length) {
						let classNames = scopedClasses.join(" ");
						let index = template.indexOf("<");
						let index2 = template.indexOf(">", index + 1);
						if (index != -1 && index2 != -1) {
							let tag = template.substring(index, index2 + 1);
							let result = /^<\s*([a-zA-Z0-9_$\.-])/.exec(tag);
							if (result) {
								let tagName = result[1];
								let regResult = tagExec(tagName, tag + "</" + tagName + ">");
								if (regResult && regResult.attrs["class"]) {
									let classObj = regResult.attrs["class"];
									let classStr = classObj.k + classObj.s1 + classNames + " " + classObj
										.content + classObj.s2;
									template = template.substring(0, index) +
										tag.substring(0, classObj.index) +
										classStr +
										tag.substring(classObj.index + classObj.fullContent.length) +
										template.substring(index2 + 1);
								} else {
									template = template.substring(0, index2) +
										" class='" + classNames + "' " +
										template.substring(index2);
								}
							}
						}
					}

					let compiledTemplate = api.compileVueTemplate(url, filepath, template, hasSourceMap,
						otherOption);

					if (compiledTemplate.render) {
						strs.push("exportsDefault.render=", compiledTemplate.render, ";\n");
					}

					if (compiledTemplate.staticRenderFns) {
						strs.push("exportsDefault.staticRenderFns=", compiledTemplate.staticRenderFns,
							";\n");
					}

					return strs.join("");
				})() +
				`origin__beforeCreate = exportsDefault.beforeCreate;
				origin__created = exportsDefault.created;
				origin_beforeMount = exportsDefault.beforeMount;
				origin__mounted = exportsDefault.mounted;
				origin__destroyed = exportsDefault.destroyed;
				
				exportsDefault.beforeCreate = function() {
				   this.$_styleObj=__styleObj&&__styleObj.init();
				   this.$keepVueStyle=false;
				   this.$thiz=thiz;
				   var that=this;
				   this.$destroyVueStyle=function(){
					   that.$_styleObj&&that.$_styleObj.destroy();
					   that.$_styleObj=null;
				   };
				   this.__xsloader_vue=true;
				   var rt;
				   if(origin__beforeCreate) {
					rt = origin__beforeCreate.apply(this, arguments);
				   }
				   return rt;
				};
				
				exportsDefault.created = function() {
				   this.$emit('vue-created',this);
				   var rt;
				   if(origin__created) {
					rt = origin__created.apply(this, arguments);
				   }
				   this.$emit('invoked-vue-created',this);
				   return rt;
				};
				
				exportsDefault.beforeMount = function() {
				   this.$emit('vue-beforeMount',this);
				   var rt;
				   if(origin_beforeMount) {
					rt = origin_beforeMount.apply(this, arguments);
				   }
				   this.$emit('invoked-vue-beforeMount',this);
				   return rt;
				};
				
				exportsDefault.mounted = function() {
				   this.$emit('vue-mounted',this);
				   
				   var rt;
				   if(origin__mounted) {
					rt = origin__mounted.apply(this, arguments);
				   }
				   
				   this.$emit('invoked-vue-mounted',this);
				   return rt;
				};
				
				exportsDefault.destroyed = function() {
				   this.$emit('vue-destroyed',this);
				   this.$keepVueStyle!==true&&this.$destroyVueStyle();
				   var rt;
				   if(origin__destroyed) {
					rt = origin__destroyed.apply(this, arguments);
				   }
				   this.$emit('invoked-vue-destroyed',this);
				   return rt;
				};
				if(xsloader.isFunction(exports.default)&&xsloader.isFunction(exports.default.extend)){
					exportsDefault = exports.default.extend(exportsDefault);
					exports.default=exportsDefault;
				}else{
					__internals__.compileVue(exports);
				}`;
		}

		customerScriptPart += '__internals__.defEsModuleProp(exports);';

		otherOption = extend({}, otherOption, {
			doStaticInclude: false,
			doStaticVueTemplate: false,
			scriptLang: scriptLang,
		});

		let result = this.parseEs6(url, filepath, scriptContent, customerScriptPart, hasSourceMap,
			otherOption);
		result.markedComments = JSON.stringify(markedComments);

		return result
	};


	api.concatSourceMap = function(items) {
		items = JSON.parse(items);
		const ws = root.globalModules['webpack-sources'];
		const SourceMapSource = ws.SourceMapSource;
		const ConcatSource = ws.ConcatSource;
		const RawSource = ws.RawSource;

		let concatSource = new ConcatSource();
		items.forEach((item) => {
			item.preCode && concatSource.add(new RawSource(item.preCode));

			if (item.generateCode) {
				if (item.sourceMap) {
					concatSource.add(new SourceMapSource(item.generateCode, item.name, item.sourceMap,
						item.originalSource));
				} else {
					concatSource.add(new RawSource(item.generateCode));
				}
			}

			item.afterCode && concatSource.add(new RawSource(item.afterCode));
		});

		let result = concatSource.sourceAndMap();

		return JSON.stringify({
			sourceMap: result.map,
			code: result.source,
		});
	}
})(typeof self !== 'undefined' ? self : this);
