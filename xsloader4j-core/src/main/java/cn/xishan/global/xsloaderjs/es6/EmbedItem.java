package cn.xishan.global.xsloaderjs.es6;

import java.util.List;

/**
 * @author Created by https://github.com/CLovinr on 2021/9/15.
 */
class EmbedItem {
    List<String[]> embedFiles;
    boolean isEmbedAll = false;

    public EmbedItem(List<String[]> embedFiles, boolean isEmbedAll) {
        this.embedFiles = embedFiles;
        this.isEmbedAll = isEmbedAll;
    }
}
