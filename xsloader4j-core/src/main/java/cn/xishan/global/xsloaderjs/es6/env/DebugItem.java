package cn.xishan.global.xsloaderjs.es6.env;

import com.caoccao.javet.interop.V8Runtime;

/**
 * @author Created by https://github.com/CLovinr on 2021/8/25.
 */
class DebugItem {
    V8Runtime v8Runtime;
    String cdtName;

    public DebugItem(V8Runtime v8Runtime, String cdtName) {
        this.v8Runtime = v8Runtime;
        this.cdtName = cdtName;
    }
}
