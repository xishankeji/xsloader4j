package cn.xishan.global.xsloaderjs.es6;

import cn.xishan.global.xsloaderjs.XsloaderFilter;
import cn.xishan.global.xsloaderjs.es6.sourcemap.SourceWithMapItem;
import cn.xishan.oftenporter.porter.core.util.FileTool;
import cn.xishan.oftenporter.porter.core.util.HashUtil;
import cn.xishan.oftenporter.porter.core.util.OftenTool;
import cn.xishan.oftenporter.servlet.HttpCacheUtil;
import cn.xishan.oftenporter.servlet.OftenServletRequest;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 缓存的转换资源对象
 *
 * @author Created by https://github.com/CLovinr on 2019/5/26.
 */
public class CachedResource {


    private static final Logger LOGGER = LoggerFactory.getLogger(CachedResource.class);
    private String path;
    private long lastModified;
    private String topFile;
    private long updatetime;
    private String contentType;
    private String encoding;
    //所有关联的文件
    private JSONArray files;
    private JSONArray filesLastModified;
    private static String VERSION = XsloaderFilter.XSLOADER_VERSION;
    private static String tempId = "default";
    private boolean isSourceMap;
    private BrowserInfo browserInfo;
    private int suffixLns;
    ////////////////////////////
    private Map<String, Object> embedParams;
    private String embedOfUrl;
    private String embedOfPath;
    private boolean isNew = false;

    //初次启动时，所有文件需要重新转换
    private static Set<String> LOADED_PATHS = ConcurrentHashMap.newKeySet();

    private CachedResource(boolean isSourceMap, BrowserInfo browserInfo) {
        this.isSourceMap = isSourceMap;
        this.browserInfo = browserInfo;
    }

    public boolean isNew() {
        return isNew;
    }

    public int getSuffixLns() {
        return suffixLns;
    }

    public void setSuffixLns(int suffixLns) {
        this.suffixLns = suffixLns;
    }

    public Map<String, Object> getEmbedParams() {
        if (embedParams == null && OftenTool.notEmpty(embedOfUrl)) {
            Map<String, Object> params = new HashMap<>();
            params.put("path", embedOfPath);
            params.put("url", embedOfUrl);
            embedParams = params;
        }

        return embedParams;
    }

    public void setHasEmbedParams(String requestUrl, String path) {
        this.embedOfUrl = requestUrl;
        this.embedOfPath = path;
    }


    static String getTempDir(String sub) {
        String tmpdir = System.getProperty("java.io.tmpdir");
        if (!tmpdir.endsWith(File.separator)) {
            tmpdir += File.separator;
        }
        return tmpdir + "xsloader-es6" + File.separator + tempId + File.separator + sub;
    }

    static void init(String tempId) {
        CachedResource.tempId = tempId;

        //启动时，删除临时文件
        File dir = new File(getTempDir("cached-v1"));
        FileTool.delete(dir);
    }

    private static File newFile(String name, String suffix, BrowserInfo browserInfo) {
        File dir = new File(getTempDir("cached-v1"));
        if (!dir.exists()) {
            dir.mkdirs();
        }

        String prefix = HashUtil.md5(name.getBytes(StandardCharsets.UTF_8));
        prefix += "-" + browserInfo.getBrowserType() + "-" + browserInfo.getBrowserMajorVersion();
        File file = new File(dir.getAbsolutePath() + File.separator + prefix
                .substring(0, 1) + File.separator + prefix + VERSION + suffix);
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        return file;
    }

    private static File getConfFile(String path, BrowserInfo browserInfo) {
        return newFile(path, "-confg.json", browserInfo);
    }

    private static File getDataFile(String path, BrowserInfo browserInfo) {
        return newFile(path, "-content.data", browserInfo);
    }

    private static File getOriginalDataFile(String path, BrowserInfo browserInfo) {
        return newFile(path, "-content.original.data", browserInfo);
    }

    private static File getMapFile(String path, BrowserInfo browserInfo) {
        return newFile(path, "-content.data.map", browserInfo);
    }

    private static File getOriginalMapFile(String path, BrowserInfo browserInfo) {
        return newFile(path, "-content.data.original.map", browserInfo);
    }

    /**
     * @param isSourceMap  是否获取其sourceMap内容
     * @param path
     * @param lastModified
     * @param encoding
     * @param contentType
     * @param result
     * @return
     * @throws IOException
     */
    public static CachedResource save(String topFile, boolean isSourceMap, String path, String sourceMapName,
            long lastModified, String encoding, String contentType, CodeParser.Result<String> result)
            throws IOException {
        long updatetime = System.currentTimeMillis();
        CachedResource cachedResource = new CachedResource(isSourceMap, result.getBrowserInfo());
        cachedResource.isNew = true;
        cachedResource.topFile = topFile;
        cachedResource.updatetime = updatetime;
        cachedResource.contentType = contentType;
        cachedResource.encoding = encoding;
        cachedResource.lastModified = lastModified;
        cachedResource.path = path;
        JSONArray files = new JSONArray();
        JSONArray filesLastModified = new JSONArray();
        cachedResource.files = files;
        cachedResource.filesLastModified = filesLastModified;
        cachedResource.suffixLns = result.getSuffixLns();
        for (File file : result.getRelatedFiles()) {
            files.add(file.getAbsolutePath());
            filesLastModified.add(file.lastModified());
        }


        File dataFile = getDataFile(path, result.getBrowserInfo());
        File mapFile = getMapFile(path, result.getBrowserInfo());

        File omapFile = getOriginalMapFile(path, result.getBrowserInfo());
        if (omapFile.exists()) {
            omapFile.delete();
        }
        File odataFile = getOriginalDataFile(path, result.getBrowserInfo());
        if (odataFile.exists()) {
            odataFile.delete();
        }

        String code = result.getContent();
        if (result.getSourceMap() != null && result.isNeedAddSourceMappingURL()) {
            code += String.format("\n//# sourceMappingURL=%s.map", sourceMapName);
        }

        if (code == null) {
            code = "";
        }

        FileTool.write2File(new ByteArrayInputStream(code.getBytes(Charset.forName(encoding))), dataFile,
                true);
        if (result.getSourceMap() != null) {
            FileTool.write2File(result.getSourceMap(), "utf-8", mapFile, true);
        }

        cachedResource.saveConfigFile(lastModified);
        dataFile.setLastModified(lastModified);
        if (OftenTool.notEmpty(topFile)) {
            new File(topFile).setLastModified(lastModified);
        }

        LOADED_PATHS.add(path);

        return cachedResource;
    }

    public File getTopFile() {
        return new File(topFile);
    }

    private void saveConfigFile(long lastModified) throws IOException {
        File confFile = getConfFile(path, browserInfo);

        JSONObject conf = getConfig();
        FileTool.write2File(conf.toJSONString(), "utf-8", confFile, true);

        if (lastModified != -1) {
            confFile.setLastModified(lastModified);
        }
    }

    public JSONObject getConfig() {
        JSONObject conf = new JSONObject();
        conf.put("topFile", topFile);
        conf.put("encoding", encoding);
        conf.put("type", contentType);
        conf.put("updatetime", updatetime);
        conf.put("files", files);
        conf.put("filesLastModified", filesLastModified);
        conf.put("lastModified", lastModified);
        conf.put("suffixLns", suffixLns);
        conf.put("embedOfUrl", embedOfUrl);
        conf.put("embedOfPath", embedOfPath);
        return conf;
    }

    /**
     * @param isSourceMap 是否获取其sourceMap内容。
     * @param path        实际文件路径
     * @return
     * @throws IOException
     */
    public static CachedResource getByPath(boolean isSourceMap, String path, BrowserInfo browserInfo)
            throws IOException {
        if (!LOADED_PATHS.contains(path)) {
            return null;
        } else {
            File confFile = getConfFile(path, browserInfo);
            File dataFile = getDataFile(path, browserInfo);
            if (!confFile.exists() || !dataFile.exists()) {
                return null;
            } else {
                JSONObject conf = JSON.parseObject(FileTool.getString(confFile, "utf-8"));
                String topFile = conf.getString("topFile");
                String contentType = conf.getString("type");
                String encoding = conf.getString("encoding");
                JSONArray files = conf.getJSONArray("files");
                JSONArray filesLastModified = conf.getJSONArray("filesLastModified");
                long lastModified = conf.getLongValue("lastModified");
                int suffixLns = conf.getIntValue("suffixLns");
                String embedOfUrl = conf.getString("embedOfUrl");
                String embedOfPath = conf.getString("embedOfPath");

                CachedResource cachedResource = new CachedResource(isSourceMap, browserInfo);
                cachedResource.topFile = topFile;
                cachedResource.contentType = contentType;
                cachedResource.encoding = encoding;
                cachedResource.lastModified = lastModified;
                cachedResource.path = path;
                cachedResource.files = files;
                cachedResource.filesLastModified = filesLastModified;
                cachedResource.suffixLns = suffixLns;
                cachedResource.embedOfUrl = embedOfUrl;
                cachedResource.embedOfPath = embedOfPath;

                if (conf.containsKey("updatetime")) {
                    cachedResource.updatetime = conf.getLongValue("updatetime");
                } else {
                    cachedResource.updatetime = 0;
                }
                return cachedResource;
            }
        }
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(long updatetime) {
        this.updatetime = updatetime;
        JSONObject conf = getConfig();
        File confFile = getConfFile(path, browserInfo);
        try {
            FileTool.write2File(conf.toJSONString(), "utf-8", confFile, true);
        } catch (IOException e) {
            LOGGER.warn(e.getMessage(), e);
        }
    }

    public void clearCache() {
        File confFile = getConfFile(path, browserInfo);
        File dataFile = getDataFile(path, browserInfo);
        OftenTool.deleteFiles(confFile, dataFile);
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }


    public boolean needReload(File realFile, boolean isDebug) {
        return realFile != null && needReload(realFile.lastModified(), isDebug);
    }

    public boolean needReload(long lastModified, boolean isDebug) {
        if (this.lastModified != lastModified || isDebug && isFilesChange()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 判断关联的文件是否变化了
     *
     * @return
     */
    private boolean isFilesChange() {
        for (int i = 0; i < this.files.size(); i++) {
            String file = this.files.getString(i);
            File f = new File(file);
            if (f.exists() && f.isFile() && f.lastModified() != filesLastModified.getLongValue(i)) {
                return true;
            }
        }
        return false;
    }

    public void setLastModified(long lastModified) {
        this.lastModified = lastModified;
    }

    public int getContentLength() {
        return (int) getDataFile().length();
    }

    public InputStream getContent() throws IOException {
        return new FileInputStream(getDataFile());
    }

    private File getDataFile() {
        return getDataFile(path, browserInfo);
    }

    public String getContentText() throws IOException {
        return FileTool.getString(getDataFile(), getEncoding());
    }

    public String getOriginalContentText() throws IOException {
        File file = getOriginalDataFile(path, browserInfo);
        if (!file.exists()) {
            file = getDataFile(path, browserInfo);
        }
        return FileTool.getString(file, getEncoding());
    }

    void setContentTextKeepOriginal(String content) throws IOException {
        File file = getDataFile(path, browserInfo);
        File ofile = getOriginalDataFile(path, browserInfo);
        if (!ofile.exists()) {
            FileTool.file2file(file, ofile, 4096, true);
        }
        FileTool.write2File(content, getEncoding(), file, true);
    }

    public String getSourceMapContentType() {
        return "text/plain";
    }

    public InputStream getSourceMap() throws FileNotFoundException {
        return new FileInputStream(getMapFile(path, browserInfo));
    }

    public String getSourceMapText() throws IOException {
        return FileTool.getString(getMapFile(path, browserInfo), getEncoding());
    }

    public String getOriginalSourceMapText() throws IOException {
        File file = getOriginalMapFile(path, browserInfo);
        if (!file.exists()) {
            file = getMapFile(path, browserInfo);
        }
        return FileTool.getString(file, getEncoding());
    }

    void setSourceMapTextKeepOriginal(String content) throws IOException {
        File file = getMapFile(path, browserInfo);
        File ofile = getOriginalMapFile(path, browserInfo);
        if (!ofile.exists()) {
            FileTool.file2file(file, ofile, 4096, true);
        }
        FileTool.write2File(content, getEncoding(), file, true);
    }

    public int getSourceMapLength() {
        return (int) getMapFile(path, browserInfo).length();
    }


    public boolean existsMap() {
        return getMapFile(path, browserInfo).exists();
    }


    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    private void doResponse(boolean isSource, HttpServletResponse response) {
        if (isSource) {
            File file = new File(topFile);
            response.setContentLength((int) file.length());
            try (OutputStream os = response.getOutputStream(); InputStream in = new FileInputStream(file)) {
                FileTool.in2out(in, os, 1024);
                os.flush();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } else {
            response.setContentLength(isSourceMap() ? getSourceMapLength() : this.getContentLength());
            try (OutputStream os = response.getOutputStream(); InputStream in = isSourceMap() ? getSourceMap() : this
                    .getContent()) {
                FileTool.in2out(in, os, 1024);
                os.flush();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

    }

    public boolean isSourceMap() {
        return isSourceMap;
    }

    public void writeResponse(HttpServletRequest request, HttpServletResponse response,
            boolean isDebug, int forceCacheSeconds) throws IOException {
        writeResponse(false, request, response, isDebug, forceCacheSeconds);
    }

    public void writeResponse(boolean isSource, HttpServletRequest request, HttpServletResponse response,
            boolean isDebug, int forceCacheSeconds) throws IOException {
        if (isSource && OftenTool.isEmpty(topFile) && !new File(topFile).exists()) {
            response.sendError(404);
            return;
        } else if (isSourceMap() && !existsMap()) {
            response.sendError(404);
            return;
        }

        long lastModified = this.lastModified;
        response.setCharacterEncoding(this.getEncoding());
        response.setContentType(
                isSource ? "text/plain" : (isSourceMap() ? this.getSourceMapContentType() : this.getContentType())
        );
        HttpCacheUtil.checkWithModified(lastModified, request, response)
                .changed((request2, response2) -> doResponse(isSource, response2))
                .cacheInfo(forceCacheSeconds, lastModified)
                .unchanged304()
                .done();
    }


    static class EmbedResource {
        String name;
        String relative;
        String orelative;
        String url;
        String parentUrl;
        CachedResource parent;
        CachedResource resource;

        public EmbedResource(String name, String relative, String orelative, String url, String parentUrl,
                CachedResource parent, CachedResource resource) {
            this.name = name;
            this.relative = relative;
            this.orelative = orelative;
            this.url = url;
            this.parentUrl = parentUrl;
            this.parent = parent;
            this.resource = resource;
        }
    }

    static class RelativeItem {
        String url;
        String depId;
        String pluginArgs;

        public RelativeItem(String url, String depId, String pluginArgs) {
            this.url = url;
            this.depId = depId;
            this.pluginArgs = pluginArgs;
        }

        public String getDep() {
            return pluginArgs == null ? depId : depId + pluginArgs;
        }
    }

    /**
     * 合并script。
     *
     * @param embedResources
     */
    public void combineScript(String mineUrl, Map<String, EmbedResource> embedResources)
            throws IOException {
        JSONObject topSourceMap = JSON.parseObject(getOriginalSourceMapText());

        List<String> scriptUrls = new ArrayList<>();
        Map<String, String> scriptMap = new HashMap<>();

        //记录url嵌入的脚本
        Map<String, Map<String, RelativeItem>> depsMap = new HashMap<>(embedResources.size());

        List<SourceWithMapItem> sourceWithMapItemList = new ArrayList<>();
        List<String> subFiles = new ArrayList<>();
        JSONArray sourcesContent = new JSONArray(embedResources.size() + 1);

        for (Map.Entry<String, EmbedResource> entry : embedResources.entrySet()) {
            String url = entry.getKey();
            if (url.equals(mineUrl)) {
                continue;
            }
            scriptUrls.add(url);

            String depId = "embed-" + HashUtil.md5(url.getBytes());
            EmbedResource embedResource = entry.getValue();
            CachedResource resource = embedResource.resource;
            String name = embedResource.name;
            if (!depsMap.containsKey(embedResource.parentUrl)) {
                depsMap.put(embedResource.parentUrl, new HashMap<>());
            }

            String pluginArgs = null;
            if (!embedResource.orelative.equals(embedResource.relative)) {
                pluginArgs = embedResource.orelative.substring(embedResource.relative.length());
            }

            depsMap.get(embedResource.parentUrl).put(embedResource.orelative, new RelativeItem(url, depId, pluginArgs));
            subFiles.add(resource.topFile);

            this.files.add(resource.topFile);
            this.filesLastModified.add(resource.getTopFile().lastModified());
            this.files.addAll(resource.files);//被引入的文件变化时，也会重新加载
            this.filesLastModified.addAll(resource.filesLastModified);

            String script = resource.getOriginalContentText();
            script = script.replace("/*__DEF_TAG_NAME__*/", String.format("'%s',", depId));

            final String MTAG = "//# sourceMappingURL";
            int mapIndex = script.lastIndexOf(MTAG);
            script = script.substring(0, mapIndex) + "\n";
            scriptMap.put(url, script);

            JSONObject sourceMapJson = JSON.parseObject(resource.getOriginalSourceMapText());
            JSONArray currentSources = sourceMapJson.getJSONArray("sources");
            if (currentSources.size() == 1) {
                currentSources.set(0, name);
            }

            sourceMapJson.remove("sourceRoot");
            sourceMapJson.remove("sourcesContent");

            sourceWithMapItemList.add(newSourceWithMapItem(name, scriptMap.get(url), sourceMapJson));
            sourcesContent.add(FileTool.getString(resource.getTopFile(), resource.getEncoding()));
        }

        scriptUrls.add(mineUrl);
        scriptMap.put(mineUrl, getOriginalContentText());

        {//inner deps
            Map<String, String> url2depIds = new HashMap<>();

            for (Map.Entry<String, Map<String, RelativeItem>> entry : depsMap.entrySet()) {
                String url = entry.getKey();
                String script = scriptMap.get(url);
                LOGGER.debug("embed:parent={},child={}", mineUrl, url);

                String tag = "/*__DEF_INNER__*/";
                int index1 = script.indexOf(tag);
                int index2 = script.indexOf(tag, index1 + tag.length());
                String innerStr = script.substring(index1 + tag.length(), index2);
                JSONObject getMaps = new JSONObject();

                for (Map.Entry<String, RelativeItem> subEntry : entry.getValue().entrySet()) {
                    String orelative = subEntry.getKey();
                    RelativeItem relativeItem = subEntry.getValue();

                    String dep = relativeItem.getDep();

                    innerStr = innerStr.replace("\"" + orelative + "\"", "\"" + dep + "\"");
                    getMaps.put(orelative, dep);
                    url2depIds.put(relativeItem.url, relativeItem.depId);
                }
                script = script.substring(0, index1) + innerStr + script.substring(index2 + tag.length());

                tag = "/*__get_maps__*/";//用于兼容以前版本的xsloader.js
                index1 = script.indexOf(tag);
                script = script.substring(0, index1) + getMaps.toJSONString() + "||" +
                        script.substring(index1 + tag.length());

                scriptMap.put(url, script);
            }

            String innerDepScript = "(function(){" +
                    "\tvar lastChangeDepBeforeLoad=xsloader.changeDepBeforeLoad;" +
                    String.format("\tvar depMaps=%s;", JSON.toJSONString(url2depIds)) +
                    "\txsloader.changeDepBeforeLoad=function(dep,module){" +
                    "\t\tif(xsloader.startsWith(dep,'.')){" +
                    "\t\t\tvar pluginArgs='';(function(index){if(index>0){pluginArgs=dep.substring(index);dep=dep" +
                    ".substring(0,index);}})(dep.indexOf('!'));" +
                    "\t\t\tvar url=module.thiz.getUrl(dep,false);" +
                    "\t\t\tif(depMaps[url]){" +
                    "\t\t\t\treturn depMaps[url]+pluginArgs;" +
                    "\t\t\t}else{" +
                    "\t\t\t\tdep+=pluginArgs;" +
                    "\t\t\t}" +
                    "\t\t}" +
                    "\t\t" +
                    "\t\tif(lastChangeDepBeforeLoad){" +
                    "\t\t\treturn lastChangeDepBeforeLoad(dep);" +
                    "\t\t}" +
                    "\t}" +
                    "})();\n";

            SourceWithMapItem forEmbedDeps = new SourceWithMapItem();
            forEmbedDeps.setPreCode(innerDepScript.replace("\t", ""));
            sourceWithMapItemList.add(forEmbedDeps);
        }

        String topName = OftenServletRequest.getPathFromURL(mineUrl);
        topSourceMap.getJSONArray("sources").set(0, topName);

        topSourceMap.remove("sourceRoot");
        topSourceMap.remove("sourcesContent");
        sourceWithMapItemList.add(newSourceWithMapItem(topName, scriptMap.get(mineUrl), topSourceMap));

        sourcesContent.add(FileTool.getString(getTopFile(), getEncoding()));

        JSONObject result = CodeParser.concatSourceMap(sourceWithMapItemList);
        JSONObject finalSourceMapJson = result.getJSONObject("sourceMap");
        finalSourceMapJson.put("sourceRoot", "/$$xs-sources$$/");
        finalSourceMapJson.put("file", topSourceMap.getString("file"));
        finalSourceMapJson.put("sourcesContent", sourcesContent);

        String genCode = result.getString("code");


        setContentTextKeepOriginal(genCode);
        setSourceMapTextKeepOriginal(finalSourceMapJson.toJSONString());
        saveConfigFile(-1);
        JsFilter.setSubFiles(topFile, subFiles);
    }

    private SourceWithMapItem newSourceWithMapItem(String name, String wrapperCode, JSONObject sourceMap) {
        String tagPrefix = "/*__DEF_PREFIX__*/";
        String tagSuffix = "/*__DEF_SUFFIX__*/";
        int index1 = wrapperCode.indexOf(tagPrefix);
        int index2 = wrapperCode.indexOf(tagSuffix, index1);
        String parsedCode = wrapperCode.substring(index1 + tagPrefix.length(), index2);

        SourceWithMapItem sourceWithMapItem = new SourceWithMapItem(name, sourceMap, parsedCode);
        sourceWithMapItem.setPreCode(wrapperCode.substring(0, index1));
        sourceWithMapItem.setAfterCode(wrapperCode.substring(index2 + tagSuffix.length()));
        return sourceWithMapItem;
    }
}
