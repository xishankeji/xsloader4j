package cn.xishan.global.xsloaderjs.es6.jbrowser;

import cn.xishan.oftenporter.porter.core.util.OftenTool;
import com.caoccao.javet.exceptions.JavetException;
import com.caoccao.javet.interop.V8Runtime;

import java.util.ArrayList;

/**
 * @author Created by https://github.com/CLovinr on 2021/10/15.
 */
public class DefaultRoot extends J2Object {
    private String tag;

    /**
     * @param tag
     * @param name 若不为null，则会添加当前对象到全局作用域、属性名为name。
     * @param v8
     */
    public DefaultRoot(String tag, String name, V8Runtime v8) {
        super();
        this.tag = tag;

        _v8 = v8;
        releasableList = new ArrayList<>();
        if (OftenTool.notEmpty(name)) {
            v8Object = newV8Object();
            try {
                v8.getGlobalObject().set(name, v8Object);
            } catch (JavetException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
