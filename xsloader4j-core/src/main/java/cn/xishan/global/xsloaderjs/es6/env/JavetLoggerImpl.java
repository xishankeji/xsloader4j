package cn.xishan.global.xsloaderjs.es6.env;

import com.caoccao.javet.interfaces.IJavetLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Created by https://github.com/CLovinr on 2021/8/25.
 */
public class JavetLoggerImpl implements IJavetLogger {
    protected Logger logger;

    public JavetLoggerImpl(String name) {
        logger = LoggerFactory.getLogger("javet-" + name);
    }

    @Override
    public void debug(String message) {
        if (logger.isDebugEnabled()) {
            logger.debug(message);
        }
    }

    @Override
    public void error(String message) {
        if (logger.isDebugEnabled()) {
            logger.error(message);
        }
    }

    @Override
    public void error(String message, Throwable throwable) {
        if (logger.isDebugEnabled()) {
            logger.error(message, throwable);
        }
    }

    @Override
    public void info(String message) {
        if (logger.isInfoEnabled()) {
            logger.info(message);
        }
    }

    @Override
    public void warn(String message) {
        if (logger.isWarnEnabled()) {
            logger.warn(message);
        }
    }
}
