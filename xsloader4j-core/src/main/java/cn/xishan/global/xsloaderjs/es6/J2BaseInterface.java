package cn.xishan.global.xsloaderjs.es6;

import cn.xishan.global.xsloaderjs.es6.jbrowser.DefaultRoot;
import cn.xishan.global.xsloaderjs.es6.jbrowser.IValueBuilder;
import cn.xishan.global.xsloaderjs.es6.jbrowser.J2Object;
import cn.xishan.global.xsloaderjs.es6.jbrowser.JsBridgeMethod;
import cn.xishan.oftenporter.porter.core.annotation.MayNull;
import cn.xishan.oftenporter.porter.core.util.*;
import com.caoccao.javet.exceptions.JavetException;
import com.caoccao.javet.interop.V8Runtime;
import com.caoccao.javet.values.reference.IV8ValueObject;
import com.inet.lib.less.Less;
import com.inet.lib.less.ReaderFactory;
import io.bit3.jsass.Compiler;
import io.bit3.jsass.Options;
import io.bit3.jsass.Output;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Created by https://github.com/CLovinr on 2019-05-31.
 */
public class J2BaseInterface extends J2Object implements AutoCloseable, IValueBuilder {

    private static final Logger LOGGER = LoggerFactory.getLogger(J2BaseInterface.class);
    static final ThreadLocal<CodeParser.Result<String>> threadLocal = new ThreadLocal<>();
    private static final ThreadLocal<String> currentUrl = new ThreadLocal<>();

    interface ILoadFileListener {
        void onLoadFile(File file);
    }

    private ILoadFileListener fileListener;
    private IFileContentGetter fileContentGetter;
    private ConcurrentKeyLock lock;
    private String debugLibDir;
    private long lastTime = System.currentTimeMillis();

    static String polyfillPath;
    static String contextPath;

    public J2BaseInterface(V8Runtime v8, boolean isAutoRegisterMethod, String debugLibDir) {
        super(new DefaultRoot("base", null, v8));

        this.debugLibDir = debugLibDir;
        lock = new ConcurrentKeyLock();
        if (isAutoRegisterMethod) {
            autoRegisterMethod();
        }
    }

    public ILoadFileListener getFileListener() {
        return fileListener;
    }

    static class J2BaseInterfaceImpl extends J2BaseInterface {
        private J2BaseInterface superInterface;

        public J2BaseInterfaceImpl(J2BaseInterface superInterface) {
            super(superInterface.getV8(), false, superInterface.debugLibDir);

            ((DefaultRoot) root).setTag("base-impl");

            this.superInterface = superInterface;
            superInterface.setValueBuilder(this);
            superInterface.root.setValueBuilder(this);
        }

        @Override
        public void setFileContentGetter(IFileContentGetter fileContentGetter) {
            superInterface.fileContentGetter = fileContentGetter;
        }

        @Override
        public void setFileListener(ILoadFileListener fileListener) {
            superInterface.fileListener = fileListener;
        }

        @Override
        public void release() {
            superInterface.fileListener = null;
            superInterface.fileContentGetter = null;
            superInterface.setValueBuilder(null);
            superInterface.root.setValueBuilder(null);
            super.release();
        }
    }

    public J2BaseInterface acquire(String url) {
        lock.lock("v8");
        if (url != null) {
            currentUrl.set(url);
        }

        long time = System.currentTimeMillis();
        if (time - lastTime > TimeUnit.MINUTES.toMillis(30)) {
            lastTime = time;
            getV8().lowMemoryNotification();
        }

        return new J2BaseInterfaceImpl(this);
    }

    public void release() {
        try {
            this.fileListener = null;
            this.fileContentGetter = null;
            currentUrl.remove();
            threadLocal.remove();
            root.release();
        } finally {
            lock.unlock("v8");
        }
    }


    public void setFileListener(ILoadFileListener fileListener) {
        this.fileListener = fileListener;
    }

    public void setFileContentGetter(IFileContentGetter fileContentGetter) {
        this.fileContentGetter = fileContentGetter;
    }

    @JsBridgeMethod
    public String shortId() {
        return HashUtil.md5_16(OftenKeyUtil.randomUUID().getBytes());
    }

    @JsBridgeMethod
    public String parseSass(String url, String filepath, String scssCode, boolean isDebug) throws Exception {
        Compiler compiler = new Compiler();
        Options options = new Options();
        options.setSourceMapEmbed(false);
        if (threadLocal.get() != null && OftenTool.notEmpty(filepath)) {
            options.setImporters(Collections.singleton(new JScssImporterImpl(threadLocal.get(), new File(filepath))));
        }
        Output output = compiler.compileString(
                scssCode,
                filepath == null ? null : new File(filepath).toURI(),
                new URI(url),
                options
        );
        return output.getCss();
    }

    @JsBridgeMethod
    public String parseLess(String url, String filepath, String lessCode, boolean isDebug) throws Exception {
        String css = Less
                .compile(new File(filepath).getParentFile().toURI().toURL(), lessCode, false, new ReaderFactory() {
                    @Override
                    public InputStream openStream(URL url) throws IOException {
                        if (url.getProtocol().equals("file") && threadLocal.get() != null) {
                            threadLocal.get().getRelatedFiles().add(new File(url.getFile()));
                        }
                        return super.openStream(url);
                    }
                });
        return css;
    }


    private static final Pattern PATTERN_STATIC_INCLUDE = Pattern
            .compile(
                    "staticInclude\\s*\\(\\s*['\"]\\s*([^`'\"\\(\\)]+)\\s*['\"](\\s+encoding=([a-zA-Z0-9_-]*))" +
                            "?\\s*\\)");

    /**
     * 处理staticInclude(...)
     *
     * @param filename
     * @param content
     * @return
     */
    @JsBridgeMethod
    public String staticInclude(String filename, String content) throws IOException {
        return staticInclude(filename, content, fileContentGetter, fileListener);
    }


    /**
     * 处理staticInclude(...)
     *
     * @param filename     参考相对地址
     * @param fileContent  如果为null，则从filename文件中读取。
     * @param getter
     * @param fileListener
     * @return
     * @throws IOException
     */
    public static String staticInclude(String filename, @MayNull String fileContent, @MayNull IFileContentGetter getter,
            @MayNull ILoadFileListener fileListener) throws IOException {
        if (OftenTool.notEmpty(filename)) {
            if (fileContent == null) {
                fileContent = FileTool.getString(new File(filename));
            }
            filename = filename.replace('\\', '/');
            Matcher matcher = PATTERN_STATIC_INCLUDE.matcher(fileContent);
            StringBuilder stringBuilder = new StringBuilder();
            int lastIndex = 0;
            while (matcher.find()) {
                stringBuilder.append(fileContent, lastIndex, matcher.start());

                String relative = matcher.group(1).trim().replace('\\', '/');
                String path = PackageUtil.getPathWithRelative(filename, relative);
                File file = new File(path);
                if (file.exists() && file.isFile()) {
                    String encoding = matcher.group(3);
                    if (OftenTool.isEmpty(encoding)) {
                        encoding = "utf-8";
                    }

                    if (getter == null) {
                        getter = new IFileContentGetter() {
                        };
                    }
                    IFileContentGetter.Result result = getter.getResult(file, encoding);
                    if (result != null) {
                        if (fileListener != null) {
                            fileListener.onLoadFile(file);
                        }

                        String includeContent = result.getContent();
                        includeContent = staticInclude(path, includeContent, getter, fileListener);//递归引入
                        stringBuilder.append(includeContent);
                    }
                } else {
                    stringBuilder.append("/*未找到文件:").append(matcher.group()).append("*/");
                }
                lastIndex = matcher.end();
            }
            stringBuilder.append(fileContent, lastIndex, fileContent.length());
            fileContent = stringBuilder.toString();
        }

        if (fileContent != null) {
            fileContent = CodeParser.removeSChars(fileContent);
        }

        return fileContent;
    }

    private static final Pattern PATTERN_STATIC_VUE_TEMPLATE = Pattern
            .compile("(['\"]?template['\"]?\\s*:\\s*)?staticVueTemplate\\s*\\(\\s*`([^`]*)`\\s*\\)");

    @JsBridgeMethod
    public String staticVueTemplate(String currentUrl, String filepath, String scriptContent, boolean isDebug,
            String funName, IV8ValueObject funMap) {
        if (scriptContent != null) {
            Matcher matcher = PATTERN_STATIC_VUE_TEMPLATE.matcher(scriptContent);
            StringBuilder stringBuilder = new StringBuilder();
            int lastIndex = 0;

            V8Runtime v8 = getV8();
            try (IV8ValueObject xsloaderServer = v8.getGlobalObject().get("XsloaderServer");) {

                while (matcher.find()) {
                    stringBuilder.append(scriptContent, lastIndex, matcher.start());
                    String template = matcher.group(2);
                    Object[] args = {
                            currentUrl,
                            filepath,
                            template,
                            isDebug
                    };

                    try (IV8ValueObject result = xsloaderServer.invoke("compileVueTemplate", args);
                    ) {
                        int lastLn = stringBuilder.lastIndexOf("\n");
                        int currentLen = stringBuilder.length();
                        String id = OftenKeyUtil.randomUUID();

                        if (result.has("render")) {
                            stringBuilder.append("render:").append(funName).append("['").append(id).append("-render']");
                            funMap.set(id + "-render", result.getString("render"));
                        }

                        if (result.has("staticRenderFns")) {
                            if (result.has("render")) {
                                stringBuilder.append(",\n");
                                if (lastLn > 0) {
                                    stringBuilder.append(stringBuilder.subSequence(lastLn + 1, currentLen));
                                }
                            }
                            stringBuilder.append("staticRenderFns:").append(funName).append("['").append(id)
                                    .append("-staticRenderFns']");
                            funMap.set(id + "-staticRenderFns", result.getString("staticRenderFns"));
                        }

                        if (result.has("staticRenderFns") || result.has("render")) {
                            stringBuilder.append("\n");
                        }

                        lastIndex = matcher.end();
                    }
                }

            } catch (JavetException e) {
                throw new RuntimeException(e);
            }
            stringBuilder.append(scriptContent, lastIndex, scriptContent.length());
            scriptContent = stringBuilder.toString();
        }

        return scriptContent;
    }

    @JsBridgeMethod(isRootFun = true)
    public void print(String str) {
        LOGGER.debug("js print:(url={})\n{}", currentUrl.get(), str);
    }

    @JsBridgeMethod(isRootFun = true)
    public void loadLib(String path) throws JavetException {
        String script;
        if (debugLibDir != null) {
            path = debugLibDir + "/xsloader-js/lib/libs" + path;
            script = FileTool.getString(new File(path));
        } else {
            path = "/xsloader-js/lib/libs" + path;
            script = ResourceUtil.getAbsoluteResourceString(path, "utf-8");
        }
        getV8().getExecutor(script).setResourceName(path).executeVoid();
    }

    @JsBridgeMethod(isRootFun = true)
    public String encodeBase64(String text) {
        return OftenBase64.encode(text.getBytes(StandardCharsets.UTF_8));
    }

    @JsBridgeMethod
    public void logText(String str, String type) {
        if ("debug".equals(type)) {
            LOGGER.debug("js debug:(url={})\n{}", currentUrl.get(), str);
        } else if ("log".equals(type)) {
            LOGGER.debug("js log:(url={})\n{}", currentUrl.get(), str);
        } else if ("info".equals(type)) {
            LOGGER.info("js info:(url={})\n{}", currentUrl.get(), str);
        } else if ("warn".equals(type)) {
            LOGGER.warn("js warn:(url={})\n{}", currentUrl.get(), str);
        } else if ("error".equals(type)) {
            LOGGER.error("js error:(url={})\n{}", currentUrl.get(), str);
        } else {
            LOGGER.warn("js:(url={})\n{}", currentUrl.get(), str);
        }
    }


    @JsBridgeMethod
    public String getPolyfillPath() {
        return polyfillPath;
    }

    @JsBridgeMethod
    public String getContextPath() {
        return contextPath;
    }

    public IV8ValueObject getRootObject(String name) {
        try {
            return getV8().getGlobalObject().get(name);
        } catch (JavetException e) {
            throw new RuntimeException(e);
        }
    }
}
