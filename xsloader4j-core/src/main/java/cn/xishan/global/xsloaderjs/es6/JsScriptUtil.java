package cn.xishan.global.xsloaderjs.es6;

import cn.xishan.global.xsloaderjs.es6.env.CDTDebugPorter;
import cn.xishan.global.xsloaderjs.es6.env.EnvOption;
import cn.xishan.global.xsloaderjs.es6.env.JavetLoggerImpl;
import cn.xishan.global.xsloaderjs.es6.jbrowser.J2Document;
import cn.xishan.global.xsloaderjs.es6.jbrowser.J2Object;
import cn.xishan.global.xsloaderjs.es6.jbrowser.J2Window;
import cn.xishan.oftenporter.porter.core.util.FileTool;
import cn.xishan.oftenporter.porter.core.util.ResourceUtil;
import com.caoccao.javet.exceptions.JavetException;
import com.caoccao.javet.interfaces.IJavetClosable;
import com.caoccao.javet.interop.V8Flags;
import com.caoccao.javet.interop.V8Host;
import com.caoccao.javet.interop.V8Runtime;
import com.caoccao.javet.interop.converters.JavetProxyConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * @author Created by https://github.com/CLovinr on 2019/1/7.
 */
public class JsScriptUtil {

    static class ScriptItem {
        String name;
        String content;
        File scriptFile;

        public ScriptItem(String resourcePath) {
            this(resourcePath, ResourceUtil.getAbsoluteResourceString(resourcePath, "utf-8"));
        }

        public ScriptItem(File scriptFile) {
            this.scriptFile = scriptFile;
        }

        public ScriptItem(String name, String content) {
            this.name = name;
            this.content = content;
        }
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(JsScriptUtil.class);

    private static EnvOption envOption;
    private static CDTDebugPorter cdtDebugPorter;
    private static String debugLibDir;
    private static ScriptItem[] scripts;
    private static ScriptItem[] scripts2;
    private static J2BaseInterface cachedInterface = null;
    private static List<Consumer<Void>> readyList = new ArrayList<>(1);


    static void release(IJavetClosable... releasables) {
        for (IJavetClosable releasable : releasables) {
            J2Object.release(releasable);
        }
    }


    public static V8Runtime createV8(EnvOption option) {
        try {
            V8Host host = V8Host.getV8Instance();
            V8Flags flags = host.getFlags();
            flags.setUseStrict(true);
            V8Runtime v8Runtime = host.createV8Runtime();
            v8Runtime.setLogger(new JavetLoggerImpl(option == null ? "default" : option.getName()));
            v8Runtime.setConverter(new JavetProxyConverter());

            if (option != null) {
                if (option.isEnableCDT()) {
                    if (cdtDebugPorter == null) {
                        LOGGER.warn("cdtDebugPorter is null!");
                    } else {
                        cdtDebugPorter.addDebugRuntime(v8Runtime, option);
                    }
                }
            }

            return v8Runtime;
        } catch (JavetException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * @param url 当前访问路径
     * @return
     */
    static synchronized J2BaseInterface getAndAcquire(String url) {
        if (cachedInterface == null) {
            synchronized (JsScriptUtil.class) {
                if (cachedInterface == null) {
                    V8Runtime v8 = null;
                    try {
                        v8 = createV8(envOption);
                        if (cdtDebugPorter != null) {
                            String version = v8.getVersion();
                            cdtDebugPorter.setV8Version(version);
                        }

                        J2BaseInterface j2BaseInterface = new J2BaseInterface(v8, true, debugLibDir);
                        v8.getGlobalObject().set("$jsBridge$", j2BaseInterface.getV8Object());

                        for (ScriptItem script : scripts) {
                            if (script.scriptFile != null) {
                                v8.getExecutor(script.scriptFile).executeVoid();
                            } else {
                                v8.getExecutor(script.content).setResourceName(script.name).executeVoid();
                            }
                        }

                        J2Document j2Document = new J2Document(j2BaseInterface.getRoot());
                        v8.getGlobalObject().set("document", j2Document.getV8Object());

                        J2Window window = new J2Window(j2BaseInterface.getRoot());
                        v8.getGlobalObject().set("window", window.getV8Object());

                        for (ScriptItem script : scripts2) {
                            if (script.scriptFile != null) {
                                v8.getExecutor(script.scriptFile).executeVoid();
                            } else {
                                v8.getExecutor(script.content).setResourceName(script.name).executeVoid();
                            }
                        }
                        cachedInterface = j2BaseInterface;
                    } catch (Throwable e) {
                        release(v8);
                        throw new RuntimeException(e);
                    }
                }
            }
        }
        return cachedInterface.acquire(url);
    }

    static void init(CDTDebugPorter cdtDebugPorter, EnvOption envOption) {
        JsScriptUtil.envOption = envOption;
        JsScriptUtil.cdtDebugPorter = cdtDebugPorter;

        if (envOption.isEnableCDT()) {
            List<ResourceUtil.RFile> list = ResourceUtil.listResources("/xsloader-js/lib/", null, true);
            File dir = new File(CachedResource.getTempDir("cdt-js"));
            String debugLibDir = dir.getAbsolutePath().replace(File.separatorChar, '/');
            if (!dir.exists()) {
                dir.mkdirs();
            }

            try {
                for (ResourceUtil.RFile rFile : list) {
                    if (!rFile.isDir() && rFile.getPath().endsWith(".js")) {
                        File file = new File(debugLibDir + "/" +
                                rFile.getPath().replace(File.separatorChar, '/'));
                        if (!file.getParentFile().exists()) {
                            file.getParentFile().mkdirs();
                        }

                        FileTool.write2File(rFile.getInputStream(), file, true);
                    }
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }

            JsScriptUtil.debugLibDir = debugLibDir;
            JsScriptUtil.scripts = new ScriptItem[]{
                    new ScriptItem(new File(debugLibDir + "/xsloader-js/lib/init1.js")),
            };

            JsScriptUtil.scripts2 = new ScriptItem[]{
                    new ScriptItem(new File(debugLibDir + "/xsloader-js/lib/init2.js")),
                    new ScriptItem(new File(debugLibDir + "/xsloader-js/lib/mine.js")),
            };

            LOGGER.warn("debugLibDir:{}", debugLibDir);
        } else {
            JsScriptUtil.scripts = new ScriptItem[]{
                    new ScriptItem("/xsloader-js/lib/init1.js"),
            };

            JsScriptUtil.scripts2 = new ScriptItem[]{
                    new ScriptItem("/xsloader-js/lib/init2.js"),
                    new ScriptItem("/xsloader-js/lib/mine.js"),
            };
        }

        try {
            List<Consumer<Void>> list = readyList;
            readyList = null;
            for (Consumer<Void> consumer : list) {
                consumer.accept(null);
            }
        } catch (Exception e) {
            LOGGER.warn(e.getMessage(), e);
        }

        try (J2BaseInterface ignore = getAndAcquire("default")) {
        }
    }

    static void onReady(Consumer<Void> consumer) {
        if (readyList == null) {
            consumer.accept(null);
        } else {
            readyList.add(consumer);
        }
    }
}
