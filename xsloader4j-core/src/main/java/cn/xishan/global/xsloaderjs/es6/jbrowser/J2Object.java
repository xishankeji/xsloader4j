package cn.xishan.global.xsloaderjs.es6.jbrowser;

import cn.xishan.oftenporter.porter.core.annotation.deal.AnnoUtil;
import cn.xishan.oftenporter.porter.core.util.OftenTool;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.caoccao.javet.enums.V8ValueReferenceType;
import com.caoccao.javet.exceptions.JavetException;
import com.caoccao.javet.interfaces.IJavetClosable;
import com.caoccao.javet.interfaces.IJavetUniConsumer;
import com.caoccao.javet.interop.V8Runtime;
import com.caoccao.javet.interop.callback.JavetCallbackContext;
import com.caoccao.javet.values.V8Value;
import com.caoccao.javet.values.primitive.V8ValuePrimitive;
import com.caoccao.javet.values.primitive.V8ValueString;
import com.caoccao.javet.values.reference.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Created by https://github.com/CLovinr on 2019/5/29.
 */
public abstract class J2Object implements AutoCloseable, IReleasableRegister, IValueBuilder {

    private static final Logger LOGGER = LoggerFactory.getLogger(J2Object.class);

    protected V8Runtime _v8;
    protected J2Object root;
    protected IV8ValueObject v8Object;

    List<IJavetClosable> releasableList;
    private boolean released = false;
    private IValueBuilder valueBuilder;

    protected J2Object(J2Object root) {
        this(root, false);
    }

    /**
     * @param root
     * @param autoRegisterMethod 是否自动注册注解了@{@link JsBridgeMethod}的java函数。
     */
    public J2Object(J2Object root, boolean autoRegisterMethod) {
        this.root = root;
        this.v8Object = newV8Object();
        if (autoRegisterMethod) {
            autoRegisterMethod();
        }
    }

    J2Object() {

    }

    public void setValueBuilder(IValueBuilder valueBuilder) {
        this.valueBuilder = valueBuilder;
    }

    public IValueBuilder getValueBuilder() {
        return valueBuilder;
    }

    public J2Object getRoot() {
        return root == null ? this : root;
    }

    public V8Runtime getV8() {
        return root == null ? _v8 : root._v8;
    }

    public IV8ValueObject getV8Object() {
        return v8Object;
    }

    /**
     * 创建的实例会被添加到待释放列表。
     *
     * @return
     */
    @Override
    public IV8ValueObject newV8Object() {
        try {
            IV8ValueObject v8Object = valueBuilder == null ? null : valueBuilder.newV8Object();
            if (v8Object == null) {
                v8Object = getV8().createV8ValueObject();
            }

            if (valueBuilder != null) {
                valueBuilder.addReleasable(v8Object);
            } else if (root != null) {
                root.addReleasable(v8Object);
            } else {
                this.addReleasable(v8Object);
            }
            return v8Object;
        } catch (JavetException e) {
            throw new RuntimeException(e);
        }

    }

    /**
     * 创建的实例会被添加到待释放列表。
     *
     * @return
     */
    @Override
    public IV8ValueArray newV8Array() {
        try {
            IV8ValueArray v8Array = valueBuilder == null ? null : valueBuilder.newV8Array();
            if (v8Array == null) {
                v8Array = getV8().createV8ValueArray();
            }

            if (valueBuilder != null) {
                valueBuilder.addReleasable(v8Array);
            } else if (root != null) {
                root.addReleasable(v8Array);
            } else {
                this.addReleasable(v8Array);
            }
            return v8Array;
        } catch (JavetException e) {
            throw new RuntimeException(e);
        }
    }

    protected void autoRegisterMethod() {
        autoRegisterMethod(this);
    }

    public void autoRegisterMethod(Object target) {
        Method[] methods = OftenTool.getAllPublicMethods(target.getClass());
        for (Method method : methods) {
            JsBridgeMethod jsMethod = AnnoUtil.getAnnotation(method, JsBridgeMethod.class);
            if (jsMethod != null) {
                addInterface(target, jsMethod, method);
            }
        }
    }

    private void addInterface(Object target, JsBridgeMethod jsMethod, Method method) {
        try {
            String jsName = jsMethod.name().equals("") ? method.getName() : jsMethod.name();
            IV8ValueObject v8Object;
            if (jsMethod.isRootFun()) {
                v8Object = this.getV8().getGlobalObject();
            } else {
                v8Object = this.v8Object;
            }
            method.setAccessible(true);
            v8Object.bindFunction(jsName, newCallback(target, method));
        } catch (JavetException e) {
            throw new RuntimeException(e);
        }
    }

    private JavetCallbackContext newCallback(Object target, Method method) {
        return new ReleasableCallback(this, target, method).getCallbackContext();
    }

    public static void release(Object object) {
        if (object instanceof IJavetClosable) {
            try {
                if (!((IJavetClosable) object).isClosed()) {
                    ((IJavetClosable) object).close();
                }
            } catch (Exception e) {
                LOGGER.warn(e.getMessage(), e);
            }
        } else if (object instanceof AutoCloseable) {
            try {
                ((AutoCloseable) object).close();
            } catch (Exception e) {
                LOGGER.warn(e.getMessage(), e);
            }
        }
    }


    public IV8ValueObject toV8Value(Map<String, Object> map) {
        return toV8Value(getV8(), map, getRoot());
    }

    public static IV8ValueObject toV8Value(V8Runtime runtime, Map<String, Object> map,
            IReleasableRegister releasableRegister) {
        if (map == null) {
            return null;
        }

        try {
            IV8ValueObject object = runtime.createV8ValueObject();
            releasableRegister.addReleasable(object);
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                add(object, entry.getKey(), entry.getValue(), releasableRegister);
            }
            return object;
        } catch (JavetException e) {
            throw new RuntimeException(e);
        }
    }

    public IV8ValueArray toV8Value(Collection collection) {
        return toV8Value(getV8(), collection, getRoot());
    }

    public static IV8ValueArray toV8Value(V8Runtime runtime, Collection collection,
            IReleasableRegister releasableRegister) {
        if (collection == null) {
            return null;
        }

        try {
            IV8ValueArray array = runtime.createV8ValueArray();
            releasableRegister.addReleasable(array);
            for (Object obj : collection) {
                add(array, obj, releasableRegister);
            }
            return array;
        } catch (JavetException e) {
            throw new RuntimeException(e);
        }
    }


    public static void add(IV8ValueObject v8Object, String name, Object javaValue,
            IReleasableRegister releasableRegister) {
        try {
            if (javaValue == null) {
                v8Object.setNull(name);
            } else if (javaValue instanceof CharSequence) {
                v8Object.set(name, String.valueOf(javaValue));
            } else if ((javaValue instanceof Integer) || (javaValue instanceof Short) || javaValue instanceof Byte) {
                v8Object.set(name, ((Number) javaValue).intValue());
            } else if (javaValue instanceof Number) {
                v8Object.set(name, ((Number) javaValue).doubleValue());
            } else if (javaValue instanceof Boolean) {
                v8Object.set(name, (Boolean) javaValue);
            } else if (javaValue instanceof V8Value) {
                v8Object.set(name, (V8Value) javaValue);
            } else if (javaValue instanceof Date) {
                v8Object.set(name, ((Date) javaValue).getTime());
            } else if (javaValue instanceof Map) {
                IV8ValueObject object = v8Object.getV8Runtime().createV8ValueObject();
                releasableRegister.addReleasable(object);
                v8Object.set(name, object);
                for (Map.Entry<String, Object> entry : ((Map<String, Object>) javaValue).entrySet()) {
                    add(object, entry.getKey(), entry.getValue(), releasableRegister);
                }
            } else if (javaValue instanceof Collection) {
                IV8ValueArray array = v8Object.getV8Runtime().createV8ValueArray();
                releasableRegister.addReleasable(array);
                v8Object.set(name, array);
                for (Object obj : (Collection) javaValue) {
                    add(array, obj, releasableRegister);
                }
            } else {
                throw new IllegalArgumentException("unknown type:name=" + name + ",type=" + javaValue.getClass());
            }
        } catch (JavetException e) {
            throw new RuntimeException(e);
        }
    }

    public static void add(IV8ValueArray v8Array, Object javaValue, IReleasableRegister releasableRegister) {
        try {
            if (javaValue == null) {
                v8Array.pushNull();
            } else if (javaValue instanceof CharSequence) {
                v8Array.push(String.valueOf(javaValue));
            } else if ((javaValue instanceof Integer) || (javaValue instanceof Short) || javaValue instanceof Byte) {
                v8Array.push(((Number) javaValue).intValue());
            } else if (javaValue instanceof Number) {
                v8Array.push(((Number) javaValue).doubleValue());
            } else if (javaValue instanceof Boolean) {
                v8Array.push((Boolean) javaValue);
            } else if (javaValue instanceof V8Value) {
                v8Array.push((V8Value) javaValue);
            } else if (javaValue instanceof Map) {
                IV8ValueObject object = v8Array.getV8Runtime().createV8ValueObject();
                releasableRegister.addReleasable(object);
                v8Array.push(object);
                for (Map.Entry<String, Object> entry : ((Map<String, Object>) javaValue).entrySet()) {
                    add(object, entry.getKey(), entry.getValue(), releasableRegister);
                }
            } else if (javaValue instanceof Collection) {
                IV8ValueArray array = v8Array.getV8Runtime().createV8ValueArray();
                releasableRegister.addReleasable(array);
                v8Array.push(array);
                for (Object obj : (Collection) javaValue) {
                    add(array, obj, releasableRegister);
                }
            } else {
                throw new IllegalArgumentException("unknown type:type=" + javaValue.getClass());
            }
        } catch (JavetException e) {
            throw new RuntimeException(e);
        }
    }

    public static Object getObjectItem(final IV8ValueObject v8Object, String key) {
        try (V8Value v8Value = v8Object.get(key);) {
            if (v8Value.isNullOrUndefined()) {
                return null;
            } else if (v8Value instanceof V8ValuePrimitive) {
                return ((V8ValuePrimitive<?>) v8Value).getValue();
            } else if (v8Value instanceof IV8ValueArray) {
                return toArray((IV8ValueArray) v8Value);
            } else if (v8Value instanceof V8ValueTypedArray) {
                V8ValueTypedArray typedArray = (V8ValueTypedArray) v8Value;
                V8ValueReferenceType type = typedArray.getType();

                switch (type) {
                    case Int8Array:
                    case Uint8Array:
                    case Uint8ClampedArray:
                        return typedArray.toBytes();
                    case Int16Array:
                    case Uint16Array:
                        return typedArray.toShorts();
                    case Int32Array:
                    case Uint32Array:
                        return typedArray.toIntegers();
                    case Float32Array:
                        return typedArray.toFloats();
                    case Float64Array:
                        return typedArray.toDoubles();
                    case BigInt64Array:
                    case BigUint64Array:
                        return typedArray.toLongs();
                }
            } else {
                V8ValueReference reference = (V8ValueReference) v8Value;
                V8ValueReferenceType type = reference.getType();
                switch (type) {
                    case Object:
                    case Function:
                    case ArrayBuffer:
                        return v8Object.get(key);
                }
            }
        } catch (JavetException e) {
            LOGGER.warn(e.getMessage(), e);
        }
        return null;
    }

    public static Object getArrayItem(final IV8ValueArray array, int index) {
        try (V8Value v8Value = array.get(index);) {
            if (v8Value.isNullOrUndefined()) {
                return null;
            } else if (v8Value instanceof V8ValuePrimitive) {
                return ((V8ValuePrimitive<?>) v8Value).getValue();
            } else if (v8Value instanceof IV8ValueArray) {
                return toArray((IV8ValueArray) v8Value);
            } else if (v8Value instanceof V8ValueTypedArray) {
                V8ValueTypedArray typedArray = (V8ValueTypedArray) v8Value;
                V8ValueReferenceType type = typedArray.getType();

                switch (type) {
                    case Int8Array:
                    case Uint8Array:
                    case Uint8ClampedArray:
                        return typedArray.toBytes();
                    case Int16Array:
                    case Uint16Array:
                        return typedArray.toShorts();
                    case Int32Array:
                    case Uint32Array:
                        return typedArray.toIntegers();
                    case Float32Array:
                        return typedArray.toFloats();
                    case Float64Array:
                        return typedArray.toDoubles();
                    case BigInt64Array:
                    case BigUint64Array:
                        return typedArray.toLongs();
                }
            } else {
                V8ValueReference reference = (V8ValueReference) v8Value;
                V8ValueReferenceType type = reference.getType();
                switch (type) {
                    case Object:
                    case Function:
                    case ArrayBuffer:
                        return array.get(index);
                }
            }
        } catch (JavetException e) {
            LOGGER.warn(e.getMessage(), e);
        }
        return null;
    }


    public static JSONObject toJSON(IV8ValueObject v8Object) {
        if (v8Object instanceof IV8ValueFunction || v8Object == null || v8Object.isUndefined()) {
            if (v8Object != null) {
                J2Object.release(v8Object);
            }
            return null;
        } else {
            JSONObject jsonObject = new JSONObject();
            try {
                v8Object.forEach((IJavetUniConsumer<V8Value, Throwable>) keyValue -> {
                    if (keyValue instanceof V8ValueString) {
                        String key = ((V8ValueString) keyValue).toPrimitive();
                        Object item = getObjectItem(v8Object, key);
                        if (item instanceof IV8ValueArray) {
                            item = toArray((IV8ValueArray) item);
                        } else if (item instanceof IV8ValueObject) {
                            item = toJSON((IV8ValueObject) item);
                        }
                        jsonObject.put(key, item);
                    }

                    release(keyValue);
                });
            } catch (Throwable e) {
                throw new RuntimeException(e);
            }

            J2Object.release(v8Object);
            return jsonObject;
        }
    }

    public static JSONArray toArray(IV8ValueArray v8Array) {
        if (v8Array == null || v8Array.isUndefined()) {
            return null;
        } else {
            try {
                int length = v8Array.getLength();
                JSONArray jsonArray = new JSONArray(length);
                for (int i = 0; i < length; i++) {
                    Object item = getArrayItem(v8Array, i);
                    if (item instanceof IV8ValueArray) {
                        item = toArray((IV8ValueArray) item);
                    } else if (item instanceof IV8ValueObject) {
                        item = toJSON((IV8ValueObject) item);
                    }
                    jsonArray.add(item);
                }
                return jsonArray;
            } catch (JavetException e) {
                throw new RuntimeException(e);
            } finally {
                J2Object.release(v8Array);
            }
        }
    }


    @Override
    public void addReleasable(IJavetClosable releasable) {
        if (valueBuilder != null) {
            valueBuilder.addReleasable(releasable);
        } else if (releasableList != null) {
            releasableList.add(releasable);
        } else {
            root.addReleasable(releasable);
        }
    }

    public void release() {
        if (!released) {
            released = true;
            if (releasableList != null) {
                for (IJavetClosable releasable : releasableList) {
                    release(releasable);
                }
                releasableList = null;
            }

            release(v8Object);
            v8Object = null;
            root = null;
            valueBuilder = null;
            releasableList = null;
            _v8 = null;
        }
    }

    @Override
    public void close() {
        this.release();
    }
}
