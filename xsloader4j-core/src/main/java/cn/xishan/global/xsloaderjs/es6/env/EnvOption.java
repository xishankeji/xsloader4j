package cn.xishan.global.xsloaderjs.es6.env;

/**
 * @author Created by https://github.com/CLovinr on 2021/8/25.
 */
public class EnvOption {
    private boolean enableCDT = false;
    private String cdtName;
    private String name = "default";


    public EnvOption() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCdtName() {
        return cdtName;
    }

    public void setCdtName(String cdtName) {
        this.cdtName = cdtName;
    }

    public boolean isEnableCDT() {
        return enableCDT;
    }

    public void setEnableCDT(boolean enableCDT) {
        this.enableCDT = enableCDT;
    }

}
