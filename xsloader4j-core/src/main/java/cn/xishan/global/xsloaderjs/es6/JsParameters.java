package cn.xishan.global.xsloaderjs.es6;

import cn.xishan.global.xsloaderjs.es6.jbrowser.IReleasableRegister;
import com.caoccao.javet.exceptions.JavetException;
import com.caoccao.javet.interfaces.IJavetClosable;
import com.caoccao.javet.values.V8Value;
import com.caoccao.javet.values.reference.IV8ValueArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Created by https://github.com/CLovinr on 2021/2/20.
 */
public class JsParameters implements IJavetClosable, AutoCloseable, IReleasableRegister {
    private static final Logger LOGGER = LoggerFactory.getLogger(JsParameters.class);

    private IV8ValueArray params;
    private List<IJavetClosable> releasableList;
    private boolean isClosed = false;


    public JsParameters(IV8ValueArray params) {
        this.params = params;
        this.releasableList = new ArrayList<>();
        this.addReleasable(params);
    }


    public IV8ValueArray getParams() {
        return params;
    }

    public Object[] getParamArgs() {
        try {
            Object[] args = new Object[params.getLength()];
            for (int i = 0; i < args.length; i++) {
                V8Value arg = params.get(i);
                if (arg != null) {
                    addReleasable(arg);
                }
                args[i] = arg;
            }
            return args;
        } catch (JavetException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() {
        for (IJavetClosable releasable : releasableList) {
            try {
                releasable.close();
            } catch (Exception e) {
                LOGGER.warn(e.getMessage(), e);
            }
        }
        this.isClosed = true;
    }

    @Override
    public boolean isClosed() {
        return isClosed;
    }

    @Override
    public void addReleasable(IJavetClosable releasable) {
        releasableList.add(releasable);
    }
}
