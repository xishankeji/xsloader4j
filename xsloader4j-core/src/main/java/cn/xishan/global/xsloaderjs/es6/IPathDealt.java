package cn.xishan.global.xsloaderjs.es6;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.regex.Pattern;

/**
 * @author Created by https://github.com/CLovinr on 2019-06-28.
 */
public interface IPathDealt {
    interface JsIgnoreCurrentRequireDepHandle {
        String handle(String path, String script);
    }

    /**
     * 用于处理path，返回实际的。
     *
     * @param servletContext
     * @param path
     * @return
     */
    default String dealPath(ServletContext servletContext, String path) {
        return path;
    }

    /**
     * 是否探测es6代码,在{@linkplain #dealPath(ServletContext, String)}之后调用。
     *
     * @param path
     * @return
     */
    default boolean detectESCode(String path) {
        return true;
    }

    /**
     * 判断是否为es6的代码
     *
     * @param script
     * @return
     */
    default boolean isESCode(String path, String script) {
        return CodeUtils.isESCode(script);
    }

    /**
     * 是否在js前增加xsloader.__ignoreCurrentRequireDep=false
     *
     * @param path
     * @param script
     * @return
     */
    default boolean ignoreCurrentRequireDep(String path, String script) {
        return CodeUtils.ignoreCurrentRequireDep(script);
    }

    /**
     * 根据路径返回实际文件。
     *
     * @param servletContext
     * @param path
     * @return
     */
    default File getRealFile(ServletContext servletContext, String path) {
        String realPath = servletContext.getRealPath(path);
        return realPath == null ? null : new File(realPath);
    }

    /**
     * 为其他资源文件时。
     *
     * @param request
     * @param response
     * @return
     */
    default boolean handleElse(HttpServletRequest request,
            HttpServletResponse response, String path, JsIgnoreCurrentRequireDepHandle handle)
            throws IOException, ServletException {
        return false;
    }

    /**
     * 对内容进行预处理。
     *
     * @param servletContext
     * @param path
     * @param realFile
     * @param content
     * @return
     * @throws IOException
     */
    default String preDealContent(ServletContext servletContext, String path, File realFile, String content)
            throws IOException {
        return content;
    }
}
