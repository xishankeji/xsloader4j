package cn.xishan.global.xsloaderjs.es6.env;

import cn.xishan.oftenporter.porter.core.annotation.PortComment;
import cn.xishan.oftenporter.porter.core.annotation.PortIn;
import cn.xishan.oftenporter.porter.core.annotation.Property;
import cn.xishan.oftenporter.porter.core.base.OftenObject;
import cn.xishan.oftenporter.porter.core.base.PortMethod;
import cn.xishan.oftenporter.porter.core.exception.OftenCallException;
import cn.xishan.oftenporter.servlet.ContentType;
import cn.xishan.oftenporter.servlet.OftenServletRequest;
import cn.xishan.oftenporter.servlet.render.mapping.PathMapping;
import cn.xishan.oftenporter.servlet.websocket.WS;
import cn.xishan.oftenporter.servlet.websocket.WebSocket;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.caoccao.javet.interop.V8Runtime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Hashtable;
import java.util.Map;

/**
 * @author Created by https://github.com/CLovinr on 2021/8/25.
 */
@PortIn
@PortComment(name = "v8调试接口")
public class CDTDebugPorter {
    private static final Logger LOGGER = LoggerFactory.getLogger(CDTDebugPorter.class);

    private Map<String, DebugItem> debugItemMap = new Hashtable<>();

    public void addDebugRuntime(V8Runtime v8Runtime, EnvOption option) {
        if (debugItemMap.containsKey(option.getCdtName())) {
            throw new OftenCallException("already exists cdt name:" + option.getCdtName());
        } else {
            DebugItem debugItem = new DebugItem(v8Runtime, option.getCdtName());
            debugItemMap.put(debugItem.cdtName, debugItem);
        }
    }

    @Property(name = "xsloader.cdt.isHttps", defaultVal = "false")
    private Boolean isHttps;
    private String v8Version;

    @PortIn(method = PortMethod.GET)
    @PathMapping(path = "/json/list")
    public void jsonList(OftenObject oftenObject, HttpServletResponse response) throws IOException {
        String url = OftenServletRequest.getPortUrl(oftenObject, "ws", isHttps);
        String wsUrlPrefix;
        if (isHttps) {
            wsUrlPrefix = "wss:" + url.substring("https:".length());
        } else {
            wsUrlPrefix = "ws:" + url.substring("http:".length());
        }

        JSONArray array = new JSONArray();

        for (DebugItem debugItem : debugItemMap.values()) {
            String wsUrl = wsUrlPrefix + "?name=" + debugItem.cdtName;
            JSONObject item = new JSONObject();
            item.put("description", "javet");
            item.put("devtoolsFrontendUrl",
                    "devtools://devtools/bundled/js_app.html?experiments=true&v8only=true&" + wsUrl);
            item.put("devtoolsFrontendUrlCompat",
                    "devtools://devtools/bundled/inspector.html?experiments=true&v8only=true&" + wsUrl);
            item.put("id", "javet-" + debugItem.cdtName);
            item.put("title", "javet-" + debugItem.cdtName);
            item.put("type", "node");
            item.put("url", "file://");
            item.put("webSocketDebuggerUrl", wsUrl);
            array.add(item);
        }

        response.setContentType(ContentType.APP_JSON.getType());
        response.setCharacterEncoding("utf-8");
        try (PrintWriter writer = response.getWriter()) {
            writer.print(array.toJSONString());
        }
    }

    @PortIn(method = PortMethod.GET)
    @PathMapping(path = "/json/version")
    public void version(HttpServletResponse response) throws IOException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("Browser", "Javet");
        jsonObject.put("Protocol-Version", "1.3");
        jsonObject.put("V8-Version", v8Version);

        response.setContentType(ContentType.APP_JSON.getType());
        response.setCharacterEncoding("utf-8");
        try (PrintWriter writer = response.getWriter()) {
            writer.print(jsonObject.toJSONString());
        }
    }

    @PortIn
    @WebSocket
    public void ws(OftenObject oftenObject, WS ws) throws IOException {
        if (ws.type() == WebSocket.Type.ON_OPEN) {
            String name = oftenObject.nece("name");
            DebugItem debugItem = debugItemMap.get(name);
            if (debugItem == null) {
                LOGGER.warn("not found debug item:name={}", name);
                ws.close();
            } else {
                DebugInstance debugInstance = new DebugInstance(debugItem, ws);
                ws.putAttribute(DebugInstance.class, debugInstance);
                debugInstance.connect();
            }
        } else if (ws.type() == WebSocket.Type.ON_MESSAGE) {
            String text = ws.object();
            DebugInstance debugInstance = ws.getAttribute(DebugInstance.class);
            if (debugInstance != null) {
                debugInstance.onText(text);
            } else {
                String name = oftenObject.unece("name");
                LOGGER.warn("not found debug instance:name={}", name);
                ws.close();
            }
        } else if (ws.type() == WebSocket.Type.ON_CLOSE) {
            DebugInstance debugInstance = ws.getAttribute(DebugInstance.class);
            if (debugInstance != null) {
                ws.putAttribute(DebugInstance.class, null);
                debugInstance.close();
            }
        }
    }

    public void setV8Version(String version) {
        this.v8Version = version;
    }
}
