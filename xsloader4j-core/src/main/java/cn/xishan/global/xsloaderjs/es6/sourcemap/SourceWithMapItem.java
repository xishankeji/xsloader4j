package cn.xishan.global.xsloaderjs.es6.sourcemap;

import com.alibaba.fastjson.JSONObject;

/**
 * @author Created by https://github.com/CLovinr on 2021/7/7.
 */
public class SourceWithMapItem {
    /**
     * 文件名
     */
    private String name;
    /**
     * 与generateCode对应，可以为空
     */
    private JSONObject sourceMap;
    private String generateCode;
    private String originalSource;
    /**
     * 合并generateCode前添加的代码
     */
    private String preCode;
    /**
     * 合并generateCode后添加的代码
     */
    private String afterCode;

    public SourceWithMapItem() {

    }

    public SourceWithMapItem(String name, JSONObject sourceMap, String generateCode) {
        this.name = name;
        this.sourceMap = sourceMap;
        this.generateCode = generateCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public JSONObject getSourceMap() {
        return sourceMap;
    }

    public void setSourceMap(JSONObject sourceMap) {
        this.sourceMap = sourceMap;
    }

    public String getGenerateCode() {
        return generateCode;
    }

    public void setGenerateCode(String generateCode) {
        this.generateCode = generateCode;
    }

    public String getOriginalSource() {
        return originalSource;
    }

    public void setOriginalSource(String originalSource) {
        this.originalSource = originalSource;
    }

    public String getPreCode() {
        return preCode;
    }

    public void setPreCode(String preCode) {
        this.preCode = preCode;
    }

    public String getAfterCode() {
        return afterCode;
    }

    public void setAfterCode(String afterCode) {
        this.afterCode = afterCode;
    }
}
