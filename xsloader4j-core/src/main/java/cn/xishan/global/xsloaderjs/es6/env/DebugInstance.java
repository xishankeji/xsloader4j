package cn.xishan.global.xsloaderjs.es6.env;

import cn.xishan.oftenporter.servlet.websocket.WS;
import com.caoccao.javet.interop.IV8InspectorListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Created by https://github.com/CLovinr on 2021/8/25.
 */
class DebugInstance implements IV8InspectorListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(DebugInstance.class);

    private DebugItem debugItem;
    private WS ws;

    public DebugInstance(DebugItem debugItem, WS ws) {
        this.debugItem = debugItem;
        this.ws = ws;
    }

    public void connect() {
        debugItem.v8Runtime.getV8Inspector().addListeners(this);
    }

    public void close() {
        debugItem.v8Runtime.getV8Inspector().removeListeners(this);
    }

    public void onText(String text) {
        try {
            debugItem.v8Runtime.getV8Inspector().sendRequest(text);
        } catch (Exception e) {
            LOGGER.warn(e.getMessage(), e);
        }
    }

    @Override
    public void flushProtocolNotifications() {

    }

    @Override
    public void receiveNotification(String message) {
        try {
            ws.session().getBasicRemote().sendText(message);
        } catch (Exception e) {
            LOGGER.warn(e.getMessage(), e);
        }
    }

    @Override
    public void receiveResponse(String message) {
        try {
            ws.session().getBasicRemote().sendText(message);
        } catch (Exception e) {
            LOGGER.warn(e.getMessage(), e);
        }
    }

    @Override
    public void runIfWaitingForDebugger(int contextGroupId) {
        try {
            debugItem.v8Runtime.getExecutor("console.log('Welcome to Javet Debugging Environment!');")
                    .executeVoid();
        } catch (Exception e) {
            LOGGER.warn(e.getMessage(), e);
        }
    }

    @Override
    public void sendRequest(String message) {

    }
}
