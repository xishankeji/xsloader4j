package cn.xishan.global.xsloaderjs.es6.jbrowser;

import com.caoccao.javet.interfaces.IJavetClosable;
import com.caoccao.javet.values.reference.IV8ValueArray;
import com.caoccao.javet.values.reference.IV8ValueObject;

/**
 * @author Created by https://github.com/CLovinr on 2021/10/15.
 */
public interface IValueBuilder {
    IV8ValueObject newV8Object();

    IV8ValueArray newV8Array();

    void addReleasable(IJavetClosable releasable);
}
