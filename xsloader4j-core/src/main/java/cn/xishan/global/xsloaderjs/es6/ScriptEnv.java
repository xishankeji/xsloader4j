package cn.xishan.global.xsloaderjs.es6;

import cn.xishan.global.xsloaderjs.es6.env.EnvOption;
import cn.xishan.global.xsloaderjs.es6.jbrowser.DefaultRoot;
import cn.xishan.global.xsloaderjs.es6.jbrowser.J2Object;
import cn.xishan.global.xsloaderjs.es6.jbrowser.JsBridgeMethod;
import cn.xishan.oftenporter.porter.core.util.OftenTool;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.caoccao.javet.exceptions.JavetException;
import com.caoccao.javet.interfaces.IJavetClosable;
import com.caoccao.javet.interop.V8Runtime;
import com.caoccao.javet.values.IV8Value;
import com.caoccao.javet.values.reference.IV8ValueArray;
import com.caoccao.javet.values.reference.IV8ValueObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Consumer;

/**
 * @author Created by https://github.com/CLovinr on 2020-04-27.
 */
public class ScriptEnv implements AutoCloseable {
    private static final Logger LOGGER = LoggerFactory.getLogger(ScriptEnv.class);

    private J2Object root;
    private String varName;
    private EnvOption option;

    public ScriptEnv(String varName) {
        this.varName = varName;
    }

    public EnvOption getOption() {
        return option;
    }

    public void setOption(EnvOption option) {
        this.option = option;
    }

    public String getVarName() {
        return varName;
    }

    public void onReady(Consumer<Void> consumer) {
        if (consumer == null) {
            throw new NullPointerException();
        }

        JsScriptUtil.onReady((n) -> {
            V8Runtime v8 = JsScriptUtil.createV8(option);
            root = new DefaultRoot("ScriptEnv", varName, v8);
            consumer.accept(null);
        });
    }

    /**
     * 注册@{@linkplain JsBridgeMethod}注解的函数。
     *
     * @param object
     */
    public void addInterface(Object object) {
        root.autoRegisterMethod(object);
    }

    public void voidScript(String scriptName, String script, boolean parseEs6) {
        if (parseEs6) {
            script = CodeParser.parseEs6Script(scriptName, script);
        }

        try {
            root.getV8().getExecutor(script).setResourceName(scriptName).executeVoid();
        } catch (JavetException e) {
            throw new RuntimeException(e);
        }
    }

    public void voidFun(String funName) {
        this.voidFun(funName, null);
    }


    public void voidFun(String funName, Object[] args) {
        try (JsParameters parameters = toParameters(args)) {
            root.getV8().getGlobalObject().invokeVoid(funName, parameters.getParamArgs());
        } catch (JavetException e) {
            throw new RuntimeException(e);
        }
    }


    public JSONObject jsonFun(String funName, Object[] args) {
        IV8ValueObject v8Object = null;
        try (JsParameters parameters = toParameters(args)) {
            v8Object = root.getV8().getGlobalObject().invoke(funName, parameters.getParamArgs());
            return toJSON(v8Object);
        } catch (JavetException e) {
            throw new RuntimeException(e);
        } finally {
            if (v8Object != null) {
                J2Object.release(v8Object);
            }
        }
    }

    public JSONArray arrayFun(String funName, Object[] args) {
        IV8ValueArray v8Array = null;
        try (JsParameters parameters = toParameters(args)) {
            v8Array = root.getV8().getGlobalObject().invoke(funName, parameters.getParamArgs());
            return toArray(v8Array);
        } catch (JavetException e) {
            throw new RuntimeException(e);
        } finally {
            if (v8Array != null) {
                J2Object.release(v8Array);
            }
        }
    }

    public String stringScript(String scriptName, String script, boolean parseEs6) {
        if (parseEs6) {
            script = CodeParser.parseEs6Script(scriptName, script);
        }

        try {
            return root.getV8().getExecutor(script).setResourceName(scriptName).executeString();
        } catch (JavetException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean boolScript(String scriptName, String script, boolean parseEs6) {
        if (parseEs6) {
            script = CodeParser.parseEs6Script(scriptName, script);
        }

        try {
            return root.getV8().getExecutor(script).setResourceName(scriptName).executeBoolean();
        } catch (JavetException e) {
            throw new RuntimeException(e);
        }
    }

    public int intScript(String name, String script, boolean parseEs6) {
        if (parseEs6) {
            script = CodeParser.parseEs6Script(name, script);
        }

        try {
            return root.getV8().getExecutor(script).setResourceName(name).executeInteger();
        } catch (JavetException e) {
            throw new RuntimeException(e);
        }
    }

    public double doubleScript(String scriptName, String script, boolean parseEs6) {
        if (parseEs6) {
            script = CodeParser.parseEs6Script(scriptName, script);
        }

        try {
            return root.getV8().getExecutor(script).setResourceName(scriptName).executeDouble();
        } catch (JavetException e) {
            throw new RuntimeException(e);
        }
    }


    public JSONObject jsonScript(String scriptName, String script, boolean parseEs6) {
        if (parseEs6) {
            script = CodeParser.parseEs6Script(scriptName, script);
        }

        IV8ValueObject v8Object = null;
        try {
            v8Object = root.getV8().getExecutor(script).setResourceName(scriptName).executeObject();
            JSONObject rs = null;
            if (v8Object != null) {
                try {
                    rs = toJSON(v8Object);
                } finally {
                    J2Object.release(v8Object);
                }
            }
            return rs;
        } catch (JavetException e) {
            throw new RuntimeException(e);
        }

    }

    public JSONArray arrayScript(String scriptName, String script, boolean parseEs6) {
        if (parseEs6) {
            script = CodeParser.parseEs6Script(scriptName, script);
        }

        IV8ValueArray v8Array = null;
        try {
            v8Array = root.getV8().getExecutor(script).setResourceName(scriptName).executeObject();
            JSONArray rs = null;
            if (v8Array != null) {
                try {
                    rs = toArray(v8Array);
                } finally {
                    J2Object.release(v8Array);
                }
            }

            return rs;

        } catch (JavetException e) {
            throw new RuntimeException(e);
        } finally {
            J2Object.release(v8Array);
        }


    }

    public void release() {
        J2Object.release(root);
        J2Object.release(root.getV8());
        root = null;
        option = null;
    }

    public void acquire() {

    }

    public void unacquire() {

    }

    public static JSONObject toJSON(IV8ValueObject v8Object) {
        return J2Object.toJSON(v8Object);
    }

    public static JSONArray toArray(IV8ValueArray v8Array) {
        return J2Object.toArray(v8Array);
    }

    public J2Object getDevV8Object() {
        return root;
    }

    public JsParameters toParameters(Object[] args) {
        if (OftenTool.isEmptyOf(args)) {
            return null;
        }


        JsParameters jsParameters = null;
        try {
            jsParameters = new JsParameters(root.getV8().createV8ValueArray());

            for (Object obj : args) {
                J2Object.add(jsParameters.getParams(), obj, jsParameters);
            }
        } catch (Exception e) {
            J2Object.release(jsParameters);
            throw new RuntimeException(e);
        }

        return jsParameters;

    }

    private Object dealReturnAndArgs(Object result) {
        if (result instanceof IJavetClosable) {
            J2Object.release(result);
            if (result instanceof IV8Value && ((IV8Value) result).isNullOrUndefined()) {
                return null;
            } else {
                throw new RuntimeException("illegal return type,not allowed Releasable result:" + result);
            }
        } else {
            return result;
        }
    }

    /**
     * 执行{@linkplain #getVarName()}下的函数。
     *
     * @param fun
     * @param args
     * @return
     */
    public Object exeVarFun(String fun, Object... args) {
        try (JsParameters parameters = toParameters(args)) {
            try {
                Object result = root.getV8Object().invoke(fun, parameters.getParamArgs());
                return dealReturnAndArgs(result);
            } catch (JavetException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * 执行全局函数。
     *
     * @param fun
     * @param args
     * @return
     */
    public Object exeFun(String fun, Object... args) {
        try (JsParameters parameters = toParameters(args)) {
            try {
                Object result = root.getV8().getGlobalObject().invoke(fun, parameters.getParamArgs());
                return dealReturnAndArgs(result);
            } catch (JavetException e) {
                throw new RuntimeException(e);
            }

        }
    }

    @Override
    public void close() {
        release();
    }
}
