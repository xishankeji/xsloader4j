package cn.xishan.global.xsloaderjs.es6;

import java.util.regex.Pattern;

/**
 * @author Created by https://github.com/CLovinr on 2021/9/15.
 */
public class CodeUtils {
    /**
     * 默认es6语法代码判断,当一行含有以下语句的：
     * <ol>
     * <li>
     * 含有:let语句
     * </li>
     * <li>
     * 含有:import静态导入语句
     * </li>
     * <li>
     * 含有:export语句
     * </li>
     * <li>
     * 含有:const语句
     * </li>
     * </ol>
     * 且不含以下语句：
     * <ol>
     *     <li>
     *         xsloader.__ignoreCurrentRequireDep=true
     *     </li>
     *     <li>
     *         __webpack_require__
     *     </li>
     *     <li>
     *         __webpack_modules__
     *     </li>
     * </ol>
     */
    private static final Pattern DEFAULT_ES6_PATTERN =
            Pattern.compile("((^|\\n)[\\s]*let[\\s]+)|((^|\\n)[\\s]*import[\\s]+)|((^|\\n)" +
                    "[\\s]*export[\\s]+)|((^|\\n)[\\s]*const[\\s]+)");

    public static boolean isESCode(String script) {
        if (ignoreCurrentRequireDep(script)) {
            return false;
        } else {
            return DEFAULT_ES6_PATTERN.matcher(script).find() &&
                    !script.contains("xsloader.__ignoreCurrentRequireDep=true") &&
                    !script.contains("__webpack_require__") && !script.contains("__webpack_modules__");
        }
    }

    public static boolean ignoreCurrentRequireDep(String script) {
        //忽略webpack打包的模块，忽略amd模块
        int index = script.indexOf("define.amd");
        if (index > 0) {
            int start = index > 128 ? index - 128 : 0;
            int end = script.length() - index > 128 ? index + 128 : script.length();

            String preScript = script.substring(start, end);

            return
                    preScript.contains("\"function\"==typeof define&&define.amd") ||
                            preScript.contains("typeof define === 'function' && define.amd") ||
                            /////////////////////////
                            preScript.contains("typeof define==='function'&&define.amd") ||
                            preScript.contains("typeof define=='function'&&define.amd") ||
                            preScript.contains("typeof define == 'function' && define.amd") ||
                            preScript.contains("typeof define===\"function\"&&define.amd") ||
                            preScript.contains("typeof define==\"function\"&&define.amd") ||
                            preScript.contains("typeof define == \"function\" && define.amd") ||
                            preScript.contains("\"function\" == typeof define && define.amd") ||
                            preScript.contains("\"function\" === typeof define && define.amd") ||
                            preScript.contains("\"function\"===typeof define&&define.amd") ||
                            preScript.contains("'function' == typeof define && define.amd") ||
                            preScript.contains("'function' === typeof define && define.amd") ||
                            preScript.contains("'function'===typeof define&&define.amd");
        } else {
            return script.contains("__webpack_require__");
        }
    }
}
