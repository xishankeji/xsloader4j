package cn.xishan.global.xsloaderjs.es6.jbrowser;

import com.caoccao.javet.exceptions.JavetException;
import com.caoccao.javet.interfaces.IJavetClosable;
import com.caoccao.javet.interop.V8Runtime;
import com.caoccao.javet.interop.callback.JavetCallbackContext;

import java.lang.reflect.Method;

/**
 * @author Created by https://github.com/CLovinr on 2021/10/15.
 */
class ReleasableCallback implements IJavetClosable {
    private boolean isClose = false;
    private V8Runtime v8Runtime;
    private JavetCallbackContext callbackContext;

    public ReleasableCallback(J2Object j2Object, Object target, Method method) {
        JavetCallbackContext callbackContext = new JavetCallbackContext(target, method);
        this.callbackContext = callbackContext;
        this.v8Runtime = j2Object.getV8();
        j2Object.addReleasable(this);
    }

    public JavetCallbackContext getCallbackContext() {
        return callbackContext;
    }

    @Override
    public void close() throws JavetException {
        if (!isClose) {
            isClose = true;
            v8Runtime.removeCallbackContext(callbackContext.getHandle());
            callbackContext = null;
            v8Runtime = null;
        }
    }

    @Override
    public boolean isClosed() {
        return isClose;
    }
}
