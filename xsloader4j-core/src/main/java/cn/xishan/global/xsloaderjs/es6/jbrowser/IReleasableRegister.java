package cn.xishan.global.xsloaderjs.es6.jbrowser;

import com.caoccao.javet.interfaces.IJavetClosable;

/**
 * @author Created by https://github.com/CLovinr on 2020/8/23.
 */
public interface IReleasableRegister {
    void addReleasable(IJavetClosable releasable);
}
