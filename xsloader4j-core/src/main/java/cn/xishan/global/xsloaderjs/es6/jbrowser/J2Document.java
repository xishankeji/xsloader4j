package cn.xishan.global.xsloaderjs.es6.jbrowser;

import com.caoccao.javet.exceptions.JavetException;
import com.caoccao.javet.values.reference.IV8ValueArray;
import com.caoccao.javet.values.reference.IV8ValueObject;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * @author Created by https://github.com/CLovinr on 2019/5/29.
 */
public class J2Document extends J2Object {
    private Document document;

    public J2Document(J2Object root) {
        super(root);
        this.document = new Document("");
        autoRegisterMethod();
    }

    @JsBridgeMethod
    public IV8ValueObject createElement(String tagName) throws JavetException {
        Element element = this.document.createElement(tagName);
        J2Element j2Element = new J2Element(root, element);

        getV8().getGlobalObject().invokeVoid("__initElement", j2Element.getV8Object());
        return j2Element.getV8Object();
    }

    @JsBridgeMethod
    public IV8ValueArray getElementsByTagName(String tagName) throws JavetException {
        Elements elements = this.document.getElementsByTag(tagName);
        IV8ValueArray array = newV8Array();
        for (Element element : elements) {
            J2Element j2Element = new J2Element(root, element);
            array.push(j2Element.getV8Object());
        }
        return array;
    }

    @Override
    public void release() {
        super.release();
        document = null;
    }
}
