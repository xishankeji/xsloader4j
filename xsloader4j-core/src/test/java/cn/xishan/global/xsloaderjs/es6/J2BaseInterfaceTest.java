package cn.xishan.global.xsloaderjs.es6;

import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

/**
 * @author Created by https://github.com/CLovinr on 2021/10/15.
 */
public class J2BaseInterfaceTest {
    @Test
    public void testTime() {
        assertEquals(30 * 60 * 1000, TimeUnit.MINUTES.toMillis(30));
    }
}