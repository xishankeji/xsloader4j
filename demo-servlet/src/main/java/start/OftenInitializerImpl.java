package start;

import cn.xishan.global.xsloaderjs.es6.env.CDTDebugPorter;
import cn.xishan.oftenporter.porter.core.annotation.ImportProperties;
import cn.xishan.oftenporter.porter.core.init.PorterConf;
import cn.xishan.oftenporter.servlet.OftenInitializer;

import javax.servlet.ServletContext;

/**
 * @author Created by https://github.com/CLovinr on 2021/8/25.
 */
@ImportProperties("/xsloader4j.properties")
public class OftenInitializerImpl implements OftenInitializer {
    @Override
    public void onStart(ServletContext servletContext, Builder builder) throws Exception {
        PorterConf porterConf = builder.newPorterConfWithImporterClasses(getClass());
        porterConf.getSeekPackages().addClassPorter(CDTDebugPorter.class);
        porterConf.setOftenContextName("often");
        builder.startOne(porterConf);
    }
}
